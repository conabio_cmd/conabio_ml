Entrenadores de imágenes
************************

El API conabio_ml define dos tipos de entrenadores para problemas de visión por computadora en imágenes, uno para cada tipo de modelo que se define: **Clasificación** y **Detección de Objetos**. El módulo conabio_ml.trainer.images.trainer define las clases ClassificationTrainer y ObjectDetectionTrainer para cada uno de ellos.

Cada una de estas clases tiene el método estático train que recibe los cuatro parámetros detallados en la sección Trainer para poder realizar el entrenamiento: dataset, modelo, configuración de ejecución, configuración de entrenamiento.

El tipo de modelo que recibe el método train debe coincidir con el tipo de entrenador que se va a utilizar. P. e., el método train de la clase ObjectDetectionTrainer debe recibir un modelo del tipo ObjectDetectionModel.

Configuración de ejecución
==========================

El API conabio_ml define un configurador de ejecución de entrenamiento (TrainerConfig) por cada plataforma utilizada para realizar los entrenamientos. Para el caso de entrenamientos de modelos de visión por computadora en imágenes, se definen en el módulo conabio_ml.trainer.images.trainer_config las clases TFSlimTrainConfig y TFObjectDetectionAPITrainConfig para usar las plataformas Tensorflow-Slim y el API de Detección de Objetos de Tensorflow, respectivamente. Ambas clases cuentan con el método estático create, que permite configurar el ambiente de ejecución de esa plataforma y crea una instancia que puede pasarse al método train del entrenador en el parámetro execution_config.

En el siguiente ejemplo se crea una instancia de configuración de ambiente para entrenar un modelo con biblioteca de modelos de clasificación de imágenes TensorFlow-Slim usando dos tarjetas gráficas (GPUs) en el ambiente local y que guarda el estado del entrenamiento cada 300 segundos::

    from conabio_ml.trainer.images.trainer_config import TFSlimTrainConfig

    tf_slim_config = TFSlimTrainConfig.create(clone_on_cpu=False,
                                            num_clones=2,
                                            save_summaries_secs=300,
                                            save_interval_secs=300)

Configuración de entrenamiento
==============================

La configuración del entrenamiento se pasa en forma de diccionario y define parámetros como la configuración de la función de optimización, learning rate, número de épocas, capas a re-entrenar, dropout, etc., y sigue la :ref:`image-train_config-specification-label`.

En el siguiente ejemplo se configura un entrenamiento de clasificación de imágenes para el modelo Inception V3 al que se re-entrenan las última cuatro capas por 20 épocas y se usa la función de optimización RMSprop con un learning rate exponencial::

    from conabio_ml.trainer.images.trainer import ClassificationTrainer

    ...
    ClassificationTrainer.train(
        snmb_dataset, 
        inception_v3_model, 
        tf_slim_config,
        train_config={
            "train_dir": os.path.join(test_path, 'train'),
            "checkpoint_exclude_scopes": "InceptionV3/Logits,InceptionV3/AuxLogits,InceptionV3/Mixed_7c,InceptionV3/Mixed_7b",
            "trainable_scopes": "InceptionV3/Logits,InceptionV3/AuxLogits,InceptionV3/Mixed_7c,InceptionV3/Mixed_7b",
            "optimizer": {
                'rms_prop': {
                    "learning_rate": {
                        'exponential': {
                            'initial_learning_rate': 0.01,
                            "decay_epochs": 2
                        }
                    },
                    "decay": 0.9,
                    "epsilon": 1.0
                },
                "moving_average_decay": 0.9999
            },
            "num_epochs": 20
        }
    )
