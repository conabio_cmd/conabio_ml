.. _image-classification-trainer_config-specification-label:

Especificación para configuración de ambiente de ejecución en modelos de clasificación
======================================================================================
El operador `oneof` indica que sólo se podrá seleccionar una de las opciones que contiene::

    trainer_config {
        master : str, optional
            The address of the TensorFlow master to use. (default is '')
        num_clones : int, optional
            Number of model clones to deploy. (default is 1)
        total_clones_in_cluster : int or None, optional
            Total number of model clones to deploy in the cluster.
            This value must be the total number of clones in all replicas of 
            the cluster.
            (default is None)
        clone_on_cpu : bool, optional
            Use CPUs to deploy clones. (default is False)
        worker_hosts : str, optional
            Comma-separated list of hostname:port for the worker jobs. 
            E.g. 'machine1:2222,machine2:1111,machine2:2222'
            (default is '')
        ps_hosts : str, optional
            Comma-separated list of hostname:port for the parameter server jobs. 
            E.g. 'machine1:2222,machine2:1111,machine2:2222'
            (default is '')
        job_name : str, optional
            In a distributed trainig session, whether is a worker or ps job. 
            Allowed values are "ps" and "worker" (default is '')
        worker_replicas : int, optional
            Number of worker replicas. (default is 1)
        num_ps_tasks : int, optional
            The number of parameter servers. If the value is 0, then the parameters
            are handled locally by the worker. 
            (default is 0)
        num_readers : int, optional
            The number of parallel readers that read data from the dataset.
            (default is 4)
        num_preprocessing_threads : int, optional
            The number of threads used to create the batches. 
            (default is 4)
        sync_replicas : bool, optional
            Whether or not to synchronize the replicas during training. (default is False)
        replicas_to_aggregate : int, optional
            Number of replicas to aggregate before making parameter updates. (default is 1)
        task : int, optional
            Task id of the replica running the training. (default is 0)
        log_every_n_steps : int, optional
            The frequency with which logs are print. 
            (default is 10)
        save_summaries_secs : int, optional
                The frequency with which summaries are saved, in seconds. (default is 600)
        save_interval_secs : int, optional
                The frequency with which the model is saved, in seconds. (default is 600)
    }
