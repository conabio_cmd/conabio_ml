Trainer
*******

Un entrenador (**Trainer**) es un elemento que ajusta un **modelo** a partir de un **dataset** y una **función de optimización**. El proceso de entrenamiento se puede llevar a cabo en distintos **entornos** (p.e., distribuído, paralelo, síncrono, asíncrono, etc.), que dependen de la plataforma que se utilice para realizar el entrenamiento. Sin embargo, la configuración del entorno de ejecución es independiente del entrenamiento y no tiene un efecto directo en el rendimiento del modelo, como sí lo tienen la función de optimización, el número de épocas o la configuración de dropout, por ejemplo. Por este motivo, en el API conabio_ml se decidió desacoplar la configuración de los parámetros de entrenamiento de la configuración del entorno de ejecución del entrenamiento.

Por lo anterior, se resume que para entrenar un modelo utilizando el API conabio_ml son necesarios cuatro componentes principales:

* **Dataset**: son los datos que usará el entrenador para ajustar el modelo, y deberán estar encapsulados en una instancia de Dataset, para lo que puede usarse cualquiera de los métodos que permiten crear un dataset a partir de una colección de datos.
* **Modelo**: es el modelo que se desea ajustar y puede tratarse de un modelo pre-entrenado o de un modelo que se desea entrenar desde cero. Para cualquiera de estos casos se puede usar la metodología para cargar un modelo descrita en la sección Model.
* **Configuración del entorno**: se trata de una subclase de TrainerConfig, que encapsula los parámetros específicos de la plataforma con la que se realizará el entrenamiento. Se han creado implementaciones para las plataformas de entrenamiento más utilizadas, como son Tensorflow-Slim, el API de Detección de Objetos de Tensorflow, Keras, etc. Si se desea usar la configuración por defecto de cada plataforma no es necesario utilizar este parámetro.
* **Configuración del entrenamiento**: se trata de un diccionario en el formato detallado en la especificación del API que contiene la configuración del entrenamiento, como la función de optimización, número de épocas, dropout, etc.

El proceso de entrenamiento se lleva a cabo a través del método estático **train** de la clase específica del entrenador que se utilizará, y que depende del problema que se esté tratando de resolver.
