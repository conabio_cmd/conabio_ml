.. _image-detection-trainer-specification-label:

Especificación para configuración de modelos de detección
=========================================================
El operador `oneof` indica que sólo se podrá seleccionar una de las opciones que contiene::

    model_config {
        oneof {
            faster_rcnn : FasterRcnn
                Configuration for Faster R-CNN models.
            ssd : Ssd
                Configuration for Single Shot Detection (SSD) models.
        }
    }

    FasterRcnn {
        number_of_stages : int, optional
            Whether to construct only the Region Proposal Network (RPN). (default is 2)
        image_resizer : ImageResizer
            Image resizer for preprocessing the input image.
        feature_extractor : FasterRcnnFeatureExtractor
            Feature extractor config.
        inplace_batchnorm_update : bool, optional
            Whether to update batch_norm inplace during training. (default is False)
        first_stage_anchor_generator : AnchorGenerator
            Anchor generator to compute RPN anchors.
        use_matmul_gather_in_matcher: bool, optional
            Whether to use an alternate implementation of tf.gather that is faster on TPUs. 
            (default is False)
        first_stage_atrous_rate: int, optional
            Atrous rate for the convolution op applied to the `first_stage_features_to_crop` tensor to 
            obtain box predictions. (default is 1)
        first_stage_box_predictor_conv_hyperparams : Hyperparams
            Hyperparameters for the convolutional RPN box predictor.
        first_stage_box_predictor_kernel_size : int, optional
            Kernel size to use for the convolution op just prior to RPN box predictions. (default is 3)
        first_stage_box_predictor_depth : int, optional
            Output depth for the convolution op just prior to RPN box predictions. (default 512)
        first_stage_minibatch_size : int, optional
            The batch size to use for computing the first stage objectness and location losses. 
            (default is 256)
        use_static_shapes : bool, optional
            If True, uses implementation of ops with static shape guarantees. (default is False)
        use_static_shapes_for_eval: bool, optional
            If True, uses implementation of ops with static shape guarantees when running evaluation. 
            (default is False)
        first_stage_positive_balance_fraction : float, optional
            Fraction of positive examples per image for the RPN. (default is 0.5)
        use_static_balanced_label_sampler : bool, optional
            Whether to use the balanced positive negative sampler implementation with static shape guarantees. 
            (default is False)
        first_stage_max_proposals : int, optional
            Maximum number of RPN proposals retained after first stage postprocessing. (default is 300)
        first_stage_nms_iou_threshold : float, optional
            Non max suppression IOU threshold applied to first stage RPN proposals. (default is 0.7)
        second_stage_batch_size : int, optional
            The batch size per image used for computing the classification and refined location loss of the 
            box classifier. (default is 64)
        first_stage_nms_score_threshold : float, optional
            Non max suppression score threshold applied to first stage RPN proposals. (default is 0.0)
        first_stage_localization_loss_weight : float, optional
            First stage RPN localization loss weight. (default is 1.0)
        first_stage_objectness_loss_weight : float, optional
            First stage RPN objectness loss weight. (default is 1.0)
        initial_crop_size : int
            Output size (width and height are set to be the same) of the initial bilinear interpolation based 
            cropping during ROI pooling.
        maxpool_kernel_size : int
            Kernel size of the max pool op on the cropped feature map during ROI pooling.
        maxpool_stride : int
            Stride of the max pool op on the cropped feature map during ROI pooling.
        second_stage_box_predictor : BoxPredictor
            Hyperparameters for the second stage box predictor. If box predictor type is set to 
            rfcn_box_predictor, a R-FCN model is constructed, otherwise a Faster R-CNN model is constructed.       
        second_stage_balance_fraction : float, optional
            Fraction of positive examples to use per image for the box classifier. (default is 0.25)
        second_stage_post_processing : PostProcessing
            Post processing to apply on the second stage box classifier predictions.
        second_stage_localization_loss_weight : float, optional
            Second stage refined localization loss weight. (default is 1.0)
        second_stage_classification_loss : ClassificationLoss
            Loss for second stage box classifers, supports Softmax and Sigmoid.
        second_stage_classification_loss_weight : float, optional
            Second stage classification loss weight. (default is 1.0)
        second_stage_mask_prediction_loss_weight : float, optional
            Second stage instance mask loss weight. (default is 1.0)
        hard_example_miner : HardExampleMiner
            If not left to default, applies hard example mining only to classification and localization loss.
        use_matmul_crop_and_resize : bool, optional
            Force the use of matrix multiplication based crop and resize instead of standard 
            tf.image.crop_and_resize while computing second stage input feature maps. (default is False)
        clip_anchors_to_image : bool, optional
            Setting this option to true, clips the anchors to be within the image instead of pruning.
            (default is False)
        resize_masks : bool, optional
            Whether the masks present in groundtruth should be resized in the model to match the image size.
            (default is True)
    }

    ImageResizer {
        oneof {
            keep_aspect_ratio_resizer : KeepAspectRatioResizer
                Configuration for image resizer that keeps aspect ratio.
            fixed_shape_resizer : FixedShapeResizer
                Configuration for image resizer that resizes to a fixed shape.
            identity_resizer : IdentityResizer
                Configuration for image identity resizer
            conditional_shape_resizer : ConditionalShapeResizer
                Configuration for image resizer that resizes only if input image height
                or width is greater or smaller than a certain size.
                Aspect ratio is maintained.
        }
    }

    KeepAspectRatioResizer {
        min_dimension : int, optional
            Desired size of the smaller image dimension in pixels. (default is 600)
        max_dimension : int, optional
            Desired size of the larger image dimension in pixels. (default is 1024)
        resize_method : BILINEAR|NEAREST_NEIGHBOR|BICUBIC|AREA, optional
            Desired method when resizing image. (default is BILINEAR)
        pad_to_max_dimension : bool, optional
            Whether to pad the image with zeros so the output spatial size is 
            [max_dimension, max_dimension]. (default is False)
        convert_to_grayscale : bool, optional
            Whether to also resize the image channels from 3 to 1 (RGB to grayscale).
            (default is False)
        per_channel_pad_value : float, optional
            Per-channel pad value. This is only used when pad_to_max_dimension is True.
            (default is 0)
    }

    FixedShapeResizer {
        height : int, optional
            Desired height of image in pixels. (default is 300)
        width : int, optional
            Desired width of image in pixels. (default is 300)
        resize_method: BILINEAR|NEAREST_NEIGHBOR|BICUBIC|AREA, optional
            Desired method when resizing image. (default is BILINEAR)
        convert_to_grayscale : bool, optional
            Whether to also resize the image channels from 3 to 1 (RGB to grayscale). 
            (default is False)
    }

    IdentityResizer {

    }

    ConditionalShapeResizer {
        condition : INVALID|GREATER|SMALLER, optional
            Condition which must be true to resize the image. 
            INVALID for default value.
            GREATER for resize image if a dimension is greater than specified size.
            SMALLER for resize image if a dimension is smaller than specified size.
            (default is GREATER)
        size_threshold : int, optional
            Threshold for the image size. If any image dimension is above or below this 
            (as specified by condition) the image will be resized so that it meets the threshold. 
            (default is 300)
        resize_method : BILINEAR|NEAREST_NEIGHBOR|BICUBIC|AREA, optional
            Desired method when resizing image. (default is BILINEAR)
        convert_to_grayscale : bool, optional
            Whether to also resize the image channels from 3 to 1 (RGB to grayscale). 
            (default is False)
    }

    FasterRcnnFeatureExtractor {
        type : str
            Type of Faster R-CNN model (e.g., 'faster_rcnn_resnet101')
        first_stage_features_stride : int, optional
            Output stride of extracted RPN feature map. (default is 16)
        batch_norm_trainable : bool, optional
            Whether to update batch norm parameters during training or not. (default is False)
    }

    AnchorGenerator {
        oneof {
            grid_anchor_generator : GridAnchorGenerator
                Configuration for GridAnchorGenerator.
            ssd_anchor_generator : SsdAnchorGenerator
                Configuration for SSD anchor generator.
            multiscale_anchor_generator : MultiscaleAnchorGenerator
                Configuration for RetinaNet anchor generator
            flexible_grid_anchor_generator : FlexibleGridAnchorGenerator
                Configuration for Flexible grid anchor generator
        }
    }    

    GridAnchorGenerator {
        height : int, optional
            Anchor height in pixels. (default is 256)
        width : int, optional
            Anchor width in pixels. (default is 256)
        height_stride : int, optional
            Anchor stride in height dimension in pixels. (default is 16)
        width_stride : int, optional
            Anchor stride in width dimension in pixels. (default is 16)
        height_offset : int, optional
            Anchor height offset in pixels. (default is 0)
        width_offset : int, optional
            Anchor width offset in pixels. (default is 0)
        scales : list of float
            List of scales for the anchors.
        aspect_ratios : list of float
            List of aspect ratios for the anchors.
    }

    SsdAnchorGenerator {
        num_layers : int, optional
            Number of grid layers to create anchors for. (default is 6)
        min_scale : float, optional
            Scale of anchors corresponding to finest resolution. (default is 0.2)
        max_scale : float, optional
            Scale of anchors corresponding to coarsest resolution. (default is 0.95)
        scales : list of float
            Can be used to override min_scale->max_scale, with an explicitly defined set of scales. 
            If empty, then min_scale->max_scale is used.
        aspect_ratios: float
            Aspect ratios for anchors at each grid point.
        interpolated_scale_aspect_ratio : float, optional
            When this aspect ratio is greater than 0, then an additional
            anchor, with an interpolated scale is added with this aspect ratio. (default is 1.0)
        reduce_boxes_in_lowest_layer : bool, optional
            Whether to use the following aspect ratio and scale combination for the
            layer with the finest resolution : (scale=0.1, aspect_ratio=1.0),
            (scale=min_scale, aspect_ration=2.0), (scale=min_scale, aspect_ratio=0.5). (default is True)
        base_anchor_height : float, optional
            The base anchor size in height dimension. (default is 1.0)
        base_anchor_width : float, optional
            The base anchor size in width dimension. (default is 1.0)
        height_stride : int
            Anchor stride in height dimension in pixels for each layer. The length of
            this field is expected to be equal to the value of num_layers.
        width_stride : int
            Anchor stride in width dimension in pixels for each layer. The length of
            this field is expected to be equal to the value of num_layers.
        height_offset : int
            Anchor height offset in pixels for each layer. The length of this field is
            expected to be equal to the value of num_layers.
        width_offset : int
            Anchor width offset in pixels for each layer. The length of this field is
            expected to be equal to the value of num_layers.
    }

    MultiscaleAnchorGenerator {
        min_level : int, optional
            minimum level in feature pyramid. (default is 3)
        max_level : int, optional
            maximum level in feature pyramid. (default is 7)
        anchor_scale : float, optional
            Scale of anchor to feature stride. (default is 4.0)
        aspect_ratios : float
            Aspect ratios for anchors at each grid point.
        scales_per_octave : int, optional
            Number of intermediate scale each scale octave. (default is 2)
        normalize_coordinates : bool, optional
            Whether to produce anchors in normalized coordinates. (default is True)
    }

    FlexibleGridAnchorGenerator {
        anchor_grid : list of AnchorGrid
            The anchor grid list
        normalize_coordinates : bool, optional
            Whether to produce anchors in normalized coordinates. (default is True)
    }

    AnchorGrid {
        base_sizes : list of float
            The base sizes in pixels for each anchor in this anchor layer.
        aspect_ratios : list of float
            The aspect ratios for each anchor in this anchor layer.
        height_stride : int
            The anchor height stride in pixels.
        width_stride : int
            The anchor width stride in pixels.
        height_offset : int, optional
            The anchor height offset in pixels. (default is 0)
        width_offset : int, optional
            The anchor width offset in pixels. (default is 0)
    }

    Hyperparams {
        op : CONV|FC, optional
            Operations affected by hyperparameters. 
            CONV for Convolution, Separable Convolution, Convolution transpose.
            FC for Fully connected
            (default is CONV)
        regularizer : Regularizer
            Regularizer for the weights of the convolution op.
        initializer : Initializer
            Initializer for the weights of the convolution op.
        activation : NONE|RELU|RELU_6, optional
            Type of activation to apply after convolution. 
            NONE for use None (no activation)
            RELU for use tf.nn.relu
            RELU_6 for use tf.nn.relu6
            (default is RELU)
    }

    Regularizer {
        oneof {
            l1_regularizer : L1Regularizer
                Configuration for L1 Regularizer.
            l2_regularizer : L2Regularizer
                Configuration for L2 Regularizer.
        }
    }

    L1Regularizer {
        weight : float, optional
            A scalar multiplier Tensor. 0.0 disables the regularizer. (default is 1.0)
    }

    L2Regularizer {
        weight : float, optional
            A scalar multiplier Tensor. 0.0 disables the regularizer. (default is 1.0)

    }

    Initializer {
        oneof {
            truncated_normal_initializer : TruncatedNormalInitializer
                Configuration for truncated normal initializer.
            variance_scaling_initializer : VarianceScalingInitializer
                Configuration for variance scaling initializer.
            random_normal_initializer : RandomNormalInitializer
                Configuration for random normal initializer.
        }
    }

    TruncatedNormalInitializer {
        mean : float, optional
            Mean of the random values to generate. (default is 0.0)
        stddev : float, optional
            Standard deviation of the random values to generate. (default is 1.0)
    }

    VarianceScalingInitializer {
        factor : float, optional
            A multiplicative factor. (default is 2.0)
        uniform : bool, optional
            Whether to use uniform or normal distributed random initialization. (default is False)
        mode : FAN_IN|FAN_OUT|FAN_AVG, optional
            FAN_IN for count only number of input connections.
            FAN_OUT for count only number of output connections.
            FAN_AVG for average number of inputs and output connections.
            (default is FAN_IN)
    }

    RandomNormalInitializer {
        mean : float, optional
            Mean for random normal initializer. (default is 0.0)
        stddev : float, optional
            stddev for random normal initializer. (default is 1.0)
    }

    BoxPredictor {
        oneof {
            convolutional_box_predictor : ConvolutionalBoxPredictor
                Configuration for Convolutional box predictor.
            mask_rcnn_box_predictor : MaskRCNNBoxPredictor
                Configuration for Mask Rcnn box predictor.
            rfcn_box_predictor : RfcnBoxPredictor
                Configuration for Rfcn box predictor.
            weight_shared_convolutional_box_predictor : WeightSharedConvolutionalBoxPredictor
                Configuration for weight shared convolutional box predictor.
        }
    }

    ConvolutionalBoxPredictor {
        conv_hyperparams : Hyperparams
            Hyperparameters for convolution ops used in the box predictor.
        min_depth : int, optional
            Minimum feature depth prior to predicting box encodings and class
            predictions. (default is 0)
        max_depth : int, optional
            Maximum feature depth prior to predicting box encodings and class
            predictions. If max_depth is set to 0, no additional feature map will be
            inserted before location and class predictions. (default is 0)
        num_layers_before_predictor : int, optional
            Number of the additional conv layers before the predictor. (default is 0)
        kernel_size : int, optional
            Size of final convolution kernel. If the spatial resolution of the feature
            map is smaller than the kernel size, then the kernel size is set to
            min(feature_width, feature_height). (default is 1)
        box_code_size : int, optional
            Size of the encoding for boxes. (default is 4)
        apply_sigmoid_to_scores : bool, optional
            Whether to apply sigmoid to the output of class predictions. (default is False)
        class_prediction_bias_init : float, optional
            Bias initialization for class prediction. (default is 0.0)
        use_depthwise : bool, optional
            Whether to use depthwise separable convolution for box predictor layers. (default is False)
        box_encodings_clip_range : BoxEncodingsClipRange, optional
            If specified, apply clipping to box encodings.
    }

    MaskRCNNBoxPredictor {
        fc_hyperparams : Hyperparams
            Hyperparameters for fully connected ops used in the box predictor.
        box_code_size : int, optional
            Size of the encoding for the boxes. (default is 4)
        conv_hyperparams : Hyperparams
            Hyperparameters for convolution ops used in the box predictor.
        predict_instance_masks : bool, optional
            Whether to predict instance masks inside detection boxes. (default is False)
        mask_prediction_conv_depth : int, optional
            The depth for the first conv2d_transpose op applied to the
            image_features in the mask prediction branch. If set to 0, the value
            will be set automatically based on the number of channels in the image
            features and the number of classes. (default is 256)
        predict_keypoints : bool, optional
            Whether to predict keypoints inside detection boxes. (default is False)
        mask_height : int, optional
            The height of the predicted mask. (default is 15)
        mask_width : int, optional
            The width of the predicted mask. (default is 15)
        mask_prediction_num_conv_layers : int, optional
            The number of convolutions applied to image_features in the mask prediction branch. 
            (default is 2)
        masks_are_class_agnostic : bool, optional
            Whether to predict class agnostic masks. Only used when
            predict_instance_masks is true. (default is False)
        share_box_across_classes : bool, optional
            Whether to use one box for all classes rather than a different box for each class. 
            (default is False)
        convolve_then_upsample_masks : bool, optional
            Whether to apply convolutions on mask features before upsampling using
            nearest neighbor resizing.
            By default, mask features are resized to [`mask_height`, `mask_width`]
            before applying convolutions and predicting masks. (default is False)
    }

    RfcnBoxPredictor {
        conv_hyperparams : Hyperparams
            Hyperparameters for convolution ops used in the box predictor.
        num_spatial_bins_height : int, optional
            Bin sizes for RFCN crops. (default is 3)
        num_spatial_bins_width : int, optional
            (default is 3)
        depth : int, optional
            Target depth to reduce the input image features to. (default is 1024)
        box_code_size : int, optional
            Size of the encoding for the boxes. (default is 4)
        crop_height : int, optional
            Size to resize the rfcn crops to. (default is 12)
        crop_width : int, optional
            Size to resize the rfcn crops to. (default is 12)
    }

    WeightSharedConvolutionalBoxPredictor {
        conv_hyperparams : Hyperparams
            Hyperparameters for convolution ops used in the box predictor.
        num_layers_before_predictor : int, optional
            Number of the additional conv layers before the predictor. (default is 0)
        depth : int, optional
            Output depth for the convolution ops prior to predicting box encodings
            and class predictions. (default is 0)
        kernel_size : int, optional
            Size of final convolution kernel. If the spatial resolution of the feature
            map is smaller than the kernel size, then the kernel size is set to
            min(feature_width, feature_height). (default is 3)
        box_code_size : int, optional
            Size of the encoding for boxes. (default is 4)
        class_prediction_bias_init : float, optional
            Bias initialization for class prediction. It has been show to stabilize
            training where there are large number of negative boxes. (default is 0.0)
        share_prediction_tower : bool, optional
            Whether to share the multi-layer tower between box prediction and class
            prediction heads. (default is False)
        use_depthwise : bool, optional
            Whether to use depthwise separable convolution for box predictor layers. (default is False)
        score_converter : IDENTITY|SIGMOID, optional
            Specify how to convert the detection scores at inference time.
            IDENTITY for input scores equals output scores.
            SIGMOID for applies a sigmoid on input scores.
            (default is IDENTITY)
        box_encodings_clip_range : BoxEncodingsClipRange, optional
            If specified, apply clipping to box encodings.
    }

    BoxEncodingsClipRange {
        min : float
            Min value for clipping to box encodings.
        max : float
            Max value for clipping to box encodings.
    }

    PostProcessing {
        batch_non_max_suppression : BatchNonMaxSuppression
            Non max suppression parameters.
        score_converter : IDENTITY|SIGMOID|SOFTMAX, optional
            Score converter to use. 
            IDENTITY for input scores equals output scores.
            SIGMOID for applies a sigmoid on input scores.
            SOFTMAX applies a softmax on input scores.
            (default is IDENTITY)
        logit_scale : float, optional
            Scale logit (input) value before conversion in post-processing step.
            Typically used for softmax distillation, though can be used to scale for
            other reasons.  (default is 1.0)
        calibration_config : CalibrationConfig
            Calibrate score outputs. Calibration is applied after score converter
            and before non max suppression.
    }

    BatchNonMaxSuppression {
        score_threshold : float, optional
            Scalar threshold for score (low scoring boxes are removed). (default is 0.0)
        iou_threshold : float, optional
            Scalar threshold for IOU (boxes that have high IOU overlap
            with previously selected boxes are removed). (default is 0.6)
        max_detections_per_class : int, optional
            Maximum number of detections to retain per class. (default is 100)
        max_total_detections : int, optional
            Maximum number of detections to retain across all classes. (default is 100)
        use_static_shapes : bool, optional
            Whether to use the implementation of NMS that guarantees static shapes.
            (default is False)
        use_class_agnostic_nms : bool, optional
            Whether to use class agnostic NMS.
            Class-agnostic NMS function implements a class-agnostic version
            of Non Maximal Suppression where if max_classes_per_detection=k,
            1) we keep the top-k scores for each detection and
            2) during NMS, each detection only uses the highest class score for sorting.
            3) Compared to regular NMS, the worst runtime of this version is O(N^2)
            instead of O(KN^2) where N is the number of detections and K the number of
            classes. (default is False)
        max_classes_per_detection : int, optional
            Number of classes retained per detection in class agnostic NMS. (default is 1)
    }

    CalibrationConfig {
        oneof {
            function_approximation : FunctionApproximation
                Calibrate predictions via 1-d linear interpolation.
            label_function_approximations : LabelFunctionApproximations
                Calibrate predictions via a function approximation.
            sigmoid_calibration : SigmoidCalibration
                Calibrate predictions via sigmoid calibration.
            label_sigmoid_calibrations : LabelSigmoidCalibrations
                Per-class sigmoid calibration.
        }
    }

    FunctionApproximation {
        x_y_pairs : list of XYPair
            Mapping class labels to indices
    }

    XYPair {
        x : float
            X value
        y : float
            Y value
    }

    LabelFunctionApproximations {
        label_xy_pairs_map : map<str, list of XYPair>
            Message mapping class labels to indices
        label_map_path : str
            Label map to map label names from to class ids.
    }

    SigmoidCalibration {
        sigmoid_parameters : SigmoidParameters
            parameters for sigmoid calibration.
    }

    SigmoidParameters {
        a : float, optional
            (default is -1.0)
        b : float, optional
            (default is 0.0)
    }

    LabelSigmoidCalibrations {
        label_sigmoid_parameters_map : map<str, SigmoidParameters>
            Mapping class index to Sigmoid Parameters
        label_map_path : str
            Label map to map label names from to class ids.
    }

    ClassificationLoss {
        oneof {
            weighted_sigmoid : WeightedSigmoidClassificationLoss
                Classification loss using a sigmoid function over class predictions.
            weighted_softmax : WeightedSoftmaxClassificationLoss
                Classification loss using a softmax function over class predictions.
            weighted_logits_softmax : WeightedSoftmaxClassificationAgainstLogitsLoss
                Classification loss using a softmax function over class predictions and
                a softmax function over the groundtruth labels (assumed to be logits).
            bootstrapped_sigmoid : BootstrappedSigmoidClassificationLoss
                Classification loss using a sigmoid function over the class prediction with
                the highest prediction score.
            weighted_sigmoid_focal : SigmoidFocalClassificationLoss
                Sigmoid Focal cross entropy loss.
        }
    }

    WeightedSigmoidClassificationLoss {
        anchorwise_output : bool, optional
            Output loss per anchor. DEPRECATED, do not use. (default is False)
    }

    WeightedSoftmaxClassificationLoss {
        anchorwise_output : bool, optional
            Output loss per anchor. DEPRECATED, do not use. (default is False)
        logit_scale : float, optional
            Scale logit (input) value before calculating softmax classification loss.
            Typically used for softmax distillation. (default is 1.0)
    }

    WeightedSoftmaxClassificationAgainstLogitsLoss {
        anchorwise_output : bool, optional
            Output loss per anchor. DEPRECATED, do not use. (default is False)
        logit_scale : float, optional
            Scale and softmax groundtruth logits before calculating softmax
            classification loss. Typically used for softmax distillation with teacher
            annotations stored as logits. (default is 1.0)
    }

    BootstrappedSigmoidClassificationLoss {
        alpha : float
            Interpolation weight between 0 and 1.
        hard_bootstrap : bool, optional
            Whether hard boot strapping should be used or not. If true, will only use
            one class favored by model. Othewise, will use all predicted class
            probabilities. (default is False)
        anchorwise_output : bool, optional
            Output loss per anchor. DEPRECATED, do not use. (default is False)
    }

    SigmoidFocalClassificationLoss {
        anchorwise_output : bool, optional
            Output loss per anchor. DEPRECATED, do not use. (default is False)
        gamma : float, optional
            Modulating factor for the loss. (default is 2.0)
        alpha : float
            Alpha weighting factor for the loss.
    }

    HardExampleMiner {
        num_hard_examples : int, optional
            Maximum number of hard examples to be selected per image. 
            If set to 0, all examples obtained after NMS are considered. (default is 64)
        iou_threshold : float, optional
            Minimum intersection over union for an example to be discarded during NMS.
            (default is 0.7)
        loss_type : BOTH|CLASSIFICATION|LOCALIZATION, optional
            Whether to use classification losses, localization losses or both losses. 
            (default is BOTH)
        max_negatives_per_positive : int, optional
            Maximum number of negatives to retain for each positive anchor. 
            (default is 0)
        min_negatives_per_image : int, optional
            Minimum number of negative anchors to sample for a given image. 
            (default is 0)
    }


    Ssd {
        image_resizer : ImageResizer
            Image resizer for preprocessing the input image.
        feature_extractor : SsdFeatureExtractor
            Feature extractor config.
        box_coder : BoxCoder
            Box coder to encode the boxes.
        matcher : Matcher
            Matcher to match groundtruth with anchors.
        similarity_calculator : RegionSimilarityCalculator
            Region similarity calculator to compute similarity of boxes.
        encode_background_as_zeros : bool, optional
            Whether background targets are to be encoded as an all
            zeros vector or a one-hot vector (where background is the 0th class).
            (default is False)
        negative_class_weight : float, optional
            Classification weight to be associated to negative
            anchors. The weight must be in [0., 1.]. (default is 1.0)
        box_predictor : BoxPredictor
            Box predictor to attach to the features.
        anchor_generator : AnchorGenerator
            Anchor generator to compute anchors.
        post_processing : PostProcessing
            Post processing to apply on the predictions.
        normalize_loss_by_num_matches : bool, optional
            Whether to normalize the loss by number of groundtruth boxes that match to
            the anchors. (default is True)
        normalize_loc_loss_by_codesize : bool, optional
            Whether to normalize the localization loss by the code size of the box
            encodings. This is applied along with other normalization factors.
            (default is False)
        loss : Loss
            Loss configuration for training.
        freeze_batchnorm : bool, optional
            Whether to update batch norm parameters during training or not.
            (default is False)
        inplace_batchnorm_update : bool, optional
            Whether to update batch_norm inplace during training. 
            (default is False)
        add_background_class : bool, optional
            Whether to add an implicit background class to one-hot encodings of
            groundtruth labels. (default is True)
        explicit_background_class : bool, optional
            Whether to use an explicit background class. (default is False)
        use_confidences_as_targets : bool, optional
            Whether to use groundtruth_condifences field to assign the targets. 
            (default is False)
        implicit_example_weight : float, optional
            A float number that specifies the weight used for the implicit negative examples. 
            (default is 1.0)
        mask_head_config : MaskHead
            Configs for mask head.
    }

    SsdFeatureExtractor {
        type : str
            Type of ssd feature extractor.
        depth_multiplier : float, optional
            The factor to alter the depth of the channels in the feature extractor.
            (default is 1.0)
        min_depth : int, optional
            Minimum number of the channels in the feature extractor. (default is 16)
        conv_hyperparams : Hyperparams
            Hyperparameters that affect the layers of feature extractor added on top
            of the base feature extractor.
        override_base_feature_extractor_hyperparams : bool, optional
            If this value is set to true, the base feature extractor's hyperparams will be
            overridden with the `conv_hyperparams`. (default is False)
        pad_to_multiple : int, optional
            The nearest multiple to zero-pad the input height and width dimensions to.
            For example, if pad_to_multiple = 2, input dimensions are zero-padded
            until the resulting dimensions are even. (default is 1)
        use_explicit_padding : bool, optional
            Whether to use explicit padding when extracting SSD multiresolution
            features. This will also apply to the base feature extractor if a MobileNet
            architecture is used. (default is False)
        use_depthwise : bool, optional
            Whether to use depthwise separable convolutions for to extract additional
            feature maps added by SSD. (default is False)
        fpn : FeaturePyramidNetworks
            Feature Pyramid Networks config.
        replace_preprocessor_with_placeholder : bool, optional
            If true, replace preprocess function of feature extractor with a
            placeholder. This should only be used if all the image preprocessing steps
            happen outside the graph. (default is False)
    }

    FeaturePyramidNetworks {
        // We recommend to use multi_resolution_feature_map_generator with FPN, and
        // the levels there must match the levels defined below for better
        // performance.
        // Correspondence from FPN levels to Resnet/Mobilenet V1 feature maps:
        // FPN Level        Resnet Feature Map      Mobilenet-V1 Feature Map
        //     2               Block 1                Conv2d_3_pointwise
        //     3               Block 2                Conv2d_5_pointwise
        //     4               Block 3                Conv2d_11_pointwise
        //     5               Block 4                Conv2d_13_pointwise
        //     6               Bottomup_5             bottom_up_Conv2d_14
        //     7               Bottomup_6             bottom_up_Conv2d_15
        //     8               Bottomup_7             bottom_up_Conv2d_16
        //     9               Bottomup_8             bottom_up_Conv2d_17
        min_level : int, optional
            minimum level in feature pyramid. (default is 3)
        max_level : int, optional
            maximum level in feature pyramid. (default is 7)
        additional_layer_depth : int, optional
            channel depth for additional coarse feature layers. (default is 256)
    }

    BoxCoder {
        oneof {
            faster_rcnn_box_coder : FasterRcnnBoxCoder
                Faster RCNN box coder.
            mean_stddev_box_coder : MeanStddevBoxCoder
                Mean stddev box coder.
            square_box_coder : SquareBoxCoder
                Encodes a 3-scalar representation of a square box.
            keypoint_box_coder : KeypointBoxCoder
                Keypoint box coder.
        }
    }

    FasterRcnnBoxCoder {
        y_scale : float, optional
            Scale factor for anchor encoded box center. (default is 10.0)
        x_scale : float, optional
            Scale factor for anchor encoded box center. (default is 10.0)
        height_scale : float, optional
            Scale factor for anchor encoded box height. (default is 5.0)
        width_scale : float, optional
            Scale factor for anchor encoded box width. (default is 5.0)
    }

    MeanStddevBoxCoder {
        stddev : float, optional
            The standard deviation used to encode and decode boxes. (default is 0.01)
    }

    SquareBoxCoder {
        y_scale : float, optional
            Scale factor for anchor encoded box center. (default is 10.0)
        x_scale : float, optional
            Scale factor for anchor encoded box center. (default is 10.0)
        length_scale : float, optional
            Scale factor for anchor encoded box length. (default is 5.0)
    }

    KeypointBoxCoder {
        num_keypoints : int
            Number of keypoints to encode/decode.
        y_scale : float, optional
            Scale factor for anchor encoded box center and keypoints. (default is 10.0)
        x_scale : float, optional
            Scale factor for anchor encoded box center and keypoints. (default is 10.0)
        height_scale : float, optional
            Scale factor for anchor encoded box height. (default is 5.0)
        width_scale : float, optional
            Scale factor for anchor encoded box width. (default is 5.0)
    }

    Matcher {
        oneof {
            argmax_matcher : ArgMaxMatcher
                Matcher based on highest value.
            bipartite_matcher : BipartiteMatcher
                Configuration for bipartite matcher.
        }
    }

    ArgMaxMatcher {
        matched_threshold : float, optional
            Threshold for positive matches. (default is 0.5)
        unmatched_threshold : float, optional
            Threshold for negative matches. (default is 0.5)
        ignore_thresholds : bool, optional
            Whether to construct ArgMaxMatcher without thresholds. (default is False)
        negatives_lower_than_unmatched : bool, optional
            If True then negative matches are the ones below the unmatched_threshold,
            whereas ignored matches are in between the matched and umatched
            threshold. If False, then negative matches are in between the matched
            and unmatched threshold, and everything lower than unmatched is ignored.
            (default is True)
        force_match_for_each_row : bool, optional
            Whether to ensure each row is matched to at least one column. (default is False)
        use_matmul_gather : bool, optional
            Force constructed match objects to use matrix multiplication based gather
            instead of standard tf.gather (default is False)
    }

    BipartiteMatcher {
        use_matmul_gather : bool, optional
            Force constructed match objects to use matrix multiplication based gather
            instead of standard tf.gather (default is False)
    }

    RegionSimilarityCalculator {
        oneof {
            neg_sq_dist_similarity : NegSqDistSimilarity
                Class to compute similarity based on the squared distance metric.
            iou_similarity : IouSimilarity
                Class to compute similarity based on Intersection over Union (IOU) metric.
            ioa_similarity : IoaSimilarity
                Class to compute similarity based on Intersection over Area (IOA) metric.
            thresholded_iou_similarity : ThresholdedIouSimilarity
                Class to compute similarity based on thresholded IOU and score.
        }
    }

    NegSqDistSimilarity {

    }

    IouSimilarity {

    }

    IoaSimilarity {

    }

    ThresholdedIouSimilarity {
        iou_threshold : float, optional
            IOU threshold used for filtering scores. (default is 0.5)
    }

    Loss {
        localization_loss : LocalizationLoss
            Localization loss to use.
        classification_loss : ClassificationLoss
            Classification loss to use.
        hard_example_miner : HardExampleMiner
            If not left to default, applies hard example mining.
        classification_weight : float, optional
            Classification loss weight. (default is 1.0)
        localization_weight : float, optional
            Localization loss weight. (default is 1.0)
        random_example_sampler : RandomExampleSampler
            If not left to default, applies random example sampling.
        equalization_loss : EqualizationLoss
            Equalization loss.
        expected_loss_weights : NONE|EXPECTED_SAMPLING|REWEIGHTING_UNMATCHED_ANCHORS, optional
            Method to compute expected loss weights with respect to balanced
            positive/negative sampling scheme. 
            EXPECTED_SAMPLING for use expected_classification_loss_by_expected_sampling
            from third_party/tensorflow_models/object_detection/utils/ops.py
            REWEIGHTING_UNMATCHED_ANCHORS for use expected_classification_loss_by_reweighting_unmatched_anchors
            from third_party/tensorflow_models/object_detection/utils/ops.py
            If NONE, use explicit sampling.
            (default is NONE)
        min_num_negative_samples : float, optional
            Minimum number of effective negative samples.
            Only applies if expected_loss_weights is not NONE.
            (default is 0)
        desired_negative_sampling_ratio : float, optional
            Desired number of effective negative samples per positive sample.
            Only applies if expected_loss_weights is not NONE.
            (default is 3)
    }

    LocalizationLoss {
        oneof {
            weighted_l2 : WeightedL2LocalizationLoss
                L2 localization loss function with anchorwise output support.
            weighted_smooth_l1 : WeightedSmoothL1LocalizationLoss
                Smooth L1 localization loss function aka Huber Loss.
            weighted_iou : WeightedIOULocalizationLoss
                IOU localization loss function.
        }
    }

    WeightedL2LocalizationLoss {
        anchorwise_output : bool, optional
            Output loss per anchor. DEPRECATED, do not use. (default is False)
    }

    WeightedSmoothL1LocalizationLoss {
        anchorwise_output : bool, optional
            Output loss per anchor. DEPRECATED, do not use. (default is False)
        delta : float, optional
            Delta value for huber loss. (default is 1.0)
    }

    WeightedIOULocalizationLoss {

    }

    RandomExampleSampler {
        positive_sample_fraction : float, optional
            The desired fraction of positive samples in batch when applying random
            example sampling. (default is 0.01)
    }

    EqualizationLoss {
        weight : float, optional
            Weight equalization loss strength. (default is 0.0)
        exclude_prefixes : list of str
            When computing equalization loss, ops that start with
            equalization_exclude_prefixes will be ignored.
    }

    MaskHead {
        mask_height : int, optional
            The height of the predicted mask. Only used when
            predict_instance_masks is true. (default is 15)
        mask_width : int, optional
            The width of the predicted mask. Only used when
            predict_instance_masks is true. (default is 15)
        masks_are_class_agnostic : bool, optional
            Whether to predict class agnostic masks. Only used when
            predict_instance_masks is true. (default is True)
        mask_prediction_conv_depth : int, optional
            The depth for the first conv2d_transpose op applied to the
            image_features in the mask prediction branch. (default is 256)
        mask_prediction_num_conv_layers : int, optional
            The number of convolutions applied to image_features in the mask
            prediction branch. (default is 2)
        convolve_then_upsample_masks : bool, optional
            Whether to apply convolutions on mask features before upsampling using
            nearest neighbor resizing. (default is False)
        mask_loss_weight : float, optional
            Mask loss weight. (default is 5.0)
        mask_loss_sample_size : int, optional
            Number of boxes to be generated at training time for computing mask loss.
            (default is 16)
        conv_hyperparams : Hyperparams
            Hyperparameters for convolution ops used in the box predictor.
        initial_crop_size : int, optional
            Output size (width and height are set to be the same) of the initial
            bilinear interpolation based cropping during ROI pooling. (default is 15)
    }
