.. _image-classification-model_config-specification-label:

Especificación para configuración de modelos de clasificación
=============================================================
El operador `oneof` indica que sólo se podrá seleccionar una de las opciones que contiene::

    model_config {
        oneof {
            alexnet_v2 : ClassificationModel
                AlexNet version 2.
            cifarnet : ClassificationModel
                Configuration for Single Shot Detection (SSD) models.
            overfeat : ClassificationModel
                Contains the model definition for the OverFeat network.
            vgg_a : ClassificationModel
                Oxford Net VGG 11-Layers version A Example.
            vgg_16 : ClassificationModel
                Oxford Net VGG 16-Layers version D Example.
            vgg_19 : ClassificationModel
                Oxford Net VGG 19-Layers version E Example.
            inception_v1 : ClassificationModel
                Defines the Inception V1 architecture.
            inception_v2 : ClassificationModel
                Inception v2 model for classification.
            inception_v3 : ClassificationModel
                Defines the Inception V3 architecture.
            inception_v4 : ClassificationModel
                Creates the Inception V4 model.
            inception_resnet_v2 : ClassificationModel
                Creates the Inception Resnet V2 model.
            i3d : ClassificationModel
                Defines the I3D architecture.
            s3dg : ClassificationModel
                Defines the S3D-G architecture.
            lenet : ClassificationModel
                Creates a variant of the LeNet model.
            resnet_v1_50 : ClassificationModel
                ResNet-50 model.
            resnet_v1_101 : ClassificationModel
                ResNet-101 model.
            resnet_v1_152 : ClassificationModel
                ResNet-152 model.
            resnet_v1_200 : ClassificationModel
                ResNet-200 model.
            resnet_v2_50 : ClassificationModel
                ResNet-50 model.
            resnet_v2_101 : ClassificationModel
                ResNet-101 model.
            resnet_v2_152 : ClassificationModel
                ResNet-152 model.
            resnet_v2_200 : ClassificationModel
                ResNet-200 model.
            mobilenet_v1 : ClassificationModel
                Mobilenet v1 model for classification.
            mobilenet_v1_075 : ClassificationModel
                Mobilenet v1 model for classification.
            mobilenet_v1_050 : ClassificationModel
                Mobilenet v1 model for classification.
            mobilenet_v1_025 : ClassificationModel
                Mobilenet v1 model for classification.
            mobilenet_v2 : ClassificationModel
                Creates mobilenet V2 network.
            mobilenet_v2_140 : ClassificationModel
                Creates mobilenet V2 network.
            mobilenet_v2_035 : ClassificationModel
                Creates mobilenet V2 network.
            nasnet_cifar : ClassificationModel
                Build NASNet model for the Cifar Dataset.
            nasnet_mobile : ClassificationModel
                Build NASNet Mobile model for the ImageNet Dataset.
            nasnet_large : ClassificationModel
                Build NASNet Large model for the ImageNet Dataset.
            pnasnet_large : ClassificationModel
                Build PNASNet Large model for the ImageNet Dataset.
            pnasnet_mobile : ClassificationModel
                Build PNASNet Mobile model for the ImageNet Dataset.
        }
    }

    ClassificationModel {
        train_image_size : int, optional
            Train image size. (default depends on each model)
        weight_decay : float, optional
            The weight decay on the model weights. (default is 0.00004)            
        label_smoothing : float, optional
            The amount of label smoothing. (default is 0.0)
    }
