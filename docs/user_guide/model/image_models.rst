Modelos de imágenes
*******************

El API conabio_ml define dos tipos de modelos para el caso de imágenes: **Clasificación** y **Detección de Objetos**. El módulo conabio_ml.trainer.images.models define las clases ClassificationModel y ObjectDetectionModel para cada uno de ellos.

Modelos de Clasificación
========================

El API conabio_ml utiliza la biblioteca de modelos de clasificación de imágenes `TensorFlow-Slim <https://github.com/tensorflow/models/tree/master/research/slim>`_, que proporciona la implementación de una serie de modelos que podrán ser ajustados a partir de un dataset representado en el formato TFRecords. 

El ajuste del modelo se podrá realizar desde cero o usando la técnica de Transfer Learning, a partir de los archivos de checkpoint generados por un entrenamiento previo. En el siguiente `enlace <https://github.com/tensorflow/models/tree/master/research/slim#pre-trained-models>`_ se pueden descargar los archivos checkpoint para los modelos disponibles en Tensorflow-slim pre-entrenados con el conjunto de imágenes `Image-net <http://www.image-net.org/>`_.

Para aplicar la técnica de Transfer learning la clase ClassificationModel proveé el método de clase load_saved_model, que crea una instancia de ClassificationModel que podrá ser utilizada en el proceso de entrenamiento o evaluación de un modelo. El método load_saved_model recibe el path donde se encuentran los archivos de checkpoint (``*.ckpt``) del modelo pre-entrenado, así como un diccionario con la configuración del modelo.

A continuación se ejemplifica la creación de una instancia del modelo de clasificación Inception V3 a partir de un archivo checkpoint y una configuración, que podrá ser utilizada en un proceso de entrenamiento y evaluación del modelo.::

  from conabio_ml.trainer.images.model import ClassificationModel

  inception_v3_model = ClassificationModel.load_saved_model(
      source_path='./files/inception_v3.ckpt',
      model_config={
          "inception_v3":{
              "weight_decay": 0.0005
          }
      }
  )

Como se puede observar, el parámetro model_config es un diccionario que contiene la configuración del modelo y sigue el formato dado en la :ref:`image-classification-model_config-specification-label`.

Modelos de Detección de Objetos
===============================

El API conabio_ml utiliza el `API de Detección de Objetos de Tensorflow <https://github.com/tensorflow/models/tree/master/research/object_detection>`_ (TFODAPI), que al igual que Tensorflow-Slim, proporciona la implementación modelos que podrán ser ajustados a partir de un dataset representado en el formato TFRecords. 

También es posible ajustar un modelo desde cero o utilizar la técnica de Transfer Learning, a partir de los archivos de checkpoint generados por un entrenamiento previo. En el siguiente `enlace <https://github.com/tensorflow/models/tree/master/research/slim#pre-trained-models>`_ se pueden descargar los archivos checkpoint para los modelos disponibles en el TFODAPI pre-entrenados sobre distintos conjuntos de imágenes.  

Para aplicar la técnica de Transfer learning la clase ObjectDetectionModel proveé el método de clase load_saved_model, que crea una instancia de ObjectDetectionModel que podrá ser utilizada en el proceso de entrenamiento o evaluación de un modelo. El método load_saved_model recibe el path donde se encuentran los archivos de checkpoint (``*.ckpt``) del modelo pre-entrenado, así como un diccionario con la configuración del modelo.

A continuación se ejemplifica la creación de una instancia del modelo de detección de objetos Faster-RCNN a partir de un archivo checkpoint y una configuración, que podrá ser utilizada en un proceso de entrenamiento y evaluación del modelo.::

  from conabio_ml.trainer.images.model import ObjectDetectionModel

  detection_model = ObjectDetectionModel.load_saved_model(
      source_path='./files/model.ckp',
      model_config={
          "faster_rcnn": {
              "image_resizer": {
                  "keep_aspect_ratio_resizer": {
                      "min_dimension": 600,
                      "max_dimension": 800
                  }
              },
              "feature_extractor": {
                  "type": 'faster_rcnn_resnet101',
                  "first_stage_features_stride": 16
              },
              "first_stage_anchor_generator": {
                  "grid_anchor_generator": {
                      "scales": [0.25, 0.5, 1.0, 2.0],
                      "aspect_ratios": [0.5, 1.0, 2.0],
                      "height_stride": 16,
                      "width_stride": 16
                  }
              },
              "first_stage_box_predictor_conv_hyperparams": {
                  "op": "CONV",
                  "regularizer": {
                      "l2_regularizer": {
                          "weight": 0.0
                      }
                  },
                  "initializer": {
                      "truncated_normal_initializer": {
                          "stddev": 0.01
                      }
                  }
              },
              "first_stage_nms_score_threshold": 0.0,
              "first_stage_nms_iou_threshold": 0.7,
              "first_stage_max_proposals": 300,
              "first_stage_localization_loss_weight": 2.0,
              "first_stage_objectness_loss_weight": 1.0,
              "initial_crop_size": 14,
              "maxpool_kernel_size": 2,
              "maxpool_stride": 2,
              "second_stage_box_predictor": {
                  "mask_rcnn_box_predictor": {
                      "fc_hyperparams": {
                          "op": "FC",
                          "regularizer": {
                              "l2_regularizer": {
                                  "weight": 0.0
                              }
                          },
                          "initializer": {
                              "variance_scaling_initializer": {
                                  "factor": 1.0,
                                  "uniform": True,
                                  "mode": "FAN_AVG"
                              }
                          }
                      }
                  }
              },
              "second_stage_post_processing": {
                  "batch_non_max_suppression": {
                      "score_threshold": 0.0,
                      "iou_threshold": 0.6,
                      "max_detections_per_class": 100,
                      "max_total_detections": 300
                  },
                  "score_converter": "SOFTMAX"
              },
              "second_stage_localization_loss_weight": 2.0,
              "second_stage_classification_loss_weight": 1.0
          }
      }
  )

El parámetro model_config es un diccionario que contiene la configuración del modelo y sigue el formato dado en la :ref:`image-detection-model_config-specification-label`.
