Datasets de imágenes
********************

Existen varios tipos de anotación que se pueden hacer en las imágenes, que dependen del tipo de problemas en que serán utilizados. El API conabio_ml separa los datasets de imágenes de acuerdo al tipo de anotaciones que contienen y define las siguientes versiones de dataset para cada colección: **clasificación** y **detección de objetos**. 

Los datasets de clasificación contienen etiquetas a nivel de toda la imagen, mientras que los de detección de objetos contienen etiquetas a nivel de objeto mediante recuadros (bounding boxes). Para el caso de datasets de colecciones de fototrampas los datasets de clasificación pueden tener etiquetas como "Sin fauna" o "Vacía", para indicar que en la imagen no se encontró ningún individuo de las especies de fauna de interés, que en algunos casos se trata de fauna silvestre.

Los constructores de Dataset a partir de un archivo JSON reciben el parámetro **version**, en el cual se especifica si se trata de un dataset de clasificación, con el valor 'classification', o de detección de objetos, con el valor 'detection'. También es posible indicar si se quiere incluir en el dataset las imágenes que aún no han sido etiquetadas, sobre las que se puede generar etiquetas automáticas o que pueden ser usadas para realizar un entrenamiento no supervisado, asignando True al parámetro **unlabeled_images**.

El parámetro **categories** permite indicar los nombres de las categorías taxonómicas por las que se quiere filtrar el dataset. Los nombres válidos se encuentran en la sección categories del archivo JSON de cada colección. Cada categoría se encuentra vinculada con sus ancestros en la jerarquía taxonómica, por lo que una imagen con anotaciones de un taxón, será incluída si se filtra por un nivel taxonómico más general que lo incluya. P. e., si se filtra por la clase Mammalia (mamíferos), se incluirán las imágenes de Panthera onca (jaguar), Canis lupus familiaris (perro doméstico), Felis silvestris catus (gato), etc. En caso de no indicar nada en este parámetro se obtendrán todas las imágenes de la colección con la etiqueta que les fue asignada.

En ocasiones no resulta práctico tener las imágenes en una resolución muy alta, por lo que suele ser útil cambiarles el tamaño antes de convertirlas a otra representación, como TFRecords. Esto se puede hacer al momento de crear un dataset a través del parámetro **images_size**, que recibe el tamaño en pixeles en el formato (width, height). Esta misma operación se puede realizar después de haber creado la instancia de un dataset con el módulo ImageProcessing y el método estático resize, que recibe el parámetro size con el mismo formato.

A continuación se muestran algunos ejemplos donde se usan los métodos para crear y manipular la información de un dataset que se describen en la sección Datasets.

Ejemplos
========

En el siguiente ejemplo se hace la descarga desde el repositorio del archivo JSON correspondiente a la versión classification con únicamente imágenes etiquetadas de la colección SNMB a la carpeta dest_path; se filtran las imágenes que fueron etiquetadas con las categorías taxonómicas: Bos, Aves y Homo y se descargan en la carpeta dest_path/data cambiandoles el tamaño a 800x600 pixeles. Se crean las particiones con las proporciones: train 0.6, test 0.2 y validation 0.2 y luego se escribe la estructura del dataset en tres archivos CSV con los nombres: train.csv, test.csv y validation.csv con las columnas item y label, incluyendo el encabezado; después se crean los archivos TFRecords divididos en 5 partes por cada partición y finalmente se copian las imágenes a la carpeta dest_path/folder separadas con la estructura partición/etiqueta.::

  import os
  from conabio_ml.datasets.images import SNMB
  from conabio_ml.utils.dataset_utils import ImageDatasetTypes
  from conabio_ml.preprocessing.images.preprocess import ImageProcessing
  
  dest_path = os.path.join("results", "example")
  os.makedirs(dest_path, exist_ok=True)
  
  snmb_dataset = SNMB.create(dest_path=dest_path,
                            version="classification",
                            categories=["Bos", "Aves", "Homo"],
                            download_images=True,
                            images_size=(800, 600),
                            unlabeled_images=False)
  snmb_dataset.split(train_perc=0.6,
                    test_perc=0.2,
                    val_perc=0.2)
  snmb_dataset.to_csv(dest_path=os.path.join(dest_path, 'csv'),
                      columns=["item", "label"],
                      header=True,
                      splitted=True)
  snmb_dataset.to_tfrecords(dest_path=os.path.join(dest_path, 'tfrecords'),
                            num_shards=5)
  snmb_dataset.to_folder(dest_path=os.path.join(dest_path, 'folder'),
                        split_in_partitions=True,
                        split_in_labels=True,
                        keep_originals=True)

En el siguiente ejemplo se crea un dataset de la colección PRONATURA buscando los archivos CSV en la carpeta  source_path con los nombres train.csv, test.csv y validation.csv, creando las particiones de acuerdo a los archivos con estos nombres que se encuentren. Los archivos deben tener al menos las columnas item y label; en caso de tener además la columna bbox se considerará un dataset de tipo detection; si no, se considera de tipo classification.::

  from conabio_ml.datasets.images import PRONATURA
  
  pronatura_dataset = PRONATURA.from_csv(source_path=source_path,
                                        split_by_filenames=True,
                                        info={
                                              'year': 2019,
                                              'info': 'Classification dataset created from PRONATURA photo collection.'
                                        })

En el siguiente ejemplo se crea un dataset de la colección IBUNAM a partir de las imágenes encontradas en la carpeta source_path, donde se deberán tener carpetas con la estructura partición/etiqueta. El dataset quedará particionado y etiquetado de acuerdo a esta estructura::

  from conabio_ml.datasets.images import IBUNAM
  
  ibunam_dataset = IBUNAM.from_folder(source_path=source_path,
                                      split_by_folder=True,
                                      info={
                                          'year': 2019,
                                          'info': 'Classification dataset created from IBUNAM photo collection.'
                                      })

A veces suele ser útil unir varios datasets en uno sólo, por lo que el API conabio_ml proveé el método from_datasets, que recibe una o más instancias de datasets con la misma estructura de columnas::

  from conabio_ml.datasets.images import ImageDataset
  
  dataset = ImageDataset.from_datasets(snmb_dataset,
                                      pronatura_dataset,
                                      ibunam_dataset)

El dataset creado contendrá los items de las tres colecciones y podrá ser tratado como cualquier otro dataset.
