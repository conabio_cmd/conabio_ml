Preparación del ambiente
========================

La manera más fácil (y la que recomendamos) para instalar el API conabio_ml es a través de un ambiente virtual, el cual debe correr sobre Python 3.6+.

El ambiente virtual puede crearse utilizando **venv** por medio de la ejecución del comando::

  python3 -m venv path/to/virtual/environment

Después de activar el ambiente virtual, utilizando el comando::

  source path/to/virtual/environment/bin/activate

Descargar el repositorio
========================

Una opción es descargar el proyecto desde el repositorio para tener acceso al código fuente y agregar a la variable de ambiente *PYTHONPATH* la ruta del proyecto y de los módulos *object_detection* y *slim*, y hacer la instalación de los paquetes necesarios como se muestra a continuación::
  
  git clone https://bitbucket.org/conabio_cmd/conabio_ml.git
  export PYTHONPATH=$PYTHONPATH:`pwd`/conabio_ml
  export PYTHONPATH=$PYTHONPATH:`pwd`/conabio_ml/conabio_ml/trainer/images/models/research:`pwd`/conabio_ml/conabio_ml/trainer/images/models/research/slim
  cd conabio_ml
  pip install -r requirements.txt

**Nota**: Los comandos export anteriores se deberán ejecutar en cada nueva terminal donde se quieran realizar experimentos con el API conabio_ml. Si se quiere evitar realizar este proceso manualmente, se pueden agregar las dos líneas *export* al final del archivo ~/.bashrc (o ~/.bash_profile) reemplazando `pwd` con la ruta absoluta de la carpeta desde donde se clonó el repositorio y después ejecutar el comando::

  source ~/.bashrc

Recomendaciones
===============

En caso de que se quieran usar tarjetas gráficas (**GPUs**) para realizar el entrenamiento de un modelo o la inferencia sobre un conjunto de imágenes, será necesario desinstalar la versión de tensorflow que viene por defecto en el archivo requirements.txt del proyecto, e instalar la versión de **tensorflow-gpu** que sea compatible con la versión de *CUDA* y *cuDNN* que se tengan instaladas en el sistema. El siguiente `enlace <https://www.tensorflow.org/install/source#linux>`_ proporciona una tabla con la compatibilidad entre las versiones de tensorflow-gpu, CUDA y cuDNN.

Para tener un ambiente estable del API de Detección de Objetos de Tensorflow (que se usa de forma predeterminada para entrenar modelos de detección de objetos sobre imágenes) configurado para el uso de GPUs, se recomienda utilizar la `imagen de Docker y el script de configuración <https://github.com/Microsoft/ai4eutils/tree/master/TF_OD_API>`_ que proporcionan los desarrolladores del equipo de Microsoft encargados del proyecto `CameraTraps <https://github.com/microsoft/CameraTraps>`_.
