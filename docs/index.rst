API CONABIO ML
==============

El API conabio_ml es un proyecto de software libre para facilitar el trabajo en tareas de Aprendizaje Automático, usando la información generada en la CONABIO y en otros proyectos de conservación de la biodiversidad.

Esta interfaz de programación se basa principalmente en los siguientes elementos:

* **Dataset**: se crea a partir de formatos estándar de datos, como archivos JSON, CSV y elementos dentro de una carpeta del sistema de archivos.
* **Modelo**: se puede definir la arquitectura completa de un modelo o elegir alguna de las más comúnmente utilizadas y únicamente configurar sus parámetros.
* **Entrenador**: ajusta los pesos de un **Modelo** durante un proceso de entrenamiento a partir de los elementos de un dataset.
* **Evaluador**: calcula una serie de **Métricas** a partir de las predicciones que hace un modelo sobre un conjunto de elementos de un dataset.

Estos elementos se pueden conectar en un Pipeline de Aprendizaje Automático que valida que las entradas y salidas de cada proceso sean las adecuadas, además de registrar el orden en que se ejecutan y los parámetros con que se configuran, permitiendo replicar experimentos y comparar el rendimiento de los modelos cuando se varían los parámetros.

A continuación se muestra cómo se pueden conectar los elementos de un experimento de Aprendizaje Automático utilizando el API conabio_ml.


.. figure:: imgs/Diagrama_flujo.png
   :align:   center

   Flujo de un Pipeline de Aprendizaje Automático

.. toctree::
   :maxdepth: 2
   :caption: Instalación

   configuration/install

.. toctree::
   :maxdepth: 2
   :caption: Guía de usuario

   user_guide/dataset/main
   user_guide/dataset/image_datasets
   user_guide/model/image_models
   user_guide/trainer/main
   user_guide/trainer/image_trainers
   user_guide/pipeline/main

.. toctree::
   :caption: API 
   
   modules
