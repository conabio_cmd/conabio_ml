#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

import os
import uuid
import json
import shutil
import pydash
import pandas as pd

from collections import defaultdict
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

from datetime import datetime
from pathlib import Path

from seaborn.rcmod import reset_defaults

from conabio_ml.process import Process, ProcessTypes

from conabio_ml.utils.utils import get_default_args
from conabio_ml.utils.report_params import languages
from conabio_ml.reports.md_reports import MD_Report

# Using for type accesses
from conabio_ml.datasets.dataset import Dataset
from conabio_ml.preprocessing.preprocessing import PreProcessing
from conabio_ml.trainer.model import Model
from conabio_ml.trainer.trainer import Trainer
from conabio_ml.trainer.trainer_config import TrainerConfig
from conabio_ml.evaluator.evaluator import Evaluator, Metrics

from conabio_ml.utils.logger import get_logger, debugger

# To rebuild callables sent to the Pipeline
import importlib
import inspect
from typing import Callable, Iterable, List

PYGRAPHVIZ_AVAILABLE = False

logger = get_logger(__name__)
debug = debugger.debug


try:
    # Since pipeline is basically a DAG is better drawn using
    # pygraphviz, nevertheless, this library use aditional resources
    # related with the platform it uses, better documentation for having
    # those resources are in:
    # https://pygraphviz.github.io/documentation/latest/install.html
    # if no pygraphviz is found we use
    # networkx
    # that obtains every dependence by pip
    import pygraphviz as pgv
    PYGRAPHVIZ_AVAILABLE = True
except Exception as ex:
    logger.warning(f"Pygraphviz is not installed in your system. "
                   f"For a correct graphical display, please install it")
    pass


class Pipeline():
    """Pipeline class manages processes that also represents a set of instructions.
    Additionally it verifies type of all processes outputs to verify compatibility.

    Attributes
    ----------
    id : str
        Unique ID for the current pipeline being executed.
    path : str
        Destination path where the pipeline and its resources will
        be stored.
    processes : dict
        Processed to be managed for the pipeline
    pipeline_file : str
        JSON file where pipeline info will be stored
    pipeline_graph : [pygraphviz.AGraph | networkx.Graph]
        Graphical representation of the current pipeline
    """

    TYPES_OF_PROCESSES = {
        # ProcessTypes.ASSET: "plum2",
        # ProcessTypes.SPECIAL: "limegreen",
        ProcessTypes.ROOT: "limegreen",
        Dataset.DatasetType: "skyblue",
        PreProcessing.PreprocessingType: "deepskyblue",
        Model.ModelType: "yellow2",
        Trainer.TrainerType: "mediumpurple1",
        TrainerConfig.TrainerConfigType: "orchid",
        Evaluator.EvaluatorType: "violet",
        Metrics.MetricsType: "black",
        "None": "azure3"
    }

    def __init__(self,
                 path,
                 name: str = None,
                 draw_graph: bool = False):
        """Constructor of the Pipeline

        Parameters
        ----------
        path : str
            Destination path of the pipeline
        name : str, optional
            Name given to the pipeline, by default None
        draw_graph : bool, optional
            Whether the pipeline network is drawn or not, by default False
        """

        # Si no se proporciona un 'name', se genera un nombre predeterminado usando la fecha y hora actual
        name = name or str(datetime.now().strftime("%Y%m%d%H%M%S"))

        # Se inicializa las variables del constructor
        self.id = name
        self.path = os.path.join(path, self.id)
        self.processes = {}
        self.pipeline_file = os.path.join(self.path, "pipeline.json")
        self.draw_graph = draw_graph
        self.pipeline_graph = None
        
        # Se verifica que el directorio no se duplique.
        if os.path.exists(self.path):
            shutil.rmtree(self.path)

        os.makedirs(self.path)

        # Crea un archivo JSON de registro inicial del pipeline
        init_pipeline_log = {
            "id": self.id,
            "processes": []
        }
        # Crea el proceso raíz con valores predeterminados y lo agrega al pipeline
        with open(self.pipeline_file, mode="w") as _file:
            json.dump(init_pipeline_log, _file)

        root = Process(name="ROOT",
                       process_type=ProcessTypes.ROOT,
                       inputs_from_processes=[],
                       args={})
        self.__add(root)

    def __add(self, process):
        """
        It stores a process to be executed in the pipeline, also asserts
        inputs of the process and previous stored processes in the pipeline

        Parameters
        ----------
        process : Process
          Process to be stored

        Returns
        -------
        Pipeline
            Self instance
        """
        if process.name in self.processes:
            logger.warning(
                f"Process '{process.name}' is already in the processes list. \n"
                f"Please check the definition of your processes.")

        if len(process.inputs_from_processes) == 0 and len(self.processes) > 0:
            process.inputs_from_processes = ["ROOT"]

        for input_process in process.inputs_from_processes:
            assert type(input_process) in (str, Process), \
                f"Invalid input type: {type(input_process)}"

            input_name = input_process if type(
                input_process) == str else input_process.name
            assert input_name in self.processes, f"Process '{input_process}' does not exist."

        self.processes[process.name] = process

        return self

    def add_process(self,
                    name: str,
                    action: Callable,
                    reportable: bool = False,
                    report_functions: List[Callable] = [],
                    inputs_from_processes: List = [],
                    args={}):
        """Creates a process to be added in the pipeline

        Parameters
        ----------
        name : str
            Name of the process
        action : Callable
            Handler to a function to be processed
        reportable : bool, optional
            Whether the process will be added to the report or not, by default False
        report_functions : List[Callable], optional
            List of functions used to perform the report, by default []
        inputs_from_processes : List, optional
            Names of the input processes. These processed have to be previously added in the
            pipeline, by default []
        args : dict, optional
            Extra arguments, by default {}

        Returns
        -------
        Pipeline
            Self instance
        """
        try:
            process_type = self.__get_action_wrapper(action)
            process_type = process_type.get_type()
        except AttributeError as error:
            logger.exception(f"The API type related to the process {name} "
                             f"cannot be found. Will be handled as annonymous method")
            process_type = None

        process = Process(name=name,
                          process_type=process_type,
                          action=action,
                          reportable=reportable,
                          report_functions=report_functions,
                          inputs_from_processes=inputs_from_processes,
                          args=args)

        return self.__add(process)

    def __get_action_wrapper(self, action):
        """Obtains the type of the process realted to the action. The types allowed are
        defined in TYPES_OF_PROCESSES. If the type is not in the dictionary
        will be assumed as None.

        Parameters
        ----------
        action : function
            Action performed by the process

        Returns
        -------
        type
            Process type
        """
        try:
            if inspect.ismethod(action):
                temp = action.__self__
                return temp

            if inspect.isfunction(action):
                temp = getattr(inspect.getmodule(action), action.__qualname__.split(
                    '.<locals>', 1)[0].rsplit('.', 1)[0])
                return temp

            return None
        except:
            return None

    def __get_process_inputs(self, process):
        """Obtains the inputs of a process

        Parameters
        ----------
        process : Process
            Process to handle

        Returns
        -------
        list
            The list of the types defined in the process inputs
        """
        process_inputs = pydash.chain(process.inputs_from_processes)\
            .filter(lambda x: self.processes[x].process_type is not ProcessTypes.ROOT)\
            .value()

        inputs = []

        # Add the process inputs for further check
        for input_process in process_inputs:
            if type(input_process) == Process:
                inputs.append(input_process.output)
            elif type(input_process) == str:
                inputs.append(self.processes[input_process].output)

        return inputs

    def __check_process_types(self):
        """Makes the type checking of all processes in the pipeline.
        For each process it asserts:
          - Type of the process
          - Process return annotation type
          - Process inputs annotation types
        """

        for _, process in self.processes.items():
            if process.process_type is ProcessTypes.ROOT:
                continue

            inputs_types = []
            for input_process in process.inputs_from_processes:
                input_process = self.processes[input_process]

                if input_process.process_type is ProcessTypes.ROOT:
                    continue

                process_annotations = input_process.action.__annotations__

                assert 'return' in process_annotations, f"You must annotate return type in actions"

                inputs_types.append(process_annotations['return'])

            params_types = process.action.__annotations__
            params_names = [x for x in params_types.keys()]
            params_values = [x for x in params_types.values()]
            for i in range(len(inputs_types)):
                if params_names[0] == 'args':  # TODO: fix this
                    param_type = params_types['args']
                elif params_names[0] == 'cls':  # TODO: fix this
                    param_type = params_values[i+1]
                else:
                    param_type = params_values[i]

                assert inputs_types[i] == param_type,\
                    f"Type of inputs and parameters in process "\
                    f"{process.action.__qualname__} did not match: "\
                    f"({inputs_types[i]} != {param_type})"

    def __indent_rep(self, pipeline_dict: dict, span="") -> str:
        """
        Creates a representation of the pipeline dictionary
        to an nicely indent plain string representation

        Parameters
        ----------
        val : dict
            [Pipeline dictionary]
        current : [type], optional
            [Current node of the pipeline dictionary], by default None

        Returns
        -------
        [str]
            [String indeted representation]
        """
        res = ""

        if isinstance(pipeline_dict, dict):
            for key, val in pipeline_dict.items():
                temp, dest_type = self.__indent_rep(val, "  "+span)

                if isinstance(dest_type, (dict, list)):
                    res += f"{span}{key}:\l{temp}"
                else:
                    res += f"{span}{key}:{temp}\l"

        elif isinstance(pipeline_dict, list):
            for val in pipeline_dict:
                temp, dest_type = self.__indent_rep(val, "  "+span)

                if isinstance(dest_type, (dict, list)):
                    res += f"{span}\l- {temp}"
                else:
                    res += f"{span}- {temp}\l"
        else:
            res = str(pipeline_dict)

        return res, pipeline_dict

    def __flat_process(self, process):
        """Checks the type of process, it to unrolls itself and make a dumpable version
        of non-dumpable values

        Parameters
        ----------
        process : Process
            Process to handle

        Returns
        -------
        [type]
            Dumpable version of the process

        Raises
        ------
        Exception
            If the value to manage in this scope still cannot be dumped
        """
        results = None
        if type(process) is dict:
            results = {}
            for key, value in process.items():
                temp_key = key
                if inspect.isfunction(temp_key):
                    temp_key = key.__name__

                results[temp_key] = self.__flat_process(value)
        elif type(process) is list:
            results = []
            for single_process in process:
                results.append(self.__flat_process(single_process))

            return results
        elif process is None:
            return None
        elif inspect.isfunction(process):
            return f"function: {process.__name__}"
        elif isinstance(process, Path):
            return str(process)
        else:
            try:
                json.dumps(process)
                return process
            except TypeError as error:
                # If nothing works we just call the string method to make it
                # serializable
                try:
                    try:
                        temp = process.__class__
                        temp = f"{temp.__module__} - {temp.__name__}"
                    except:
                        temp = str(process)

                    logger.warning(error)
                    logger.warning((f"The process cannot be serializable\n"
                                    f"The value received was converted to its string representation"))

                    return temp
                except Exception as ex:
                    logger.exception(f"The process {process} is not serializable. "
                                     f"Pipeline cannot be created")
                    raise Exception(ex)
            except Exception as ex:
                logger.exception(ex)
                raise(ex)

        return results

    def __update_pipeline(self, process):
        """Updates the processes of the pipeline JSON file.
        It adds the current process

        Parameters
        ----------
        process : Process
            Process to handle
        """
        with open(self.pipeline_file, mode="r") as _file:
            temp_pipeline = json.load(_file)

        process_args = process.args
        name = process.name
        inputs_from_processes = process.inputs_from_processes

        write_args = {}
        for name, value in process_args.items():
            try:
                write_args[name] = value
            except Exception as error:
                logger.exception(error)
                raise(error)

        default_args = get_default_args(process.action)
        for name, val in default_args.items():
            if name not in process_args:
                process_args[name] = val

                if name not in write_args:
                    write_args[name] = val

        temp_pipeline["processes"].append({
            "name": process.name,
            "action": process.action.__qualname__,
            "inputs_from_processes": [x if type(x) == str else x.name
                                      for x in inputs_from_processes],
            "args": write_args
        })

        with open(self.pipeline_file, mode="w") as _file:
            try:
                temp_pipeline = PipelineJSONEncoder().encode(temp_pipeline)
                json.dump(temp_pipeline, _file)
            except Exception as ex:
                logger.error(
                    "An error occurred while serializing the pipeline file.")
                logger.error(ex)
                raise

    def __report_process(self, process, **kwargs):
        """Executes the reporter method of the class of the process action
        If process is not part of the defined classes in the API, tries to find
        the reporter handler

        Parameters
        ----------
        process : Process
            Process to handle
        **kwargs
            Extra named arguments to be passed to reporter method
        """
        try:
            wrapper = self.__get_action_wrapper(process.action)

            if wrapper is not None:
                process_type = wrapper.get_type()
                if process_type in self.TYPES_OF_PROCESSES:
                    i = process.output
                    assets_path = os.path.join(self.path, process.name)
                    if not os.path.exists(assets_path):
                        os.makedirs(assets_path)

                    if inspect.isclass(i):
                        i = i()

                    res = i.reporter(assets_path, process.args, **kwargs)
                    report = {"asset": res,
                              "type": i.get_type()}

                    for rep in process.report_functions:
                        new_report = rep(i,
                                         assets_path,
                                         process.args,
                                         **{})

                        if not isinstance(new_report, dict):
                            logger.warning(f"The return value for the reporer {rep.__qualname__}"
                                           f"should have to be a dictionary")
                            continue

                        new_keys = set([k for k in new_report.keys()])
                        report_keys = set([k for k in report["asset"].keys()])
                        repeated = report_keys.intersection(new_keys)

                        if len(repeated) > 0:
                            logger.warning(f"The following keys {repeated} of the report "
                                           f"are being overwritten")

                        report["asset"].update(new_report)
                else:
                    ex = f"Not implemented yet"
                    logger.exception(ex)
                    raise Exception(ex)
            else:
                ex = f"Not implemented yet"
                logger.exception(ex)
                raise Exception(ex)

            self.__update_reported_process(process, report)
        except AttributeError as error:
            # Output expected AttributeErrors.
            logger.exception(f"Wrapper class not found, possible custom action"
                             f"trying to search a handler")
            raise error
        except Exception as ex:
            logger.exception(ex)
            raise

    def __update_reported_process(self, process, report):
        """Updates the processes of the pipeline JSON file.
        It adds the current process

        Parameters
        ----------
        process : Process
            Process to handle
        report : dict
            Dictionary with assets and type of the report
        """
        with open(self.pipeline_file, mode="r") as _file:
            temp_pipeline = json.load(_file)

        pipeline_report = temp_pipeline.get("report", {})

        write_args = {"process_type": str(report["type"])}

        assets = {}

        for k, asset in report["asset"].items():
            res = {"type": asset["type"].value,
                   "asset": PipelineJSONEncoder().encode(asset["asset"])}

            assets[k] = res

        write_args["asset"] = assets
        pipeline_report[process.name] = write_args

        temp_pipeline["report"] = pipeline_report

        with open(self.pipeline_file, mode="w") as _file:
            json.dump(temp_pipeline, _file)

    def __exec_process(self, process):
        """Executes the process action

        Parameters
        ----------
        process : Process
            Process to handle
        """
        inputs = self.__get_process_inputs(process)

        process.output = process.action(*inputs, **process.args)

        name_process = process.name \
            if hasattr(process, "name") else process.action.__name__

        try:
            inputs.append(self)
            kw = {type(t).__name__.lower(): t for t in inputs}

            wrapper = self.__get_action_wrapper(process.action)
            wrapper.on_finish(name_process, {}, **kw)
        except Exception as ex:
            logger.error(ex)
            logger.error(
                f"on_finish method of the process {name_process} cannot be executed")

    def exec(self, process, **kwargs):
        """Helper method that deals with the substages of a process execution.

        Parameters
        ----------
        process : Process
            Process to handle
        """
        if process.process_type is ProcessTypes.ROOT:
            return

        try:
            self.__exec_process(process)
            self.__update_pipeline(process)

            if not process.reportable:
                logger.debug(f"Process {process.name} skipped for reporting.")
            else:
                self.__report_process(process, **kwargs)

            if self.draw_graph and PYGRAPHVIZ_AVAILABLE:
                self.update_process_completed(process)
        except AttributeError as error:
            logger.exception(f"Process reportable atribute not found")
            logger.exception(error)
            raise
        except Exception as ex:
            logger.exception(ex)
            raise

    def draw(self):
        """Draws the connections between processes of the pipeline.
        """

        def unwrap(value):
            v = ""
            label = ""
            if type(value) == type({}):
                temp = value.items()

                if len(temp) == 0:
                    label = "{}"
                else:
                    v = "obj: \n " + str(
                        "\n ".join(
                            pydash.chain(value.items()).map(lambda x: str(
                                x[0]) + ":" + str(unwrap(x[1]))).value()
                        )
                    )
                label += "\n ".join(v.split("\n"))
            elif type(value) == type([]):
                if len(value) == 0:
                    label = "[]"
                else:
                    v = "array: \n" + str("\n".join(
                        pydash.chain(value)
                        .map(lambda x:  f" {unwrap(x)}")
                        .value()))

                    label += v + "\n"
            else:
                label += str(value)

            return label

        nodes = []
        edges = []

        for _, process in self.processes.items():
            nodes.append(process.name)
            edges += [(x,
                       process.name,
                       process.args.items())
                      for x in process.inputs_from_processes]

        if PYGRAPHVIZ_AVAILABLE:
            G = pgv.AGraph(strict=False, directed=True)
        else:
            G = nx.Graph()

        # ADD NODES
        for node in nodes:
            G.add_node(node)

            if PYGRAPHVIZ_AVAILABLE:
                n = G.get_node(node)
                proc = self.processes[node]

                color = self.TYPES_OF_PROCESSES.get(proc.process_type,
                                                    self.TYPES_OF_PROCESSES["None"])
                n.attr['color'] = color

        # G.add_node("END")

        # ADD EDGES
        for edge in edges:
            G.add_edge(edge[0], edge[1])

            if PYGRAPHVIZ_AVAILABLE:
                label = {}
                e = G.get_edge(edge[0], edge[1])

                for k, v in edge[2]:
                    temp = ""
                    if k == "dest_path":
                        temp = "PIPELINE_PATH"
                    else:
                        try:
                            json.dumps(v)
                            temp = v
                        except:
                            temp = self.__flat_process(v)

                    label[k] = temp

                e.attr['label'], _ = self.__indent_rep(label)

        # G.add_edge(edge[1], "END")

        self.pipeline_graph = G

        try:
            if PYGRAPHVIZ_AVAILABLE:
                self.pipeline_graph.layout(prog='dot')
                self.pipeline_graph.layout()
                self.pipeline_graph\
                    .draw(os.path.join(self.path, 'pipeline.png'))
            else:
                nx.draw(self.pipeline_graph,
                        with_labels=True, font_weight='bold')
                plt.savefig(os.path.join(self.path, 'pipeline.png'),
                            format='png', dpi=1000)

        except Exception as ex:
            print(ex)

    def update_process_completed(self,
                                 process):
        """Updates the graph of the pipeline after the execution of a process.

        Parameters
        ----------
        process : Process
          Process executed
        """
        node = process.name
        node = self.pipeline_graph.get_node(node)

        node.attr['style'] = "filled"

        self.pipeline_graph.layout(prog='dot')
        self.pipeline_graph\
            .draw(os.path.join(self.path, 'pipeline.png'))

    def run(self,
            report_lang=languages.EN,
            report_pipeline=True):
        """Runs the set of actions of each process stored in the pipeline.
        It follows the following process:
            - Draws the pipeline graph
            - Makes the pipeline type checking of: process itself, process
              inputs and process outputs
            - Execute each process action

        Parameters
        ----------
        report_lang : languages, optional
            Language of the report, by default languages.EN

        Returns
        -------
        Pipeline
            Self instance
        """
        self.__check_process_types()

        if self.draw_graph:
            self.draw()

        for process in self.processes.values():
            self.exec(process, lang=report_lang)

        if report_pipeline:
            self.generate_report(lang=report_lang)

        return self

    def generate_report(self, lang=languages.EN):
        """Method that generates the pipeline report from the information stored in the `report`
        property of the `pipeline_file` JSON file.
        The information used by this method is generated in the `reporter` methods of the classes
        and subclasses of `Dataset`, `Model` and `Metrics`, to generate a report in a file in
        Markdown format that will be stored in `path`/report.md

        Parameters
        ----------
        lang : languages, optional
            Language of the report, by default languages.EN
        """
        with open(self.pipeline_file, mode="r") as _file:
            pipeline_info = json.load(_file)

        if "report" not in pipeline_info:
            return

        process_type_list = defaultdict(list)
        for process_name, process_info in pipeline_info["report"].items():
            process_info["process_name"] = process_name
            process_type_list[process_info["process_type"]].append(
                process_info)

        # Dataset
        dataset_data = []
        for process_info in process_type_list.get('~DatasetType', []):
            temp = {}
            temp["process_name"] = process_info["process_name"]
            dataset_asset = process_info["asset"]
            # Dataset info
            info_asset = dataset_asset.get("info", {"asset": {}})["asset"]
            temp["info"] = {}
            if info_asset:
                year = info_asset.get("year", "")
                version = info_asset.get("version", "")
                temp["info"]["collection"] = info_asset.get("collection", None)
                temp["info"]["version"] = (f"{year}, {version}"
                                           if year and version else f"{year}{version}"
                                           if year or version else None)
                temp["info"]["url"] = info_asset.get("url", None)
            # Dataset preprocessing
            preproc_asset = dataset_asset.get(
                "preprocessing", {"asset": {}})["asset"]
            temp["preprocessing"] = []
            if preproc_asset:
                for asset_name, asset_val in preproc_asset.items():
                    temp["preprocessing"].append({asset_name: asset_val})
            # Dataset augmentation
            augment_asset = dataset_asset.get(
                "augmentation", {"asset": {}})["asset"]
            temp["augmentation"] = []
            if augment_asset:
                for asset_name, asset_val in augment_asset.items():
                    temp["augmentation"].append({asset_name: asset_val})
            # Dataset distribution for labels and partitions
            distribution_asset = dataset_asset.get(
                "samples_per_class", {"asset": {}})["asset"]
            temp["samples_info"] = {}
            if distribution_asset:
                df = pd.read_csv(distribution_asset)
                temp["samples_info"]["samples_per_class"] = {
                    label: np.sum(df.loc[df["label"] == label]["count_label"])
                    for label in df["label"].unique()
                }
                if "partition" in df.columns:
                    counts_part = defaultdict(dict)
                    for _, row in df.iterrows():
                        counts_part[row["label"]][row["partition"]
                                                  ] = row["count_label"]
                    temp["samples_info"]["samples_per_class_part"] = counts_part
                    temp["samples_info"]["samples_per_part"] = {
                        part: np.sum(
                            df.loc[df["partition"] == part]["count_label"])
                        for part in df["partition"].unique()
                    }
                temp["samples_info"]["samples_per_class_plot"] = dataset_asset.get(
                    "samples_per_class_plot", {"asset": {}})["asset"]
                temp["samples_info"]["samples_per_class_plot_trunc"] = dataset_asset.get(
                    "samples_per_class_plot_trunc", {"asset": {}})["asset"]
            # Additional criteria
            additional_asset = dataset_asset.get(
                "additional_criteria", {"asset": {}})["asset"]
            temp["additional_criteria"] = []
            if additional_asset:
                for asset_name, asset_val in additional_asset.items():
                    temp["additional_criteria"].append({asset_name: asset_val})

            dataset_data.append(temp)

        # Model
        model_data = []
        for process_info in process_type_list.get('~ModelType', []):
            temp = {}
            temp["process_name"] = process_info["process_name"]
            model_asset = process_info["asset"]
            # Model data
            temp["model_data"] = model_asset.get(
                "model_data", {"asset": {}})["asset"]
            model_data.append(temp)

        # Metrics
        metrics_data = []
        for process_info in process_type_list.get('~MetricsType', []):
            temp = {}
            temp["process_name"] = process_info["process_name"]
            metrics_asset = process_info["asset"]
            # Results plots
            temp["plots"] = metrics_asset.get(
                "result_plots", {"asset": {}})["asset"]
            # Truncated results plots
            temp["plots_trunc"] = metrics_asset.get(
                "result_plots_trunc", {"asset": {}})["asset"]
            # Classification examples
            temp["classification_examples"] = metrics_asset.get("classification_examples",
                                                                {"asset": {}})["asset"]
            # Metrics sets
            eval_config = metrics_asset.get(
                "eval_config", {"asset": {}})["asset"]
            temp["metrics_set"] = eval_config.get("metrics_set", {})
            # Results tables
            temp["results_tables"] = metrics_asset.get(
                "results_tables", {"asset": {}})["asset"]
            metrics_data.append(temp)

        report = MD_Report(id=self.id,
                           assets_path=self.path,
                           dataset_data=dataset_data,
                           model_data=model_data,
                           metrics_data=metrics_data)
        report.generate(file_name="report.md", lang=lang)


class PipelineJSONEncoder():
    def unroll_pipeline(self,
                        pipeline):
        result = None

        if isinstance(pipeline, dict):
            result = {}
            for key, value in pipeline.items():
                temp = self.unroll_pipeline(value)
                result[key] = temp
        elif isinstance(pipeline, list):
            result = []
            for value in pipeline:
                temp = self.unroll_pipeline(value) 
                result.append(temp)
        else:
            try:
                json.dumps(pipeline)
                result = pipeline
            except Exception:
                try:
                    result = self.default(pipeline)
                except Exception as ex:
                    result = str(result)
                    logger.warning(ex)

        return result

    def encode(self, value):
        try:
            json.dumps(value)
            return value
        except TypeError:
            serialized = self.unroll_pipeline(value)
            return serialized
        except Exception as ex:
            logger.error(ex)
            raise

    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()

        if inspect.isfunction(obj):
            return f"function: {obj.__qualname__}"

        if isinstance(obj, pd.DataFrame):
            return str(obj.__class__)

        if pd.api.types.is_numeric_dtype(obj):
            return float(obj)

        if isinstance(obj, Path):
            return str(obj)
        # if isinstance(obj, Pos):

        # If nothing works we just call the string method to make it
        # serializable
        logger.warning(
            f"The object {obj.__class__} can only be encoded as string")
        obj = str(obj)

        return obj
