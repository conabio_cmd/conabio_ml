import inspect
import os
import copy

from enum import Enum

from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)


class ProcessTypes(Enum):
    """Allowed types of processes
    """
    ROOT = 1
    ASSET = 2


class Process():
    """
    Represents a general process to be added in the pipeline

    Pipeline class manages processes that also represents a set of
    instructions.
    Additionally it verifies type of all processes outputs to verify
    compatibility.

    Attributes
    ----------
    name : str
        Name of the process
    action : func
        Handler to a function to be processed
    inputs_from_processes : list of str
        Names of the input processes. These processed have to be
        previously added in the pipeline
    args : dict
        Extra arguments for execution
    output : type
        After the action of the process is executed, this value is
        updated with the returning annotation
    """

    def __init__(self,
                 name,
                 process_type,
                 action=None,
                 inputs_from_processes=[],
                 report_functions=[],
                 reportable=False,
                 args={}):
        self.name = name
        self.action = action
        self.reportable = reportable
        self.report_functions = report_functions
        self.inputs_from_processes = inputs_from_processes
        self.args = args
        self.output = None
        self.process_type = process_type

        # for report_function in report_functions:
        #     self.assert_reporter(report_function)

        if process_type is not ProcessTypes.ROOT:
            assert action is not None, f"You must pass an action for the Process."

    def assert_reporter(self, reporter):
        """
        Asserts the signature of the reporter functions.
        The suganture should have to match the following signature:
            - First param: Should match self.process_type
            - Second param: str (Report destination parth)
            - Third param: dict (Arguments of the process)


        Arguments:
            reporter {[Callable]} -- Function to assert its signature
        """
        try:
            signature = inspect.signature(reporter)
            parameters = [param for _, param in signature.parameters.items()]

            assert len(parameters) > 3, \
                logger.exception(f"The signature must contain at least type_handler,"
                                 f"dest_path and process_args")

            assert parameters[0].annotation == self.process_type, \
                logger.exception("The first parameter of the signature for the process" +
                                 self.name + " must be the destination type " +
                                 " of the process consider using " + str(self.process_type) +
                                 " as first parameter type.")

        except AssertionError as error:
            logger.exception(
                "The signature does not accomplish the requirements. Check the signature require_map in https://hackmd.io/y-uuPRJ5S6mAYefn-b9oEQ#Extensi%C3%B3n-del-reporte")
            raise error
        except Exception as ex:
            logger.exception(ex)
            raise ex


class ProcessState():

    @classmethod
    def on_finish(cls,
                  process_name: str,
                  on_finish_data: dict = None,
                  **kargs):
        """
        This method is intended to be chained by a child class to report
        data after executed.

        Parameters
        ----------
        process_name : str
            [Name of the process who is executing this method]
        on_finish_data : dict, optional
            [Data reported by a child class], by default {}

        Returns
        -------
        [dict]
            [Data reported for a child class]
        """
        on_finish_data = {} if on_finish_data is None else on_finish_data
        return on_finish_data
