#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import TypeVar

from conabio_ml.process import ProcessState
from conabio_ml.datasets.dataset import Dataset, PredictionDataset
from abc import abstractmethod

from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.utils import store_json_file, load_json_file

logger = get_logger(__name__)


class Evaluator(ProcessState):
    """
    Base class that contains the `eval` method to perform the evaluation of a model from the
    prediction dataset resulting from the inference on a set of elements, the original dataset
    and an evaluation configuration

    Attributes
    ----------
    dataset_true : Dataset
        Dataset of true labels
    dataset_pred : PredictionDataset
        Dataset of predictions that coming from Model.predict method
    eval_config : dict
        Options to define the evaluation params of the model
    """
    EvaluatorType = TypeVar('EvaluatorType', bound='Evaluator')

    results = {
        "one_class": {},
        "all_classes": {}
    }

    @classmethod
    @abstractmethod
    def eval(cls,
             dataset_true: Dataset.DatasetType,
             dataset_pred: PredictionDataset.DatasetType,
             eval_config: dict) -> dict:
        """Method that performs the evaluation of a model from the prediction dataset resulting
        from the inference on a set of elements, the original data set, and an evaluation
        configuration

        Parameters
        ----------
        dataset_true : Dataset
            Dataset of true labels
        dataset_pred : PredictionDataset
            Dataset of predictions that coming from Model.predict method
        eval_config : dict
            Options to define the evaluation params of the model

        Returns
        -------
        Metrics
            Result with evaluation metrics

        """
        raise Exception("Method not implemented")

    """
    GET METHODS
    """
    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        EvaluatorType
        """
        return cls.EvaluatorType


class Metrics():
    """Base class with the metrics results of a model evaluation
    """

    MetricsType = TypeVar('MetricsType', bound='Metrics')

    def __init__(self, results):
        self.results = results
        self.__check_results()

    def __check_results(self):
        """Helper method that performs the validation of the names and the structure of the
        metric sets to be evaluated

        Raises
        ------
        ValueError
            In case the name of the metric set is invalid or the structure of the results is
            invalid
        """
        for name, metrics in self.results.items():
            if self.Sets.NAMES and name not in self.Sets.NAMES:
                raise ValueError(f"{name} is not a valid metric.")
            if 'one_class' not in metrics or 'per_class' not in metrics:
                raise ValueError(f"Invalid structure of metric {name}.")

    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        MetricsType
        """
        return cls.MetricsType

    def store_eval_metrics(self: MetricsType, dest_path: str) -> MetricsType:
        """Stores the result metrics in a JSON file in `dest_path`

        Parameters
        ----------
        dest_path : str
            Path of the JSON file.
            In case it is a directory, the file will be stored with the name 'results.json'

        Returns
        -------
        Metrics
            Metrics object instance from which results were stored
        """
        store_json_file(data=self.results, dest_path=dest_path)
        return self

    @classmethod
    def load_eval_metrics(cls,
                          dataset_true: Dataset.DatasetType,
                          dataset_pred: PredictionDataset.DatasetType,
                          source_path: str) -> MetricsType:
        """Creates an instance of a Metrics object from the results previously saved with the
        `store_eval_metrics` method

        Parameters
        ----------
        dataset_true : Dataset.DatasetType
            Dataset of true labels
        dataset_pred : PredictionDataset.DatasetType
            Dataset of predictions that coming from Model.predict method
        source_path : str
            Path of the JSON file that contains the metrics

        Returns
        -------
        Metrics
            Created Metrics instance with the loaded results
        """
        results = load_json_file(source_path=source_path)
        return cls(results, dataset_true, dataset_pred)

    def reporter(self: MetricsType,
                 dest_path: str,
                 process_args: dict,
                 data_to_report: dict = {}):
        """Method that generates the report elements of evaluated metrics

        Parameters
        ----------
        dest_path : str
            Path of the folder to store the resulting files
        process_args : dict
            The args of the process that wraps this class
        data_to_report : dict, optional
            The data to update. This params is passed to child instances.
            The param is read to check a possible overwrite proces of child instances,
            if it was overwritten just bypass the current process for THIS param.
            By default {}

        Returns
        -------
        dict
            Params to be reported in the Pipeline
        """
        try:
            return data_to_report
        except AttributeError as error:
            logger.exception(error)
            return data_to_report
        except Exception as ex:
            logger.exception(ex)
            raise

    class Sets():
        """Allowed types of metrics sets
        """
        # Names of valid metrics sets for each specific implementation.
        # E.g. "BINARY", "MULTICLASS", "PASCAL_VOC", etc.
        NAMES = []
