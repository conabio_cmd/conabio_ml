#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import json
import multiprocessing

import numpy as np
import pandas as pd

import seaborn as sns
import matplotlib.pyplot as plt

from multiprocessing import Pool
from functools import partial
from collections import defaultdict

from conabio_ml.datasets.dataset import Dataset, PredictionDataset
from conabio_ml.evaluator.evaluator import Evaluator as EV
from conabio_ml.evaluator.evaluator import Metrics as M

from conabio_ml.utils.utils import get_and_validate_args

from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
from sklearn.metrics import hamming_loss, accuracy_score
from sklearn.preprocessing import OneHotEncoder
from conabio_ml.utils import report_params
from conabio_ml.utils.report_params import languages
from conabio_ml.utils.report_params import report_transl as transl
from conabio_ml.utils.logger import get_logger, debugger
from conabio_ml.assets import AssetTypes

logger = get_logger(__name__)
debug = debugger.debug

procs = multiprocessing.cpu_count() - 1


class Metrics(M):
    """Class containing the results of the evaluation of a model in a dataset
    """

    def __init__(self, results, dataset_true, dataset_pred, eval_config):
        """Create a Metrics instance for an evaluation of a model

        Parameters
        ----------
        results : dict
            Dictionary that contains the results of an evaluation of a model,
            with the following structure:
            {
                `metric_set`: {
                    'one_class': {
                        `metric_name`: `value`, ...
                    },
                    'per_class': {
                        `metric_name`: `value`, ...
                    }
                }
            }

            Where `metric_set` must be one of ("BINARY", "MULTICLASS", "MULTILABEL")
        dataset_true : Dataset
            Dataset of true labels
        dataset_pred : PredictionDataset
            Prediction dataset that coming from a Model's predict method
        eval_config : dict
            Dictionary that contains the configuration of the evaluation process.
        """
        super().__init__(results)
        self.dataset_true = dataset_true
        self.dataset_pred = dataset_pred
        self.eval_config = eval_config

    # region bynary_evaluator
    def BINARY(y_true,
               y_pred,
               opts):
        """Performs binary evaluation with true and predicted elements

        Parameters
        ----------
        y_true : pd.DataFrame
            Dataframe with true elements. It must contain the columns 'item' and 'true_label'
        y_pred : pd.DataFrame
            Dataframe with true elements. It must contain the columns 'item' and 'pred_label'
        opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`

        Returns
        -------
        dict
            Dictionary with the results of the binary evaluation
        """
        results = {"one_class": {},
                   'per_class': {}}

        opts["average"] = "binary"
        _from = opts.get("from", "categorical")

        if _from == "categorical":
            Y = pd.concat([y_true, y_pred], axis=1, sort=False)
            classes_to_eval = opts.get("classes_to_eval", None)

            possible_values = set(y_true["true_label"].unique())\
                .union(set(y_pred["pred_label"].unique()))

            apply_args = {"Y": Y,
                          "opts": opts,
                          "possible_values": possible_values}

            binary_results = {}

            try:
                with Pool(processes=procs) as pool:
                    func = partial(Metrics.binary_helper, apply_args)
                    temp = pool.map(func, classes_to_eval)
            except Exception as ex:
                logger.error(ex)
                raise ex

            [binary_results.update(br) for br in temp]
            results["per_class"] = binary_results
        elif _from == "ordinal":
            Y = pd.DataFrame()
            Y["true_label"] = y_true.astype('bool')
            Y["pred_label"] = y_pred.astype('bool')

            results = Metrics.binary(Y, opts)
        else:
            logger.error("Method not implemented yet")

        return {Metrics.Sets.BINARY: results}

    @staticmethod
    def binary(Y, opts):
        """Performs the evaluation of the binary default set metrics

        Parameters
        ----------
        Y : pd.DataFrame
            DataFrame with the true and predicted labels
        opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`

        Returns
        -------
        dict
            Dictionary with the results of the binary evaluation

        Raises
        ------
        ex
            In case some metric cannot be evaluated
        """
        results = {}

        metrics = {
            "precision": Metrics.PRECISION,
            'recall': Metrics.RECALL,
            'f1_score': Metrics.F1
        }

        for metric_name, metric in metrics.items():
            try:
                metric_opts = get_and_validate_args(opts,
                                                    Metrics.evaluation_def[metric_name],
                                                    ignore_invalid=True)
                results[metric_name] = metric(
                    Y["true_label"], Y["pred_label"], **metric_opts)
            except Exception as ex:
                logger.error(f"The metric {metric_name} cannot be evaluated")
                logger.error(ex)
                raise ex

        return results

    @staticmethod
    def binary_helper(apply_args, class_to_eval):
        """Help function to perform binary evaluation

        Parameters
        ----------
        apply_args : dict
            Dictionary with the used arguments
        class_to_eval : str
            Class to be evaluated

        Returns
        -------
        dict
            Dictionary with the results of the evaluation for `class_to_eval`
        """
        res = {class_to_eval: {}}
        possible_values = apply_args["possible_values"]
        Y = apply_args["Y"]
        opts = apply_args["opts"]

        if class_to_eval not in possible_values:
            logger.warning(
                f"The class {class_to_eval} does not appear in the datasets")
            res[class_to_eval] = {
                "precision": 0.,
                'recall': 0.,
                'f1_score': 0.
            }
        else:
            temp = Y.apply(lambda x: x == class_to_eval, axis=1)
            res[class_to_eval] = Metrics.binary(temp, opts)

        return res

    # endregion

    # region multiclass_evaluation
    def MULTICLASS(y_true, y_pred, opts):
        """Performs multi-class evaluation with true and predicted elements

        Parameters
        ----------
        y_true : pd.DataFrame
            Dataframe with true elements. It must contain the columns 'item' and 'true_label'
        y_pred : pd.DataFrame
            Dataframe with true elements. It must contain the columns 'item' and 'pred_label'
        opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`

        Returns
        -------
        dict
            Dictionary with the results of the multi-class evaluation

        Raises
        ------
        ex
            In case some metric cannot be evaluated
        """
        try:
            results = {"one_class": {},
                       'per_class': {}}
            per_class = opts.get("per_class", False)

            metrics = {
                "precision": Metrics.PRECISION,
                'recall': Metrics.RECALL,
                'f1_score': Metrics.F1,
                'confusion_matrix': Metrics.CONFUSION_MATRIX
            }

            for metric_name, metric in metrics.items():
                try:
                    fn_args = get_and_validate_args(opts,
                                                    Metrics.evaluation_def[metric_name],
                                                    ignore_invalid=True)
                    res = metric(y_true["true_label"],
                                 y_pred["pred_label"], **fn_args)

                    results["one_class"][metric_name] = res
                except Exception as ex:
                    logger.error(
                        f"The metric {metric_name} cannot be evaluated")
                    logger.error(ex)
                    raise ex

            results["one_class"]["confusion_matrix"] = results["one_class"]["confusion_matrix"].tolist()
            results["one_class"]["labels"] = opts["labels"]

            if per_class:
                opts["classes_to_eval"] = opts["labels"]
                opts["from"] = "categorical"
                binary_results = Metrics.BINARY(y_true, y_pred, opts)

                results["per_class"] = binary_results[Metrics.Sets.BINARY]["per_class"]

            return {Metrics.Sets.MULTICLASS: results}
        except Exception as ex:
            logger.error(ex)
            raise ex

    # endregion

    # region multilabel_evaluation

    def MULTILABEL(y_true, y_pred, opts):
        """Performs multi-label evaluation with true and predicted elements

        Parameters
        ----------
        y_true : pd.DataFrame
            Dataframe with true elements. It must contain the columns 'item' and 'true_label'
        y_pred : pd.DataFrame
            Dataframe with true elements. It must contain the columns 'item' and 'pred_label'
        opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`

        Returns
        -------
        dict
            Dictionary with the results of the multi-label evaluation

        Raises
        ------
        ex
            In case some metric cannot be evaluated
        """
        results = {
            "one_class": {},
            "per_class": {}
        }

        items = [i for i in set(y_true["item"].unique())]

        true_dataset = pd.DataFrame(y_true).reset_index()
        pred_dataset = pd.DataFrame(y_pred).reset_index()

        true_indices = true_dataset.groupby("item").indices
        pred_indices = pred_dataset.groupby("item").indices

        labels = [[l] for l in opts["labels"]]

        encoder = OneHotEncoder(sparse=False)
        encoder.fit_transform(labels)

        apply_args = {
            "y_true": true_dataset,
            "y_pred": pred_dataset,
            "true_indices": true_indices,
            "pred_indices": pred_indices,
            "encoder": encoder
        }

        try:
            with Pool(processes=procs) as pool:
                func = partial(Metrics.expand_classes, apply_args)
                temp = pool.map(func, items)
        except Exception as ex:
            logger.error(ex)
            raise ex

        df = pd.DataFrame(temp)
        results["one_class"] = Metrics.__multilabel_metrics(df, opts)
        per_class = opts.get("per_class", None)
        ix_span = len(labels)

        per_class_metrics = {}
        if per_class:
            for ix_class in range(ix_span):
                try:
                    label = labels[ix_class][0]
                    opts["from"] = "ordinal"
                    temp = Metrics.BINARY(df[df.columns[ix_class]],
                                          df[df.columns[ix_class+ix_span]],
                                          opts)
                    per_class_metrics[label] = temp[Metrics.Sets.BINARY]
                except Exception as err:
                    logger.exception(err)
                    raise err

        results["per_class"] = per_class_metrics
        return {Metrics.Sets.MULTILABEL: results}

    @staticmethod
    def __multilabel_metrics(df, opts):
        """Performs the multi-label evaluation using macro, micro or binary average

        Parameters
        ----------
        df : pd.DataFrame
            Dataframe with true and predicted elements to be evaluated
        opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`

        Returns
        -------
        dict
            Dictionary with the results of the multi-label evaluation
        """
        metrics = {
            "precision": Metrics.PRECISION,
            'recall': Metrics.RECALL,
            'f1_score': Metrics.F1,
            'hamming_loss': Metrics.HAMMING_LOSS
        }

        results = {}
        average = opts.get("average", "binary")

        for metric_name, metric in metrics.items():
            results[metric_name] = {
                "macro": Metrics.__multilabel_macro,
                "micro": Metrics.__multilabel_micro,
                "binary": Metrics.__multilabel_binary}\
                .get(average, "binary")(metric,
                                        df,
                                        get_and_validate_args(opts,
                                                              Metrics.evaluation_def[metric_name],
                                                              ignore_invalid=True))

        return results

    @staticmethod
    def __multilabel_micro(metric, df, opts):
        """Performs the multi-label evaluation using micro average

        Parameters
        ----------
        df : pd.DataFrame
            Dataframe with true and predicted elements to be evaluated
        opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`

        Returns
        -------
        float
            Result of the evaluation
        """
        len_sample = int(df.shape[1]/2)
        if "average" in opts:
            opts["average"] = "binary"

        results = df.apply(lambda x: metric(list(x[0:len_sample]),
                                            list(x[len_sample:]),
                                            **opts), axis=1)

        return np.mean(results)

    @staticmethod
    def __multilabel_macro(metric, df, opts):
        """Performs the multi-label evaluation using macro average

        Parameters
        ----------
        df : pd.DataFrame
            Dataframe with true and predicted elements to be evaluated
        opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`

        Returns
        -------
        float
            Result of the evaluation
        """
        len_sample = int(df.shape[1]/2)

        if "average" in opts:
            opts["average"] = "binary"

        results = []
        for ix in range(len_sample):
            y_true = df[df.columns[ix]]
            y_pred = df[df.columns[ix + len_sample]]

            res = metric(list(y_true), list(y_pred), **opts)
            results.append(res)

        return np.mean(results)

    @staticmethod
    def expand_classes(apply_args,
                       row):
        """Converts a row from the dataset from label to its array-like representation
        label 1 -> [1 0]
        label 2 -> [0 1]
        label 1, 1 -> [1 1]

        Parameters
        ----------
        apply_args : dict
            Dictionary of the form
            {
                "y_true": pd.DataFrame, 
                "y_pred": pd.DataFrame, 
                "true_indices": dict,
                "pred_indices": dict,
                "labelmap": dict
            }
        row : str
            Item to be expanded

        Returns
        -------
        Array
            Results of encodig of the row
        """
        y_true = apply_args["y_true"]
        y_pred = apply_args["y_pred"]
        true_indices = apply_args["true_indices"]
        pred_indices = apply_args["pred_indices"]
        encoder = apply_args["encoder"]

        try:
            ix_true = true_indices.get(row, [])
            ix_pred = pred_indices.get(row, [])

            true_labels = np.zeros((1, len(encoder.get_feature_names())))
            pred_labels = np.zeros((1, len(encoder.get_feature_names())))

            if len(ix_true) > 0:
                true_labels_transform = [[label]
                                         for label
                                         in y_true.loc[ix_true]["label"].tolist()]
                true_labels = np.sum([encoder.transform([label])
                                      for label
                                      in true_labels_transform], axis=0)

            if len(ix_pred) > 0:
                pred_labels_transform = [[label]
                                         for label
                                         in y_pred.loc[ix_pred]["label"].tolist()]

                pred_labels = np.sum([encoder.transform([label])
                                      for label
                                      in pred_labels_transform], axis=0)

            result = np.concatenate([true_labels[0], pred_labels[0]], axis=0)

            return result
        except Exception as ex:
            logger.error(ex)
            raise ex

    @staticmethod
    def __multilabel_binary(metric, df, opts):
        """Performs the multi-label evaluation using binary average

        Parameters
        ----------
        df : pd.DataFrame
            Dataframe with true and predicted elements to be evaluated
        opts : dict
            Dictionary with the configuration options of the evaluation.
            Options will be validated against `evaluation_def`

        Returns
        -------
        float
            Result of the evaluation
        """
        result = metric(list(df[df.columns[0]]),
                        list(df[df.columns[1]]),
                        **opts)

        return result
    # endregion

    PRECISION = precision_score
    RECALL = recall_score
    F1 = f1_score
    HAMMING_LOSS = hamming_loss
    ACCURACY = accuracy_score
    CONFUSION_MATRIX = confusion_matrix

    evaluation_def = {
        "precision": {
            'labels': {
                'type': list,
                'optional': False
            },
            'pos_label': {
                'type': str,
                'optional': True
            },
            'average': {
                'type': str,
                'optional': True,
                'default': 'micro'
            },
            'zero_division': {
                'type': [float, str],
                'optional': True,
                'default': 'warn'
            }
        },
        'recall': {
            'labels': {
                'type': list,
                'optional': False
            },
            'pos_label': {
                'type': str,
                'optional': True
            },
            'average': {
                'type':  str,
                'optional': True,
                'default': 'micro'
            },
            'zero_division': {
                'type': [float, str],
                'optional': True,
                'default': 'warn'
            }
        },
        'f1_score': {
            'labels': {
                'type': list,
                'optional': False
            },
            'pos_label': {
                'type': str,
                'optional': True
            },
            'average': {
                'type':  str,
                'optional': True,
                'default': 'micro'
            },
            'zero_division': {
                'type': [float, str],
                'optional': True,
                'default': 'warn'
            }
        },
        'confusion_matrix': {
            'labels': {
                'type': list,
                'optional': False
            },
            'normalize': {
                'type': str,
                'optional': True,
                'default': None
            }
        },
        'hamming_loss': {
        },
        "accuracy": {
            'normalize': {
                'type': bool,
                'optional': True,
                'default': True
            },
            'zero_division': {
                'type': [float, str],
                'optional': True,
                'default': 'warn'
            }
        },
    }

    class Sets():
        """
        Allowed types of metrics sets
        """
        BINARY = "BINARY"
        MULTICLASS = "MULTICLASS"
        MULTILABEL = "MULTILABEL"
        NAMES = [BINARY, MULTICLASS, MULTILABEL]

    MAPPER = {Sets.BINARY: BINARY,
              Sets.MULTICLASS: MULTICLASS,
              Sets.MULTILABEL: MULTILABEL}

    # region reporter methods
    def reporter(self,
                 dest_path: str,
                 process_args: dict,
                 **kwargs):
        """Method that generates the report elements of evaluated metrics

        Parameters
        ----------
        dest_path : str
            Path of the folder to store the resulting files
        process_args : dict
            The args of the process that wraps this class
        **kwargs :
            Extra named arguments that may contains:
                * lang : language of the report, by default languages.EN

        Returns
        -------
        dict
            Params to be reported in the Pipeline
        """
        lang = kwargs.get("lang", languages.EN)
        try:
            results_path = os.path.join(dest_path, "results.json")
            with open(results_path, mode="w") as _f:
                json.dump(self.results, _f, indent=4)

            data_to_report = {
                "results": {"asset": results_path,
                            "type": AssetTypes.DATA},
                "result_plots": {
                    "asset": self.result_plots(dest_path, lang=lang, report=True),
                    "type": AssetTypes.PLOT
                },
                "result_plots_trunc": {
                    "asset": self.result_plots(
                        dest_path=dest_path,
                        truncate_categories=report_params.MAX_REGISTERS_RESUME, lang=lang,
                        report=True),
                    "type": AssetTypes.PLOT
                },
                "results_tables": {
                    "asset": self.get_results_tables(),
                    "type": AssetTypes.DATA
                },
                "eval_config": {
                    "asset": self.eval_config,
                    "type": AssetTypes.DATA
                }
            }
            return super(Metrics, self).reporter(dest_path,
                                                 process_args,
                                                 data_to_report)

        except Exception as ex:
            logger.error(
                f'The report process for the Evaluator cannot be performed')
            logger.error(ex)
            return None

    def get_results_tables(self):
        """Arrange the evaluation results in a format that makes it easy to create the tables that
        will be displayed in a report

        Returns
        -------
        dict
            Dictionary of the form
            {`metric_set`: 'one_class': {'metric': [`metrics`], 'value': [`metric_values`]}, 
                           'per_class': {'category': [`categories`], 
                                         `metric_name`: [`metric_values`]}}
        """
        # metric_set = 'BINARY', 'MULTICLASS'
        for metric_set, metrics_info in self.results.items():
            data_one_class = defaultdict(lambda: defaultdict(list))
            data_per_class = defaultdict(lambda: defaultdict(list))
            # metric_type = one_class, per_class
            for metric_type, metric_values in metrics_info.items():
                if metric_type == 'one_class':
                    for metric_name, metric_value in metric_values.items():
                        if metric_name not in ("is_class_correctly_detected_in_images",
                                               "confusion_matrix", "labels"):
                            data_one_class[metric_set]["metric"].append(
                                metric_name)
                            data_one_class[metric_set]["value"].append(
                                metric_value)
                else:
                    for category, metrics_cat in metric_values.items():
                        data_per_class[metric_set]["category"].append(
                            category.capitalize())
                        for metric_name, metric_value in metrics_cat.items():
                            if metric_name not in ("precisions", "recalls"):
                                data_per_class[metric_set][metric_name].append(
                                    metric_value)
            # Order per-class data
            metrics_names = list(
                set([x for x in data_per_class[metric_set].keys()]) - set(["category"]))
            df_per_class = pd.DataFrame(data=data_per_class[metric_set])
            df_per_class.sort_values(
                by=metrics_names, axis=0, ascending=False, inplace=True)
            data_per_class[metric_set] = df_per_class.to_dict(orient="list")

        return {
            metric_set: {
                "one_class": dict(data_one_class[metric_set]),
                "per_class": dict(data_per_class[metric_set])
            } for metric_set in self.results.keys()
        }

    def result_plots(self,
                     dest_path: str = "",
                     truncate_categories: int = None,
                     lang=languages.EN,
                     report=False,
                     **kwargs):
        """Plot the results of the evaluation in different graphs depending on its type

        Parameters
        ----------
        dest_path : str, optional
            Base path where the plots will be saved, by default ""
        truncate_categories : int, optional
            Maximum number of records that will be shown in the per-class plots.
            The `truncate_categories` records with the highest value will be displayed.
            In case there are fewer records than `truncate_categories`, `None` will be returned.
            By default None
        lang : languages, optional
            Language of the report, by default languages.EN
        report : bool, optional
            Whether the plot will be saved as an image file or displayed in a window,
            by default False

        Returns
        -------
        dict
            Dictionary with the resulting assets in the form
            {`metric_set`: {'per_class': `metric_name`: `asset`, 
                            'one_class': `metric_name`: `asset`}
            }
        """
        result_assets = {}
        # metric_set: 'BINARY', 'MULTICLASS', 'MULTILABEL'
        for metric_set, metrics in self.results.items():
            data_one_class = defaultdict(list)
            data_per_class = defaultdict(lambda: defaultdict(list))
            data_confusion_matrix = None
            data_labels = None
            result_assets[metric_set] = {"per_class": {}, "one_class": {}}
            # metric_type: one_class, per_class
            for metric_type, metric_values in metrics.items():
                if metric_type == 'one_class':
                    # metric_name: precision, recall, f1_score, confusion_matrix
                    for metric_name, metric_value in metric_values.items():
                        if metric_name == 'confusion_matrix':
                            data_confusion_matrix = np.array(metric_value)
                        elif metric_name == 'labels':
                            data_labels = metric_value
                        else:
                            data_one_class["metric"].append(metric_name)
                            data_one_class["value"].append(metric_value)
                else:
                    for category, metrics_cat in metric_values.items():
                        # metric_name: precision, recall, f1_score
                        for metric_name, metric_value in metrics_cat.items():
                            data_per_class[metric_name]["category"].append(
                                category.capitalize())
                            data_per_class[metric_name]["value"].append(
                                metric_value)
            # Per-class results
            for metric_name, metrics_val in data_per_class.items():
                df = pd.DataFrame(data=metrics_val, columns=[
                                  "category", "value"])
                df.sort_values(by=["value"], axis=0,
                               ascending=False, inplace=True)
                if truncate_categories is not None:
                    if len(df) <= truncate_categories:
                        return None
                    df = df.head(truncate_categories)
                if report:
                    metric_id = f"{metric_set}-per_class-{metric_name}"
                    filename = os.path.join(dest_path, metric_id)
                    filename = f"{filename}-trunc" if truncate_categories is not None else filename
                else:
                    filename = None
                paths = self.plot_numeric_metric(metric_results=df,
                                                 x_axis="category",
                                                 y_axis="value",
                                                 x_label=transl["metric_plt"]["category"][lang],
                                                 y_label=metric_name,
                                                 filename=filename,
                                                 **kwargs)
                if report:
                    result_assets[metric_set]["per_class"][metric_name] = paths["plot"]
            # One-class results
            if report:
                metric_id = f"{metric_set}-one_class"
                filename = os.path.join(dest_path, metric_id)
                filename = f"{filename}-trunc" if truncate_categories is not None else filename
            else:
                filename = None
            df = pd.DataFrame(data=data_one_class, columns=["metric", "value"])
            paths = self.plot_numeric_metric(metric_results=df,
                                             x_axis="metric",
                                             y_axis="value",
                                             x_label=transl["metric_plt"]["metric"][lang],
                                             y_label=transl["metric_plt"]["value"][lang],
                                             filename=filename,
                                             **kwargs)
            if report:
                result_assets[metric_set]["one_class"]["general_metrics"] = paths["plot"]
            if data_confusion_matrix is not None:
                if report:
                    metric_id = f"{metric_set}-one_class-confusion_matrix"
                    filename = os.path.join(dest_path, metric_id)
                    filename = f"{filename}-trunc" if truncate_categories is not None else filename
                else:
                    filename = None
                paths = self.plot_confusion_matrix(data_confusion_matrix,
                                                   data_labels, filename=filename)
                if report:
                    result_assets[metric_set]["one_class"]["confusion_matrix"] = paths["plot"]
        if report:
            return result_assets

    def plot_numeric_metric(self,
                            metric_results,
                            x_axis, y_axis,
                            x_label, y_label,
                            filename=None,
                            **kwargs):
        """Plot metric results as bar graphs, either saving it to an image file or displaying it
        in a window

        Parameters
        ----------
        metric_results : pd.DataFrame
            Data containing the results of the metrics with columns `x_axis` and `y_axis`
        x_axis : str
            Column name for x-axis
        y_axis : str
            Column name for y-axis
        x_label : str
            Label for x-axis
        y_label : str
            Label for y-axis
        filename : str, optional
            Full path without file extension where the bar plot is saved as an image.
            If None, the bar plot will be displayed in a window instead. By default None

        Returns
        -------
        dict
            Dictionary of the form (plot, asset)
        """
        title = kwargs.get('title', None)
        try:
            if filename is not None:
                plot_filename = f"{filename}.png"
                asset_filename = f"{filename}.csv"
                metric_results.to_csv(asset_filename, index=False)

            if len(metric_results) > report_params.MAX_REGISTERS_RESUME:
                x_axis, y_axis = y_axis, x_axis
                x_label, y_label = y_label, x_label
                figsize = (17, 30)
                rotation = 0
            else:
                figsize = (10, 10)
                rotation = 30

            sns.set(style="whitegrid")
            f, ax = plt.subplots(1, 1, figsize=figsize)
            g = sns.barplot(x=x_axis, y=y_axis, data=metric_results, ax=ax)
            if rotation > 0:
                g.set_xticklabels(g.get_xticklabels(), rotation=rotation)
                ax.set_ylim(0, 1)
            else:
                ax.set_xlim(0, 1)
            plt.xlabel(x_label)
            plt.ylabel(y_label)
            if title is not None:
                plt.title(title)
            if filename is not None:
                plt.savefig(plot_filename, dpi=100)

                metric_asset = {"plot": plot_filename,
                                "asset": asset_filename}
                return metric_asset
            else:
                plt.show()
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)

    def plot_confusion_matrix(self,
                              data,
                              labels,
                              filename=None):
        """Plot the confusion matrix of `data`, either saving it to an image file or displaying it
        in a window

        Parameters
        ----------
        data : ndarray
            Array containing the data to display
        labels : list
            List of category names
        filename : str, optional
            Full path without file extension where the confusion matrix is saved as an image.
            If None, the confusion matrix will be displayed in a window instead. By default None

        Returns
        -------
        dict
            Dictionary of the form (plot, asset)
        """
        if filename is not None:
            plot_filename = f"{filename}.png"
        fig, ax = plt.subplots(1, 1, figsize=(40, 30))
        labels = labels or 'auto'
        vmax = 1 if data.max() <= 1 else data.max()
        ax = sns.heatmap(data, cmap='coolwarm',
                         yticklabels=labels, xticklabels=labels,
                         annot=False, vmin=0, vmax=vmax)

        if filename is not None:
            plt.savefig(plot_filename, dpi=100)

            metric_asset = {"plot": plot_filename,
                            "asset": None}
            return metric_asset
        else:
            plt.show()
    # endregion


class Evaluator(EV):

    @classmethod
    def eval(cls,
             dataset_true: Dataset.DatasetType,
             dataset_pred: PredictionDataset.DatasetType,
             eval_config: dict = {}) -> M.MetricsType:
        """Method that performs the evaluation of a model from the prediction dataset resulting
        from the inference on a set of elements, the original dataset, and an evaluation
        configuration

        Parameters
        ----------
        dataset_true : Dataset.DatasetType
            Dataset of true labels
        dataset_pred : PredictionDataset.DatasetType
            Dataset of predictions that coming from Model.predict method
        eval_config : dict, optional
            Options to define the evaluation params of the model, by default {}

        Returns
        -------
        M.MetricsType
            Result with evaluation metrics

        Raises
        ------
        ValueError
            In case both the prediction and true items not be the same
        Exception
            In case a metric set is not implemented
        ex
            In case some error occured when the evaluation is performed
        """

        if "partition" in eval_config:
            partition = eval_config["partition"]
        else:
            partition = None

        df_true = dataset_true.get_rows(partition=partition, sort_by="item")
        df_pred = dataset_pred.as_dataframe()

        y_true = df_true[["item", "label"]]

        if not set(df_pred["item"]) == set(df_true["item"]):
            raise ValueError(f'The dataset_pred and the partition selected of dataset_true '
                             f"must have the same elements")

        results = {}

        for metric, metric_opts in eval_config["metrics_set"].items():
            try:
                def_y_pred = dataset_pred.as_dataframe()[
                    ["item", "label", "score"]]
                y_pred_func = metric_opts.get(
                    "pre_eval_func", lambda x, *args: def_y_pred)
                y_pred = y_pred_func(dataset_pred, metric_opts)

                metric_opts["labels"] = (eval_config.get("labels", None) or
                                         list(set(df_true["label"]).union(set(df_pred["label"]))))

                if metric in Metrics.Sets.NAMES:
                    if metric != Metrics.Sets.MULTILABEL:
                        y_true = y_true[["item", "label"]]
                        y_true = y_true.rename(columns={"label": "true_label"})
                        y_pred = y_pred[["item", "label"]]
                        y_pred = y_pred.rename(columns={"label": "pred_label"})
                    else:
                        y_true = df_true[["item", "label"]]

                    y_true = y_true.sort_index()
                    y_pred = y_pred.sort_index()
                    results.update(Metrics.MAPPER.get(metric)
                                   (y_true, y_pred, metric_opts))
                else:
                    raise Exception("Method not implemented")
            except Exception as ex:
                logger.error(
                    f"An error occured when performing the evaluation of the metric {metric}")
                logger.error(ex)
                raise ex

        return Metrics(results, dataset_true, dataset_pred, eval_config)
