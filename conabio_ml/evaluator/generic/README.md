# Evaluation defaults

## Common cases
There is a default way to evaluate a model according to a specific problem.

The common use cases are the following:

### Binary

The metrics defined for a binary classification problems are:
- Precision
- Recall
- F1

By default only the positive label is evaluated, assuming that the positive class is labelled `1`. This evaluation is done setting the parameter `evaluation_type=‘binary’` 

The expected output is:
```javascript
eval_config{
	precision   : float
	recall      : float
	f1          : float
}
```

The same behavior is expected if you set the `metrics_set` parameter to `[precision, recall, f1]`

### Multiclass

### Multilabel

### Cluster evaluation

## Interface of evaluation

## Using pipeline


