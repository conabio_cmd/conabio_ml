import os
import json
import pandas
import inspect
from math import floor
import platform
import pydash
import sys
import tarfile
import tempfile
import urllib.request
import zipfile

from functools import reduce
from collections import defaultdict
from multiprocessing.managers import BaseManager, DictProxy
import multiprocessing

from conabio_ml.utils.logger import get_logger, debugger

logger = get_logger(__name__)
debug = debugger.debug
debug_parallel = debugger.get_debug_parallel_env()
global_temp_dir = None


def get_default_args(func):
    """Get default arguments of a function

    Parameters
    ----------
    func : function
        Function to get its default arguments

    Returns
    -------
    dict
        Dictionary of the form {`name_param`: `value`}
    """
    signature = inspect.signature(func)
    return {
        k: v.default for k, v in signature.parameters.items()
        if v.default is not inspect.Parameter.empty
    }


def get_and_validate_args(config,
                          args_def,
                          ignore_invalid=False):
    """Get default arguments for a configuration.

    Parameters
    ----------
    config : dict
        A dictionary that contains args configuration. 
        For a complete reference of the parameters, please follow:
        https://ecoinformatica.atlassian.net/wiki/spaces/CONML/pages/370376715
    args_def : dict
        Dictionary that contains the arguments definition with the argument 
        type, default value and optional flag for each config entry.
    ignore_invalid : bool
        Whether to ignore invalid entries or not.
        If False, it will throw an exception in case of finding parameters that are not contained
        in `args_def` (default is False)

    Returns
    -------
    dict
        Dictionary that contains arguments for a configuration.
    """

    invalid_entries = []
    for key in config.keys():
        if key not in args_def.keys():
            if not ignore_invalid:
                invalid_entries.append(key)
    if invalid_entries:
        raise ValueError(f"Invalid entries in config: {invalid_entries}")

    args = {}
    missing_entries = []
    for entry_name, entry_def in args_def.items():
        if entry_name not in config:
            if args_def[entry_name].get('optional', True) == False:
                missing_entries.append(entry_name)
            elif 'default' in entry_def:
                args[entry_name] = entry_def['default']
            continue
        if type(entry_def['type']) in (list, tuple):
            def_types = entry_def['type']
        else:
            def_types = [entry_def['type']]

        instance_types = [isinstance(config[entry_name], t) for t in def_types if t is not None]
        is_instance = reduce(lambda x, y: x or y, instance_types)

        assert_cond = (
            (config[entry_name] is None and None in def_types)
            or type(config[entry_name]) in def_types or is_instance
        )
        assert assert_cond, f"Invalid value type for {entry_name} in config"
        args[entry_name] = config[entry_name]
    if missing_entries:
        raise ValueError(f"Missing entries in config: {missing_entries}")

    return args


def is_array_like(obj):
    """Determine if an object is of type array

    Parameters
    ----------
    obj : Object
        Object instance

    Returns
    -------
    Bool
        Whether the passed object is like an array or not
    """
    return hasattr(obj, '__iter__') and hasattr(obj, '__len__') and not type(obj) == str


def store_json_file(data, dest_path):
    """Stores the content of `data` in a JSON file in the path `dest_path`

    Parameters
    ----------
    data : dict
        Dictionary containing the information that will be stored in the JSON file
    dest_path : str
        Path of the JSON file.
        In case it is a directory, the file will be stored with the name 'results.json'
    """
    logger.debug(f"Storing data in file {dest_path}...")
    if os.path.isdir(dest_path):
        dest_path = os.path.join(dest_path, "results.json")
    with open(dest_path, 'w') as f:
        json.dump(data, f)


def load_json_file(source_path: str) -> dict:
    """Loads the JSON file stored in `source_path`.

    Parameters
    ----------
    source_path : str
        Path of the JSON file.
        In case it is a directory, the file with the name 'results.json' will be loaded

    Returns
    -------
    dict
        Dictionary with the content of the JSON file
    """
    logger.debug(f"Loading data from file {source_path}...")
    if os.path.isdir(source_path):
        source_path = os.path.join(source_path, "results.json")
    with open(source_path) as f:
        return json.load(f)


def str2bool(v):
    """Converts a string representation to its corresponding Boolean value. It can be used by the
    ``parser.add_argument`` function to include arguments that can receive a Boolean value.
    Accepted values for `v` are the following case insensitive strings:
        - True: yes, true, t, y, 1
        - False: no, false, f, n, 0
    E.g. ``parser.add_argument("--bool_arg", type=str2bool, nargs='?', const=True, default=True)``
    And the usage in the command line could be as follows: ``python my_script.py --bool_arg False``

    Parameters
    ----------
    v : str
        String value that will be converted to its Boolean representation

    Returns
    -------
    bool
        Boolean representation of `v`

    Raises
    ------
    Exception
        In case `v` is not a valid string representation of a Boolean value
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise Exception('Boolean value expected.')


def create_shared_defaultdict(data_type=list):
    """Creates an instance of `defaultdict` of type `data_type` that can be used between processes
    running in parallel. 
    E.g., by passing it as a parameter to `starmap` method of `multiprocessing.Pool`

    Parameters
    ----------
    data_type : `type`, optional
        Type of the constructor of `defaultdict`, by default list

    Returns
    -------
    dict
        Instance of a `defaultdict` that can be used between processes using multiprocessing
    """
    class MyManager(BaseManager):
        pass
    MyManager.register('defaultdict', defaultdict, DictProxy)
    mgr = MyManager()
    mgr.start()
    return mgr.defaultdict(data_type)


def get_chunk(elements, num_chunks, chunk_num, verbose=True, sort_elements=True):
    """Divide a set of `elements` into chunks so that they can be processed in different tasks.
    If `num_chunks` is None or `chunk_num` is None, the original elements will be returned.

    Parameters
    ----------
    elements : list
        List of elements to divide in tasks
    num_chunks : int or None
        Number of tasks between which you want to divide the elements
    chunk_num : int or None
        Current task number. Must be in range [1, `num_chunks`]
    verbose : bool, optional
        Whether to be verbose or not, by default True
    sort_elements : bool, optional
        Whether to sort the elements before taking the chunk or not, by default True

    Returns
    -------
    list
        Resulting list of elements to be processed

    """
    if num_chunks is None or chunk_num is None:
        return elements

    _elements = sorted(elements) if sort_elements else elements
    if not 0 < chunk_num <= num_chunks:
        raise ValueError(f"chunk_num must be in [1, num_chunks]")
    chunk_size = floor(len(_elements) / num_chunks)
    first = chunk_size*(chunk_num-1)
    last = len(_elements) if chunk_num == num_chunks else chunk_size*chunk_num
    if verbose:
        logger.debug(f"Processing elements {first+1} to {last} of {len(_elements)}")
    return _elements[first:last]


class Chained:
    """
    This helper class is intended to allow context while copying 
    dataframe views directly to dataframe. 

    Such as in the following example. In
        dataframe["field"] = "value"
        dataframe[row_indexer, column_indexer] = value

    It throws the warning
        SettingWithCopyWarning: A value is trying to be set on a copy of a slice from a DataFrame

    Since it's a permitted behavior, we wrap chained assignments into a `with` block.
    """

    def __init__(self, chain_mode=None) -> None:
        acceptable_modes = [None, "warn", "raise"]
        assert chain_mode in acceptable_modes,\
            (f"Verify available modes of chain assignments in "
             f"https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#indexing-evaluation-order")
        self.chain_mode = chain_mode

    def __enter__(self):
        self.prev_mode = pandas.options.mode.chained_assignment
        pandas.options.mode.chained_assignment = self.chain_mode
        return self

    def __exit__(self, *args):
        pandas.options.mode.chained_assignment = self.prev_mode


def parallel_exec(func, elements, **kwargs):
    """Function to perform the execution of `func` in parallel from the elements in `elements`,
    sending the arguments contained in `kwargs`.
    If any of those arguments is a callable, it will be called by sending it the `elem` element
    obtained from the iteration in `elements`.
    In case the environment variable `DEBUG_PARALLEL == True` the execution will be performed
    iteratively, allowing the debugging of the `func` function.

    Parameters
    ----------
    func : Callable
        Function to be executed in parallel (or iteratively)
    elements : Iterable
        Set of elements to iterate over to execute the `func` function
    **kwargs :
        Set of named arguments that will be sent to the `func` function, and that in case of
        depending on the elements obtained from the iteration of `elements` must be passed in the
        form of a callable, whose argument will be the element of the iteration
        (e.g. `lambda elem: elem` to send the element itself)
    """
    tuples = []
    warn_shown = False
    if 'debug_parallel' in kwargs:
        _debug = kwargs['debug_parallel']
        del kwargs['debug_parallel']
    else:
        _debug = debug_parallel
    for elem in elements:
        args = tuple([fld(elem) if callable(fld) else fld for fld in kwargs.values()])
        if _debug:
            if not warn_shown:
                logger.warning("You are currently debugging in parallel mode")
                warn_shown = True
            func(*args)
        else:
            tuples.append(args)
    if not _debug:
        with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
            pool.starmap(func, tuples)


def get_temp_folder(preferred_name='conabio_ml'):
    global global_temp_dir

    if global_temp_dir is None:
        tmp_dir = '/tmp' if platform.system() == 'Darwin' else tempfile.gettempdir()
        global_temp_dir = os.path.join(tmp_dir, preferred_name)
        os.makedirs(global_temp_dir, exist_ok=True)

    return global_temp_dir


def download_file(url, dest_filename=None, force_download=False, verbose=False):
    """Download an image from an URL and store it with the specified file name
    
    Parameters
    ----------
    url : str
        URL from where the image is downloaded
    dest_filename : str, optional
        File name or directory where the file will be stored, by default None
    force_download : bool, optional
        Whether or not to force the download, by default False
    verbose : bool, optional
        Whether or not to be verbose, by default False

    Returns
    -------
    str
        Downloaded file path
    """
    def _progress(count, block_size, total_size):
        perc = int(min(float(count * block_size) / total_size * 100.0, 100))
        sys.stdout.write('\r>> Downloading %s %.1f%%' % (dest_filename, perc))
        sys.stdout.flush()
    if dest_filename is None:
        dest_filename = get_temp_folder()
    if not os.path.exists(dest_filename) or force_download or os.path.isdir(dest_filename):
        if os.path.isdir(dest_filename):
            dest_filename = os.path.join(dest_filename, url.split('?', 1)[0].split('/')[-1])
        dir_name = os.path.dirname(dest_filename)
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name, exist_ok=True)
        try:
            urllib.request.urlretrieve(url, dest_filename, _progress)
        except Exception as e:
            logger.exception(f"Exception in retrieving file from {url}")
    elif verbose:
        logger.debug(f'Bypassing download of already-downloaded file {dest_filename}')

    return dest_filename


def unzip_file(input_file: str, output_folder: str = None) -> None:
    """Unzip a zipfile to the specified output folder, defaulting to the same location as
    the input file

    Parameters
    ----------
    input_file : str
        Input zip file
    output_folder : str, optional
        Folder where to extract all the files. If None, it will be the same location as
        `input_file`.
        By default None
    """
    if output_folder is None:
        output_folder = os.path.dirname(input_file)

    with zipfile.ZipFile(input_file, 'r') as zf:
        zf.extractall(output_folder)


def untar_file(input_file: str, output_folder: str = None) -> bool:
    """Extract a tar file to the specified output folder, defaulting to the same location as
    the input file

    Parameters
    ----------
    input_file : str
        Input tar file
    output_folder : str, optional
        Folder where to extract all the files. If None, it will be the same location as
        `input_file`.
        By default None
    """
    def unwrap_path(path):
        temp = os.path.split(path)
        temp = pydash.chain(temp).filter(lambda x: len(x) > 0).value()

        if len(temp) == 1:
            return temp
        elif temp[0] == ".":
            return [t for t in temp]
        else:
            return unwrap_path(temp[0]) + [temp[1]]
    try:
        logger.debug(f"Trying to uncompress {input_file}")

        tar = tarfile.open(input_file)

        for member in tar.getmembers():
            try:
                temp_name = unwrap_path(member.name)[1:]
                temp_name = os.path.join(".", *temp_name)

                member.name = temp_name
                tar.extract(member, output_folder)
            except:
                pass
        tar.close()

        logger.debug("TAR file decompressed... Removing TAR file ")
        os.remove(input_file)

        return True
    except Exception as ex:
        logger.exception(f"There is an error decompressing TAR file")
        raise ex
