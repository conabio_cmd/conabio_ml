import os

files_base_path = os.environ.get('FILES_REPORT_BASE_PATH', '')
files_base_url = os.environ.get('FILES_REPORT_BASE_URL', '')


class languages():
    """Allowed languages
    """
    EN = "en"
    ES = "es"
    NAMES = [EN, ES]


report_transl = {
    "default_title": {
        languages.EN: "Report",
        languages.ES: "Reporte"
    },
    "dataset_distr_tbl_hdr": {
        "label": {
            languages.EN: "Label",
            languages.ES: "Etiqueta"
        },
        "counts": {
            languages.EN: "Samples",
            languages.ES: "Muestras"
        }
    },
    "metric_plt": {
        "metric": {
            languages.EN: "Metric",
            languages.ES: "Métrica"
        },
        "value": {
            languages.EN: "Value",
            languages.ES: "Valor"
        },
        "category": {
            languages.EN: "Category",
            languages.ES: "Categoría"
        },
        "metric_name": {
            "precision": {
                languages.EN: "Precision",
                languages.ES: "Precisión"
            },
            "recall": {
                languages.EN: "Recall",
                languages.ES: "Recuperación"
            },
            "f1_score": {
                languages.EN: "F1-score",
                languages.ES: "Score F1"
            },
            "accuracy": {
                languages.EN: "Accuracy",
                languages.ES: "Accuracy"
            },
            "average_precision": {
                languages.EN: "Average precision",
                languages.ES: "Precision promedio"
            },
            "mAP": {
                languages.EN: "Mean average precision",
                languages.ES: "Media de la precision promedio"
            },
            "hamming_loss": {
                languages.EN: "Hamming loss",
                languages.ES: "Hamming loss"
            }
        }
    }
}

MAX_REGISTERS_RESUME = 10
EXAMPLES_LARGEST_SIDE = 512
EXAMPLES_SMALLEST_SIDE = 256
