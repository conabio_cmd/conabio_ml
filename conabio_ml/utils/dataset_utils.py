#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import pandas as pd

from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)

LABELMAP_FILENAME = 'labels.txt'


def get_all_files(path,
                  seek_name=None, seek_extension=None, recursive=True, order_by_filename=False):
    """
    Retrieve all files in a folder on format
        path, relative path (from path), file name

    Parameters
    ----------
    path : str
        Path to find all files
    seek_name : str, optional
        Name of file needed (it could be a regex) (default is None)
    seek_extension : str, optional
        Extension of file needed (it could be a regex) (default is None)
    recursive : bool
        Whether or not to seek files also in subdirectories (default is True)
    order_by_filename : bool
        Whether or not to order results by filename.

    Returns
    -------
    List
        List of found files
    """
    paths = []
    
    assert_cond = seek_name is not None or seek_extension is not None
    assert assert_cond, "You must provide at least one of seek_name and seek_extension"

    if seek_extension is not None:
        seek_extension = seek_extension if isinstance(seek_extension, list) else [seek_extension]
        seek_extension = [*map(lambda x: re.sub("[.]*", "", x, count=1), seek_extension)]
    else:
        seek_extension = ['\w*']

    if seek_name is not None:
        seek_name = seek_name if isinstance(seek_name, list) else [seek_name]
    else:
        seek_name = ['.*']

    for _file in [name for name in os.listdir(path)]:
        if _file.startswith("."):
            continue

        fname = os.path.basename(_file)
        matches = [
            f'{name}.{ext}'
            for name in seek_name for ext in seek_extension if re.match(f"^{name}[.]{ext}$", fname)
        ]

        if recursive and os.path.isdir(os.path.join(path, _file)):
            temps = get_all_files(os.path.join(path, _file),
                                  seek_name=seek_name,
                                  seek_extension=seek_extension,
                                  recursive=True)
            paths += temps
        elif len(matches) > 0:
            if os.path.isfile(os.path.join(path, _file)):
                paths += [{"path": path,
                           "title": _file}]

    if order_by_filename:
        paths = sorted(paths, key=lambda i: i['title'])

    return paths


def read_labelmap_file(labelmap_path):
    """Reads the labels file and returns a mapping from ID to class name.

    Parameters
    ----------
    labelmap_path : str
        The filename where the class names are read.

    Returns
    -------
    dict
        A map from a label (integer) to class name.
    """
    if os.path.isdir(labelmap_path):
        labelmap_path = os.path.join(labelmap_path, LABELMAP_FILENAME)
        if not os.path.isfile(labelmap_path):
            return None
    with open(labelmap_path, 'r') as f:
        lines = f.read()
    lines = lines.split('\n')
    lines = filter(None, lines)

    labels_to_class_names = {}
    for line in lines:
        index = line.index(':')
        labels_to_class_names[int(line[:index])] = line[index+1:]
    return labels_to_class_names


def write_labelmap_file(labelmap, dest_path):
    """Writes a file with the map of labels to class names.

    Parameters
    ----------
    labelmap: dict
        A map of (integer) labels to class names.
    dest_path: str
        The path of the file (or directory) in which the labelmap file should be written.

    Returns
    -------
    str
        Path of the file where the labelmap file was written
    """
    if not dest_path.lower().endswith('.txt'):
        dest_path = os.path.join(dest_path, LABELMAP_FILENAME)
    os.makedirs(os.path.dirname(dest_path), exist_ok=True)
    with open(dest_path, 'w') as f:
        for label in labelmap:
            class_name = labelmap[label]
            f.write('%d:%s\n' % (label, class_name))
    logger.debug(f"File {dest_path} with labelmap was created")
    return dest_path


def get_mapping_classes(mapping_classes,
                        clean_cat_names=True,
                        from_col=None,
                        to_col=None,
                        filter_expr=None):
    """Function that gets a mapping of categories, either to group them into super-categories or to
    match them to those in other datasets.
    The mapping can be done either with a dictionary or with a CSV file.
    In both cases you can use the wildcard `*` (as the `key` of the dict or the column `0` of the
    CSV) to indicate 'all other current categories in the data set'.
    E.g., `{'Homo sapiens': 'Person', '*': 'Animal'}` will designate the Homo sapiens category as
    'Person' and the rest of the categories as 'Animal'.
    By default, the resulting mappings will be in the form `{orig_cat_id: dest_cat_name}`

    Parameters
    ----------
    mapping_classes : dict or str or None
        Dictionary or path to a CSV file containing the mappings.
        In the case of a dictionary, the `key` of each element is the current name of the category,
        and `value` is the name to be given to that category.
        In the case of a CSV file, the file must contain two columns and have no header.
        The column `0` is the current name of the category, and the column `1` is the name to be
        given to that category.
        If None, None is returned
    clean_cat_names : bool, optional
        Whether to clean or not the category names, converting to lower case and removing spaces at
        the beginning and at the end. By default True
    from_col : str or int, optional
        Name or position (0-based) of the column to be used as 'from' in the mapping, in case of
        `mapping_classes` is a CSV. By default None
    to_col : str or int, optional
        Name or position (0-based) of the column to be used as 'to' in the mapping, in case of
        `mapping_classes` is a CSV. By default None
    filter_expr : Callable, optional
        A Callable that will be used to filter the CSV records in which the mapping is found,
        in case of `mapping_classes` is a CSV. By default None

    Returns
    -------
    dict
        Dictionary containing the category mappings

    Raises
    ------
    ValueError
        In case `mapping_classes` is neither dictionary nor file path
    """
    if mapping_classes is None:
        return None
    if type(mapping_classes) == dict:
        return get_mapping_classes_from_dict(
            mapping_classes_dict=mapping_classes, clean_cat_names=clean_cat_names)
    elif os.path.isfile(mapping_classes):
        return get_mapping_classes_from_csv(
            mapping_classes_csv=mapping_classes, clean_cat_names=clean_cat_names,
            from_col=from_col, to_col=to_col, filter_expr=filter_expr)
    else:
        raise ValueError(f"Invalid value for mapping_classes")


def get_mapping_classes_from_csv(mapping_classes_csv,
                                 clean_cat_names=True,
                                 from_col=None,
                                 to_col=None,
                                 filter_expr=None):
    """Function that gets a mapping of categories, either to group them into super-categories or to
    match them to those in other datasets, from the definitions contained in `mapping_classes_csv`.
    You can use the wildcard `*` in the column `0` to indicate 'all other current categories in the
    dataset'. E.g.,
       `Homo sapiens  |   Person`
        `*          |   Animal`
    will designate the 'Homo sapiens' category as 'Person' and the rest of the categories as
    'Animal'.
    By default, the resulting mappings will be in the form {`orig_cat_id`: `dest_cat_name`}

    Parameters
    ----------
    mapping_classes_csv : str
        Path to a CSV file containing the mappings. The file must contain two columns and have no
        header. The column `0` is the current name of the category and the column `1` is the name
        to be given to that category.
    clean_cat_names : bool, optional
        Whether to clean or not the category names, converting to lower case and removing spaces at
        the beginning and at the end. By default True
    from_col : str or int, optional
        Name or position (0-based) of the column to be used as 'from' in the mapping,
        by default None
    to_col : str or int, optional
        Name or position (0-based) of the column to be used as 'to' in the mapping,
        by default None
    filter_expr : Callable, optional
        A Callable that will be used to filter the CSV records in which the mapping is found.
        By default None

    Returns
    -------
    dict
        Dictionary containing the category mappings

    """
    if not os.path.isfile(mapping_classes_csv):
        return None
    mapping_classes_dict = {}
    if from_col is not None and to_col is not None:
        df = pd.read_csv(mapping_classes_csv, header=0, na_values=['nan'], keep_default_na=False)
        if callable(filter_expr):
            df = df[df.apply(filter_expr, axis=1)].reset_index(drop=True)
        df = df.rename(columns={from_col: 'from', to_col: 'to'})[['from', 'to']]
    else:
        df = pd.read_csv(mapping_classes_csv, header=None, names=["from", "to"],
                         na_values=['nan'], keep_default_na=False)
    for _, x in df.iterrows():
        if type(x["from"]) is str and clean_cat_names:
            key = get_cleaned_label(x["from"])
        else:
            key = x["from"]
        value = x["to"]
        if clean_cat_names:
            mapping_classes_dict[key] = get_cleaned_label(value)
        else:
            mapping_classes_dict[key] = value
    return mapping_classes_dict


def get_mapping_classes_from_dict(mapping_classes_dict,
                                  clean_cat_names=True):
    """Function that gets a mapping of categories, either to group them into super-categories or to
    match them to those in other datasets, from the definitions contained in
    `mapping_classes_dict`.
    You can use the wildcard `*` as the `key` to indicate 'all other current categories in the
    dataset'. E.g.,
    E.g., `{'Homo sapiens': 'Person', '*': 'Animal'}` will designate the Homo sapiens category as
    'Person' and the rest of the categories as 'Animal'.
    By default, the resulting mappings will be in the form `{orig_cat_id: dest_cat_name}`

    Parameters
    ----------
    mapping_classes_dict : dict
        Dictionary containing the mappings. The `key` of each element is the current name of the
        category, and `value` is the name to be given to that category.
    clean_cat_names : bool, optional
        Whether to clean or not the category names, converting to lower case and removing spaces at
        the beginning and at the end. By default True

    Returns
    -------
    dict
        Dictionary containing the category mappings

    """
    mapping_classes_dict_new = {}
    for key, value in mapping_classes_dict.items():
        if clean_cat_names:
            if type(key) is str:
                key = get_cleaned_label(key)
            mapping_classes_dict_new[key] = get_cleaned_label(value)
        else:
            mapping_classes_dict_new[key] = value
    return mapping_classes_dict_new


def get_cleaned_label(value):
    return " ".join(value.lower().strip().split())
