from enum import Enum


class AssetTypes(Enum):
    """Allowed types of assets
    """
    FILE = 1
    PLOT = 2
    DATA = 3
