# Preprocesamiento

El módulo de preprocesamiento presenta la interfaz para definir procesos de transformación o procesamiento aplicado a un conjunto de datos (Dataset) de la API CONABIO_ML.

La interfaz general solo presenta el tipo de control que se enlazará a un Pipeline de la API CONABIO_ML.

Dado que los procesamiento son muy diferntes de acuerdo a la tarea a realizar las siguientes consideraciones deben seguirse para poder realizar esta tarea en un experimento.

1. La clase que realizará el trabajo de preprocesamiento debe ser subclase de `conabio_ml.preprocessing.PreProcessing`

2. Debido a que se espera un transformaciòn al Dataset en curso, se sugiere una implementación estática que siempre tenga presenta el conjunto de datos a procesar, como sigue:

```
@staticmethod
def NAME_OF_FUNCTION(dataset:Dataset.DatasetType,
                    opts:dict={ PROCESSING OPTIONS }):
    [ Implementation ]

    return DATASET_TRANSFORMED_OR_PROCESSED
```

El módulo model del paquete trainer define la manera en la que un modelo puede ser creado utilizando el API CONABIO ML.