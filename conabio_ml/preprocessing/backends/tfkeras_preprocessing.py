#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
We define the some transformations can be applied to a dataset to handle as
tf.keras train objects. Such as:
    - ImageGenerators
    - TF.Dataset

Since one of the main objects for the CONABIO_ML API is to hold a picture of the
resources used, we hold an internal representatation(s) of the dataset destination type
in the Preprocessor param

- representations

So that, when using a tf.keras backend the trainer/models will ask for this representatation
for every dataset partition

This script includes the Preprocessor subclasses to handle:
    - Images (With ImagePreprocessor)
    - Text (With TextPreprocessor)
    - Data (With Preprocessor)
"""
import sys
import os
import pydash
import pandas as pd

import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator

from typing import Callable

from conabio_ml.datasets import Dataset, Partitions
from conabio_ml.preprocessing import PreProcessing as PreProc
from conabio_ml.utils.utils import get_and_validate_args

from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)

# Default generators
IMAGE_GENERATOR = "image_generator"
TENSOR_SLICES = "tensor_slices"
LABEL_MAPPING = "label_mapping"

# Default flows
FROM_DATAFRAME = "from_dataframe"


class Preprocessor(PreProc):
    preprocessing_defs = {
        IMAGE_GENERATOR: {
            'featurewise_center': {
                'type': bool,
                'default': False
            },
            'samplewise_center': {
                'type': bool,
                'default': False
            },
            'featurewise_std_normalization': {
                'type': bool,
                'default': False
            },
            'samplewise_std_normalization': {
                'type': bool,
                'default': False
            },
            'zca_whitening': {
                'type': bool,
                'default': False
            },
            'zca_epsilon': {
                'type': float,
                'default': 1e-06
            },
            'rotation_range': {
                'type': int,
                'default': 0
            },
            'width_shift_range': {
                'type': float,
                'default': 0.
            },
            'height_shift_range': {
                'type': float,
                'default': 0.
            },
            'brightness_range': {
                'type': tuple,
                'default': None
            },
            'shear_range': {
                'type': float,
                'default': 0.
            },
            'zoom_range': {
                'type': float,
                'default': 0.
            },
            'channel_shift_range': {
                'type': float,
                'default': 0.
            },
            'fill_mode': {
                'type': str,
                'default': 'nearest'
            },
            'horizontal_flip': {
                'type': bool,
                'default': False
            },
            'vertical_flip': {
                'type': bool,
                'default': False
            },
            'rescale': {
                'type': float,
                'default': 0.
            },
            'preprocessing_function': {
                'type': Callable,
                'default': None
            },
            'cval': {
                'type': float,
                'default': 0.
            },
            'data_format': {
                'type': [Callable, None],
                'default': None
            },
            'validation_split': {
                'type': float,
                'default': 0.
            },
            'dtype': {
                'type': tf.DType,
                'default': None
            }
        },
        TENSOR_SLICES: {}
    }

    flow_defs = {
        FROM_DATAFRAME: {
            'directory': {
                'type': str,
                'default': None
            },
            'x_col': {
                'type': str,
                'default': "item"
            },
            'y_col': {
                'type': str,
                'default': "label"
            },
            'weight_col': {
                'type': str,
                'default': None
            },
            'target_size': {
                'type': tuple,
                'optional': False
            },
            'color_mode': {
                'type': str,
                'default': 'rgb'
            },
            'classes': {
                'type': list,
                'default': None
            },
            'class_mode': {
                'type': str,
                'default': 'categorical'
            },
            'batch_size': {
                'type': int,
                'default': 32
            },
            'shuffle': {
                'type': bool,
                'default': True
            },
            'seed': {
                'type': [None, float],
                'default': None
            },
            'save_to_dir': {
                'type': str,
                'default': None
            },
            'save_format': {
                'type': str,
                'default': None
            },
            'save_prefix': {
                'type': str,
                'default': ''
            },
            'subset': {
                'type': str,
                'default': None
            },
            'interpolation': {
                'type': str,
                'default': 'nearest'
            },
            'validate_filenames': {
                'type': bool,
                'default': True
            }
        }
    }

    PREPROCESSING_OPTS = {
        IMAGE_GENERATOR: lambda args: ImageDataGenerator(
            **get_and_validate_args(args,
                                    Preprocessor.preprocessing_defs[IMAGE_GENERATOR],
                                    ignore_invalid=True)
        )
    }

    FLOW_OPTS = {
        FROM_DATAFRAME: lambda datagen, dataframe, args: datagen.flow_from_dataframe(
            dataframe,
            **get_and_validate_args(args,
                                    Preprocessor.flow_defs[FROM_DATAFRAME],
                                    ignore_invalid=True)
        )
    }

    @staticmethod
    def __build_labels(label_series: pd.Series,
                       inverse_labelmap: dict,
                       dataset_handling: dict):
        """
        Checks if the label can be cast to the destination type.
        Additionally, if the labels are expressed as string and destination type
        is numeric a categorical hash is performed.

        Args:
            dataframe (pd.Series): [Series to check]
            inverse_labelmap ([dict]): [Inverse labels dictionary]
            dataset_handling ([dict]): [dataset handling options]


        Raises:
            error: [If the cast process cannot be performed]
            ex: [General exception if the process cannot be completed]

        Returns:
            [numpy.array]: [Array of labels converted]
        """
        dest_type = dataset_handling["label_type"]
        as_categorical = dataset_handling.get("as_categorical", False)

        y = label_series.values
        if as_categorical:
            y = label_series.apply(lambda x: inverse_labelmap[x])
            y = tf.keras.utils.to_categorical(y, dtype="int32")

        try:
            tf.dtypes.cast(y, dest_type)

            return y
        except tf.errors.UnimplementedError as error:
            logger.error(f"The destination type of the labels should "
                         f"be compatible to tf.dataset destination type")
            raise error
        except Exception as ex:
            raise ex

        return y

    @staticmethod
    def as_image_generator(
            dataset: Dataset.DatasetType,
            preproc_args: dict = {
                'use_partitions': True,
                'preproc_opts': {},
                'dataset_handling': {}
            }) -> Dataset.DatasetType:
        """
        Creates the intermediate representation ImageDataGenerator
        provided by tf.keras ans stores it the `representations` internal variable

        Args:
            dataset (Dataset.DatasetType): [Dataset to represent as ImageDataGenerator]

        Returns:
            Dataset.DatasetType: [Dataset with the param `representations` defined]
        """
        use_partitions = preproc_args.get('use_partitions', True)
        preproc_opts = preproc_args.get("preproc_opts", {})
        dsh = preproc_args.get("dataset_handling", {})
        temp = {}

        partitions = [None] if not use_partitions \
            else [Partitions.TRAIN, Partitions.VALIDATION, Partitions.TEST]

        for part in partitions:
            partition_df = dataset.get_rows(part)
            if len(partition_df) == 0:
                logger.debug(f"Partition {part} is not present in dataset. Skipped")
                continue
            _dsh = {**dsh, "classes": dataset.get_classes()}
            datagen = Preprocessor.PREPROCESSING_OPTS.get(IMAGE_GENERATOR)(preproc_opts)
            generator = Preprocessor.FLOW_OPTS.get(FROM_DATAFRAME)(datagen, partition_df, _dsh)

            if not part:
                part = Partitions.TRAIN

            temp[part] = generator

        dataset.representations = {IMAGE_GENERATOR: temp}
        # Register operations for reporting
        [dataset._register_augmentation_op(k, v) for k, v in preproc_opts.items()]
        [dataset._register_augmentation_op(k, v) for k, v in dsh.items()]
        return dataset

    @staticmethod
    def as_tensor_slices(dataset: Dataset.DatasetType,
                         preproc_args: dict = {
                             'use_partitions': True,
                             'dataset_handling': {}
                         }) -> Dataset.DatasetType:
        use_partitions = preproc_args.get('use_partitions', True)
        inverse_labelmap = dataset._get_inverse_labelmap()

        dsh = preproc_args.get("dataset_handling", {})

        item_field = dsh.get("item_field", "item")
        label_field = dsh.get("label_field", "label")
        temp = {}

        partitions = [None] if not use_partitions \
            else [Partitions.TRAIN, Partitions.VALIDATION, Partitions.TEST]

        for part in partitions:
            partition_df = dataset.get_rows(part)

            x = partition_df[item_field].values
            y = Preprocessor.__build_labels(partition_df[label_field],
                                            inverse_labelmap,
                                            dsh)
            data = (
                tf.data.Dataset.from_tensor_slices(
                    (
                        tf.cast(x, dsh["item_type"]),
                        tf.cast(y, dsh["label_type"])
                    )
                )
            )

            if not part:
                part = Partitions.TRAIN

            temp[part] = data

        dataset.representations = {TENSOR_SLICES: temp}
        return dataset
