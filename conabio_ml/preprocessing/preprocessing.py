#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import TypeVar, List
from conabio_ml.process import ProcessState

class PreProcessing(ProcessState):
    """Base class that defines the type to chain proprocessing 
    """

    PreprocessingType = TypeVar('PreprocessType', bound='PreProcessing')

    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        PreprocessingType
        """
        return cls.PreprocessingType
