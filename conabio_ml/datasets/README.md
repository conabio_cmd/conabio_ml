Creación
--------

Los conjuntos de datos se pueden encontrar en una amplia variedad de
formatos, y no siempre en uno que les permita formar parte del flujo de
un experimento de Aprendizaje Automático.

El API conabio\_ml proporciona varias maneras de crear una instancia de
Dataset a partir de algunos de los formatos de conjuntos de datos más
utilizados, lo que permite aplicar las mismas operaciones sobre los
datos sin importar el formato original. Esto es útil sobre todo porque
permite desacoplar la etapa de creación del dataset de las etapas
posteriores del experimento. La clase Dataset también es consistente
para datos de distintos tipos (imagen, texto, audio, video, etc.), por
lo que la gama de experimentos que es posible realizar con esta
herramienta es bastante amplia.

Para cada tipo de dato (por ahora imagen y texto) existen versiones
específicas de Dataset en la forma de subclases, que implementan algunas
funcionalidades para ese tipo de dato. Así, por ejemplo, tenemos la
clase ImageDataset en el módulo conabio\_ml.datasets.images, que permite
la creación de un dataset a partir de un conjunto de datos de imágenes.

### A partir de un archivo JSON

El API conabio\_ml permite utilizar el estándar de almacenamiento
colecciones de datos y anotaciones
[COCO](http://cocodataset.org/#format-data), en la creación de
instancias de Dataset a través del método estático **from\_json**.

De esta manera, si se cuenta con un archivo JSON con el estilo COCO de
una colección de imágenes (p.e., algunas de las colecciones del proyecto
[LILA BC](http://lila.science/datasets)), se puede usar el método
estático from\_json de COCOImageDataset (que es una subclase de
ImageDataset) para crear una instancia de Dataset con los datos de esa
colección.

Existen archivos JSON basados en el formato COCO para algunas de las
colecciones generadas en la CONABIO, y pueden ser descargados de manera
pública junto con los datos que las componen (p.e., imágenes de
colecciones de fototrampas) para que puedan ser usadas en experimentos
de Aprendizaje Automático. Para simplificar el proceso de creación
existen clases específicas para las colecciones de imágenes del SNMB,
IBUNAM y PRONATURA, y de texto la MWS, y que a través de su método
estático create realiza automáticamente la descarga del archivo JSON
necesario desde el repositorio y crea un instancia de Dataset con los
datos de esas colecciones.

### A partir de un archivo CSV

También es posible crear una instancia de Dataset a partir de uno o
varios archivos CSV a través del método estático **from\_csv**. Los
archivos CSV importados deberán contener al menos las columnas item,
label y bbox (en caso de datasets de imágenes con anotaciones a nivel de
objeto), pero todas las demás columnas también podrán ser importadas a
la información contenida en el dataset. La columna item deberá ser la
ruta dentro del sistema de archivos de cada elemento. El dataset
resultante podrá ser particionado a partir del valor de alguna de las
columnas del archivo CSV, especificada en el parámetro
split\_by\_column, cuyos valores aceptados serán: train, test y
validation. También podrá ser particionado a partir del nombre del
archivo, cuyos nombres válidos son: train.csv, test.csv y validation.csv
en asignando True al parámetro split\_by\_filenames.

### A partir de los elementos contenidos en un folder

Otra forma de crear una instancia de Dataset es a partir de los
elementos contenidos en un folder, a través del método estático
**from\_folder**. Los elementos serán buscados por las extensiones de
los archivos especificadas en el parámetro extensions, y la etiqueta de
cada elemento será determinada por el nombre de la carpeta en que se
encuentre, y si el parámetro split\_by\_folder se asigna a True, el
dataset será particionado en caso de que la estructura de directorios
sea la siguiente dentro de la ruta especificada por source\_path:

- source_path
	- train
		- folders por clase
	- test
		- folders por clase
	- validation
		- folders por clase

Convertir el dataset a otras representaciones
---------------------------------------------

La interfaz de Dataset posee métodos que permiten crear representaciones
estándar de los datos que contiene. Esto puede resultar útil si se
quiere almacenar el dataset para ser utilizado posteriormente y cargarlo
con alguno de los métodos expuestos en la sección anterior, o para se
utilizado con alguna biblioteca de Aprendizaje Automático, como Keras o
Tensorflow.

### Crear archivos CSV

El método de instancia **to\_csv** permite guardar la información del
dataset en uno o varios archivos CSV con las columnas especificadas en
el parámetro columns, usando por defecto item, label y bbox (para
datasets de imágenes con anotaciones a nivel de objeto). Los archivos
creados tendrán la estructura usada por el método from\_csv.

A continuación se muestra un ejemplo en el que se creará un archivo CSV
con las columnas item y label por cada partición del dataset:

    dataset.to_csv(dest_path=dest_path,
                   columns=["item", "label"],
                   header=True)

### Copiar los archivos a una estructura de directorios

Es posible copiar los elementos del dataset a una estructura de
directorios a través del método de instancia **to\_folder**, separando
los elementos en carpetas según la partición a la que pertenecen
(asignando True el parámetro split\_in\_partitions) y en carpetas según
la etiqueta que les corresponde (asignando a True el parámetro
split\_in\_labels). En caso de que el parámetro keep\_originals sea
False los elementos originales serán removidos. La estructura de
directorios creada es la misma usada por el método from\_folder.

A continuación se muestra un ejemplo en el que los elementos de un
dataset serán movidos a la carpeta dest\_path con una estructura de
directorios de la forma partición/etiqueta:

    dataset.to_folder(dest_path=dest_path,
                      split_in_partitions=True,
                      split_in_labels=True,
                      keep_originals=False)

### Crear TFRecords

Para el caso de datasets de imágenes existe el método to\_tfrecords para
crear una representación en TFRecords de los datos que componen el
dataset en la forma en que está particionado. Este formato es comúnmente
usado por la biblioteca de Aprendizaje Automático Tensorflow, que es la
utilizada por defecto en los módulos model, trainer y evaluator del API
conabio\_ml. A continuación se muestra un ejemplo de uso, en el que se
especifica que la partición de los records se haga en 5 fragmentos por
cada una de la particiones:

    dataset.to_tfrecords(dest_path=dest_path,
                         num_shards=5)

### Particionar el dataset

El API conabio\_ml permite dividir los datos que conforman un dataset en
particiones que se utilicen en distintas tareas del experimento de
Aprendizaje Automático, como son entrenamiento, validación y evaluación.
Lo anterior se realiza con el método **split** de la interfaz Dataset,
como se muestra a continuación:

    dataset.split(train_perc=0.7,
                  test_perc=0.2,
                  val_perc=0.1)

Lo anterior generará una representación interna que permita en cualquier
momento determinar a qué partición pertenece cada registro del dataset.
Esta división se realiza de acuerdo a las proporciones configuradas en
los parámetros del método split.

# Representación interna de la clase Dataset #

El siguiente diagrama UML muestra la jerarquía de clases para las implementaciones de los tipos ImageDataset y TextDataset.

![Diagrama UML de la clase Dataset](https://bitbucket.org/conabio_cmd/conabio_ml/raw/fbe88cc76da18f2f947cd9add578988c19baf820/docs/source/imgs/Dataset_UML.png)

La propiedad data de Dataset es la que contendrá la representación estándar como un objeto DataFrame de pandas. 

La propiedad info contendrá información específica del conjunto de datos utilizado. Esta información es libre y se podrá utilizar posteriormente para llevar el track de operaciones aplicadas al Dataset en el pipeline del experimento.

Cada implementación específica de Dataset contendrá propiedades y métodos útiles para convertir la información en la representación estándar, y dependerá de la naturaleza de cada conjunto de datos.

## Representación estándar ##

La propiedad data de Dataset contendrá la representación estándar de los datos, a través de un objeto de tipo DataFrame de pandas, y contará con la información mínima para poder determinar las etiquetas de cada ítem y a qué partición pertenece. 

De este modo se puede pasar de una representación de los datos en un formato específico, a una representación tabular estándar independiente del formato original y que puede fácilmente alimentar un pipeline de Aprendizaje Automático.

![Creación de dataset](https://bitbucket.org/conabio_cmd/conabio_ml/raw/fbe88cc76da18f2f947cd9add578988c19baf820/docs/source/imgs/DataFrame.png)

Por ejemplo, se puede pasar de una representación de un dataset a través de un archivo json con el formato propuesto por el API conabio_ml, y a través del método from_json() de la clase Dataset, crear una instancia de Dataset, que contendrá la información en el formato estándar, que se manipulará de la misma manera sin importar el formato original.

![Dataset a partir de un archivo JSON](https://bitbucket.org/conabio_cmd/conabio_ml/raw/fbe88cc76da18f2f947cd9add578988c19baf820/docs/source/imgs/Rep_estandar_from_JSON.png)