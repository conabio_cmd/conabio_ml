#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import annotations
import inspect
import json
import numpy as np
import os
import pandas as pd
import pydash
import threading
from typing import Union, Callable
import uuid
import multiprocessing

from abc import abstractmethod
from pathlib import Path

from sklearn import model_selection

from typing import TypeVar, List, Union, Callable

from conabio_ml.process import ProcessState
from conabio_ml.assets import AssetTypes
import conabio_ml.utils.dataset_utils as utils
from conabio_ml.utils.utils import is_array_like
from conabio_ml.utils.logger import get_logger, debugger

logger = get_logger(__name__)
debug = debugger.debug

lock = threading.Lock()


class Partitions():
    """Allowed types of dataset partitions
    """
    TRAIN = "train"
    TEST = "test"
    VALIDATION = "validation"
    NAMES = [TRAIN, TEST, VALIDATION]

    @classmethod
    def check_partition(cls, partition):
        assert partition in cls.NAMES, f"Partition must be one of: {', '.join(cls.NAMES)}"


class Dataset(ProcessState):
    """
    Represent a Dataset specification.

    Attributes
    ----------
    data : pd.DataFrame
        DataFrame object that holds dataset elements and contains at least
        the following columns:
            * item
            * label
    info: dict
        Dictionary that contains dataset information
    params: dict
        Dictionary that contains configuration and state parameters of the
        dataset
    """
    DatasetType = TypeVar('DatasetType', bound='Dataset')

    """SPECIAL METHODS
    """

    def __repr__(self):
        return repr(self.as_dataframe())

    def __getitem__(self, position):
        return self.as_dataframe().iloc[position]

    def __len__(self):
        return len(self.as_dataframe())

    """
    CONSTRUCTOR
    """

    def __init__(self: DatasetType,
                 data: pd.DataFrame,
                 info: dict = None,
                 **kwargs) -> DatasetType:
        """
        Parameters
        ----------
        data : pd.DataFrame
            DataFrame object that make up the dataset.
        info : dict
            Information of the dataset.
        **kwargs
            Extra named arguments passed to `_callback_dataset`.
            * exec_callback_dataset : bool
            Whether to avoid or not the call of the method '_callback_dataset'. This is used in
            the method 'copy' to avoid the creation of all the fields and to speed up the execution
        """
        exec_callback_dataset = kwargs.get('exec_callback_dataset', True)
        allow_empty_datasets = kwargs.get('allow_empty_datasets', False)

        info = {} if info is None else info
        assert isinstance(data, pd.DataFrame), "A pd.DataFrame must be provided for data"
        if len(data) == 0:
            if allow_empty_datasets:
                logger.warning(f"The dataset does not contain any elements.")
            else:
                raise Exception("The dataset does not contain any elements.")
        self.info = info
        self.data = data
        if exec_callback_dataset:
            self._callback_dataset(**kwargs)

    # region FROM METHODS

    @classmethod
    def from_json(cls,
                  source_path: str = None,
                  **kwargs) -> DatasetType:
        """Create a Dataset from a json file in COCO format.

        Parameters
        ----------
        source_path : str, optional
            Path of a json file that will be converted into a Dataset.
            (default is None)
        categories : list of str or None, optional
            List of categories to filter registers.
            If None, registers of all categories will be included.
            (default is None)
        **kwargs :
            Extra named arguments passed to `_callback_from_json`

        Returns
        -------
        Dataset
            Instance of the created Dataset
        """
        assert_cond = source_path is None or os.path.isfile(source_path)
        assert assert_cond, f"{source_path} is not a valid file."
        if source_path is not None:
            json_path = os.path.abspath(source_path)
            logger.info(f"Creating dataset from JSON file {json_path}")
        data, info = cls._callback_from_json(source_path, **kwargs)
        return cls(data, info, **kwargs)

    @classmethod
    def from_csv(cls,
                 source_path: str,
                 columns: Union[List[str], List[int], None] = None,
                 header: bool = True,
                 recursive: bool = True,
                 column_mapping: dict = None,
                 split_by_filenames: bool = False,
                 split_by_column: Union[str, int] = "partition",
                 include_id: bool = False,
                 info: dict = None,
                 **kwargs) -> DatasetType:
        """Create a Dataset from a csv file.

        Parameters
        ----------
        source_path : str
            Path of a csv file or a folder containing csv files that will be
            converted into a Dataset.
            The csv file(s) must have at least two columns, which represents
            `item` and `label` data.
        columns : list of str, list of int, or None, optional
            List of column names, or list of column indexes, to load from the
            csv file(s). If None, load all columns. (default is None)
        header : bool, optional
            Whether or not csv contains header at row 0 (default is True).
            If False, you need to map at least `item` and `label` columns by
            position in column_mapping parameter.
        recursive : bool
            Whether or not to seek files also in subdirectories
            (default is True)
        column_mapping : dict, optional
            Dictionary to map column names in dataset.
            If header=True mapping must be done by the column names,
            otherwise must be done by column positions.
            E.g.::
                header=True: column_mapping={"c1": "item", "c2": "label"}
                header=False: column_mapping={0: "item", 1: "label"}
            (default is None)
        split_by_filenames : bool, optional
            If `source_path` is the path of a folder containing csv files,
            splits the dataset by the file names. (default is False).
            The file names must be the following::
                `train.csv` for `train` partition
                `test.csv` for `test` partition
                `validation.csv` for `validation` partition
        split_by_column : str or int, optional
            Column name or column index in the csv file(s) to split the dataset
            (default is "partition").
            The values in the column must be the following::
                `train` for `train` partition
                `test` for `test` partition
                `validation` for `validation` partition
        include_id : bool, optional
            Wheter or not to include an id for each register (default is False)
        info : dict, optional
            Information of the csv file (default is {})
        **kwargs :
            Extra named arguments passed to `_callback_from_csv` and the dataset constructor

        Returns
        -------
        Dataset
            Instance of the created `Dataset`
        """
        regex_fname = kwargs.get('regex_filename')
        info = {} if info is None else info
        isdir = os.path.isdir(source_path)
        if split_by_column and columns and split_by_column not in columns:
            columns.append(split_by_column)
        header = 0 if header is True else None

        if isdir:
            dir_path = os.path.abspath(source_path)
            logger.info(f"Creating dataset from CSV files in folder {dir_path}")
            columns_buffer = None
            csvs = utils.get_all_files(
                source_path, seek_name=regex_fname, seek_extension=[".csv", ".CSV"],
                recursive=recursive)
            assert len(
                csvs) > 0, f"Folder {source_path} does not contain valid csv files."
            df = pd.DataFrame()
            for csv_file in csvs:
                valid_split_names = [f"{x}.csv" for x in Partitions.NAMES]
                if split_by_filenames and csv_file["title"] not in valid_split_names:
                    logger.warning(
                        f"{csv_file['title']} is not a valid CSV name for a partition.")
                    continue
                csv_path = os.path.join(csv_file["path"], csv_file["title"])
                logger.debug(f"Reading data from CSV file {csv_path}")
                temp = pd.read_csv(csv_path, header=header,
                                   usecols=columns, na_values=['nan'], keep_default_na=False)
                if split_by_filenames:
                    part_name = csv_file["title"].split('.csv')[0]
                    temp["partition"] = part_name
                    logger.debug(f"Data loaded for partition {part_name}")

                if not columns_buffer:
                    columns_buffer = [c for c in temp.columns]
                else:
                    def check_columns(cols_1, cols_2):
                        if len(cols_1) != len(cols_2):
                            return False
                        check = pydash.chain(cols_1)\
                            .map(lambda x: x in cols_2)\
                            .reduce(lambda x, y: x & y, True)\
                            .value()
                        return check

                    assert check_columns(temp.columns, columns_buffer),\
                        f"CSV files in {source_path} are not in the correct column format"
                df = pd.concat([df, temp], ignore_index=True)
        elif os.path.isfile(source_path):
            csv_path = os.path.abspath(source_path)
            logger.info(f"Creating dataset from CSV file {csv_path}")
            if header is None and columns:
                df = pd.read_csv(source_path, header=header,
                                 na_values=['nan'], keep_default_na=False)
            else:
                df = pd.read_csv(source_path, header=header, usecols=columns,
                                 na_values=['nan'], keep_default_na=False)
        else:
            raise Exception(
                f"Source {source_path} is neither a valid file nor a folder.")

        if column_mapping is not None:
            assert all([col in df.columns for col in column_mapping.keys()]),\
                f"'column_mapping' could not be performed, column names "\
                f"{column_mapping.keys()} are no present in result DataFrame"
            df = df.rename(columns=column_mapping)
        elif header is None and columns:
            column_mapping = {i: val for i, val in enumerate(columns)}
            df = df.rename(columns=column_mapping)

        if include_id and "id" not in df.columns:
            df["id"] = [str(uuid.uuid4()) for x in range(len(df))]

        df, info = cls._callback_from_csv(df, info, **kwargs)
        if split_by_column:
            kwargs['split_by_column'] = split_by_column
        elif split_by_filenames:
            kwargs['split_by_column'] = "partition"
        instance = cls(df, info, **kwargs)
        return instance

    @classmethod
    def from_folder(cls,
                    source_path: str,
                    extensions: List[str] = [""],
                    recursive: bool = True,
                    label_by_folder_name: bool = True,
                    split_by_folder: bool = True,
                    include_id: bool = False,
                    info: dict = None,
                    item_reader: Callable = None,
                    **kwargs) -> DatasetType:
        """Create a `Dataset` from a folder structure

        Parameters
        ----------
        source_path : str
            Path of a folder to be loaded and converted into a Dataset object.
        extensions : list of str, optional
            List of extensions to seek files in folders.
            [""] to seek all files in folders (default is [""])
        recursive : bool
            Whether or not to seek files also in subdirectories
            (default is True)
        label_by_folder_name : bool
            Whether or not you want to label each item of the dataset according
            to the name of the folder that contains it.
            (default is True)
        split_by_folder : bool, optional
            Split the dataset from the directory structure (default is False).
            For this option, directories must be in the following structure::
                - source_path
                - `train`
                    - class folders
                - `test`
                    - class folders
                - `validation`
                    - class folders
        include_id : bool, optional
            Wheter or not to include an id for each register (default is False)
        info : dict, optional
            Information of the folder structure (default is {})
        item_reader : Callable, optional
            Function that receives the file to process.
            The result of the function will be stores
            Information of the folder structure (default is None)
        **kwargs :
            Extra named arguments that may contains the following parameters:
            * lower_case_exts: bool
                Whether to convert `extensions` to lower case or not, by default True

        Returns
        -------
        Dataset
            Instance of the created `Dataset`
        """
        info = {} if info is None else info
        assert os.path.isdir(source_path), f"{source_path} is not a valid folder name"
        folder_path = os.path.abspath(source_path)
        logger.info(f"Creating dataset from folder {folder_path}")
        res = []

        lower = kwargs.get("lower_case_exts", True)
        extensions = list(set([x.lower() if lower else x for x in extensions]))
        _files = utils.get_all_files(
            source_path, seek_extension=extensions, recursive=recursive)

        logger.debug(
            f"{len(_files)} files found for extensions {', '.join(extensions)} in {source_path}")

        for f in _files:
            path = str(Path(f["path"]).relative_to(source_path))
            # path = f["path"].replace(source_path, "")

            if label_by_folder_name:
                if split_by_folder:
                    label = "/".join(path.split("/")[1:])
                else:
                    label = path
            if item_reader is not None:
                assert inspect.isfunction(item_reader), \
                    "The item reader must be a function to process the row"
                temp_item = item_reader(os.path.join(
                    source_path, path, f["title"]))
            else:
                temp_item = os.path.join(source_path, path, f["title"])

            temp = {
                "item": temp_item
            }
            if label_by_folder_name:
                temp["label"] = label
            if include_id:
                temp["id"] = str(uuid.uuid4())
            if split_by_folder:
                partition = path.split("/")[0]
                assert partition in Partitions.NAMES,\
                    f"Invalid folder name for splitting: {partition}"
                temp["partition"] = partition
                logger.debug(f"Folder for partition {partition} was found")
            res.append(temp)

        data = pd.DataFrame(res)
        if len(data) > 0:
            data['item'] = data['item'].apply(lambda x: os.path.normpath(x))
        data, info = cls._callback_from_folder(data, info)
        if split_by_folder:
            kwargs['split_by_column'] = "partition"
        instance = cls(data, info, **kwargs)

        return instance

    @classmethod
    def from_datasets(cls,
                      *args: DatasetType,
                      **kwargs) -> DatasetType:
        """Create a Dataset from several dataset instances.

        Parameters
        ----------
        *args :
            Dataset instances to build a new dataset instance
        **kwargs :
            Arguments to be passed to the Dataset constructor for each Dataset in `*args`

        Returns
        -------
        Dataset
            Instance of the created Dataset
        """
        datasets = []
        for dataset in args:
            assert isinstance(
                dataset, Dataset), f"{type(dataset)} is not a Dataset instance."
            datasets.append(dataset)
        dataframes, info = cls._callback_from_datasets(datasets)
        return cls.from_dataframes(dataframes,
                                   info=info,
                                   split_by_column="partition",
                                   validate_columns=False,
                                   **kwargs)

    @classmethod
    def from_dataframes(cls,
                        dataframes: List[pd.DataFrame],
                        split_by_column: str = None,
                        validate_columns: bool = True,
                        info: dict = None,
                        **kwargs) -> DatasetType:
        """Create a Dataset from dataframes instances.

        Parameters
        ----------
        dataframes : list of dataframe
            List of DataFrame instances to build a new dataset instance
        split_by_column : str or int, optional
            Column name or column index in the csv file(s) to split the dataset
            (default is None).
            The values in the column must be the following::
                `train` for `train` partition
                `test` for `test` partition
                `validation` for `validation` partition
        validate_columns : bool, optional
            Whether to validate that the columns of the dataset are the same
            or not (default is True)
        info : dict, optional
            Information of the folder structure (default is {})
        **kwargs :
            Extra named arguments passed to the dataset constructor

        Returns
        -------
        Dataset
            Instance of the created Dataset
        """
        info = {} if info is None else info
        if validate_columns:
            columns_buffer = None
            df_result = pd.DataFrame()
            for df in dataframes:
                if not columns_buffer:
                    columns_buffer = [c for c in df.columns]
                else:
                    def check_columns(cols_1, cols_2):
                        if len(cols_1) != len(cols_2):
                            return False
                        check = pydash.chain(cols_1)\
                            .map(lambda x: x in cols_2)\
                            .reduce(lambda x, y: x & y, True)\
                            .value()
                        return check

                    check_columns(df.columns, columns_buffer)

                df_result = pd.concat([df_result, df], ignore_index=True)
        else:
            df_result = pd.concat(
                dataframes, axis=0, ignore_index=True, sort=False)
            df_result = df_result.fillna("")

        if split_by_column is not None:
            split_by_column = (
                split_by_column
                if split_by_column in df_result.columns
                and len(df_result[df_result[split_by_column] == '']) == 0
                else None
            )

        kwargs['split_by_column'] = split_by_column
        instance = cls(df_result, info, **kwargs)

        return instance

    # endregion
    """
    FILTER METHODS
    """

    def filter_dataset(self: DatasetType,
                       column: str = "label",
                       filter_expression: Union[Callable, List, str] = None) -> DatasetType:
        """ Filters the dataset using an expression applied to a column.

        Parameters
        ----------
        column : str, optional
            Name of the column to perform the filter expression.
            (default is "label")
            Train set percentage. Should be between 0 and 1 (default is 0.8)
        filter_expression : Callable | list of values | value
            If value compares the column value with the sent value
            If list of values compares thta the value is in the list
            If Callable point to a function that has to return the value that
            will replace the column, the function definition expected is
                lambda row: x

        Returns
        -------
        Dataset
            Instance of the Dataset filtered
        """

        """
        """
        assert self.params["partitions"] == {},\
            "Filter process has to be performed before split process"

        expr = filter_expression

        if isinstance(filter_expression, list):
            def expr(x): return x in filter_expression
        elif isinstance(filter_expression, str):
            def expr(x): return x == filter_expression

        self.data = self.data.loc[self.data[column].apply(expr)]
        self.data = self.data.reset_index(drop=True)
        self.params["categories"] = self.data["label"].unique().tolist()

        return self

    """
    SPLIT METHODS
    """

    def split(self: DatasetType,
              train_perc: float = 0.8,
              test_perc: float = 0.2,
              val_perc: float = 0,
              split_params: dict = {
                  "random_state": None,
                  "stratify": None
              }) -> DatasetType:
        """Split the dataset in `train`, `test` and `validation`
        partitions.
        `train_perc`, `test_perc` and `val_perc` must sum to 1.

        Parameters
        ----------
        train_perc : float, optional
            Train set percentage. Should be between 0 and 1 (default is 0.8)
        test_perc : float, optional
            Test set percentage. Should be between 0 and 1 (default is 0.2)
        val_perc : float, optional
            Validation set percentage. Should be between 0 and 1
            (default is 0)

        Returns
        -------
        Dataset
            Instance of the Dataset
        """
        random_state = split_params.get("random_state", None)
        stratify = split_params.get("stratify", lambda x: None)

        perc_sum = train_perc + test_perc + val_perc
        assert perc_sum > 0.99 and perc_sum < 1.01, 'Partitions have to SUM 1'

        partitions = {Partitions.TRAIN: train_perc,
                      Partitions.TEST: test_perc}
        if val_perc > 0.:
            partitions[Partitions.VALIDATION] = val_perc

        def _split(_d, _size, _stratify=None):
            _s = _stratify(_d)
            return model_selection.train_test_split(_d,
                                                    test_size=_size,
                                                    random_state=random_state,
                                                    stratify=_s)

        total = 0.

        data = self.data
        unique_items = data.groupby('item').groups
        uniques_map = dict([*map(lambda x: (x[0], np.array(x[1:])),
                                 unique_items.values())])
        unique_ixs = set(uniques_map.keys())

        for part, val in partitions.items():
            tmp_ds = data.loc[np.array([*unique_ixs])]

            if total + val == 1:
                partition = tmp_ds
            else:
                part_size = val/(1. - total)

                tmp_ds = data.loc[np.array([*unique_ixs])]
                offset, partition = _split(_d=tmp_ds,
                                           _size=part_size,
                                           _stratify=stratify)

                unique_ixs = set(offset.index)
                total += val

            partition_ixs = partition.index
            self._set_partition(part, partition_ixs)
            self._set_repeated_partition_ixs(part, dict(map(lambda x: (x, uniques_map[x]),
                                                            partition_ixs)))

        return self

    """
    AS METHODS
    """
    # region AS METHODS

    def as_dataframe(self: DatasetType,
                     columns: List[str] = None,
                     sort_by: Union[str, List[str]] = None,
                     sort_asc: bool = True) -> Union[pd.DataFrame, List[pd.DataFrame]]:
        """Gets a DataFrame representation of the Dataset.

        Parameters
        ----------
        columns : list of str, optional
            List of columns in the DataFrame (default is None)
        sort_by : str or list of str, optional
            Column or list of columns to sort by (default is None)
        sort_asc : bool or list of bool, optional
            Whether to sort ascending the elements in the dataset, by field(s) specified in
            `sort_by`. Specify a list of bools for multiple sort orders, in that case this must
            match the length of the `sort_by`.

        Returns
        -------
        DataFrame
            DataFrame representing the Dataset.
        """
        df = self._get_data(columns)

        if sort_by is not None:
            if not is_array_like(sort_by):
                sort_by = [sort_by]
            if all(x in df.columns for x in sort_by):
                df = df.sort_values(by=sort_by, axis=0,
                                    ascending=sort_asc, inplace=False)
            else:
                logger.warning(
                    "Not all fields in sort_by exist in the dataset, so it has not been sorted.")

        # In case the dataset is partitioned, add the column 'partition'
        if self.get_num_partitions() > 0:
            df["partition"] = ""
            partitions = pydash.chain(
                self.params["partitions"].items()).value()
            for partition, value in partitions:
                df.loc[value, "partition"] = partition
        return df

    # endregion

    # region TO METHODS

    def to_folder(self: DatasetType,
                  dest_path: str,
                  split_in_partitions: bool = True,
                  split_in_labels: bool = True,
                  keep_originals: bool = True,
                  **kwargs) -> str:
        """Create a filesystem representation of the dataset.

        Parameters
        ----------
        dest_path : str
            Path where the dataset files will be stored
        split_in_partitions : bool, optional
            Whether or not to split items in folders with partition names.
            Only works if the dataset has been previously split.
            (default is True)
        split_in_labels : bool, optional
            Whether or not to split items in folders with label names.
            These folders will be below the partition folders (default is True)
        keep_originals : bool, optional
            Whether to keep original files or delete them (default is True)
        **kwargs :
            Extra named arguments passed to `_callback_to_folder`

        Returns
        -------
        str
            Path of folder created
        """
        parallel_processing = kwargs.get("parallel_processing", False)
        preserve_directories_structure = kwargs.get("preserve_directories_structure", False)

        folder = os.path.abspath(dest_path)
        logger.info(f"Writing the dataset elements in folder {folder}")

        df = self.as_dataframe()
        split_in_partitions = split_in_partitions and 'partition' in df.columns
        tuples = list()
        for _, row in df.iterrows():
            dest_folder = dest_path
            if split_in_partitions:
                dest_folder = os.path.join(dest_folder, row["partition"])
            if split_in_labels:
                dest_folder = os.path.join(dest_folder, str(row["label"]))
            if not os.path.exists(dest_folder):
                with lock:
                    os.makedirs(dest_folder)
            if parallel_processing:
                tuples.append((row, dest_folder, keep_originals, preserve_directories_structure))
            else:
                self._filesystem_writer(
                    row, dest_folder, keep_originals, preserve_directories_structure)
        if parallel_processing:
            with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
                pool.starmap(self._filesystem_writer, tuples)
        self._callback_to_folder(dest_path,
                                 split_in_partitions,
                                 split_in_labels,
                                 **kwargs)

        return dest_path

    def to_csv(self: DatasetType,
               dest_path: Union[str, Path],
               columns: List[str] = None,
               header: bool = True,
               **kwargs) -> Union[str, List[str]]:
        """Create csv file(s) representing the dataset.

        Parameters
        ----------
        dest_path : str or Path
            Path where the csv file(s) will be stored
        columns : list of str, optional
            List of columns to add in the csv (default is None)
        header : bool, optional
            Whether or not to add a header in the csv file (default is True)
        **kwargs :
            Extra named arguments passed to `_callback_to_csv`

        Returns
        -------
        str or list of str
            Path or list of paths of csv file(s) created
        """
        dest_path = dest_path if isinstance(
            dest_path, Path) else Path(dest_path)

        is_dir = dest_path.suffix != '.csv'

        df = self.as_dataframe(columns=columns)

        if is_dir:
            dest_path.mkdir(parents=True, exist_ok=True)
            csv_name = dest_path / "dataset.csv"
        else:
            csv_name = dest_path

        if os.path.dirname(csv_name):
            os.makedirs(os.path.dirname(csv_name), exist_ok=True)

        df = self._callback_to_csv(df, **kwargs)
        df.to_csv(csv_name, index=False, header=header, columns=columns)

        logger.info(f"CSV file {os.path.abspath(csv_name)} created")
        return csv_name

    # endregion

    # region GET METHODS
    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        DatasetType
        """
        return cls.DatasetType
    # endregion

    # region CATEGORIES
    def get_categories(self, sort_by_name=True):
        """Get the list of distinct values in the 'label' column of the dataset.
        This method could be used to construct the labelmap of the dataset in case it did not be
        provided in the constructor.
        The list of categories could be ordered and/or lowered.

        Parameters
        ----------
        sort_by_name : bool, optional
            Whether to sort categories by name or not. (default True)

        Returns
        -------
        list of str
            List of sorted categories of the dataset
        """
        if self.params['categories'] is None:
            return None
        if sort_by_name:
            return sorted(self.params['categories'])
        return self.params['categories']

    def get_classes(self):
        """Get the list of classes contained in the labelmap of the dataset
        The labelmap could have been provided in the constructor of the dataset or constructed
        with the `get_categories` method.

        Returns
        -------
        list
            List of classes from the labelmap
        """
        labelmap = self.get_labelmap()
        cats = labelmap.values() if labelmap is not None else []
        return [cat for cat in cats]

    def get_num_categories(self):
        """Get the number of categories in the dataset.

        Returns
        -------
        int
            The number of categories in the dataset
        """
        return len(self.params["categories"])

    def get_labelmap(self):
        """Get dictionary with ids to class names

        Returns
        -------
        dict
            Dictionary with ids to class names
        """
        return self.params.get("labelmap", None)

    def _get_inverse_labelmap(self):
        """Get dictionary with class names to ids

        Returns
        -------
        dict
            Dictionary with class names to ids in the form {'class_name': 'id'}
        """
        labelmap = self.get_labelmap()
        if labelmap is None:
            return None
        return {v: k for k, v in labelmap.items()}

    def get_category_id(self, category_name):
        """Get category id of `category_name` from the labelmap

        Parameters
        ----------
        category_name : str
            Name of the category

        Returns
        -------
        int
            Id of the category
        """
        inverse_labelmap = self._get_inverse_labelmap()
        assert inverse_labelmap is not None, "Dataset does not contain categories"
        assert category_name in inverse_labelmap, f"Category name '{category_name}' not valid"
        return inverse_labelmap[category_name]

    # endregion

    # region PARTITIONS
    def get_partition(self, partition: str):
        """Get a DataFrame with all rows in a partition of the dataset.

        Parameters
        ----------
        partition : str
            Partition name to get the labels from.

        Returns
        -------
        pd.DataFrame
            DataFrame with all rows in a partition of the dataset
        """
        return self.get_rows(partition)

    def get_partitions_names(self):
        """Get a list with the partition names of the dataset.

        Returns
        -------
        list of str
            List with the partition names of the dataset.
        """
        return [x for x in self.params["partitions"].keys()]

    def get_partition_rows(self, partitions=[]):
        """Get a dictionary that contains rows in each one of `partitions`.

        Parameters
        ----------
        partitions : list of str, optional
            List of the partitions to include in the result.
            If empty list, partitions defined in the dataset will be included (default is [])

        Returns
        -------
        dict
            Dictionary in the form {`partition`: rows}.
            If the datased is not partitionated, {"NO_PARTITION": data} will be returned
        """
        assert type(partitions) is list, logger.exception(
            "The partitions parameter must be a list")

        defined_partitions = self.params.get("partitions", None)

        if len(partitions) == 0:
            seek_partitions = defined_partitions
        else:
            seek_partitions = partitions

        if "partition" in self.data.columns:
            defined_partitions = self.data["partition"].unique().tolist()
            return dict(pydash.chain(defined_partitions)
                        .map(lambda x: (x, self.data.loc[self.data["partition"] == x]))
                        .value())
        elif seek_partitions:
            return dict(pydash.chain(seek_partitions.items())
                        .map(lambda x: (x[0], self.data.iloc[x[1]]))
                        .value())
        else:
            return {"NO_PARTITION": self.data}

    def get_num_partitions(self):
        """Get the number of partitions in the dataset.

        Returns
        -------
        int
            The number of partitions in the dataset
        """
        return len(self.get_partitions_names())
    # endregion

    # region DATA/ROWS
    def _get_data(self, columns=None):
        """Gets a reference to the `pd.DataFrame` instance with the data of the dataset

        Parameters
        ----------
        columns : list, optional
            List with the columns to be obtained from the dataset, by default None

        Returns
        -------
        pd.DataFrame
            Instance with the data of the dataset
        """
        return self.data[columns] if columns else self.data

    def get_rows(self, partition=None, sort_by=None, sort_asc=True, drop_partition_column=True):
        """Get a DataFrame with all rows of a dataset, optionally filtered by
        partition name.

        Parameters
        ----------
        partition : str or None
            Partition name to get the labels from. If None dataset will not
            be filtered.
        sort_by : str or list of str, optional
            Column or list of columns to sort by (default is None)
        sort_asc : bool or list of bool, optional
            Whether to sort ascending the elements in the dataset, by field(s) specified in
            `sort_by`. Specify a list of bools for multiple sort orders, in that case this must
            match the length of the `sort_by`.
        drop_partition_column : bool, optional
            Whether to remove `partition` column in the resulting DataFrame or not, by default True

        Returns
        -------
        pd.DataFrame
            DataFrame with all rows of a dataset
        """
        df = self.as_dataframe(sort_by=sort_by, sort_asc=sort_asc)
        if partition is None:
            return df
        else:
            try:
                df_cpy = df.loc[df["partition"] == partition].copy()
                if drop_partition_column:
                    df_cpy = df_cpy.drop(labels="partition", axis="columns")
                return df_cpy
            except KeyError:
                return df

    def get_num_rows(self, partition=None):
        """Get the number of all rows in the dateset, optionally filtered
        by partition name.

        Parameters
        ----------
        partition : str or None
            Name of the partition from which the records will be obtained.
            If None, dataset will not be filtered.

        Returns
        -------
        int
            The number of all rows in the dataset
        """
        return len(self.get_rows(partition=partition))

    def get_unique_items(self, partition=None):
        """Get a list of unique items in the dataset, optionally filtered by
        partition name.

        Parameters
        ----------
        partition : str or None
            Name of the partition from which the records will be obtained.
            If None, dataset will not be filtered.

        Returns
        -------
        list of str
            List of unique items in the dataset
        """
        if partition is None:
            return [x for x in self.params["items"]]
        else:
            Partitions.check_partition(partition)
            df = self.as_dataframe()
            items = df.loc[df["partition"] == partition]["item"].unique()
            return [x for x in items]

    def get_column_values(self, column: str, unique: bool = False):
        """Gets all (optionally unique) values of the `column` in the dataset

        Parameters
        ----------
        column : str
            Name of the column to get the values
        unique : bool, optional
            Whether or not to get unique values, by default False

        Returns
        -------
        list or np.array
            List or array with the obtained values
        """
        if unique:
            return self.as_dataframe()[column].unique()
        else:
            return self.as_dataframe()[column].values

    def label_counts(self) -> pd.Series:
        """Get the `value_counts` of the label field in the dataset

        Returns
        -------
        pd.Series
            Series with the `value_counts` of the label field
        """
        df = self.as_dataframe()
        assert 'label' in df.columns
        return df.label.value_counts()

    def partition_counts(self, include_perc: bool = True) -> pd.DataFrame:
        """Gets a pd.DataFrame with the counts of elements of each category grouped by the
        partition column, and optionally adds the percentage that those elements represent in the
        partition

        Parameters
        ----------
        include_perc : bool, optional
            Whether or not to include the `perc` column in the result, by default True

        Returns
        -------
        pd.DataFrame
            Dataframe with the counts of each category
        """
        cols = ['label', 'partition']
        df = self.as_dataframe()
        assert all(x in df.columns for x in cols)

        def counts_fn(grp): return len(
            df[(df.label == grp.name[0]) & (df.partition == grp.name[1])])
        counts_df = df.groupby(cols).apply(counts_fn).to_frame().rename(columns={0: 'counts'})

        if include_perc:
            # grp.name == (label, partition)
            def perc_fn(grp): return round(len(grp) / len(df[df.label == grp.name[0]]), 3)
            perc_df = df.groupby(cols).apply(perc_fn).to_frame().rename(columns={0: 'perc'})
            return counts_df.join(perc_df, on=cols)
        return counts_df

    # endregion

    # region DATASET DESCRIPTION METHODS
    def is_partitioned(self) -> bool:
        """Whether the dataset is partitioned or not.

        Returns
        -------
        bool
            `True` if the dataset is partitioned, `False` if the dataset is not partitioned
        """
        return self.get_num_partitions() > 0

    def is_labeled(self) -> bool:
        """
        Reports if the dataset contains tha label columns

        Returns
        -------
        bool
            [True if the dataset is labeled]
        """
        return "label" in self.data.columns
    # endregion

    # region setmethods

    def _set_partition(self, partition, value):
        """Set the indexes of the elements passed in `value` in the
        `partition` entry in params["partitions"] of the dataset.

        Parameters
        ----------
        partition : str
            Name of the partition to set
        value : np.array
            Indices of the elements in the dataset that will be part of the
            partition
        """
        Partitions.check_partition(partition)
        if len(value) == 0 and partition in self.params["partitions"]:
            del self.params["partitions"][partition]
        else:
            self.params["partitions"][partition] = value

    def _set_repeated_partition_ixs(self, partition, value):
        """Sets the hasmap of partitions with repeated ixs on fashion
        main_ixs: np.array([other_repeated_ixs])

        Parameters
        ----------
        partition : str
            Name of the partition to set
        value : dict(int: np.array)
            Hashmap with repeated ixs
        """
        try:
            Partitions.check_partition(partition)
            if 'rep_item_partitions' not in self.params:
                self.params['rep_item_partitions'] = {}

            self.params['rep_item_partitions'][partition] = value
        except Exception as ex:
            logger.exception(ex)
            raise

    def _set_items(self, items=None):
        """Set the `item` variable in `self.params`

        Parameters
        ----------
        items : list or np.array
            List of items of the dataset
        """
        if items is not None:
            items = items
        elif len(self.data) > 0:
            items = self.data['item'].unique()
        else:
            items = []
        self.params["items"] = items

    def _set_categories(self):
        """Set the `categories` variable in `self.params`
        """
        if "label" not in self.data.columns:
            self.params['categories'] = []
        else:
            self.params["categories"] = self.data["label"].unique().tolist()

    def _set_labelmap(self, labelmap=None):
        """Set the map from a label (integer) to class name.

        Parameters
        ----------
        labelmap : dict, str, list or None
            If dict, assigns the labelmap directly with `labelmap`.
            If str, reads the labelmap from the file path passed in `labelmap`.
            If list, forms the labelmap with the elements in the list.
            If None, forms the labelmap with the categories in the dataset.
            Otherwise, raise an exception.
        """
        store_path = None
        if type(labelmap) is dict:
            pass
        elif type(labelmap) is str or isinstance(labelmap, Path):
            if os.path.isfile(labelmap) or os.path.isdir(labelmap):
                labelmap_path = labelmap
                labelmap = self.read_labelmap_file(labelmap)
                # If the labelmap file does not exist, create it
                if labelmap is None:
                    store_path = labelmap_path
                else:
                    self._set_labelmap_filename(labelmap_path)
            else:
                raise ValueError("Invalid path for the labelmap file")
        elif type(labelmap) is list:
            labelmap = self._generate_labelmap_from_list(labelmap)
        if labelmap is None:
            labelmap = self._generate_labelmap_from_categories()
        # If there are labels, validate the labelmap
        if labelmap is not None:
            categories = self.get_categories()
            values = [x.lower()
                      if type(x) is str
                      else x for x in labelmap.values()]
            assert all([(x.lower() if type(x) is str else x) in values for x in categories]),\
                "Some categories of the dataset are not contained in the labelmap"
            if categories and not all([x in categories for x in values]):
                logger.warning((f"Some categories of the labelmap"
                                f"are not contained in the dataset"))
            n_cats = len(labelmap.items())
            if n_cats > 0:
                labelmap_str = ', '.join([f'{k}: {v}'
                                          for k, v in labelmap.items()][:10])
                labelmap_str += "..." if n_cats >= 10 else ""
                logger.debug((f"Assigning labelmap with {n_cats} "
                              f"categories ({labelmap_str})"))
        else:
            logger.debug((f"Dataset without categories. "
                          f"Assigning labelmap with None"))

        self.params["labelmap"] = labelmap
        if store_path is not None:
            labelmap_filename = self.write_labelmap_file(store_path)
            self._set_labelmap_filename(labelmap_filename)

    def _set_labelmap_filename(self, labelmap_filename):
        """Set the labelmap_filename variable in self.params

        Parameters
        ----------
        labelmap_filename : str
            Path to the labelmap filename of the dataset.
        """
        self.params["labelmap_filename"] = labelmap_filename
        logger.debug(f"Assigning labelmap_filename with {labelmap_filename}")

    def map_categories(self,
                       mapping_classes: Union[dict, str],
                       remove_missing: bool = False,
                       inplace: bool = True,
                       from_col: Union[str, int] = None,
                       to_col: Union[str, int] = None,
                       filter_expr: Callable = None,
                       clean_cat_names: bool = True) -> Union[None, Dataset]:
        """Function that performs a mapping of the categories that a dataset contains, changing the
        value of the column `label` of the rows of each category by its corresponding target value

        Parameters
        ----------
        mapping_classes : Union[dict, str]
            Dictionary or path to a CSV file containing the mappings
        remove_missing : bool, optional
            Whether to remove elements that do not have labels present in the mapping definition
            or not, by default False
        inplace : bool, optional
            If True, perform operation in-place, by default True
        from_col : Union[str, int], optional
            Name or position (0-based) of the column to be used as 'from' in the mapping, in case
            of `mapping_classes` is a CSV. By default None
        to_col : Union[str, int], optional
            Name or position (0-based) of the column to be used as 'to' in the mapping, in case of
        `mapping_classes` is a CSV. By default None
        filter_expr : Callable, optional
            A Callable that will be used to filter the CSV records in which the mapping is found,
            in case of `mapping_classes` is a CSV. By default None
        clean_cat_names : bool, optional
            Whether to clean or not the category names, converting to lower case and removing
            spaces at the beginning and at the end. By default True

        Returns
        -------
        Union[None, Dataset]
            Instance of the Dataset or None if `inplace=True`
        """
        assert "label" in self.data.columns,\
            "The dataset doesn't contain the 'label' field and a label mapping cannot be performed"
        instance = self if inplace else self.copy()
        mapping_classes_dict = utils.get_mapping_classes(
            mapping_classes, clean_cat_names=clean_cat_names,
            from_col=from_col, to_col=to_col, filter_expr=filter_expr)
        for from_cat, to_cat in mapping_classes_dict.items():
            if from_cat == '*' and to_cat != '*':
                remaining_cats = set(mapping_classes_dict.values()) - {to_cat}
                instance.data.loc[~instance.data["label"].isin(remaining_cats),
                                  "label"] = to_cat
            else:
                instance.data.loc[instance.data["label"] == from_cat, "label"] = to_cat
        if remove_missing:
            new_cats = [x for x in mapping_classes_dict.values()]
            instance.data = instance.data[instance.data["label"].isin(new_cats)]
        instance._set_categories()
        instance._set_labelmap()

        if not inplace:
            return instance

    # endregion

    # region AUXILIAR METHODS

    def _append_to_partition(self, partition, value):
        Partitions.check_partition(partition)
        if not isinstance(value, (np.ndarray,)):
            value = np.array(value)
        old_value = self.params["partitions"].get(partition, [])
        self.params["partitions"][partition] = np.append(old_value, value)

    def _split_by_column(self, column='partition', delete_column=True):
        """ Set the `partitions` property in `params` with the `index` of the `pd.DataFrame`
        and the partition of each item in the dataset.

        Parameters
        ----------
        column : str, optional
            Name of the column in `data` from which the partitions are taken
            (default is 'partition')
        delete_column : boolean, optional
            Whether to delete or not `column` from `data` after the operation is completed
            (default is `True`)
        """
        if "partitions" not in self.params:
            self.params["partitions"] = {}

        if column is None:
            return
        if column not in self.data.columns:
            return
        if len(self.data[self.data.partition == '']) > 0:
            pass
        else:
            partitions = self.data[column].unique()
            for partition in partitions:
                self._set_partition(
                    partition, np.array(self.data.loc[self.data["partition"] == partition].index)
                )
        if delete_column:
            self.data.drop(column, axis=1, inplace=True)

        return self

    # endregion

    # region ABSTRACT METHODS

    @abstractmethod
    def _filesystem_writer(self, item, dest_path, keep_originals, preserve_directories_structure):
        """Auxiliar function to move items in a to_folder call

         Parameters
        ----------
        item : dict
            Dictionary that contains imformation of an item.
        dest_path : str
            Path where data will be copied
        keep_originals: bool
            Whether to conserve original data after a move operation or not
        preserve_directories_structure : bool
            Whether or not to preserve the original structure of the directories.
            If False, all the media will be stored directly under the directory `dest_path`
        """
        raise Exception("Method not implemented")

    @abstractmethod
    def _callback_to_folder(self,
                            dest_path,
                            split_in_partitions,
                            split_in_labels,
                            **kwargs):
        """Auxiliar function to move items in a to_folder call
        """
        pass

    @abstractmethod
    def _get_annotations_info_cols(self):
        """Get default colums for this type of dataset.

        Returns
        -------
        list of str
            List of default columns
        """
        return ["item", "label"]

    @abstractmethod
    def read_labelmap_file(self, source_path):
        """Reads the labels file and returns a mapping from ID to class name.

        Parameters
        ----------
        source_path: str
            The filename where the class names are written.

        Returns
        -------
        dict
            A map from a label (integer) to class name.
        """
        return utils.read_labelmap_file(source_path)

    @abstractmethod
    def write_labelmap_file(self, dest_path):
        """Writes a file with the map of (integer) labels to class names.

        Parameters
        ----------
        dest_path: str
            The path of the file (or directory) in which the labelmap file should be written.

        Returns
        -------
        str
            Path of the file where the labelmap file was written
        """
        labelmap = self.get_labelmap()
        assert labelmap is not None, "Dataset without categories"
        return utils.write_labelmap_file(labelmap, dest_path)

    @abstractmethod
    def _generate_labelmap_from_list(self, labelmap_list):
        """Generates the labelmap from a list of class names, enumerating the elements that the
        list contains.

        Parameters
        ----------
        labelmap_list: list of str
            List that contains the class names of the labelmap

        Returns
        -------
        dict
            Dictionary of the form {index: class_name} with the elements of the labelmap
        """
        return {i: lbl for i, lbl in enumerate(labelmap_list)}

    @abstractmethod
    def _generate_labelmap_from_categories(self):
        """Generates the labelmap from the different categories that the dataset contains.
        If the dataset does not contain categories, `None` will be returned.

        Returns
        -------
        dict
            Dictionary of the form {index: class_name} with the elements of the labelmap
        """
        cat_names = self.get_categories()
        if cat_names is not None:
            return dict(zip(range(len(cat_names)), cat_names))
        # Dataset without categories
        return None

    # endregion

    # region CONSTRUCTOR CALLBACKS
    @abstractmethod
    def _callback_dataset(self, **kwargs):
        """Method that is called each time a dataset instance is created and that initializes the
        required variables and properties of the dataset.

        Parameters
        ----------
        **kwargs
            Extra named arguments passed to initialization methods and may contains the
            following parameters:
            * labelmap : dict
                Labelmap with which the dataset will be initialized.
                If dict, assigns the labelmap directly with `labelmap`.
                If str, reads the labelmap from the file path passed in `labelmap`.
                If list, forms the labelmap with the elements in the list.
            * split_by_column : str
                Name of the column in `data` from which the partitions are taken
        """
        self.params = {}
        self._set_items()
        self._set_categories()
        self._set_labelmap(labelmap=kwargs.get("labelmap"))
        self._split_by_column(column=kwargs.get("split_by_column"))

    @classmethod
    def _callback_from_datasets(cls, datasets):
        """Method that is executed in the calls to the `from_datasets` constructor, and that
        assigns the content of the `info` field with the information contained in the dataset
        instances.

        Parameters
        ----------
        datasets : list of Dataset
            List of Dataset instances from wich the new instance will be created
        """
        dataframes = [ds.as_dataframe() for ds in datasets]
        infos = [ds.info for ds in datasets]
        all_fields = set([field for info in infos for field in info.keys()])
        new_info = {}
        for field in all_fields:
            info_field = []
            for info in infos:
                if field in info:
                    info_field.append(str(info[field]))
            new_info[field] = ", ".join(info_field) if len(
                set(info_field)) > 1 else info_field[0]
        return dataframes, new_info

    @classmethod
    def _callback_from_csv(cls, data, info, **kwargs):
        """Method that is executed in the calls to the `from_csv` constructor and assigns the
        content of the `info` field and `data` property of the dataset.

        Parameters
        ----------
        data : pd.DataFrame
            DataFrame that contains in tabular format the main information of the elements of the
            dataset, such as the element-label pairs.
        info : dict
            Dictionary containing additional dataset information, which could be useful
            for different purposes
        **kwargs
            Extra named arguments that may be used for the method.
        """
        return data, info

    @classmethod
    def _callback_from_json(cls, json_path, **kwargs):
        """Method that is executed in the calls to the `from_json` constructor and and creates the
        content of the Dataset.

        Parameters
        ----------
        json_path: str
            Path to a JSON file that contains the information of a collection.
        categories : list of str, optional
            List of categories to filter items.
        **kwargs
            Extra named arguments that may be used for the method.
        """
        raise Exception("Method not implemented")

    @classmethod
    def _callback_from_folder(cls, data, info):
        """Method that is executed in the call to the `from_folder` constructor and allows to
        modify the content of `data` and to assign other parameters in `info`.

        Parameters
        ----------
        data : pd.DataFrame
            DataFrame that contains in tabular format the main information of the elements of the
            dataset, such as the element-label pairs.
        info : dict
            Dictionary containing additional dataset information, which could be useful
            for different purposes

        Returns
        -------
        (pd.DataFrame, dict)
            Tuple of DataFrame object and info dict
        """
        return data, info

    # endregion

    # region TO's CALLBACKS
    @abstractmethod
    def _callback_to_csv(self, data, **kwargs):
        """Method that is executed in the calls to the `to_csv` method and allows to modify the
        default form that the information will be stored in the csv file.

        Parameters
        ----------
        data : pd.DataFrame
            DataFrame that contains in tabular format the main information of the elements of the
            dataset, such as the element-label pairs.
        **kwargs
            Extra named arguments that may be used for the method.

        Returns
        -------
        pd.DataFrame
            DataFrame with the modified information of the dataset
        """
        return data

    # endregion

    # region REPORT

    # TODO: change name to _reporter (check conabio_ml_text)
    def reporter(self: DatasetType,
                 dest_path: Union[str, Path],
                 process_args: dict,
                 data_to_report: dict = {},
                 **kwargs):
        """Reports all default resources of the current dataset. It contains:
            - categories: Categories/labels in the dataset
            - partitions: Items per partition, calculated by item index
            - dataset: The current dataset to persist. Returns the address of the csv file to
            handle
            - samples_per_class: Label count for partition
            - samples_per_class_plot: Plot file of the samples distribution

        Parameters
        ----------
        dest_path : str
            Path where the resulting files will be stored
        process_args : dict
            The args of the process that wraps this class
        data_to_report : dict, optional
            The data to update. This params is passed to child instances.
            The param is read to check a possible overwrite proces of child instances,
            if it was overwritten just bypass the current process for THIS param.
            By default {}

        Returns
        -------
        dict
            Params to be reported in the Pipeline
        """
        try:
            dest_path = dest_path \
                if isinstance(dest_path, Path) else Path(dest_path)
            dest_path.mkdir(exist_ok=True, parents=True)

            samples_per_class = self._samples_per_class(dest_path)

            def store_dataset(p):
                dataset_path = os.path.join(p, "dataset.csv")

                self.to_csv(dest_path=p)

                if not os.path.exists(dataset_path):
                    logger.exception(
                        "The dataset cannot be created succesfully")
                    return ""

                return dataset_path

            def store_partitions(p):
                partition_rows = dict(pydash.chain(self.get_partition_rows().items()).map(
                    lambda x: (x[0], x[1].index.values.tolist())).value())

                partition_rows_path = os.path.join(p, "partition_rows.json")
                with open(partition_rows_path, mode="w") as _f:
                    json.dump(partition_rows, _f)

                return partition_rows_path

            transform = {
                "categories": {"asset": self.get_categories(),
                               "type": AssetTypes.DATA},
                "partitions": {"asset": store_partitions(dest_path),
                               "type": AssetTypes.FILE},
                "samples_per_class": {"asset": samples_per_class,
                                      "type": AssetTypes.FILE},
                "samples_per_class_plot": {
                    "asset": self.plot_samples_per_class(
                        dest_path=dest_path,
                        **{"samplesperclass_file": samples_per_class}),
                    "type": AssetTypes.PLOT},
                "dataset": {"asset": store_dataset(dest_path),
                            "type": AssetTypes.FILE},
                "info": {"asset": self._get_info(),
                         "type": AssetTypes.DATA},
            }

            params_to_report = set(['categories',
                                    'partitions',
                                    'dataset',
                                    'samples_per_class',
                                    'samples_per_class_plot',
                                    'info'])
            params_to_report = params_to_report.union(
                set(data_to_report.keys()))
            results = {}

            if type(data_to_report) is not dict:
                logger.warning(f"The reported child data_to_report params must be defined as"
                               f" dictionary current type {type(data_to_report)}. Child params "
                               f"cannot be reported")

            for k in params_to_report:
                try:
                    if k in data_to_report:
                        results[k] = data_to_report[k]
                    else:
                        results[k] = transform.get(k, None)
                except AttributeError as error:
                    logger.exception(f"The attribute {k} is not defined in the dataset."
                                     f"Cannot build the report of the class {self}")
                    logger.exception(error)

                    raise error

            return results
        except AttributeError:
            return data_to_report
        except Exception as ex:
            logger.exception(ex)

    def _samples_per_class(self, dest_path):
        """Creates and stores in the `dest_path`/samples_per_class.csv file, using
        a DataFrame which contains the number of samples for each class
        per partition in format (label, count_label, partition)

        Parameters
        ----------
        dest_path : str
            Path of the folder in which the file will be stored

        Returns
        -------
        str
            Path of the CSV file
        """
        try:
            res = pd.DataFrame()
            rows_per_partition = self.get_partition_rows([])

            for partition, rows in rows_per_partition.items():
                if "label" not in rows.columns:
                    logger.exception("The column label is not present in the "
                                     "partition, hence, samples per class cannot be computed")
                    break

                label_ocurr = rows["label"].value_counts()

                temp = pd.DataFrame(
                    {"label": label_ocurr.index,
                     "count_label": label_ocurr.values,
                     "partition": partition})

                res = pd.concat([res, temp])

            if len(res) > 0:
                res.sort_values(by=["count_label"], axis=0,
                                ascending=False, inplace=True)

            file_path = os.path.join(dest_path, "samples_per_class.csv")
            res.to_csv(file_path)

            return file_path
        except Exception as ex:
            logger.exception(
                "An error ocurred while computing samples_per_class")
            logger.exception(f"{ex}")

    def plot_samples_per_class(self, dest_path: str, **kwargs):
        """Plot the samples_per_class bar plot and stores it in the
        `dest_path`/plot_samples_per_class.png file.
        If there is a reference file in `kwargs` the method will use it to plot the word count,
        if not, it will infer using the dataset in the class

        Parameters
        ----------
        dest_path : str
            Path where the resulting files will be stored
        **kwargs :
            Extra named arguments that may contains the following parameters:
                * samplesperclass_file: Path of a CSV file that contains the samples per class

        Returns
        -------
        str
            Destination of the plot
        """
        import seaborn as sns
        import matplotlib.pyplot as plt

        try:
            samplesperclass_file = kwargs.get("samplesperclass_file")

            if samplesperclass_file is None:
                samplesperclass_file = self._samples_per_class(dest_path,
                                                               **kwargs)

            samplesperclass_count = pd.read_csv(samplesperclass_file)

            if len(samplesperclass_count) == 0:
                logger.error(
                    "An error ocurred while drawing samples_per_class")
                logger.error("Samples per class is empty")
                return ""

            partitions = samplesperclass_count["partition"].unique().tolist()

            f, ax = plt.subplots(len(partitions), 1,
                                 figsize=(10, 10), squeeze=False)
            ax = ax.flatten()

            for ix, partition in enumerate(partitions):
                temp = samplesperclass_count.loc[samplesperclass_count["partition"] == partition]

                most_popular = temp.head(25)

                g = sns.barplot(data=most_popular, x="label", y="count_label",
                                hue_order=most_popular.index,  ax=ax[ix])
                g.set_xticklabels(g.get_xticklabels(), rotation=30)

            filepath = os.path.join(dest_path, "plot_samples_per_class.png")
            plt.savefig(filepath, dpi=800)

            return filepath
        except Exception as ex:
            logger.exception(
                "An error ocurred while drawing samples_per_class")
            logger.exception(f"{ex}")

    def _register_preprocessing_op(self, op_name, op_value):
        """Register preprocessing operations that are performed on the dataset elements in a
        dictionary with the format {`op_name`: `op_value`}

        Parameters
        ----------
        op_name : str
            Name of the preprocessing operation
        op_value : str
            Value of the preprocessing operation
        """
        if "preprocessing" not in self.params:
            self.params["preprocessing"] = {}
        self.params["preprocessing"][op_name] = op_value

    def _register_augmentation_op(self, op_name, op_value):
        """Register data augmentation operations that are performed on the dataset elements in a
        dictionary with the format {`op_name`: `op_value`}

        Parameters
        ----------
        op_name : str
            Name of the augmentation operation
        op_value : str
            Value of the augmentation operation
        """
        if "augmentation" not in self.params:
            self.params["augmentation"] = {}
        self.params["augmentation"][op_name] = op_value

    def _register_additional_criteria(self, criteria_name, criteria_value):
        """Register additional criteria that were considered to partition the dataset in a
        dictionary with the format {`criteria_name`: `criteria_value`}
        I. e., if the elements were grouped according to some criteria.

        Parameters
        ----------
        criteria_name : str
            Name of the additional criteria
        criteria_value : str
            Value of the additional criteria
        """
        if "additional_criteria" not in self.params:
            self.params["additional_criteria"] = {}
        self.params["additional_criteria"][criteria_name] = criteria_value

    def _get_info(self):
        """Get the information of the datasets that includes `version`, `description`, `year`,
        `collection` and `url`.

        Returns
        -------
        dict
            Dictionary with the information of the dataset in format (version, description, year,
            collection, url)
        """
        return self.info

    def _get_preprocessing_ops(self):
        """Get the preprocessing operations that were performed on the dataset elements.

        Returns
        -------
        dict
            Dictionary with the preprocessing operations in the format {`op_name`: `op_value`}
        """
        return self.params.get("preprocessing", None)

    def _get_augmentation_ops(self):
        """Get the data augmentation operations that were performed on the dataset elements.

        Returns
        -------
        dict
            Dictionary with the augmentation operations in the format {`op_name`: `op_value`}
        """
        return self.params.get("augmentation", None)

    def _get_additional_criteria(self):
        """Get the additional criteria that were considered to partition the dataset.
        I. e., if the elements were grouped according to some criteria

        Returns
        -------
        dict
            Dictionary with the additional criteria in the format
            {`criteria_name`: `criteria_value`}
        """
        return self.params.get("additional_criteria", None)

    # endregion


class PredictionDataset(Dataset):
    """Represent a Dataset specification for predictions."""

    def __init__(self, data, info=None, **kwargs):
        """Create a `PredictionDataset` instance

        Parameters
        ----------
        data : pd.DataFrame
            DataFrame object that make up the dataset.
        info : dict, optional
            Information of the dataset, by default an empty dictionary
        **kwargs
            Extra named arguments passed to the `Dataset` constructor
        """
        info = {} if info is None else info
        super().__init__(data, info, **kwargs)

    """
    FROM METHODS
    """
    @classmethod
    def from_json(cls):
        raise Exception("Prediction datasets does not allow from_json method.")

    @classmethod
    def from_csv(cls,
                 source_path: str,
                 columns: Union[List[str], List[int], None] = None,
                 header: bool = True,
                 recursive: bool = True,
                 column_mapping: dict = None,
                 include_id: bool = False,
                 info: dict = None,
                 **kwargs) -> Dataset.DatasetType:
        """Create a PredictionDataset from a csv file.

        Parameters
        ----------
        source_path : str
            Path of a csv file or a folder containing csv files that will be
            converted into a Dataset.
            The csv file(s) must have at least three columns, which represents
            `item`, `label` and `score` data.
        columns : Union[List[str], List[int], None], optional
            List of column names, or list of column indexes, to load from the
            csv file(s). If None, load all columns. By default None
        header : bool, optional
            Whether or not csv contains header at row 0.
            If False, you need to map at least `item` and `label` columns by
            position in column_mapping parameter. By default True
        recursive : bool, optional
            Whether or not to seek files also in subdirectories, by default True
        column_mapping : dict, optional
            Dictionary to map column names in dataset.
            If header=True mapping must be done by the column names,
            otherwise must be done by column positions.
            E.g.::
                header=True: column_mapping={"c1": "item", "c2": "label"}
                header=False: column_mapping={0: "item", 1: "label"}.
            By default None
        include_id : bool, optional
            Wheter or not to include an id for each register, by default False
        info : dict, optional
            Information of the csv file, by default {}
        **kwargs
            Extra named arguments passed to the `from_csv` method of `Dataset`

        Returns
        -------
        Dataset
            Instance of the created `PredictionDataset`
        """
        info = {} if info is None else info
        if columns is not None and 'score' not in columns:
            columns.append('score')
        kwargs['split_by_filenames'] = False
        kwargs['split_by_column'] = None
        instance = super().from_csv(source_path=source_path,
                                    columns=columns,
                                    header=header,
                                    recursive=recursive,
                                    column_mapping=column_mapping,
                                    include_id=include_id,
                                    info=info,
                                    **kwargs)
        return instance

    @classmethod
    def from_folder(cls,
                    source_path: str,
                    extensions: List[str] = [""],
                    recursive: bool = True,
                    include_id: bool = False,
                    info: dict = None,
                    **kwargs) -> Dataset.DatasetType:
        """Create a `PredictionDataset` from a folder structure

        Parameters
        ----------
        source_path : str
            Path of a folder to be loaded and converted into a Dataset object.
        extensions : List[str], optional
            List of extensions to seek files in folders. [""] to seek all files in folders.
            By default [""]
        recursive : bool, optional
            Whether or not to seek files also in subdirectories, by default True
        include_id : bool, optional
            Wheter or not to include an id for each register, by default False
        info : dict, optional
            Information of the folder structure, by default {}

        Returns
        -------
        Dataset
            Instance of the created `PredictionDataset`
        """
        info = {} if info is None else info
        kwargs['split_by_folder'] = False
        return super().from_folder(source_path=source_path,
                                   extensions=extensions,
                                   recursive=recursive,
                                   include_id=include_id,
                                   info=info,
                                   **kwargs)

    """
    SPLIT METHODS
    """

    def split(self: Dataset.DatasetType,
              train_perc: float = 0.8,
              test_perc: float = 0.2,
              val_perc: float = 0,
              max_secs_in_sequences: int = None) -> Dataset.DatasetType:
        raise Exception("Prediction datasets does not allow split method.")

    """
    TO METHODS
    """

    def to_folder(self: Dataset.DatasetType,
                  dest_path: str,
                  split_in_labels: bool = True,
                  keep_originals: bool = True,
                  append_score_to_filename: bool = True,
                  **kwargs) -> str:
        """Create a filesystem representation of the prediction dataset.

        Parameters
        ----------
        dest_path : str
            Path where the prediction dataset files will be stored
        split_in_labels : bool, optional
            Whether or not to split items in folders with label names, by default True
        keep_originals : bool, optional
            Whether to keep original files or delete them, by default True
        append_score_to_filename : bool, optional
            Whether to include the score of the prediction as a prefix of each file name or not,
            by default True

        Returns
        -------
        str
            Path of folder created
        """
        kwargs['split_in_partitions'] = kwargs.get('split_in_partitions',
                                                   False)
        return super().to_folder(dest_path=dest_path,
                                 split_in_labels=split_in_labels,
                                 keep_originals=keep_originals,
                                 append_score_to_filename=append_score_to_filename,
                                 **kwargs)

    def as_dataframe(self: Dataset.DatasetType,
                     columns: List[str] = None,
                     sort_by: Union[str, List[str]] = None,
                     sort_asc: bool = True,
                     only_highest_score: bool = False) -> pd.DataFrame:
        """Gets a DataFrame representation of the Dataset.

        Parameters
        ----------
        columns : list of str, optional
            List of columns in the DataFrame (default is None)
        sort_by : str or list of str, optional
            Column or list of columns to sort by (default is None)
        sort_asc : bool or list of bool, optional
            Whether to sort ascending the elements in the dataset, by field(s) specified in
            `sort_by`. Specify a list of bools for multiple sort orders, in that case this must
            match the length of the `sort_by`.
        only_highest_score : bool, optional
            Whether to include only predictions with highest score for each item or not,
            by default False

        Returns
        -------
        DataFrame
            DataFrame representing the PredictionDataset.
        """
        if only_highest_score:
            if sort_by is None:
                sort_by = "score"
                sort_asc = False
            elif type(sort_by) not in (list, tuple):
                sort_by = [sort_by, "score"]
                sort_asc = [sort_asc, False]
            else:
                sort_by = sort_by + ["score"]
                sort_asc = sort_asc + [False]
        df = super().as_dataframe(columns=columns,
                                  sort_by=sort_by,
                                  sort_asc=sort_asc)
        if only_highest_score:
            return df.drop_duplicates(subset='item', keep='first', inplace=False)
        return df
    """
    GET METHODS
    """

    def _get_annotations_info_cols(self):
        """Get default colums for this type of dataset.

        Returns
        -------
        list of str
            List of default columns
        """
        return ["item", "label", "score"]
