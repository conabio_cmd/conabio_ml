#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import json

import math
import os
import pydash
import urllib.request
import tarfile
from datetime import datetime
from ftplib import FTP
from enum import Enum
from conabio_ml.utils.logger import get_logger
from conabio_ml.utils.conf import COLLECTIONS_ADDRESS
from conabio_ml.utils.utils import untar_file

from urllib.request import urlretrieve

logger = get_logger(__name__)

EXTRACT_FILES = ["tar"]


class Collection:
    """
    Obtains Dataset JSON files and provides simple functionality
    for searching those JSON files.

    Attributes
    ----------
    TYPES : enum
      avalible types of dataset
    Text : obj
      provider for Text datasets
    Images : obj
      provider for Images datasets
    """

    class TYPES(Enum):
        """
        Allowed types of collections
        """
        Text = "text"
        Images = "images"

    class __Loader:
        PROGRESSBAR_SPAN = 5
        """
        Base class used by an specific type of Dataset
        """

        class DatasetListReader:
            """
            Reader used to retrieve files of FTP server
            """

            def __init__(self):
                self.data = None

            def __call__(self, s):
                temp = s.decode("utf-8")
                self.data = json.loads(temp)

        def download_file(self,
                          url: str,
                          target_file: str):

            def reporthook(blocknum, blocksize, totalsize):
                def convert_size(size_bytes):
                    try:
                        if size_bytes == 0:
                            return "0B"
                        size_name = ("B", "KB", "MB", "GB", "TB",
                                     "PB", "EB", "ZB", "YB")
                        i = int(math.floor(math.log(size_bytes, 1024)))
                        p = math.pow(1024, i)
                        s = round(size_bytes / p, 2)

                        return "%s %s" % (s, size_name[i])

                    except Exception as ex:
                        logger.exception(ex)
                        print(ex)

                readsofar = blocknum * blocksize
                if totalsize > 0:
                    try:
                        percent = readsofar * 1e2 / totalsize
                        total_span = int((100-percent)/self.PROGRESSBAR_SPAN)
                        percent_span = int(percent/self.PROGRESSBAR_SPAN)

                        ss = '\r[{}{}]  {} of {}'.format('█' * percent_span,
                                                         '.' * total_span,
                                                         convert_size(
                                                             readsofar),
                                                         convert_size(totalsize))
                        sys.stderr.write(ss)

                        if readsofar >= totalsize:  # near the end
                            sys.stderr.write("\n")
                    except Exception as ex:
                        logger.exception(
                            "Error retrieving information from server.")
                        raise

                else:  # total size is unknown
                    sys.stderr.write("read %d\n" % (readsofar,))

            urlretrieve(url, target_file, reporthook)

        def get_file(self,
                     dest_path,
                     collection_type,
                     year,
                     version,
                     collection) -> str:
            """ It checks if Dataset file is in dest_path, and retrieves filepath
                if not: tries to find tar file
                if tar not fount: tries to download from server

            Parameters
            ----------
            dest_path : str
              Destination path to search Dataset file
            collection_type : str
              Type of collection to search, possible options: text/images
            year : int
              Year of collection
            version : str
              Version of collection
            collection : str
              Collection ID in server

            Returns
            -------
            str
              Path in filesystem to Dataset JSON file
            """
            try:

                collection_id = collection + "_" + \
                    str(year) + "_" +\
                    str(version)

                json_file = os.path.join(
                    dest_path, collection_id + ".json")
                tar_file = os.path.join(
                    dest_path, collection_id + ".tar.gz")

                # SCENARIOS
                if os.path.exists(json_file):
                    return json_file
                elif os.path.exists(tar_file):
                    if self.untar(collection_id=collection_id,
                                  dest_path=dest_path):
                        return self.get_file(dest_path, collection_type,
                                             year, version, collection)
                else:
                    if self.download_collection(collection=collection,
                                                collection_type=collection_type,
                                                year=year,
                                                version=version,
                                                dest_path=dest_path):

                        return self.get_file(dest_path, collection_type,
                                             year, version, collection)

            except:
                logger.exception("Error retrieving information from server.")
                return None

        def download_collection(self,
                                collection,
                                collection_type,
                                year,
                                version,
                                dest_path) -> bool:
            """Checks the availability of collection in server and tries to retreive it

            Parameters
            ----------
            collection : str
              ID of collection in server
            collection_type : str
              Type of collection. text/images
            year : int
              Year of collection
            version : str
              Version of collection
            dest_path : str
              Folder to store collection if found

            Returns
            -------
            bool
            True if the process was succesful

            """
            try:
                collection_id = collection + "_" + \
                    str(year) + "_" + str(version)

                datasets = self.get_dataset_list().get(collection)

                if not datasets:
                    raise Exception("Dataset " + collection
                                    + " not found")

                dataset = None
                for d in datasets:
                    if str(d["year"]) == str(year) and str(d["version"]) == str(version):
                        dataset = d["path"]
                        break

                if dataset:
                    logger.debug(f"Trying to download {collection_id}")
                    return self.get_dataset_file(
                        os.path.join(collection_type,
                                     collection_id + ".tar.gz"),
                        os.path.join(dest_path))
                else:
                    raise Exception(f"Dataset {collection} "
                                    f"version {int(year)}_{version} "
                                    f"not found")

                return True
            except TimeoutError as ex:
                logger.exception(f"Timeout exception, please check your internet"
                                 f"connection")
                raise ex
            except Exception as ex:
                logger.exception("Error while downloading collection.")
                raise ex

        def get_dataset_list(self) -> dict:
            """
            Obtains the available collections in server

            Returns
            -------
            dict
            """
            logger.debug("Fetching dataset list")

            if COLLECTIONS_ADDRESS is None:
                raise ValueError(
                    f"You must specify the COLLECTIONS_ADDRESS environment "
                    f"variable in order to download dataset list.")
            try:
                collections = None
                collections_url = os.path.join(COLLECTIONS_ADDRESS,
                                               "datasets.json")

                req = urllib.request.Request(collections_url)

                with urllib.request.urlopen(req) as response:
                    collections = response.read()

                if not collections:
                    ex = f"Collections {collections_url} cannot be acquired"
                    logger.exception(ex)
                    raise Exception(ex)

                res = json.loads(collections)
                return res
            except:
                logger.exception(
                    f"An error ocurred while obtaining the available "
                    f"collections in the server")
                raise

        def get_dataset_file(self,
                             dataset,
                             dest_path) -> bool:
            """
            Checks the Dataset file in destination path
              if not exists: downloads from server

            Parameters
            ----------
            source_path : str
              Path to search Dataset file
            dest_path : str
              If not found: path to store the retireved collection

            Returns
            -------
            bool
              True if the process was succesful
            """
            if COLLECTIONS_ADDRESS is None:
                raise ValueError(
                    f"You must specify the COLLECTIONS_ADDRESS environment "
                    f"variable in order to download the collections.")

            try:
                dataset_path = os.path.normpath(dataset)

                id = os.path.join(*os.path.split(dataset_path)[1:])

                logger.debug(f"Trying to download: {dataset}")

                if not os.path.isdir(dest_path):
                    os.makedirs(dest_path)

                dataset_filename = os.path.join(dest_path, id)

                url = os.path.join(COLLECTIONS_ADDRESS, dataset)
                self.download_file(url, dataset_filename)

                if not os.path.isfile(dataset_filename):
                    logger.error("The file cannot de donwloaded")
                    return False

                return True
            except:
                raise

        def untar(self,
                  collection_id,
                  dest_path) -> bool:
            """ Check tar file is found, and if found untars it
              in destination folder

            Parameters
            ----------
            collection_id : str
              Collection file to search
            dest_path : str
              Destination folder to untars the file

            Returns
            -------
            bool
              True if the process was succesful
            """
            tar_file = os.path.join(dest_path, collection_id + ".tar.gz")
            return untar_file(input_file=tar_file, output_folder=dest_path)

    class Text(__Loader):
        """
        Provides functionality related to Text datasets
        """

        @classmethod
        def fetch(cls,
                  dest_path,
                  **kwargs) -> str:
            """ Retrieves the Dataset file of type Text and returns its filepath

            Parameters
            ----------
            dest_path : str
              Destination path to search Dataset file
            kwargs: Optional params to download dataset file. Options:
              year : int, optional
                Year of collection
              version : str, optional
                Version of collection
              collection : str, optional
                Collection ID in server

            Returns
            -------
            str
              Path in filesystem to Dataset JSON file
            """
            collections = Collection.list(Collection.TYPES.Text)
            default_collection = pydash.chain(collections)\
                .filter(lambda x: x["id"] == "mws")\
                .value()[0]

            year = kwargs.get("year") or default_collection["year"]
            version = kwargs.get("version") or default_collection["version"]
            collection = kwargs.get("collection") or default_collection["id"]

            dataset_file = cls().get_file(dest_path, "text",
                                          year, version, collection)

            return dataset_file

    class Images(__Loader):
        """
        Provides functionality related to Images datasets
        """

        @classmethod
        def fetch(cls,
                  dest_path,
                  **kwargs) -> str:
            """ Retrieves the Dataset file of type Images and returns its filepath

            Parameters
            ----------
            dest_path : str
              Destination path to search Dataset file
            kwargs: Optional params to download dataset file. Options:
              year : int, optional
                Year of collection
              version : str, optional
                Version of collection
              collection : str, optional
                Collection ID in server

            Returns
            -------
            str
              Path in filesystem to Dataset JSON file
            """

            year = kwargs.get("year") or datetime.now().year
            version = kwargs.get("version") or "detection"
            collection = kwargs.get("collection")

            dataset_file = cls().get_file(dest_path, "images",
                                          year, version, collection)

            return dataset_file

    @staticmethod
    def download(url: str,
                 dest_path: str):
        """
        Downloads a file from a URL and, if it's a compressed .tar.gz file, unpacks it into the destination directory.

        Parameters:
        - url (str): The URL from which the file will be downloaded.
        - dest_path (str): The destination directory path where the downloaded or unpacked file will be stored.

        Returns:
        - dest_filename (str): The full path of the downloaded or unpacked file in the destination directory.

        """
        i = Collection().__Loader()
        if not os.path.isdir(dest_path):
            os.makedirs(dest_path)

        dest_filename = os.path.split(url)[-1]
        dest_filename = os.path.join(dest_path, dest_filename)

        i.download_file(url=url,
                        target_file=dest_filename)

        if os.path.isfile(dest_filename):
            id_file = os.path.split(dest_filename)[1]
            parts = id_file.split(".")
            id_file = parts[0]

        can_untar = True
        fname = os.path.basename(dest_filename)
        dest_path = os.path.dirname(dest_filename)

        if ".tar.gz" in fname:
            fname = fname.replace(".tar.gz", "")
        elif ".tar.gz" in fname:
            fname = fname.replace(".tar.gz", "")
        else:
            can_untar = True

        if can_untar:
            i.untar(fname, dest_path)

        return dest_filename

    @classmethod
    def list(cls, type_collection) -> list:
        """
        Lists the collections available in server by type

        Parameters
        ----------
        type_collection : Collections.TYPES
          Type of collection to enlist 

        Returns
        -------
        list(dict)
        """

        assert type(type_collection) == Collection.TYPES,\
            "Incorrect type of collection. See Collection.Types"

        collection_list = cls.__Loader().get_dataset_list()
        collections = pydash.chain(collection_list.items())\
            .filter(lambda x: any(
                [os.path.split(xx["path"])[0] ==
                 type_collection.value for xx in x[1]]
            ))\
            .map(lambda x:  [{
                "version": xx["version"],
                "year": xx["year"],
                "path": xx["path"],
                "info": xx["info"],
                "id": x[0]
            } for xx in x[1]])\
            .flatten()\
            .value()

        return collections
