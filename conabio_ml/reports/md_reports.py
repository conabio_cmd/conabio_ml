import os
import json
from conabio_ml.utils import report_params as params
import numpy as np
import uuid
from shutil import copyfile
from mdutils import mdutils
from conabio_ml.utils.report_params import languages
from . import appendix as appendix_templ
from conabio_ml.utils.logger import get_logger, debugger

logger = get_logger(__name__)
debug = debugger.debug

class MD_Report():
    """Class to generate a report in Markdown format
    """

    def __init__(self,
                 id,
                 assets_path,
                 dataset_data,
                 model_data,
                 metrics_data,
                 max_regs=None):
        """Creates an instance that allows generating a report in Markdown format

        Parameters
        ----------
        id : str
            Id of the report that will be used to generate the folder to store the resulting file
            in remote server
        assets_path : str
            Path where the report file will be stored
        dataset_data : dict
            Dictionary that contains the information of the datasets that will be reported
        model_data : dict
            Dictionary that contains the information of the models that will be reported
        metrics_data : dict
            Dictionary that contains the information of the result metrics that will be reported
        max_regs : int, optional
            Maximum number of records that will be displayed in a report table. If there are more
            records, they will be hidden in a collapsible table.
            If None, `MAX_REGISTERS_RESUME` records will be taken.
            By default None
        """
        self.id = id
        self.assets_path = assets_path
        self.dataset_data = dataset_data
        self.model_data = model_data
        self.metrics_data = metrics_data
        self.templ = None
        self.mdFile = None
        if params.files_base_path and os.path.isdir(params.files_base_path):
            self.id = os.path.join(self.id, str(uuid.uuid4()))
            self.imgs_remote_path = os.path.join(params.files_base_path, self.id)
            os.makedirs(self.imgs_remote_path, exist_ok=True)
        else:
            self.imgs_remote_path = None
        self.max_regs = params.MAX_REGISTERS_RESUME if max_regs is None else max_regs

    def index(self):
        """Shows the index of the report
        """
        self.__paragraph("[TOC]")

    def motivation(self):
        """Shows the Motivation section of the report, with default elements so that they can be
        filled in by the user
        """
        templ = self.templ["motivation"]
        self.__header_1(templ["title"])
        motiv_1 = self.__list_desc_elem(templ["person_or_organization"]["title"],
                                        templ["person_or_organization"]["description"],
                                        italic_desc=True)
        motiv_2 = self.__list_desc_elem(templ["abstract"]["title"],
                                        templ["abstract"]["description"],
                                        italic_desc=True)
        motiv_3 = self.__list_desc_elem(templ["proposed_solution"]["title"],
                                        templ["proposed_solution"]["description"],
                                        italic_desc=True)
        ptype_desc = ", ".join([templ["problem_type"][x]
                                for data in self.metrics_data
                                for x in data["metrics_set"].keys()])
        motiv_4 = self.__list_desc_elem(templ["problem_type"]["title"],
                                        ptype_desc or templ["problem_type"]["description"],
                                        italic_desc=ptype_desc == "")
        self.__list([motiv_1, motiv_2, motiv_3, motiv_4])

    def dataset(self):
        """Shows the Dataset section of the report, with information of the datasets used in the
        pipeline, including: dataset information, data preprocessing and augmentation, and the
        distribution of dataset classes and partitions
        """
        self.__header_1(self.templ["dataset"]["title"])
        if len(self.dataset_data) == 0:
            self.__warning(self.templ["dataset"]["empty"])
            return
        self.dataset_information()
        self.preprocessing_augmentation()
        self.dataset_distribution()
        self.dataset_partitions()

    def dataset_information(self):
        """Shows dataset information in the report, including: collection, version and url
        """
        templ = self.templ["dataset_information"]
        self.__header_2(templ["title"])
        for i, data in enumerate(self.dataset_data):
            if len(self.dataset_data) > 1:
                self.__header_3(f'Dataset {i+1}: {data["process_name"]}')
            if not data["info"]:
                self.__warning(templ["empty"])
                continue
            info_1 = self.__list_desc_elem(templ["collection"]["title"],
                                           data["info"].get("collection", "-") or "-")
            info_2 = self.__list_desc_elem(templ["version"]["title"],
                                           data["info"].get("version", "-") or "-")
            info_3 = self.__list_desc_elem(templ["url"]["title"],
                                           data["info"].get("url", "-") or "-")
            self.__list([info_1, info_2, info_3])

    def preprocessing_augmentation(self):
        """Shows information of the preprocessing and data augmentation of the dataset in the
        report
        """
        templ = self.templ["preprocessing_information"]
        self.__header_2(templ["title"])
        self.__paragraph(templ["description"])
        hdr_offset = 0
        for i, data in enumerate(self.dataset_data):
            if len(self.dataset_data) > 1:
                self.__header_3(f'Dataset {i+1}: {data["process_name"]}')
                hdr_offset = 1
            if not data["preprocessing"] and not data["augmentation"]:
                self.__warning(templ["augment_preproc_empty"])
            # Preprocessing information
            if data["preprocessing"]:
                self.__header(templ["preproc_hdr"], level=3+hdr_offset)
                self.__list([
                    self.__list_desc_elem(
                        templ["preproc_opers"].get(oper, oper.capitalize()), val, bold_title=False)
                    for preproc_oper in data["preprocessing"]
                    for oper, val in preproc_oper.items()])
            # Data augmentation information
            if data["augmentation"]:
                self.__header(templ["augment_hdr"], level=3+hdr_offset)
                self.__list([
                    self.__list_desc_elem(
                        templ["augment_opers"].get(oper, oper.capitalize()), val, bold_title=False)
                    for augment_oper in data["augmentation"]
                    for oper, val in augment_oper.items()
                ])

    def dataset_distribution(self):
        """Shows the number of samples for each category of the dataset in the report
        """
        templ = self.templ["dataset_distribution"]
        self.__header_2(templ["title"])
        for i, data in enumerate(self.dataset_data):
            if len(self.dataset_data) > 1:
                self.__header_3(f'Dataset {i+1}: {data["process_name"]}')
            if not data.get("samples_info"):
                self.__warning(templ["empty"])
                continue
            samples_per_class = data["samples_info"]["samples_per_class"]
            header_strings = [templ["table_hdr"]["label"], templ["table_hdr"]["counts"]]
            n_labels = len(samples_per_class.keys())
            if n_labels > self.max_regs:
                # Resume
                resume = header_strings[:]
                rest = header_strings[:]
                for i, (label, counts) in enumerate(samples_per_class.items()):
                    if i < self.max_regs:
                        resume.extend([label, str(counts)])
                    else:
                        rest.extend([label, str(counts)])
                title = templ["n_labels"].format(self.max_regs)
                summary = templ["remaining_x_labels"].format(n_labels - self.max_regs)
                self.__collapsable_table(resume, rest, 2, summary, title)
            else:
                # Complete
                complete_strings = header_strings[:]
                for label, counts in samples_per_class.items():
                    complete_strings.extend([label, str(counts)])
                self.__table(complete_strings, cols=2, title=templ["all_labels"])

    def percents(self):
        """Shows the percent of each partition of datasets in the report
        """
        templ = self.templ["dataset_partitions"]["percents"]
        self.__header_3(templ["title"])
        for i, data in enumerate(self.dataset_data):
            if len(self.dataset_data) > 1:
                self.__header_4(f'Dataset {i+1}: {data["process_name"]}')
            if not data.get("samples_info", {}).get("samples_per_part"):
                self.__warning(templ["empty"])
                continue
            samples_per_part = data["samples_info"]["samples_per_part"]
            n_sampls = np.sum([sampls for part, sampls in samples_per_part.items()])
            list_of_strings = [self.templ["dataset_partitions"]["partitions"][part]
                               for part, sampls in samples_per_part.items()
                               if part != "NO_PARTITION"]
            n_parts = len(list_of_strings)
            if n_parts == 0:
                self.__warning(templ["no_partitions"])
                continue
            list_of_strings.extend([f"{int(round(sampls / n_sampls * 100.))}%"
                                    for part, sampls in samples_per_part.items()])
            self.__table(list_of_strings, n_parts)

    def samples_partition(self):
        """Shows the number of samples in each partition of datasets in the report
        """
        templ = self.templ["dataset_partitions"]["samples"]
        self.__header_3(templ["title"])
        for i, data in enumerate(self.dataset_data):
            if len(self.dataset_data) > 1:
                self.__header_4(f'Dataset {i+1}: {data["process_name"]}')
            if not data.get("samples_info", {}).get("samples_per_part"):
                self.__warning(templ["empty"])
                continue
            partitions = [k for k, v in data["samples_info"]["samples_per_part"].items()]
            header_strings = [self.templ["dataset_distribution"]["table_hdr"]["label"]]\
                + [self.templ["dataset_partitions"]["partitions"][part] for part in partitions]
            n_labels = len(data["samples_info"]["samples_per_class"].keys())
            # There is truncated plots
            if data["samples_info"]["samples_per_class_plot_trunc"]:
                # Resume
                resume = header_strings[:]
                rest = header_strings[:]
                for i, (lbl, parts_counts) in enumerate(
                        data["samples_info"]["samples_per_class_part"].items()):
                    if i < self.max_regs:
                        resume.extend([lbl])
                        for part in partitions:
                            val = str(parts_counts[part] if part in parts_counts else 0)
                            resume.extend([val])
                    else:
                        rest.extend([lbl])
                        for part in partitions:
                            val = str(parts_counts[part] if part in parts_counts else 0)
                            rest.extend([val])
                title = templ["n_labels"].format(self.max_regs)
                summary = templ["remaining_x_labels"].format(n_labels - self.max_regs)
                self.__collapsable_table(resume, rest, len(partitions)+1, summary, title)
                self.__insert_image(data["samples_info"]["samples_per_class_plot_trunc"])
                lnk_txt = self.templ["dataset_partitions"]["plots"]["to_see_all_labels_link"]
                self.__insert_link(data["samples_info"]["samples_per_class_plot"], lnk_txt)
            else:
                # Complete
                complete_strings = header_strings[:]
                for lbl, parts_counts in data["samples_info"]["samples_per_class_part"].items():
                    complete_strings.extend([lbl])
                    for part in partitions:
                        val = str(parts_counts[part] if part in parts_counts else 0)
                        complete_strings.extend([val])
                self.__table(complete_strings, len(partitions)+1, title=templ["all_labels"])
                # Add asset plot
                self.__insert_image(data["samples_info"]["samples_per_class_plot"])

    def additional_criteria(self):
        """Show additional criteria done in the partition process of datasets in the report
        """
        templ = self.templ["dataset_partitions"]["additional_criteria"]
        self.__header_3(templ["title"])
        for i, data in enumerate(self.dataset_data):
            if len(self.dataset_data) > 1:
                self.__header_4(f'Dataset {i+1}: {data["process_name"]}')
            if not data["additional_criteria"]:
                self.__warning(templ["empty"])
                continue
            self.__paragraph(templ["description"])
            self.__list([self.__list_elem(templ["operations"][oper].format(val))
                         for additional_oper in data["additional_criteria"]
                         for oper, val in additional_oper.items()])

    def dataset_partitions(self):
        """Shows partitions information of the datasets in the report
        """
        self.__header_2(self.templ["dataset_partitions"]["title"])
        self.percents()
        self.samples_partition()
        self.additional_criteria()

    def model(self):
        """Shows the Model section of the report, with information of the models used in the
        pipeline, including: model information and additional notes of the model
        """
        self.__header_1(self.templ["model"]["title"])
        if len(self.model_data) == 0:
            self.__warning(self.templ["model"]["empty"])
            return
        self.model_information()
        self.additional_notes()

    def model_information(self):
        """Shows model information, including: name of the model, type of the model, type of the
        input data and type of the output data
        """
        templ = self.templ["model_data"]
        self.__header_2(templ["title"])
        for i, data in enumerate(self.model_data):
            if len(self.model_data) > 1:
                self.__header_3(f'Model {i+1}: {data["model_data"]["model_name"]}')
            desc = data["model_data"].get("model_name", templ["model_name"]["description"])
            model_1 = self.__list_desc_elem(templ["model_name"]["title"], desc, True,
                                            data["model_data"].get("model_name", False) == False)
            desc = data["model_data"].get("model_type", templ["model_type"]["description"])
            model_2 = self.__list_desc_elem(templ["model_type"]["title"], desc, True,
                                            data["model_data"].get("model_type", False) == False)
            desc = data["model_data"].get("input_data", templ["input_data"]["description"])
            model_3 = self.__list_desc_elem(templ["input_data"]["title"], desc, True,
                                            data["model_data"].get("input_data", False) == False)
            desc = data["model_data"].get("output_data", templ["output_data"]["description"])
            model_4 = self.__list_desc_elem(templ["output_data"]["title"], desc, True,
                                            data["model_data"].get("output_data", False) == False)
            self.__list(items=[model_1, model_2, model_3, model_4])

    def additional_notes(self):
        """Shows the section Additional notes of the model, with default elements so that they can
        be filled in by the user
        """
        templ = self.templ["additional_notes"]
        self.__header_2(templ["title"])
        model_add_1 = self.__list_desc_elem(templ["primary_intended_users"]["title"],
                                            templ["primary_intended_users"]["description"],
                                            italic_desc=True)
        model_add_2 = self.__list_desc_elem(templ["primary_intended_uses"]["title"],
                                            templ["primary_intended_uses"]["description"],
                                            italic_desc=True)
        model_add_3 = self.__list_desc_elem(templ["out_of_scope_uses"]["title"],
                                            templ["out_of_scope_uses"]["description"],
                                            italic_desc=True)
        self.__list(items=[model_add_1, model_add_2, model_add_3])

    def evaluation(self):
        """Shows the Evaluation section of the report, with information of the metrics evaluated
        in the pipeline, including: methodology, evaluation results, used metrics, analysis and
        conclusions and classification examples
        """
        self.__header_1(self.templ["evaluation"]["title"])
        if len(self.metrics_data) == 0:
            self.__warning(self.templ["evaluation"]["empty"])
            return
        self.evaluation_results()
        self.used_metrics()
        self.analysis_and_conclusions()
        self.classification_examples()

    def evaluation_results(self):
        """Shows the plots and tables of the evaluation results
        """
        self.__header_2(self.templ["evaluation_metrics"]["results"]["title"])
        self.__paragraph(self.templ["evaluation_metrics"]["results"]["description"])
        self.evaluation_plots()
        self.results_tables()

    def evaluation_plots(self):
        """Shows the plots of the evaluation.
        If there are elements in the `plots_trunc` entry, it will show links to the graphs with all
        the elements of the evaluation
        """
        templ = self.templ["evaluation_metrics"]["results"]["plots"]
        self.__header_3(templ["title"])
        self.__paragraph(templ["description"])
        hdr_offset = 0
        for i, data in enumerate(self.metrics_data):
            if len(self.metrics_data) > 1:
                self.__header_4(f'Evaluation {i+1}: {data["process_name"]}')
                hdr_offset = 1
            if data["plots_trunc"]:
                # Truncated plots
                self.__paragraph(templ["n_labels"].format(self.max_regs))
                for metric_set, metrics in data["plots_trunc"].items():
                    self.__header(templ["metrics_sets"][metric_set], level=4+hdr_offset)
                    # One-class plot
                    self.__header(title=templ["one_class_results"], level=5+hdr_offset)
                    for metric_name, plot_path in metrics["one_class"].items():
                        self.__paragraph(templ["metrics"].get(metric_name, metric_name))
                        self.__insert_image(plot_path)
                    # Per-class plots
                    self.__header(title=templ["per_class_results"], level=5+hdr_offset)
                    for metric_name, plot_path in metrics["per_class"].items():
                        self.__paragraph(templ["metrics"].get(metric_name, metric_name))
                        self.__insert_image(plot_path)
                # Complete links
                self.__paragraph(templ["to_see_all_labels_link"])
                for metric_set, metrics in data["plots"].items():
                    for metric_name, plot_path in metrics["per_class"].items():
                        self.__insert_link(plot_path,
                                           os.path.splitext(os.path.basename(plot_path))[0])
            else:
                # Complete plots
                for metric_set, metrics in data["plots"].items():
                    self.__header(templ["metrics_sets"][metric_set], level=4+hdr_offset)
                    # One-class plot
                    self.__header(title=templ["one_class_results"], level=5+hdr_offset)
                    self.__insert_image(metrics["one_class"]["general_metrics"])
                    # Per-class plots
                    self.__header(title=templ["per_class_results"], level=5+hdr_offset)
                    for metric_name, plot_path in metrics["per_class"].items():
                        self.__paragraph(templ["metrics"].get(metric_name, metric_name))
                        self.__insert_image(plot_path)

    def results_tables(self):
        """Shows the results of the evaluation in tables. If there are more than `max_regs`
        records, they will be hidden in a collapsible table.
        """
        templ = self.templ["evaluation_metrics"]["results"]["tables"]
        self.__header_3(templ["title"])
        self.__paragraph(templ["description"])
        hdr_offset = 0
        for i, data in enumerate(self.metrics_data):
            if len(self.metrics_data) > 1:
                self.__header_4(f'Evaluation {i+1}: {data["process_name"]}')
                hdr_offset = 1
            for metric_set, metrics in data["results_tables"].items():
                self.__header(templ["metrics_sets"][metric_set], level=4+hdr_offset)
                m = metrics["one_class"]
                nc = [type(x) in (float, int) for x in m["value"]]  # Numeric columns
                # One-class table
                complete_strings = (
                    [templ["metrics_hdr"][m["metric"][i]] for i, x in enumerate(nc) if x == True] +
                    [str(round(m["value"][i], 2)) for i, x in enumerate(nc) if x == True])
                self.__header(title=templ["one_class_results"], level=5+hdr_offset)
                self.__table(complete_strings, sum(nc))
                # Per-class table
                hdr_keys = [x for x in metrics["per_class"].keys()]
                hdr_str = [templ["metrics_hdr"][x] for x in metrics["per_class"].keys()]
                n_labels = len(metrics["per_class"]["category"])
                if n_labels > self.max_regs:
                    # Resume
                    resume = hdr_str[:]
                    rest = hdr_str[:]
                    for j in range(n_labels):
                        for hdr_key in hdr_keys:
                            x = metrics["per_class"][hdr_key][j]
                            if type(x) == float:
                                x = str(round(x, 2))
                            if j < self.max_regs:
                                resume.extend([x])
                            else:
                                rest.extend([x])
                    title = templ["n_labels"].format(self.max_regs)
                    summary = templ["remaining_x_labels"].format(n_labels - self.max_regs)
                    self.__header(templ["per_class_results"], level=5+hdr_offset)
                    self.__collapsable_table(resume, rest, len(hdr_keys), summary, title)
                else:
                    # Complete
                    complete_strings = hdr_str[:]
                    for j in range(n_labels):
                        for hdr_key in hdr_keys:
                            x = metrics["per_class"][hdr_key][j]
                            if type(x) == float:
                                x = str(round(x, 2))
                            complete_strings.extend([x])
                    self.__header(templ["per_class_results"], level=5+hdr_offset)
                    self.__table(complete_strings, len(hdr_keys))

    def used_metrics(self):
        """Shows a description of each of the metrics used in the evaluation
        """
        templ = self.templ["evaluation_metrics"]["used_metrics"]
        self.__header_2(templ["title"])
        hdr_offset = 0
        for i, data in enumerate(self.metrics_data):
            if len(self.metrics_data) > 1:
                self.__header_3(f'Evaluation {i+1}: {data["process_name"]}')
                hdr_offset = 1
            if not data["metrics_set"]:
                self.__warning(templ["empty"])
                continue
            for metric_set, eval_config in data["metrics_set"].items():
                self.__header(templ["metrics_sets"][metric_set]["title"], level=3+hdr_offset)
                self.__list([self.__list_desc_elem(metric["title"], metric["description"])
                             for metric in templ["metrics_sets"][metric_set]["metrics"]])
                self.__header(templ["eval_config"]["title"], level=4+hdr_offset)
                self.__list([
                    self.__list_desc_elem(templ["eval_config"]["params"][p], val, bold_title=False)
                    for p, val in eval_config.items() if p in templ["eval_config"]["params"]])

    def analysis_and_conclusions(self):
        """Shows the Analysis of the results and Conclusion section of the report, with default
        elements so that they can be filled in by the user
        """
        templ = self.templ["evaluation_metrics"]
        self.__header_2(templ["analysis"]["title"])
        self.__paragraph(self.__italic(templ["analysis"]["description"]))
        self.__header_2(templ["conclusions"]["title"])
        self.__paragraph(self.__italic(templ["conclusions"]["description"]))

    def classification_examples(self):
        """Shows examples of classification, including: best, worst and in the boundary of two
        classes
        """
        templ = self.templ["evaluation_metrics"]["examples"]
        self.__header_2(templ["title"])
        self.__paragraph(templ["description"])
        hdr_offset = 0
        for i, data in enumerate(self.metrics_data):
            if len(self.metrics_data) > 1:
                self.__header_3(f'Evaluation {i+1}: {data["process_name"]}')
                hdr_offset = 1
            if not data["classification_examples"]:
                self.__warning(templ["empty"])
                continue
            for ex_type, examples in data["classification_examples"].items():
                self.__header(templ[ex_type], level=3+hdr_offset)
                # self.mdFile.new_line()
                for example in examples:
                    self.__insert_image(example["item"])
                    # Top-n classifications
                    if not "top_n" in example:
                        continue
                    complete_strings = [templ["tbl_hdr_top_n"]["label"],
                                        templ["tbl_hdr_top_n"]["score"]]
                    for elem in example["top_n"]:
                        complete_strings.extend([elem["label"], f'{int(elem["score"]*100)}%'])
                    self.__table(complete_strings, 2)

    def appendix(self, lang):
        """Shows the Appendix section of the report

        Parameters
        ----------
        lang : languages
            Language of the report
        """
        if len(self.metrics_data) > 0:
            self.__header_1(self.templ["appendix"]["title"])
        for i, data in enumerate(self.metrics_data):
            for metric_set in data.get("metrics_set", {}).keys():
                sets = appendix_templ.metodology["metrics_sets"]
                self.__header_2(sets[metric_set]["title"][lang])
                self.__paragraph(sets[metric_set]["description"][lang])

    def create_file(self):
        """It creates a new Markdown file.
        """
        self.mdFile.create_md_file()

    def generate(self, file_name, title=None, lang=languages.EN):
        """Generates the report in the `file_name` file with the `title` in `language`

        Parameters
        ----------
        file_name : str
            The name of the Markdown file
        title : str, optional
            The title of the Markdown file. It is written with Setext-style, by default None
        lang : languages, optional
            Language of the report, by default languages.EN
        """
        assert lang in languages.NAMES, "Invalid language"
        logger.info("Generating report in Markdown format")

        json_template_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                          f"template_{lang}.json")
        with open(json_template_path, mode="r") as _file:
            self.templ = json.load(_file)

        self.mdFile = mdutils.MdUtils(file_name=os.path.join(self.assets_path, file_name),
                                      title=title or params.report_transl["default_title"][lang])
        # self.index()
        self.motivation()
        self.dataset()
        self.model()
        self.evaluation()
        self.appendix(lang)

        self.create_file()

    def __get_remote_or_local_path(self, path):
        """Obtains the path where the report elements will be saved, either locally or remotely

        Parameters
        ----------
        path : str
            Path where the element to be inserted is already stored on the local computer

        Returns
        -------
        str
            Path where the element is when it is inserted into the report, either on the local
            computer or on a remote one
        """
        if self.imgs_remote_path is not None:
            image_name = os.path.basename(path)
            copyfile(path, os.path.join(self.imgs_remote_path, image_name))
            path = f"{params.files_base_url}/{self.id}/{image_name}"
        else:
            path = os.path.relpath(path, self.assets_path)
        return path

    def __insert_image(self, path, text=""):
        """Insert an image to the report

        Parameters
        ----------
        path : str
            Path where the image is stored
        text : str, optional
            Text that is going to be displayed in the markdown file as a iamge, by default ""
        """
        if not path:
            return
        path = self.__get_remote_or_local_path(path)
        self.mdFile.new_line(self.mdFile.new_inline_image(path=path, text=text))

    def __insert_link(self, path, text):
        """Creates a inline link in markdown format.

        Parameters
        ----------
        path : str
            Path of the resource, either on the local computer or on a remote one
        text : str
            Text that is going to be displayed in the markdown file as a link.
        """
        if not path:
            return
        path = self.__get_remote_or_local_path(path)
        self.mdFile.new_line(self.mdFile.new_inline_link(link=path, text=text))

    def __paragraph(self, msg):
        """Add a new paragraph to Markdown file.

        Parameters
        ----------
        msg : str
            String containing the paragraph text
        """
        self.mdFile.new_paragraph(msg)

    def __warning(self, msg):
        """Show a paragraph in italic style and then a new line

        Parameters
        ----------
        msg : str
            String containing the paragraph text
        """
        self.__paragraph(self.__italic(msg))
        self.mdFile.new_line()

    def __bold(self, msg):
        """Returns a message in bold style

        Parameters
        ----------
        msg : str
            String containing the message text

        Returns
        -------
        str
            Bold message in Markdown format
        """
        return f'**{msg}**'

    def __italic(self, msg):
        """Returns a message in italic style

        Parameters
        ----------
        msg : str
            String containing the message text

        Returns
        -------
        str
            Italic message in Markdown format
        """
        return f'*{msg}*'

    def __list_elem(self, msg):
        """Returns a message as an element of a list

        Parameters
        ----------
        msg : str
            String containing the message text

        Returns
        -------
        str
            Element of a list in Markdown format 
        """
        return f'- {msg}'

    def __list_desc_elem(self, title, desc, bold_title=True, italic_desc=False):
        """Forms the description of an item as an item in a list

        Parameters
        ----------
        title : str
            Element to be described
        desc : str
            Description of the element
        bold_title : bool, optional
            Whether the element will be shown in bold or not, by default True
        italic_desc : bool, optional
            Whether the description will be shown in italic or not, by default False

        Returns
        -------
        str
            Element of a list in the form: `title`: `desc`
        """
        title = self.__bold(title) if bold_title else title
        desc = self.__italic(desc) if italic_desc else desc
        return self.__list_elem(f'{title}: {desc}')

    def __list(self, items):
        """Add unordered or ordered list in MarkDown file.

        Parameters
        ----------
        items : list
            Array of items for generating the list.
        """
        if items:
            self.mdFile.new_line()
            self.mdFile.new_list(items=items)

    def __table(self, content, cols, title=None):
        """Shows a list with the `content` distributed in `cols` columns, and optionally shows a
        paragraph with `title` before the table

        Parameters
        ----------
        content : list of str
            List with the content of the table
        cols : int
            Total number of columns that make up the table
        title : str, optional
            Paragraph text displayed before the table, by default None
        """
        rows = len(content) // cols
        self.mdFile.new_line()
        if title is not None:
            self.mdFile.new_paragraph(title)
        self.mdFile.new_table(text=content, columns=cols, rows=rows)

    def __collapsable_table(self, resume, rest, cols, summary=None, title=None):
        """Makes part of the table visible, while another part hides it and allows it to be viewed
        by clicking on an element, and optionally shows a paragraph with `title` before the table

        Parameters
        ----------
        resume : list of str
            List with visible elements of the table
        rest : list of str
            List with hidden elements of the table
        cols : int
            Total number of columns that make up the table
        summary : str, optional
            Text displayed in the element that expands the hidden part of the table,
            by default None
        title : str, optional
            Paragraph text displayed before the table, by default None
        """
        self.mdFile.new_line()
        if title is not None:
            self.mdFile.new_paragraph(title)
        self.__table(resume, cols)
        self.mdFile.new_paragraph("<details>")
        self.mdFile.new_paragraph(f'<summary>{summary}</summary>')
        self.mdFile.new_line()
        self.__table(rest, cols)
        self.mdFile.new_paragraph("</details>")
        self.mdFile.new_line()

    def __header(self, title, level):
        """Add a new header to the Markdown file

        Parameters
        ----------
        title : str
            Header title.
        level : int
            Header level. It can take values from 1 to 6
        """
        self.mdFile.new_line()
        self.mdFile.new_header(title=title, level=level)

    def __header_1(self, title):
        """Add a new header of level 1 to the Markdown file

        Parameters
        ----------
        title : str
            Header title.
        """
        self.__header(title=title, level=1)

    def __header_2(self, title):
        """Add a new header of level 2 to the Markdown file

        Parameters
        ----------
        title : str
            Header title.
        """
        self.__header(title=title, level=2)

    def __header_3(self, title):
        """Add a new header of level 3 to the Markdown file

        Parameters
        ----------
        title : str
            Header title.
        """
        self.__header(title=title, level=3)

    def __header_4(self, title):
        """Add a new header of level 4 to the Markdown file

        Parameters
        ----------
        title : str
            Header title.
        """
        self.__header(title=title, level=4)

    def __header_5(self, title):
        """Add a new header of level 5 to the Markdown file

        Parameters
        ----------
        title : str
            Header title.
        """
        self.__header(title=title, level=5)

    def __header_6(self, title):
        """Add a new header of level 6 to the Markdown file

        Parameters
        ----------
        title : str
            Header title.
        """
        self.__header(title=title, level=6)

    def __image(self, path, text=""):
        """Add inline images in a markdown file. For example `[MyImage](../MyImage.jpg)`

        Parameters
        ----------
        path : str
            Image's path / link.
        text : str, optional
            Text that is going to be displayed in the markdown file as a iamge, by default ""
        """
        self.mdFile.new_line(self.mdFile.new_inline_image(path=path, text=text))
