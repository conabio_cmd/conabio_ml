#!/usr/bin/env python
# -*- coding: utf-8 -*-
from conabio_ml.utils.report_params import languages
metodology = {
    "metrics_sets": {
        "BINARY": {
            "title": {
                languages.EN: "Binary evaluation methodology",
                languages.ES: "Metodología de evaluación binaria"
            },
            "description": {
                languages.EN: """
In binary classification, we generally have two classes, often called Positive and Negative, and we try to predict the class for each sample. For example, if we have a set of images labeled in a way that allows us to know which images contain animals and which do not, and we are interested in detecting photos that contain animals, in this case, our Positive class is that of animal images and the Negative class is that of images that do not contain animals. In other words, if a sample image contains an animal, it is a Positive, if not, it is Negative. Our classifier predicts, for each photo, whether it is Positive (P) or Negative (N): is there an animal in the photo?

### Confusion matrix

The confusion matrix is one of the most intuitive and easy ways used to find the accuracy and precision of the model. It is used for the classification problem where the output can be two or more classes. The confusion matrix itself is not a performance measure as such, but almost all performance metrics are based on the confusion matrix and the numbers it contains.

#### Terms associated with the confusion matrix

- **True positives (TP)**: True positives are cases where the actual class of the data sample was 1 (True) and the predicted class is also 1 (True).
- **False positives (FP)**: False positives are cases where the actual class of the data sample was 0 (False) and the predicted class is 1 (True). False is because the model has incorrectly predicted and positive because the predicted class was positive (1).
- **False negatives (FN)**: false negatives are cases where the actual class of the data sample was 1 (True) and the predicted class is 0 (False). False is because the model has incorrectly predicted and negative because the predicted class was negative (0).
- **True negatives (TN)**: true negatives are cases where the actual class of the data sample was 0 (False) and the predicted class is also 0 (False).

For binary classification, a confusion matrix has two rows and two columns, and shows in the first column how many positive samples were predicted as positive (first row) or negative (second row), and in the second column how many negative samples were predicted. as positive (first row) or negative (second row). Therefore, it has a total of 4 cells. Every time our classifier makes a prediction, one of the cells in the table is increased by one, following the four criteria listed above, and in the corresponding position according to the following figure:

![](https://i.imgur.com/wUjcemk.png)

At the end of the process, we can see exactly how our classifier performed.

### Metrics

#### Precision

The precision tries to answer the following question: **What proportion of positive predictions was really correct?**
Precision is defined as follows:

![](https://i.imgur.com/Mk2hNoB.png)

#### Recall
The recall tries to answer the following question: **What proportion of positive elements was correctly identified?**
Mathematically, the recall is defined as follows:

![](https://i.imgur.com/Sj2WkNY.png)

The above can be seen in the following diagram:

![](https://i.imgur.com/piIgsei.png)

In general, we prefer classifiers with good precision and good recall. However, there is a trade-off between precision and recall: by adjusting a classifier, improving precision often results in penalizing the recall and vice versa. Because of this, depending on the type of problem, one aspect or another may be given more relevance, depending on whether it is more important to have more correct predictions or to find most of the elements of the positive class. For example, in a wildlife monitoring system, it might be more important to find the largest number of images with animals, although many of the detections contain images without animals.

#### Score-F1
Sometimes it is desirable to have a metric that contains the precision and recall information so that two models can be directly compared. One way to obtain this metric is through the arithmetic mean of the precision and the recall. However, this has the problem that it does not represent the performance of models for datasets with a much higher class than the other and that it always generates classifications of the highest class. In contrast, the harmonic mean gives more weight to smaller numbers, and will penalize more, for example, a model with high precision but a very low recall.

The score-F1 is based on the harmonic mean and is defined as follows:

![](https://i.imgur.com/7FmBHvv.png)

And in general, the F-score is defined for a positive real β in which precision is given β times more importance than the recall:

![](https://i.imgur.com/P42ajqA.png)

In this sense, the score-F1 is a special case of the score-F where β has a value of 1.

#### Accuracy
Accuracy in classification problems is the number of correct predictions made by the model over all kinds of predictions made.

![](https://i.imgur.com/IX1if69.png)

This metric should only be used to evaluate datasets that are more or less balanced, and never when there is a class that has the most elements.
                """,
                languages.ES: """
En la clasificación binaria, generalmente tenemos dos clases, a menudo llamadas Positiva y Negativa, y tratamos de predecir la clase para cada muestra. Por ejemplo, si tenemos un conjunto de imágenes etiquetado de tal forma que nos permita saber cuáles imágenes contienen animales y cuáles no, y estamos interesados en detectar las fotos que contienen animales, en este caso, nuestra clase Positiva es la de las imágenes de animales y la clase Negativa es la de las imágenes que no contienen animales. En otras palabras, si una imagen de muestra contiene un animal, es un Positivo, si no es así, es Negativo. Nuestro clasificador predice, para cada foto, si es Positiva ( P) o Negativa (N): ¿hay un animal en la foto?

### Matriz de confusión

La matriz de confusión es una de las maneras más intuitivas y fáciles que se utilizan para encontrar la exactitud y precisión del modelo. Se utiliza para el problema de clasificación donde la salida puede ser de dos o más clases. La matriz de confusión en sí misma no es una medida de rendimiento como tal, pero casi todas las métricas de rendimiento se basan en la matriz de confusión y los números que contiene.

#### Términos asociados con la matriz de confusión

- **Verdaderos Positivos (TP)**: Los verdaderos positivos son los casos en que la clase real de la muestra de datos era 1 (Verdadero) y la predicha también es 1 (Verdadero).
- **Falsos Positivos (FP)**: Los falsos positivos son los casos en que la clase real de la muetra de datos era 0 (Falso) y la predicha es 1 (Verdadero). Falso es porque el modelo ha predicho incorrectamente y positivo porque la clase pronosticada fue positiva (1).
- **Falsos negativos (FN)**: los falsos negativos son los casos en que la clase real de la muestra de datos era 1 (Verdadero) y la predicha es 0 (Falso). Falso es porque el modelo ha predicho incorrectamente y negativo porque la clase predicha fue negativa (0).
- **Verdaderos negativos (TN)**: los verdaderos negativos son los casos en que la clase real de la muestra de datos era 0 (Falso) y la predicha también es 0 (Falso).

Para la clasificación binaria, una matriz de confusión tiene dos filas y dos columnas, y muestra en la primera columna cuántas muestras positivas se predijeron como positivas (primera fila) o negativas (segunda fila), y en la segunda columna cuántas muestras negativas se predijeron como positivas (primera fila) o negativas (segunda fila). Por lo tanto, tiene un total de 4 celdas. Cada vez que nuestro clasificador hace una predicción, una de las celdas de la tabla se incrementa en uno, siguiendo los cuatro criterios listados arriba, y en la posición que le corresponda según la siguiente figura:

![](https://i.imgur.com/wUjcemk.png)

Al final del proceso, podemos ver exactamente cómo se desempeñó nuestro clasificador.

### Métricas

#### Precisión

La precision intenta responder la siguiente pregunta: **¿Qué proporción de predicciones positivas fue realmente correcta?**
La precisión se define de la siguiente manera:

![](https://i.imgur.com/Mk2hNoB.png)

#### Recall
El recall intenta responder la siguiente pregunta: **¿Qué proporción de elementos positivos se identificó correctamente?**
Matemáticamente, el recall se define de la siguiente manera:

![](https://i.imgur.com/Sj2WkNY.png)

Lo anterior se puede visualizar en el siguiente diagrama:

![](https://i.imgur.com/piIgsei.png)

En general, preferimos clasificadores con buena precisión y buen recall. Sin embargo, existe una compensación entre la precisión y el recall: al ajustar un clasificador, mejorar la precisión a menudo resulta en penalizar el recall y viceversa. Debido a esto, según el tipo de problema se le podrá dar más relevancia a un aspecto u otro, dependiendo si es más importante tener más predicciones correctas o encontrar la mayoría de los elementos de la clase positiva. Por ejemplo, en un sistema de monitoreo de fauna silvestre, podría ser más importante encontrar la mayor cantidad de imágenes con animales, aunque muchas de las detecciones contengan imágenes sin animales.

#### Score-F1
A veces es deseable contar con una métrica que contenga la información de la precisión y el recall de tal manera que se puedan comparar dos modelos directamente. Una forma de obtener esta métrica es a través de la media aritmética de la precisión y el recall. Sin embargo, esta tiene el problema de que no representa bien el desempeño de modelos para datasets con una clase mucho mayor que la otra y que siempre genere clasificaciones de la clase mayor. Por el contrario, la media armónica da mayor peso a los números más pequeños, y penalizará más, por ejemplo, un modelo con una alta precisión pero un recall muy bajo.

El score-F1 se basa en la media armónica y se define de la siguiente manera:

![](https://i.imgur.com/7FmBHvv.png)

Y en general, el score-F se define para un β real positivo en el cual se le da β veces más importancia a la precisión que al recall:

![](https://i.imgur.com/P42ajqA.png)

En este sentido, el score-F1 es un caso especial del score-F donde β tiene un valor de 1.

#### Accuracy
La exactitud (Accuracy) en los problemas de clasificación es el número de predicciones correctas realizadas por el modelo sobre todo tipo de predicciones realizadas.

![](https://i.imgur.com/IX1if69.png)

Esta métrica solo debe usarse para evaluar datasets que están más o menos balanceados, y nunca cuando exista una clase que tenga la mayoría de elementos.
                """
            }
        },
        "MULTICLASS": {
            "title": {
                languages.EN: "Multiclass evaluation methodology",
                languages.ES: "Metodología de evaluación multi-clase"
            },
            "description": {
                languages.EN: """
Binary classification problems focus on a positive class that we want to detect. In contrast, in a typical multiclass classification problem, we must classify each sample into one of N different classes. For example, we might want to classify a photo containing animals into one of several different categories, which could be part of a taxonomic hierarchy.

For this type of model, the metrics described in the Binary Classification section for each of the categories are usually calculated, that is, taking one category as the positive class, and the sum of all the others as the negative class.

For example, suppose a model tries to classify photos that have animals of the following classes: Mammals, Fish and Birds. Our classifier has to predict which animal is shown in each photo. This is a classification problem with N = 3 classes.

Suppose the following is the confusion matrix after classifying 25 photos:

![](https://i.imgur.com/YFDw4Q8.png)

Similar to the binary case, we can define precision and recall for each of the classes. For example, the precision for the Mammal class is the number of correctly predicted mammal photos (4) of all the predicted mammal photos (4 + 3 + 6 = 13), which equals 4/13 = 30.8%. So only about a third of the photos our predictor classifies as Mammal are actually mammals.
On the other hand, the recall for the Mammal class is the number of correctly predicted photos of mammals (4) of the number of photos of real mammals (4 + 1 + 1 = 6), which is 4/6 = 66.7%. This means that our classifier classified 2/3 of the mammal photos as Mammal.

![](https://i.imgur.com/7kiKwkY.png) ![](https://i.imgur.com/Hqat3ni.png) ![](https://i.imgur.com/9uPsgxM.png)

Similarly, we can calculate the precision and recall of the other two classes: Fish and Birds. For Fish, the numbers are 66.7% and 20.0% respectively. For Birds, the number of both precision and recall is 66.7%.
The F1-score values for each category are: Mammal = 42.1%, Fish = 30.8% and Birds = 66.7%.
All these metrics are summarized in the following table:

![](https://i.imgur.com/ItD679X.png)

### Macro averaging

Macro averaging reduces your multiclass predictions down to multiple sets of binary predictions, calculates the corresponding metric for each of the binary cases, and then averages the results together. As an example, consider `precision` for the binary case.

![](https://i.imgur.com/TB7Mb3T.png)

In the multiclass case, if there were levels `A`, `B`, `C` and `D`, macro averaging reduces the problem to multiple one-vs-all comparisons. The truth and estimate columns are recoded such that the only two levels are `A` and `other`, and then precision is calculated based on those recoded columns, with `A` being the “relevant” column. This process is repeated for the other 3 levels to get a total of 4 precision values. The results are then averaged together.

The formula representation looks like this. For `k` classes:

![](https://i.imgur.com/wJls8UN.png)

where `PR1` is the precision calculated from recoding the multiclass predictions down to just `class 1` and `other`.

Note that in macro averaging, all classes get equal weight when contributing their portion of the precision value to the total (here 1/4). This might not be a realistic calculation when you have a large amount of class imbalance. In that case, a *weighted macro average* might make more sense, where the weights are calculated by the frequency of that class in the `truth` column.

![](https://i.imgur.com/gFwqiHE.png)

### Micro averaging

Micro averaging treats the entire set of data as an aggregate result, and calculates 1 metric rather than `k` metrics that get averaged together.

For precision, this works by calculating all of the true positive results for each class and using that as the numerator, and then calculating all of the true positive and false positive results for each class, and using that as the denominator.

![](https://i.imgur.com/gew60vU.png)

In this case, rather than each class having equal weight, each *observation* gets equal weight. This gives the classes with the most observations more power.
                """,
                languages.ES: """
Los problemas de clasificación binaria se centran en una clase positiva que queremos detectar. Por el contrario, en un típico problema de clasificación multiclase, debemos clasificar cada muestra en una de N clases diferentes. Por ejemplo, podríamos querer clasificar una foto que contiene animales en alguna de varias categorías diferentes, que podrían ser parte de una jerarquía taxonómica.

Para este tipo de modelos se suele realizar el cálculo de las métricas descritas en la sección Clasificación binaria para cada una de las categorías, esto es, tomando una categoría como la clase positiva, y la suma de todas las demás como la clase negativa.

Por ejemplo, supongamos un modelo que trata de clasificar fotos que tienen animales de las siguientes clases: Mamíferos, Peces y Aves. Nuestro clasificador tiene que predecir qué animal se muestra en cada foto. Este es un problema de clasificación con N=3 clases.

Supongamos que la siguiente es la matriz de confusión después de clasificar 25 fotos:

![](https://i.imgur.com/YFDw4Q8.png)

De manera similar al caso binario, podemos definir precisión y recall para cada una de las clases. Por ejemplo, la precisión para la clase Mamífero es el número de fotos de mamíferos predichas correctamente (4) de todas las fotos de mamíferos predichas (4 + 3 + 6 = 13), lo que equivale a 4/13 = 30.8%. Entonces, solo alrededor de un tercio de las fotos que nuestro predictor clasifica como Mamífero son en realidad de mamíferos.
Por otro lado, el recall para la clase Mamífero es el número de fotos de mamíferos (4) pronosticadas correctamente del número de fotos de mamíferos reales (4 + 1 + 1 = 6), que es 4/6 = 66.7%. Esto significa que nuestro clasificador clasificó 2/3 de las fotos de mamíferos como Mamífero.

![](https://i.imgur.com/7kiKwkY.png) ![](https://i.imgur.com/Hqat3ni.png) ![](https://i.imgur.com/9uPsgxM.png)

De manera similar, podemos calcular la precisión y el recall de las otras dos clases: Peces y Aves. Para Peces, los números son 66.7% y 20.0% respectivamente. Para Aves, el número tanto de precisión como de recall es del 66,7%.
Los valores de score-F1 para cada categoría son: Mamífero = 42.1%, Peces = 30.8% y Aves = 66.7%.
Todas estas métricas se resumen en la siguiente tabla:

![](https://i.imgur.com/ItD679X.png)

### Macro-promedio

Ahora que tenemos los valores de las métricas para cada categoría, el siguiente paso es combinarlas para obtener las métricas para todo el clasificador. Hay varias formas de hacer esto y aquí analizaremos las dos más utilizadas. Empecemos con la más sencilla: calcular la media aritmética de la precisión y el recall y a partir de ellas el score-F1 global. Esta es llamada el Macro-promedio de la métrica, y se calcula:

![](https://i.imgur.com/edqb9hQ.png)

Una variante para calcular el **Macro-F1** que se suele preferir al evaluar datasets desbalanceados es con la media aritmética de los score-F1 individuales de todas las clases, ya que [se ha demostrado](https://arxiv.org/abs/1911.03347) que el otro método es excesivamente "benevolente" hacia clasificadores muy sesgados y puede dar valores engañosamente altos cuando se están evaluando este tipo de datasets, y ya que se suele usar el Macro-F1 con la intención de asignar igual peso a la clase más frecuente y a la menos frecuente, se recomienda utilizar esta última definición, que es significativamente más robusta hacia la distribución de este tipo de error.

### Micro-promedio

Otra forma de realizar el promedio de las métricas es con el micro-promedio, para el cuál se deben tomar todas las muestras juntas para hacer el cálculo de la precisión y el recall, y después con estos valores hacer el cálculo del score-F:

![](https://i.imgur.com/cX74dhp.png)

Para calcular la **micro-precisión** tenemos que sumar el total de TP y dividirlo entre el total de predicciones positivas. Para nuestro ejemplo, sumamos el total de elementos en la diagonal (color verde) para obtener TP=12. Después sumamos los falsos positivos, es decir, los elementos fuera de la diagonal, y obtenemos FP=13. Nuestra precisión es, por lo tanto, 12 / (12 + 13) = 48.0%.
Para calcular el **micro-recall** tenemos que sumar el total de falsos negativos (FN) para todas las clases, que al igual que para los FP, consiste en sumar todos los elementos fuera de la diagonal, con lo que se obtendría nuevamente FN=13 y el valor del micro-recall también es 12 / (12 + 13) = 48.0%.
Dado que el micro-recall es igual a la micro-precisión, entonces la media armónica, y por lo tanto el **micro-F1**, también será igual a este valor. Por tanto, en general se puede decir que:

![](https://i.imgur.com/Z1kZmPz.png)
                """
            }
        },
        "MULTILABEL": {
            "title": {
                languages.EN: "Multilabel evaluation methodology",
                languages.ES: "Metodología de evaluación multi-etiqueta"
            },
            "description": {
                languages.EN: """
For a sample of the type.

|       |    Animal | Person   |    empty    |
| ------| ----------| -------- | ----------  |
| image_id    | ${0, 1}$  |${0, 1}$  | $Animal \lor Person$|

The evaluation scenarios are as follows:


1. Global multiclase

    The performance of both `animal` and` person` classes is analyzed, example:
    
    | Y_true    |  Y_pred   | 
    | ------    | --------  | 
    | ![](https://i.imgur.com/dk07i9x.png)  | ![](https://i.imgur.com/i7jz8jQ.png)

    Several metrics can be used, in the example **precision** will be used.
    
    **Using macro averaging**, for a small sample, we have:
        
    Animali class
    $y_{true} = [0,0,0,0,1]$
    $y_{pred} = [1,1,0,0,1]$
    
    $TP = 1, FP = 2: precision=\\frac{1}{1+2}=1/3$
    
    Person class
    $y_{true} = [0,0,0,0,0]$
    $y_{pred} = [0,0,0,0,0]$
    
    $TP = 0, FP = 0: precision=\\frac{0}{0+0}=1$
    
    $Precision_{macro}= \\frac{1}{2}(1/3 +1) = 4/6$
        
2. In addition to the overview, the binary evaluation by class (`Animalia`,` Person`) is delivered, as well as for the empty tag (`empty`).
    The calculation of **precision**, for the sample previously presented, will have 2 values:
    
    for Animal:
    $y_{true} = [0,0,0,0,1]$
    $y_{pred} = [1,1,0,0,1]$
    
    $TP = 1, FP = 2: precision=\\frac{1}{1+2}=1/3$
    
    For Person:
    $y_{true} = [0,0,0,0,0]$
    $y_{pred} = [0,0,0,0,0]$
    
    $TP = 0, FP = 0: precision=\\frac{0}{0+0}=1$""",
                languages.ES: """
Para una muestra del tipo.

|       |    Animal | Person   |    empty    |
| ------| ----------| -------- | ----------  |
| image_id    | ${0, 1}$  |${0, 1}$  | $Animal \lor Person$|

Los escenarios de evaluación son los siguientes:


1. Global multiclase

    Se analiza el desempeño de ambas clases `animal` y `person`, ejemplo:
    
    | Y_true    |  Y_pred   | 
    | ------    | --------  | 
    | ![](https://i.imgur.com/dk07i9x.png)  | ![](https://i.imgur.com/i7jz8jQ.png)

    Se pueden utilizar varias métricas, en el ejemplo se utilizará **precision**. 
    
    **Utilizando el promediado macro**, para un muestra pequeña, se tiene:
        
    Clase Animalia
    $y_{true} = [0,0,0,0,1]$
    $y_{pred} = [1,1,0,0,1]$
    
    $TP = 1, FP = 2: precision=\\frac{1}{1+2}=1/3$
    
    Clase Person
    $y_{true} = [0,0,0,0,0]$
    $y_{pred} = [0,0,0,0,0]$
    
    $TP = 0, FP = 0: precision=\\frac{0}{0+0}=1$
    
    $Precision_{macro}= \\frac{1}{2}(1/3 +1) = 4/6$
        
2. Además del panorama general, se entrega la evaluación binaria por clase (`Animalia`, `Person`), así como para la etiqueta vacía (`empty`).
    El cálculo de **precision**, para la muestra presentada previamente, tendrá tendran 2 valores:
    
    Para Animalia:
    $y_{true} = [0,0,0,0,1]$
    $y_{pred} = [1,1,0,0,1]$
    
    $TP = 1, FP = 2: precision=\\frac{1}{1+2}=1/3$
    
    Para Person:
    $y_{true} = [0,0,0,0,0]$
    $y_{pred} = [0,0,0,0,0]$
    
    $TP = 0, FP = 0: precision=\\frac{0}{0+0}=1$"""
            }
        },
        "PASCAL_VOC": {
            "title": {
                languages.EN: "Object detection evaluation metrics concepts",
                languages.ES: "Conceptos de métricas de evaluación de detección de objetos"
            },
            "description": {
                languages.EN: """
A model usually produces a set of about 100 detections per image, where in general, most have a very small score value (<0.1) and are very likely to be false detections. Therefore, it is necessary to set a score threshold and consider only those detections that exceed this value.

In the case of camera traps photos, setting this threshold too high can mean that some species are difficult to find and a considerable part of the photos with animals are lost. On the other hand, if the threshold is set too low, there may be too many false detections and the model would not be helping to reduce the effort required to find the photos that interest us.

The relationship between the number of photos with animals that the model is capable of recovering and the number of photos that we have to review for it, can be measured with **precision** and **recovery** (recall).

![](https://i.imgur.com/mapxa0b.png)

An ideal model will retrieve all true objects (recall = 1) without generating false detections (precision = 1).

A Precision x Recall curve is a way of measuring the relationship between the precision and the recall of a model. The idea behind this method is to estimate the **impact** that only detections with a score greater than a certain threshold value will have.

Although traditional metrics for evaluating object detection models evaluate that the location of the boxes found is very similar to that of the boxes of the manual annotations, in this case it is not necessary to evaluate this, so the levels are simply analyzed score of the model detections for the construction of the Precision x Recall curves.

[In the following link](https://hackmd.io/@api-conabio-ml/BktTPdhQL) The concepts of object detection models are detailed.

### Methodology for calculating the Precision x Recall curve
This methodology does not take into account the location of the bounding boxes of the detections.
First, a `TP_FP` list is created where the True Positives (TP) and False Positives (FP) of all the images are accumulated, as follows:
- For each `image`:
    - For each category `cat`:
        - The actual groundtruth boxes (`groundtruth_boxes`), the detection boxes (` detected_boxes`) and the detection scores (`detected_scores`) of the category `cat` in the image `image` are obtained.
        - We discard all the boxes of `detected_boxes` except the one with the highest score, and we take one of `groundtruth_boxes` (if any), for the category `cat`.
        - If the `detected_box` corresponds to the `groundtruth_box`, the detection is accumulated as TP (a `True` is inserted) in `TP_FP [cat]`. If there was not at least one `groundtruth_box` for the category, a FP is accumulated (insert a `0`) in `TP_FP [cat]`, otherwise 1 is added to `num_total_gt [cat]` which will do later for the calculation of the FN of the category `cat`.

Then the precision and recall are calculated on the list of `TP_FP` of each category:
- For each category `cat`:
    - `TP_FP [cat]` is sorted in descending order based on the detection score.
    - A cumulative sum of True Positives and False Positives is performed. For example, if we have 10 detections and only the second is a True Positive, we would have the following:
        `true_positives` => `[False, True, False, False, False, False, False, False, False, False]`
        `false_positives` => `[True, False, True, True, True, True, True, True, True, True]`
        `cum_true_positives` => `[0, 1, 1, 1, 1, 1, 1, 1, 1, 1]`
        `cum_false_positives` => `[ 1.,  1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.]`
    - The precision and recall calculation is performed with these 'accumulated' arrays and the number of annotations of the category 'cat' with the following formulas:
        `precision = cum_true_positives / (cum_true_positives + cum_false_positives)`
        `recall = cum_true_positives / num_gt`
        Obtaining:
            `precision` => `[0., 0.5, 0.33, 0.25, 0.2, 0.167, 0.14, 0.125, 0.11, 0.1]`
            `recall` => `[0., 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25]`

[In the following link](https://hackmd.io/lpUAQgeWQLe55GQ46KhK-w?view#Ejemplo-ilustrativo) An illustrative example of the above procedure is shown.""",
                languages.ES: """
Un modelo suele producir un conjunto de unas 100 detecciones por imagen, donde por lo general, la mayoría tienen un valor de score muy pequeño (<0.1) y es muy probable que se trate de detecciones falsas. Por lo tanto, es necesario poner un umbral de score y considerar sólo aquéllas detecciones que superen este valor. 

Para el caso de las fototrampas, configurar este umbral demasiado alto puede suponer que sea difícil encontrar algunas especies y se pierda una parte considerable de las fotos con animales. Por otro lado, si se pone el umbral muy bajo, se pueden tener demasiadas detecciones falsas y el modelo no estaría ayudando a reducir el esfuerzo requerido para encontrar las fotos que nos interesan.

La relación entre la cantidad de fotos con animales que el modelo es capaz de recuperar y la cantidad de fotos que tenemos que revisar para ello, se puede medir con la **precisión** y la **recuperación** (recall).

![](https://i.imgur.com/mapxa0b.png)

Un modelo ideal recuperará todos los objetos verdaderos (recall=1) sin generar detecciones falsas (precisión=1).

Una curva de Precisión x Recall es una forma de medir la relación entre la precisión y la recuperación de un modelo. La idea detrás de este método es la de estimar el **impacto** que tendrá tomar únicamente las detecciones con un score mayor a un cierto valor de umbral.

Aunque las métricas tradicionales para evaluar los modelos de detección de objetos evalúan que la localización de los recuadros encontrados sea muy similar a la de los recuadros de las anotaciones manuales, en este caso no es necesario evaluar esto, por lo que simplemente se analizan los nivel de score de las detecciones del modelo para la construcción de las curvas de Precisión x Recall.

[En el siguiente enlace](https://hackmd.io/@api-conabio-ml/BktTPdhQL) se detallan los conceptos de los modelos de detección de objetos.

### Metodología para el cálculo de la curva de Precisión x Recall
Esta metodología no toma en cuenta la localización de los bounding boxes de las detecciones.
Primero se crea una lista `TP_FP` donde se van acumulando los Verdaderos Positivos (TP) y Falsos Positivos (FP) de todas las imágenes, de la siguiente manera:
- Para cada imagen `image`:
    - Para cada categoría `cat`:
        - Se obtienen los recuadros de las etiquetas reales (`groundtruth_boxes`), los recuadros de las detecciones (`detected_boxes`) y los scores de las detecciones (`detected_scores`) de la categoría `cat` en la imagen `image`.
        - Descartamos todos los recuadros de `detected_boxes` excepto el que tenga el score más alto, y tomamos uno de `groundtruth_boxes` (en caso de haber), para la categoría `cat`.
        - Si el `detected_box` corresponde con el `groundtruth_box`, la detección es acumulada como TP (se inserta un `True`) en `TP_FP[cat]`. Si no hubo al menos un `groundtruth_box` para la categoría, se acumula un FP (se inserta un `0`) en `TP_FP[cat]`, en caso contrario, se suma 1 a `num_total_gt[cat]`, que servirá posteriormente para el cálculo de los FN de la categoría `cat`.

Después se calcula la precisión y recall sobre la lista de `TP_FP` de cada categoría:
- Para cada categoría `cat`:
    - Se ordena `TP_FP[cat]` de forma descendente a partir del score de las detecciones.
    - Se realiza una suma acumulada de True Positives y False Positives. P.e., si tenemos 10 detecciones y únicamente la segunda es un True Positive, tendríamos lo siguiente:
        `true_positives` => `[False, True, False, False, False, False, False, False, False, False]`
        `false_positives` => `[True, False, True, True, True, True, True, True, True, True]`
        `cum_true_positives` => `[0, 1, 1, 1, 1, 1, 1, 1, 1, 1]`
        `cum_false_positives` => `[ 1.,  1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.]`
    - Se realiza el cálculo de la precisión y el recall con estos arreglos 'acumulados' y el número de anotaciones de la categoría `cat` con las siguientes fórmulas:
        `precision = cum_true_positives / (cum_true_positives + cum_false_positives)`
        `recall = cum_true_positives / num_gt`
        Obteniendo:
            `precision` => `[0., 0.5, 0.33, 0.25, 0.2, 0.167, 0.14, 0.125, 0.11, 0.1]`
            `recall` => `[0., 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25]`

[En el siguiente enlace](https://hackmd.io/lpUAQgeWQLe55GQ46KhK-w?view#Ejemplo-ilustrativo) se muestra un ejemplo ilustrativo del procedimiento anterior."""
            }
        },
        "CLASSIFICATION_LIKE": {
            "title": {
                languages.EN: "Object detection evaluation metrics concepts",
                languages.ES: "Conceptos de evaluación de métricas de detección de objetos"
            },
            "description": {
                languages.EN: """
A model usually produces a set of about 100 detections per image, where in general, most have a very small score value (<0.1) and are very likely to be false detections. Therefore, it is necessary to set a score threshold and consider only those detections that exceed this value.

In the case of camera traps photos, setting this threshold too high can mean that some species are difficult to find and a considerable part of the photos with animals are lost. On the other hand, if the threshold is set too low, there may be too many false detections and the model would not be helping to reduce the effort required to find the photos that interest us.

The relationship between the number of photos with animals that the model is capable of recovering and the number of photos that we have to review for it, can be measured with **precision** and **recovery** (recall).

![](https://i.imgur.com/mapxa0b.png)

An ideal model will retrieve all true objects (recall = 1) without generating false detections (precision = 1).

A Precision x Recall curve is a way of measuring the relationship between the precision and the recall of a model. The idea behind this method is to estimate the **impact** that only detections with a score greater than a certain threshold value will have.

Although traditional metrics for evaluating object detection models evaluate that the location of the boxes found is very similar to that of the boxes of the manual annotations, in this case it is not necessary to evaluate this, so the levels are simply analyzed score of the model detections for the construction of the Precision x Recall curves.

[In the following link](https://hackmd.io/@api-conabio-ml/BktTPdhQL) The concepts of object detection models are detailed.

### Methodology for calculating the Precision x Recall curve
This methodology does not take into account the location of the bounding boxes of the detections.
First, a `TP_FP` list is created where the True Positives (TP) and False Positives (FP) of all the images are accumulated, as follows:
- For each `image`:
    - For each category `cat`:
        - The actual groundtruth boxes (`groundtruth_boxes`), the detection boxes (` detected_boxes`) and the detection scores (`detected_scores`) of the category `cat` in the image `image` are obtained.
        - We discard all the boxes of `detected_boxes` except the one with the highest score, and we take one of `groundtruth_boxes` (if any), for the category `cat`.
        - If the `detected_box` corresponds to the `groundtruth_box`, the detection is accumulated as TP (a `True` is inserted) in `TP_FP [cat]`. If there was not at least one `groundtruth_box` for the category, a FP is accumulated (insert a `0`) in `TP_FP [cat]`, otherwise 1 is added to `num_total_gt [cat]` which will do later for the calculation of the FN of the category `cat`.

Then the precision and recall are calculated on the list of `TP_FP` of each category:
- For each category `cat`:
    - `TP_FP [cat]` is sorted in descending order based on the detection score.
    - A cumulative sum of True Positives and False Positives is performed. For example, if we have 10 detections and only the second is a True Positive, we would have the following:
        `true_positives` => `[False, True, False, False, False, False, False, False, False, False]`
        `false_positives` => `[True, False, True, True, True, True, True, True, True, True]`
        `cum_true_positives` => `[0, 1, 1, 1, 1, 1, 1, 1, 1, 1]`
        `cum_false_positives` => `[ 1.,  1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.]`
    - The precision and recall calculation is performed with these 'accumulated' arrays and the number of annotations of the category 'cat' with the following formulas:
        `precision = cum_true_positives / (cum_true_positives + cum_false_positives)`
        `recall = cum_true_positives / num_gt`
        Obtaining:
            `precision` => `[0., 0.5, 0.33, 0.25, 0.2, 0.167, 0.14, 0.125, 0.11, 0.1]`
            `recall` => `[0., 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25]`

[In the following link](https://hackmd.io/lpUAQgeWQLe55GQ46KhK-w?view#Ejemplo-ilustrativo) An illustrative example of the above procedure is shown.""",
                languages.ES: """
Un modelo suele producir un conjunto de unas 100 detecciones por imagen, donde por lo general, la mayoría tienen un valor de score muy pequeño (<0.1) y es muy probable que se trate de detecciones falsas. Por lo tanto, es necesario poner un umbral de score y considerar sólo aquéllas detecciones que superen este valor. 

Para el caso de las fototrampas, configurar este umbral demasiado alto puede suponer que sea difícil encontrar algunas especies y se pierda una parte considerable de las fotos con animales. Por otro lado, si se pone el umbral muy bajo, se pueden tener demasiadas detecciones falsas y el modelo no estaría ayudando a reducir el esfuerzo requerido para encontrar las fotos que nos interesan.

La relación entre la cantidad de fotos con animales que el modelo es capaz de recuperar y la cantidad de fotos que tenemos que revisar para ello, se puede medir con la **precisión** y la **recuperación** (recall).

![](https://i.imgur.com/mapxa0b.png)

Un modelo ideal recuperará todos los objetos verdaderos (recall=1) sin generar detecciones falsas (precisión=1).

Una curva de Precisión x Recall es una forma de medir la relación entre la precisión y la recuperación de un modelo. La idea detrás de este método es la de estimar el **impacto** que tendrá tomar únicamente las detecciones con un score mayor a un cierto valor de umbral.

Aunque las métricas tradicionales para evaluar los modelos de detección de objetos evalúan que la localización de los recuadros encontrados sea muy similar a la de los recuadros de las anotaciones manuales, en este caso no es necesario evaluar esto, por lo que simplemente se analizan los nivel de score de las detecciones del modelo para la construcción de las curvas de Precisión x Recall.

[En el siguiente enlace](https://hackmd.io/@api-conabio-ml/BktTPdhQL) se detallan los conceptos de los modelos de detección de objetos.

### Metodología para el cálculo de la curva de Precisión x Recall
Esta metodología no toma en cuenta la localización de los bounding boxes de las detecciones.
Primero se crea una lista `TP_FP` donde se van acumulando los Verdaderos Positivos (TP) y Falsos Positivos (FP) de todas las imágenes, de la siguiente manera:
- Para cada imagen `image`:
    - Para cada categoría `cat`:
        - Se obtienen los recuadros de las etiquetas reales (`groundtruth_boxes`), los recuadros de las detecciones (`detected_boxes`) y los scores de las detecciones (`detected_scores`) de la categoría `cat` en la imagen `image`.
        - Descartamos todos los recuadros de `detected_boxes` excepto el que tenga el score más alto, y tomamos uno de `groundtruth_boxes` (en caso de haber), para la categoría `cat`.
        - Si el `detected_box` corresponde con el `groundtruth_box`, la detección es acumulada como TP (se inserta un `True`) en `TP_FP[cat]`. Si no hubo al menos un `groundtruth_box` para la categoría, se acumula un FP (se inserta un `0`) en `TP_FP[cat]`, en caso contrario, se suma 1 a `num_total_gt[cat]`, que servirá posteriormente para el cálculo de los FN de la categoría `cat`.

Después se calcula la precisión y recall sobre la lista de `TP_FP` de cada categoría:
- Para cada categoría `cat`:
    - Se ordena `TP_FP[cat]` de forma descendente a partir del score de las detecciones.
    - Se realiza una suma acumulada de True Positives y False Positives. P.e., si tenemos 10 detecciones y únicamente la segunda es un True Positive, tendríamos lo siguiente:
        `true_positives` => `[False, True, False, False, False, False, False, False, False, False]`
        `false_positives` => `[True, False, True, True, True, True, True, True, True, True]`
        `cum_true_positives` => `[0, 1, 1, 1, 1, 1, 1, 1, 1, 1]`
        `cum_false_positives` => `[ 1.,  1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.]`
    - Se realiza el cálculo de la precisión y el recall con estos arreglos 'acumulados' y el número de anotaciones de la categoría `cat` con las siguientes fórmulas:
        `precision = cum_true_positives / (cum_true_positives + cum_false_positives)`
        `recall = cum_true_positives / num_gt`
        Obteniendo:
            `precision` => `[0., 0.5, 0.33, 0.25, 0.2, 0.167, 0.14, 0.125, 0.11, 0.1]`
            `recall` => `[0., 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25]`

[En el siguiente enlace](https://hackmd.io/lpUAQgeWQLe55GQ46KhK-w?view#Ejemplo-ilustrativo) se muestra un ejemplo ilustrativo del procedimiento anterior."""
            }
        }
    }
}
