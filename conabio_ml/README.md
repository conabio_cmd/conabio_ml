# Pipeline de experimentos

Un flujo típico de un experimento de Aprendizaje Automático para crear un dataset, ajustar y evaluar un modelo de visión por computadora usando el API conabio_ml podría ser el siguiente:

```python
# CREACIÓN Y CONFIGURACIÓN DEL DATASET
snmb_dataset = SNMB.create([ . . . ])
snmb_dataset.split([ . . . ])
snmb_dataset.to_tfrecords([ . . . ])

# CREACIÓN Y AJUSTE DEL MODELO
classification_model = ClassificationModel.load_saved_model([. . . ])
tf_slim_config = TFSlimConfig.create([. . . ])
ClassificationTrainer.train(dataset=snmb_dataset, 
							model=classification_model, 
							execution_config=tf_slim_config)

# EVALUACIÓN
prediction_dataset = classification_model.predict(dataset=snmb_dataset, 
												  prediction_config=prediction_config)
ClassificationEvaluator.eval(dataset_pred=prediction_dataset, 
							 dataset_true=snmb_dataset, 
							 eval_config=eval_config)
```

## Procesos

En el esquema del módulo Pipeline del API conabio_ml cada una de las instrucciones del ejemplo anterior son llamados **Procesos**, y puede realizar una o varias tareas:

- A partir de ciertos parámetros (**entradas**) crear instancias de nuevos objetos (**salidas**). Algunos ejemplos pueden ser los métodos create del dataset SNMB y del configurador de entorno `TFSlimConfig`, el método `load_saved_model` de `Model` o el método `predict` de `Model`.
- Modificar la información o el estado de estos objetos, como en el caso del método `split` de `Dataset` o `train` de `Trainer`.
- Generar información que puede ser persistente a partir de los objetos que se han generado y modificado. P. e., el método `to_tfrecords` de `Dataset`, el método `eval` de `Evaluator` y también el método `train` de `Trainer`.

Por lo anterior, podemos decir que cada `Proceso` tiene tres componentes:

- **Acción**: se trata de un método del API conabio_ml o cualquier función externa, y es fundamentalmente la operación que se ejecutará durante el proceso.
- **Entradas**: son los parámetros enviados a la acción que se va a ejecutar. Algunas de estas entradas pueden ser las salidas de otros procesos.
- **Salida**: es el valor que opcionalmente regresa una acción y puede servir de entrada a otros procesos.

En el ejemplo anterior se resalta que existen tres "etapas" principales en un experimento:

1. Creación y configuración del dataset.
2. Creación y ajuste del modelo.
3. Evaluación.

Generalmente las etapas de un experimento se deben ejecutar en este orden, pero no es obligatorio que en el pipeline estén presentes las tres. Se podría, por ejemplo, únicamente entrenar un modelo sin evaluarlo, o sólo descargar un dataset y almacenarlo en una carpeta.

## Pipeline

El módulo `Pipeline` del API conabio_ml proveé un administrador de los procesos de un experimento, el cual es útil para:

- Establecer el orden en que se ejecutan los **procesos** en un flujo de trabajo.
- Verificar la consistencia entre las **entradas** y **salidas** de los **procesos**.
- Registrar la secuencia y configuración de los **procesos** ejecutados para posibilitar la reproducibilidad de experimentos y la comparativa de métricas para cada configuración.

Además de validar que los Procesos tengan el tipo de entradas adecuadas, no se impone ninguna otra restricción en el orden o la manera en que se ejecutan estos.

### Creación del pipeline

La forma de crear una instancia de Pipeline es simplemente indicando el path donde se almacenarán los archivos generados e indicar si se quiere generar la imagen del grafo del pipeline, como se muestra a continuación:

```python
from conabio_ml.pipeline import Pipeline

pipe = Pipeline(path=test_path, draw_graph=False)
```

### Agregar procesos a un pipeline

Una vez creada la instancia del pipeline es necesario agregar los procesos en el orden en que se ejecutarán, a través del método de instancia `add_process`. A continuación se muestra un ejemplo que agrega cuatro procesos a un pipeline:

```python
pipe.add_process(
        name='create_dataset_snmb',
        action=SNMB.create,
        args={
            "dest_path": test_path,
            "version": "detection",
            "categories": ["bos", "aves", "homo"],
            "images_size": (800, 600)
        })\
    .add_process(
        name='split_dataset_snmb',
        action=ImageDataset.split,
        inputs_from_processes=['create_dataset_snmb'],
        args={
            "train_perc": 0.7,
            "test_perc": 0.3
        })\
    .add_process(
        name='load_object_detection_model',
        action=ObjectDetectionModel.load_saved_model,
        args={
            "source_path": checkpoint_path,
            "model_config": {
				[. . .]
            }
        })\
    .add_process(
        name='train_object_detection_model',
        action=ObjectDetectionTrainer.train,
        inputs_from_processes=[
            'create_dataset_snmb', 'load_object_detection_model'],
        args={
            "train_config": {
                [. . .]
            }
        })
```

Debido a que el método `add_process` regresa la instancia del pipeline, es posible "encadenar" varias llamadas a este método.

El método `add_process` recibe cuatro parámetros:

- `name`: es el nombre que recibe el proceso, y servirá para identificarlo cuando se quiera enviar la salida del proceso como entrada de otro proceso a través del parámetro `inputs_from_processes`. Dos procesos no pueden tener el mismo nombre.
- `action`: es la referencia al método o función de la acción que se quiere ejecutar.
- `inputs_from_processes`: se trata de una lista opcional de **nombres** de procesos agregados previamente al pipeline, de los cuáles se tomarán sus salidas para ser enviadas como entradas del proceso en la forma de parámetros posicionales del método (en el orden en que aparecen en la lista).
- `args`: se trata de un diccionario opcional con el resto de parámetros enviados al método en la forma de parámetros nombrados.

Este esquema presenta una dificultad, y es que hasta este punto sólo se ha definido la estructura de ejecución del pipeline y no se han creado las instancias de los objetos necesarios para poder realizar llamadas a **métodos de instancia** (p. e., el método `split` de un dataset), por lo que no es posible escribir de manera explícita la llamada de un método a través de una instancia. Sin embargo, Python tiene tiene una manera alternativa de hacer llamadas a métodos de instancia, y es a través del nombre de la clase y pasando la instancia como primer argumento (como si se tratara de la llamada a un método estático con la instancia del objeto como primer parámetro). Por ejemplo, las dos llamadas siguientes son equivalentes:

```python
instancia_de_objeto.metodo(argumentos)
Clase.metodo(instancia_de_objeto, argumentos)

```

De esta manera es posible encadenar la salida de un proceso y usar la instancia que regrese para realizar la llamada de alguno de sus métodos en otro proceso, únicamente asegurándose de enviar el nombre del primero proceso en la primera posición de la lista del parámetro `inputs_from_processes` del segundo proceso, como en la llamada del método `split` en el ejemplo anterior.

### Ejecución de un pipeline

Una vez que se agregaron todos los procesos de un pipeline y se desea iniciar su ejecución, simplemente se debe ejecutar el método run sobre la instancia del pipeline:

```python
pipe.run()
```

En ese momento se comenzará a validar en cada proceso que el tipo de entradas sean del tipo correcto, esto a través de las [anotaciones de funciones](https://www.python.org/dev/peps/pep-3107/ "anotaciones de funciones") que soportan las versiones de Python 3x, en las que es posible indicar el tipo en cada parámetro y el retorno de la función. Internamente se realiza la comparación de estos tipos antes de comenzar la ejecución del pipeline, para verificar la consistencia de las entradas y salidas entre procesos, y si al menos un parámetro no coincide con la entrada que recibe, se suspende la ejecución y se lanza una excepción indicando la inconsistencia de tipos.

En caso de que todas las entradas de los procesos sean del tipo correcto, se creará una carpeta dentro de la ruta especificada por `path` del constructor del Pipeline, con el nombre compuesto por la fecha y hora, y dentro de ésta carpeta se guardará el archivo JSON con la estructura del pipeline, con el fin de poder llevar el seguimiento del experimento. Después se comenzará con la ejecución de las acciones de cada proceso en el orden en que fueron agregadas, pasando las salidas generadas en cada proceso como entradas a los procesos correspondientes.

### Flujo de adquisición de procesos

Se presenta el diagrama de adquisición de procesos de un Pipeline.

![Diagrama de procesamiento de pipeline](https://bytebucket.org/conabio_cmd/conabio_ml/raw/95ec435bf03697d30b62cce745cff9d4a6e77081/docs/source/imgs/Diagrama_procesamiento_pipeline.png)

## Ejemplos

A continuación se muestra el de un pipeline de entrenamiento y predicción de un modelo de detección con un dataset de imágenes del SNMB:

```python
import os,sys
from conabio_ml.pipeline import Pipeline
from conabio_ml.datasets.dataset import Partitions
from conabio_ml.utils.dataset_utils import ImageDatasetTypes
from conabio_ml.datasets.images import SNMB, ImagePredictionDataset
from conabio_ml.trainer.images.model import ObjectDetectionModel
from conabio_ml.trainer.images.trainer import ObjectDetectionTrainer
from conabio_ml.trainer.images.trainer_config import \
    TFObjectDetectionAPITrainConfig

test_path = os.path.join("results", "train_snmb_1")
os.makedirs(test_path, exist_ok=True)
checkpoint_path = 'files/model.ckpt'

pipe = Pipeline(test_path, draw_graph=True)
pipe.add_process(
        name='create_dataset_snmb',
        action=SNMB.create,
        args={
            "dest_path": test_path,
            "version": ImageDatasetTypes.DETECTION,
            "categories": ["bos", "aves", "homo"],
            "multilabel": False,
            "download_images": True,
            "images_size": (800, 600),
            "unlabeled_images": False
        })\
    .add_process(
        name='split_dataset_snmb',
        action=SNMB.split,
        inputs_from_processes=['create_dataset_snmb'],
        args={
            "train_perc": 0.7,
            "test_perc": 0.2,
            "val_perc": 0.1,
            "max_secs_in_sequences": 2
        })\
    .add_process(
        name='to_tfrecords_dataset_snmb',
        action=SNMB.to_tfrecords,
        inputs_from_processes=['split_dataset_snmb'],
        args={
            "dest_path": os.path.join(test_path, 'tfrecords'),
            "num_shards": 5
        })\
    .add_process(
        name='create_tf_object_detection_api_config',
        action=TFObjectDetectionAPITrainConfig.create,
        args={
            "num_clones": 2,
            "clone_on_cpu": False
        })\
    .add_process(
        name='from_config_object_detection_model',
        action=ObjectDetectionModel.load_saved_model,
        args={
            "source_path": checkpoint_path,
            "model_config": {
                "faster_rcnn": {
                    [. . .]
                }
            }
        })\
    .add_process(
        name='train_object_detection_trainer',
        action=ObjectDetectionTrainer.train,
        inputs_from_processes=[
            'to_tfrecords_dataset_snmb', 'from_config_object_detection_model', 
            'create_tf_object_detection_api_config'],
        args={
            "train_config": {
                "train_dir": os.path.join(test_path, 'train'),
                "batch_size": 2,
                [. . .]
            }
        })\
    .add_process(
        name='apply_detection_model',
        action=ObjectDetectionModel.predict,
        inputs_from_processes=["train_object_detection_trainer", "create_dataset_snmb"],
        args={
            "batch_size": 2,
            "num_threads": 8,
            "partition": Partitions.TEST
        })

pipe.run()
```

A continuación se muestra una gráfico donde se ejemplifica el pipeline del ejemplo anterior:

![Grafo de pipeline de entrenamiento](https://bytebucket.org/conabio_cmd/conabio_ml/raw/95ec435bf03697d30b62cce745cff9d4a6e77081/docs/source/imgs/Grafo_ejemplo_entrenamiento_deteccion.png)

### Formato del archivo Pipeline

Con el fin de posibilitar el seguimiento de los experimentos es necesario generar un formato que almacene los datos más relevantes de los procesos de un pipeline, por lo que se propone generar un archivo JSON por cada experimento con la siguiente estructura:

```javascript
{
    "id": pipeline_id,
    "processes": [
        {	
            "name": process_name,
            "action": process_action,
            "inputs_from_processes": [name_of_process_1, name_of_process_2, . . . , name_of_process_n],
            "args": {
				"arg1": arg1_value,
				"arg2": arg2_value,
				. . .
			}
		}
		. . .
}

```

## Reproducción de experimentos

Es importante llevar un registro del código, la configuración y los datos con que se ejecutan los experimentos de Machine Learning, con el objetivo de comparar los resultados que se obtienen en cada evaluación y poder reproducir cualquiera de ellos.

Por ello es deseable que el API CONABIO ML tenga la capacidad de guardar la información relevante de un experimento, sin generar demasiada redundancia de la información.

Para el caso de los datos utilizados, es suficiente con guardar una representación del dataset utilizado el archivo CSV que representa al dataset, pero dado que a veces es redundante aplicar las operaciones de creación y transformación a un dataset, también será posible guardar los ítems ya transformados, con lo cual no sería necesario volver a transformarlos.

Se propone un esquema que permita ejecutar scripts que contengan experimentos de ML y que haga respaldos de la información que permita reproducir un experimento a través de una interfaz de línea de comandos que ofrezca lo siguiente:

- Inicializar un directorio que contendrá los respaldos de los experimentos. En dicho directorio se creará una carpeta (p.e. .conabio_ml_exps) en donde se creará una carpeta por cada experimento que se ejecute.
- El nombre de la carpeta para cada experimento se creará a partir de un identificador único y contendrá:
	1. Una copia del script que ejecutó el experimento.
	2. Un archivo CSV con la información del dataset utilizado (referencias a los ítems, etiquetas, etc.).
	3. El modelo generado en el entrenamiento (p.e. checkpoints).
	4. Archivos JSON que contengan las estadísticas resultantes en la evaluación.
	5. Un archivo que contenga datos del ambiente de ejecución (p.e. versión del paquete conabio_ml, versión de python, etc.).
	6. Archivos de configuración del experimento (p.e. hiperparámetros, gpus, workers, etc.).
	7. Opcionalmente la carpeta con los datos utilizados (p.e. imágenes)
	8. Un comando para listar los experimentos que se hayan generado en la carpeta actual y muestre información relevante de cada uno (p.e. Id, script que se ejecutó, estadísticas, configuraciones, fecha de creación, etc.)
- Un comando para volver a ejecutar un experimento por su Id.
- Un comando para eliminar un experimento por su Id.
- Un comando para copiar un experimento a una carpeta del sistema e indicar si se deben copiar los ítems o no.
- Se podría pensar en subir los experimentos a un repositorio y tener un tablero que los muestre y administre.
