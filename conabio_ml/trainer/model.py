#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pydash
import json

from conabio_ml.process import ProcessState

from conabio_ml.datasets.dataset import Dataset
from conabio_ml.trainer.predictor_config import PredictorConfig
from conabio_ml.trainer.trainer_config import TrainerConfig
from conabio_ml.utils.report_params import languages
from conabio_ml.assets import AssetTypes
from abc import ABCMeta, abstractmethod
from typing import List, TypeVar

from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)


class Model(ProcessState):
    """Represents a Model specification.

    Attributes
    ----------
    name: str
        Name of the model
    ModelType: type
        The type of process when is used in conabio_ml API
    """
    name = ""
    ModelType = TypeVar('ModelType', bound='Model')

    """
    CONSTRUCTOR
    """

    def __init__(self, name):
        """
        Parameters
        ----------
        name : str
            Name of the model
        """
        self.name = name

    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        ModelType
        """
        return cls.ModelType

    @classmethod
    @abstractmethod
    def load_saved_model(cls,
                         source_path: str,
                         model_config: dict = None,
                         pipeline_filepath: str = None,
                         model_name: str = None
                         ) -> ModelType:
        """Create an instance of a Model from a previously saved model and 
        either a `model_config` dictionary or a `pipeline_filepath` route.

        Parameters
        ----------
        source_path : str
            The path to a saved model. E.g. a checkpoint path.
        model_config : dict
            A dictionary that contains a model configuration.
            For a complete reference of the classification model parameters,
            follow:
            https://ecoinformatica.atlassian.net/wiki/spaces/CONML/pages/370343939/Model
        pipeline_filepath : str
            File path of the JSON that defines a filepath
        model_name : str
            Name of the model, it has to be defined inside the 
            pipeline filepath

        Returns
        -------
        Model
            Instance of the created model.
        """
        raise Exception("Method not implemented")

    @classmethod
    @abstractmethod
    def from_pipeline(cls: ModelType,
                      model_name: str,
                      pipeline_filepath: str,
                      saved_model_filepath: str = None) -> ModelType:
        """Creates an instance of a Model using the pipeline JSON file
        and optionally a checkpoint file

        Parameters
        ----------
        model_name: str
            Name of the model, it has to be defined inside the
            pipeline filepath
        pipeline_filepath : str
            File path of the JSON that defines a filepath
        saved_model_filepath: str, optional
            Destination folder of the previously deployed model

        Returns
        -------
        Model
            Instance of the created model.
        """
        raise Exception("Method not implemented")

    @classmethod
    @abstractmethod
    def create(cls,
               model_config: dict,
               **kwargs) -> ModelType:
        """Creates an instance of Model class with the name set.

        Parameters
        ----------
        model_config: dict
            Model definition needs the following structure

            Name of the Model : dict
                Name of the model

                layers: dict
                    Definition of all the layers the model will use

        Returns
        -------
        ModelType
          Instance of the created Model class
        """
        raise Exception("Method not implemented")

    @classmethod
    def assert_creation(cls,
                        model_config: dict) -> List:
        models = []
        assert type(model_config) is type({}),\
            "Model config must be a dictionary of " +\
            "Model_name: Config_params. \n" +\
            "See: "

        for k, val in model_config.items():
            assert 'layers' in val.keys(),\
                f"You must define layers of the model in"\
                f"{{k}} model. See layer definition in:"\
                f""

            models.append({"name": k,
                           "config": val})

        return models

    @abstractmethod
    def predict(self,
                dataset: Dataset.DatasetType,
                execution_config: PredictorConfig = None,
                prediction_config: dict = {}) -> ModelType:
        raise Exception("Method not implemented")

    @classmethod
    def get_model_config_from_pipeline(cls, filepath):
        with open(filepath, mode="r") as _file:
            pipeline = json.load(_file)

        for process in pipeline['processes']:
            if 'model_config' in process['args']:
                return process['args']['model_config']

        return None

    @abstractmethod
    def fit(self,
            dataset: Dataset,
            train_config: dict,
            execution_config: TrainerConfig) -> ModelType:
        """Template fot the minimum requirements to perform the process
        of fitting a model using a dataset

        Arguments:
            dataset {Dataset} -- Dataset to fit the model
            train_config {dict} -- Configuration to fit the model
            execution_config {TrainerConfig} -- Option of environment configuration
        """
        raise Exception("Method not implemented")

    def get_model_data(self, lang=languages.EN):
        """Gets the model data, that may contains: model_name, model_type, input_data, output_data

        Parameters
        ----------
        lang : languages, optional
            Language in which the model data will be obtained, by default languages.EN

        Returns
        -------
        dict
            Dictionary that contains the date of the model in format (model_name, model_type,
            input_data, output_data)
        """
        try:
            return {
                "model_name": self.report_data[self.name]["model_name"][lang],
                "model_type": self.report_data[self.name]["model_type"][lang],
                "input_data": self.report_data[self.name]["input_data"][lang],
                "output_data": self.report_data[self.name]["output_data"][lang]
            }
        except:
            logger.warning("No data to report for model")
            return {
                "model_name": None, "model_type": None, "input_data": None, "output_data": None
            }

    def reporter(self,
                 dest_path: str,
                 process_args: dict,
                 data_to_report: dict = None,
                 **kwargs):
        """Method that generates the report elements of a model. It Contains:
            - model_data: dictionary that contains the date of the model in format
            (model_name, model_type, input_data, output_data)

        Parameters
        ----------
        dest_path : str
            Path of the folder to store the resulting files
        process_args : dict
            The args of the process that wraps this class
        data_to_report : dict, optional
            The data to update. This params is passed to child instances.
            The param is read to check a possible overwrite proces of child instances,
            if it was overwritten just bypass the current process for THIS param.
            By default {}

        Returns
        -------
        dict
            Params to be reported in the Pipeline
        """
        data_to_report = {} if data_to_report is None else data_to_report
        lang = kwargs.get("lang", languages.EN)
        results = {
            "model_data": {
                "asset": self.get_model_data(lang=lang),
                "type": AssetTypes.DATA
            }
        }
        return {**results, **data_to_report}
