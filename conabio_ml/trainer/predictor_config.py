from abc import ABCMeta, abstractmethod
from typing import Type, TypeVar

class PredictorConfig():
    """Represents a configuration to be used in the predict method of a Model 
    subclass.

    Attributes
    ----------
    PredictorConfigType: type
        The type of process when is used in conabio_ml API
    """

    PredictorConfigType = TypeVar('PredictorConfigType', bound='PredictorConfig')

    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        PredictorConfigType
        """
        return cls.PredictorConfigType

    def __init__(self, **kwargs):
        pass
    