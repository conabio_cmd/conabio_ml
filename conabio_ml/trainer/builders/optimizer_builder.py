"""Functions to build Model training optimizers."""
import tensorflow as tf

from conabio_ml.utils.logger import get_logger

from conabio_ml.utils.utils import get_and_validate_args

logger = get_logger(__name__)

# Backends
TF_BACKEND = "tensorflow"
KERAS_BACKEND = "keras"
TF_KERAS_BACKEND = "tfkeras"

# Optimizer values
RMSPROP_OPT = 'rms_prop'
MOMENTUM_OPT = 'momentum'
ADAM_OPT = 'adam'
ADADELTA_OPT = 'adadelta'
ADAGRAD_OPT = 'adagrad'
FTRL_OPT = 'ftrl'
SGD_OPT = 'sgd'

# Learning rate values
CONSTANT_LR = 'constant'
EXPONENTIAL_LR = 'exponential'
POLYNOMIAL_LR = 'polynomial'
MANUAL_LR = 'manual_step'
COSINE_LR = 'cosine'
FIXED_LR = 'fixed'

# Loss functions

MEAN_SQUARED_ERROR = 'mean_squared_error'
MEAN_ABSOLUTE_ERROR = 'mean_absolute_error'
CATEGORICAL_CROSS_ENTROPY = 'categorical_crossentropy'
BINARY_CROSS_ENTROPY = 'binary_crossentropy'

# Validation for learning_rates, optimizers, and loss_functions
VALID_LEARNING_RATES = [
    CONSTANT_LR, EXPONENTIAL_LR, MANUAL_LR, COSINE_LR, FIXED_LR,
    POLYNOMIAL_LR]
VALID_OPTIMIZERS = [
    ADADELTA_OPT, ADAGRAD_OPT, ADAM_OPT, FTRL_OPT, MOMENTUM_OPT,
    RMSPROP_OPT, SGD_OPT]
VALID_LOSS_FUNCTIONS = [
    MEAN_SQUARED_ERROR, MEAN_ABSOLUTE_ERROR, CATEGORICAL_CROSS_ENTROPY,
    BINARY_CROSS_ENTROPY
]

# Default values
optimizers_args_def = {
    RMSPROP_OPT: {
        'type': dict,
        'optional': True
    },
    MOMENTUM_OPT: {
        'type': dict,
        'optional': True
    },
    ADAM_OPT: {
        'type': dict,
        'optional': True
    },
    ADADELTA_OPT: {
        'type': dict,
        'optional': True
    },
    ADAGRAD_OPT: {
        'type': dict,
        'optional': True
    },
    FTRL_OPT: {
        'type': dict,
        'optional': True
    },
    SGD_OPT: {
        'type': dict,
        'optional': True
    },
    'use_moving_average': {
        'type': bool,
        'default': True
    },
    'moving_average_decay': {
        'type': float,
        'default': 0.9999
    }
}
optimizer_args_def = {
    RMSPROP_OPT: {
        'learning_rate': {
            'type': dict,
            'optional': False
        },
        'momentum_optimizer_value': {
            'type': float,
            'default': 0.9
        },
        'decay': {
            'type': float,
            'default': 0.9
        },
        'epsilon': {
            'type': float,
            'default': 1.0
        },
        'rho': {
            'type': float,
            'default': 0.9
        }
    },
    MOMENTUM_OPT: {
        'learning_rate': {
            'type': dict,
            'optional': False
        },
        'momentum_optimizer_value': {
            'type': float,
            'default': 0.9
        }
    },
    ADAM_OPT: {
        'learning_rate': {
            'type': dict,
            'optional': False
        },
        'beta1': {
            'type': float,
            'default': 0.9
        },
        'beta2': {
            'type': float,
            'default': 0.999
        },
        'epsilon': {
            'type': float,
            'default': 1.0
        },
        'decay': {
            'type': float,
            'default': 0.0
        }
    },
    ADADELTA_OPT: {
        'learning_rate': {
            'type': dict,
            'optional': False
        },
        'rho': {
            'type': float,
            'default': 0.95
        },
        'epsilon': {
            'type': float,
            'default': 1.0
        },
        'decay': {
            'type': float,
            'default': 0.0
        }
    },
    ADAGRAD_OPT: {
        'learning_rate': {
            'type': dict,
            'optional': False
        },
        'epsilon': {
            'type': float,
            'default': 1.0
        },
        'decay': {
            'type': float,
            'default': 0.0
        },
        'initial_accumulator_value': {
            'type': float,
            'default': 0.1
        }
    },
    FTRL_OPT: {
        'learning_rate': {
            'type': dict,
            'optional': False
        },
        'learning_rate_power': {
            'type': float,
            'default': -0.5
        },
        'initial_accumulator_value': {
            'type': dict,
            'default': 0.1
        },
        'l1_regularization_strength': {
            'type': float,
            'default': 0.0
        },
        'l2_regularization_strength': {
            'type': float,
            'default': 0.0
        }
    },
    SGD_OPT: {
        'learning_rate': {
            'type': dict,
            'optional': False
        }
    }
}

learning_rates_args_def = {
    CONSTANT_LR: {
        'learning_rate': {
            'type': float,
            'default': 0.002
        }
    },
    FIXED_LR: {
        'learning_rate': {
            'type': float,
            'default': 0.002
        }
    },
    EXPONENTIAL_LR: {
        'initial_learning_rate': {
            'type': float,
            'default': 0.002
        },
        'decay_steps': {
            'type': int,
            'optional': False
        },
        'decay_factor': {
            'type': float,
            'default': 0.95
        },
        'staircase': {
            'type': bool,
            'default': True
        },
        'burnin_learning_rate': {
            'type': float,
            'default': 0.0
        },
        'burnin_steps': {
            'type': int,
            'default': 0
        },
        'burnin_epochs': {
            'type': int,
            'default': None
        },
        'min_learning_rate': {
            'type': float,
            'default': 0.0
        }
    },
    POLYNOMIAL_LR: {
        'initial_learning_rate': {
            'type': float,
            'default': 0.002
        },
        'decay_steps': {
            'type': int,
            'optional': False
        },
        'end_learning_rate': {
            'type': float,
            'default': 0.0001
        }
    },
    MANUAL_LR: {
        'initial_learning_rate': {
            'type': float,
            'default': 0.002
        },
        'schedule': {
            'type': list,
            'optional': True
        },
        'warmup': {
            'type': bool,
            'default': False
        }
    },
    COSINE_LR: {
        'learning_rate_base': {
            'type': float,
            'default': 0.002
        },
        'total_steps': {
            'type': int,
            'default': 4000000
        },
        'total_epochs': {
            'type': int,
            'default': None
        },
        'warmup_learning_rate': {
            'type': float,
            'default': 0.0002
        },
        'warmup_steps': {
            'type': int,
            'default': 10000
        },
        'warmup_epochs': {
            'type': int,
            'default': None
        },
        'hold_base_rate_steps': {
            'type': int,
            'default': 0
        },
        'hold_base_rate_epochs': {
            'type': int,
            'default': None
        }
    },
}

loss_function_args_def = {
    MEAN_SQUARED_ERROR: {
        'type': dict,
        'optional': True
    },
    MEAN_ABSOLUTE_ERROR: {
        'type': dict,
        'optional': True
    },
    CATEGORICAL_CROSS_ENTROPY: {
        'type': dict,
        'optional': True
    },
    BINARY_CROSS_ENTROPY: {
        'type': dict,
        'optional': True
    }
}


def manual_stepping(global_step, boundaries, rates, warmup=False):
    """Manually stepped learning rate schedule.

    This function provides fine grained control over learning rates.  One must
    specify a sequence of learning rates as well as a set of integer steps
    at which the current learning rate must transition to the next.  For example,
    if boundaries = [5, 10] and rates = [.1, .01, .001], then the learning
    rate returned by this function is .1 for global_step=0,...,4, .01 for
    global_step=5...9, and .001 for global_step=10 and onward.

    Parameters
    ----------
        global_step : int
            int64 (scalar) tensor representing global step.
        boundaries : list
            A list of global steps at which to switch learning rates.
            This list is assumed to consist of increasing positive integers.
        rates : list of float
            A list of (float) learning rates corresponding to intervals between
            the boundaries.  The length of this list must be exactly
            len(boundaries) + 1.
        warmup : bool
            Whether to linearly interpolate learning rate for steps in
            [0, boundaries[0]].

    Returns
    -------
    float
        A tensor representing learning rate.
    """
    if any([b < 0 for b in boundaries]) or any(
            [not isinstance(b, int) for b in boundaries]):
        raise ValueError('boundaries must be a list of positive integers')
    if any([bnext <= b for bnext, b in zip(boundaries[1:], boundaries[:-1])]):
        raise ValueError('Entries in boundaries must be strictly increasing.')
    if any([not isinstance(r, float) for r in rates]):
        raise ValueError('Learning rates must be floats')
    if len(rates) != len(boundaries) + 1:
        raise ValueError('Number of provided learning rates must exceed '
                         'number of boundary points by exactly 1.')

    if boundaries and boundaries[0] == 0:
        raise ValueError('First step cannot be zero.')

    if warmup and boundaries:
        slope = (rates[1] - rates[0]) * 1.0 / boundaries[0]
        warmup_steps = range(boundaries[0])
        warmup_rates = [rates[0] + slope * step for step in warmup_steps]
        boundaries = warmup_steps + boundaries
        rates = warmup_rates + rates[1:]
    else:
        boundaries = [0] + boundaries
    num_boundaries = len(boundaries)

    rate_index = tf.reduce_max(tf.where(
        tf.greater_equal(global_step, boundaries),
        list(range(num_boundaries)),
        [0] * num_boundaries))
    return tf.reduce_sum(rates * tf.one_hot(rate_index, depth=num_boundaries),
                         name='learning_rate')


def cosine_decay_with_warmup(global_step,
                             learning_rate_base,
                             total_steps,
                             warmup_learning_rate=0.0,
                             warmup_steps=0,
                             hold_base_rate_steps=0):
    """Cosine decay schedule with warm up period.

    Cosine annealing learning rate as described in:
        Loshchilov and Hutter, SGDR: Stochastic Gradient Descent with Warm Restarts.
        ICLR 2017. https://arxiv.org/abs/1608.03983
    In this schedule, the learning rate grows linearly from warmup_learning_rate
    to learning_rate_base for warmup_steps, then transitions to a cosine decay
    schedule.

    Args:
        global_step: int64 (scalar) tensor representing global step.
        learning_rate_base: base learning rate.
        total_steps: total number of training steps.
        warmup_learning_rate: initial learning rate for warm up.
        warmup_steps: number of warmup steps.
        hold_base_rate_steps: Optional number of steps to hold base learning rate
        before decaying.

    Returns:
        If executing eagerly:
        returns a no-arg callable that outputs the (scalar)
        float tensor learning rate given the current value of global_step.
        If in a graph:
        immediately returns a (scalar) float tensor representing learning rate.

    Raises:
        ValueError: if warmup_learning_rate is larger than learning_rate_base,
        or if warmup_steps is larger than total_steps.
    """
    if total_steps < warmup_steps:
        raise ValueError('total_steps must be larger or equal to '
                         'warmup_steps.')
    learning_rate = 0.5 * learning_rate_base * (1 + tf.cos(np.pi *
                                                           (tf.cast(global_step, tf.float32) - warmup_steps - hold_base_rate_steps
                                                            ) / float(total_steps - warmup_steps - hold_base_rate_steps)))
    if hold_base_rate_steps > 0:
        learning_rate = tf.where(
            global_step > warmup_steps + hold_base_rate_steps,
            learning_rate, learning_rate_base)
    if warmup_steps > 0:
        if learning_rate_base < warmup_learning_rate:
            raise ValueError('learning_rate_base must be larger or equal to '
                             'warmup_learning_rate.')
        slope = (learning_rate_base - warmup_learning_rate) / warmup_steps
        warmup_rate = slope * tf.cast(global_step,
                                      tf.float32) + warmup_learning_rate
        learning_rate = tf.where(global_step < warmup_steps, warmup_rate,
                                 learning_rate)
    return tf.where(global_step > total_steps, 0.0, learning_rate,
                    name='learning_rate')


def create_learning_rate(learning_rate_name, learning_rate_config, global_step):
    """Create optimizer learning rate based on config.

    Parameters
    ----------
    learning_rate_name : str
        Learning rate name.
    learning_rate_config : dict
        A learning rate dictionary configuration.
    global_step: `Tensor`
        The global_step tensor.

    Returns
    -------
    `Tensor`
        A learning rate.
    """

    learning_rate = None
    if learning_rate_name in (CONSTANT_LR, FIXED_LR):
        learning_rate = tf.constant(
            learning_rate_config['learning_rate'],
            dtype=tf.float32,
            name='learning_rate')
    elif learning_rate_name == EXPONENTIAL_LR:
        learning_rate = tf.train.exponential_decay(learning_rate_config['initial_learning_rate'],
                                                   global_step,
                                                   learning_rate_config['decay_steps'],
                                                   learning_rate_config['decay_factor'],
                                                   learning_rate_config['staircase'],
                                                   name='learning_rate')
    elif learning_rate_name == MANUAL_LR:
        if not learning_rate_config['schedule']:
            raise ValueError('Empty learning rate schedule.')
        learning_rate_step_boundaries = [x['step']
                                         for x in learning_rate_config['schedule']]
        learning_rate_sequence = [
            learning_rate_config['initial_learning_rate']]
        learning_rate_sequence += [x['learning_rate']
                                   for x in learning_rate_config['schedule']]
        learning_rate = manual_stepping(global_step,
                                        learning_rate_step_boundaries,
                                        learning_rate_sequence,
                                        learning_rate_config['warmup'])
    elif learning_rate_name == COSINE_LR:
        learning_rate = learning_schedules.cosine_decay_with_warmup(
            global_step,
            learning_rate_config['learning_rate_base'],
            learning_rate_config['total_steps'],
            learning_rate_config['warmup_learning_rate'],
            learning_rate_config['warmup_steps'],
            learning_rate_config['hold_base_rate_steps'])
    elif learning_rate_name == POLYNOMIAL_LR:
        learning_rate = tf.train.polynomial_decay(learning_rate_config['initial_learning_rate'],
                                                  global_step,
                                                  learning_rate_config['decay_steps'],
                                                  learning_rate_config['end_learning_rate'],
                                                  power=1.0,
                                                  cycle=False,
                                                  name='learning_rate')

    if learning_rate is None:
        raise ValueError(f'Learning_rate {learning_rate_name} not supported.')

    return learning_rate


def create_optimizer(optimizer_name, optimizer_config, learning_rate):
    """Create optimizer based on config.

    Parameters
    ----------
    optimizer_name : str
        Learning rate name.
    optimizer_config : dict
        A learning rate dictionary configuration.
    learning_rate: `Tensor`
        The learning rate tensor.

    Returns
    -------
    `Tensor`
        A optimizer.
    """

    optimizer = None
    if optimizer_name == ADADELTA_OPT:
        optimizer = tf.train.AdadeltaOptimizer(
            learning_rate,
            rho=optimizer_config['rho'],
            epsilon=optimizer_config['epsilon'])
    elif optimizer_name == ADAGRAD_OPT:
        optimizer = tf.train.AdagradOptimizer(
            learning_rate,
            initial_accumulator_value=optimizer_config['initial_accumulator_value'])
    elif optimizer_name == ADAM_OPT:
        optimizer = tf.train.AdamOptimizer(
            learning_rate,
            beta1=optimizer_config['beta1'],
            beta2=optimizer_config['beta2'],
            epsilon=optimizer_config['epsilon'])
    elif optimizer_name == FTRL_OPT:
        optimizer = tf.train.FtrlOptimizer(
            learning_rate,
            learning_rate_power=optimizer_config['learning_rate_power'],
            initial_accumulator_value=optimizer_config['initial_accumulator_value'],
            l1_regularization_strength=optimizer_config['l1_regularization_strength'],
            l2_regularization_strength=optimizer_config['l2_regularization_strength'])
    elif optimizer_name == MOMENTUM_OPT:
        optimizer = tf.train.MomentumOptimizer(
            learning_rate,
            momentum=optimizer_config['momentum_optimizer_value'])
    elif optimizer_name == RMSPROP_OPT:
        optimizer = tf.train.RMSPropOptimizer(
            learning_rate,
            decay=optimizer_config['decay'],
            momentum=optimizer_config['momentum_optimizer_value'],
            epsilon=optimizer_config['epsilon'])
    elif optimizer_name == SGD_OPT:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate)

    if optimizer is None:
        raise ValueError(f'Optimizer {optimizer_name} not supported.')

    return optimizer

# Implementations for plain Keras


def create_keras_learning_rate(learning_rate_name,
                               learning_rate_config,
                               backend):
    """Create optimizer learning rate based on config.

    Parameters
    ----------
    learning_rate_name : str
        Learning rate name.
    learning_rate_config : dict
        A learning rate dictionary configuration.

    Returns
    -------
    `Tensor`
        A learning rate.
    """
    backend_imports(backend)

    learning_rate = None
    if learning_rate_name in (CONSTANT_LR, FIXED_LR):
        learning_rate = learning_rate_config['learning_rate']
    else:
        raise ValueError(f'Learning_rate {learning_rate_name} not supported.')

    return learning_rate


def create_keras_optimizer(optimizer_name,
                           optimizer_config,
                           learning_rate,
                           backend):
    """ Creates an optimizer based on config attributes.

    Parameters
    ----------
    optimizer_name : str
        Optimizer name
    optimizer_config : dict
        Configuration params of the optimizer
    learning_rate : float | learning_rate
        Learning rate used for the optimizer used.
        It could be a simple value of the learning rate or a
        created learning rate with `create_keras_learning_rate` function

    Returns
    -------
    `Tensor`
        A optimizer.
    """

    optimizers, _ = backend_imports(backend)

    optimizer = None
    clipnorm = 1.
    clipvalue = 0.5

    if optimizer_name == ADADELTA_OPT:
        optimizer = optimizers.Adadelta(lr=learning_rate,
                                        rho=optimizer_config['rho'],
                                        epsilon=optimizer_config['epsilon'],
                                        decay=optimizer_config['decay'])
    elif optimizer_name == ADAGRAD_OPT:
        optimizer = optimizers.Adagrad(lr=learning_rate,
                                       epsilon=optimizer_config['epsilon'],
                                       decay=optimizer_config['decay'])
        optimizer = optimizers.Adagrad(learning_rate=learning_rate)
    elif optimizer_name == ADAM_OPT:
        optimizer = optimizers.Adam(lr=learning_rate,
                                    beta_1=optimizer_config['beta1'],
                                    beta_2=optimizer_config['beta2'],
                                    epsilon=optimizer_config['epsilon'],
                                    decay=optimizer_config['decay'])
    elif optimizer_name == RMSPROP_OPT:
        optimizer = optimizers.RMSprop(lr=learning_rate,
                                       rho=optimizer_config['rho'],
                                       epsilon=optimizer_config['epsilon'],
                                       decay=optimizer_config['decay'],
                                       clipnorm=clipnorm,
                                       clipvalue=clipvalue)
    elif optimizer_name == SGD_OPT:
        momentum = 0.0 or optimizer_config['momentum_optimizer_value']

        optimizer = optimizers.SGD(lr=learning_rate,
                                   momentum=momentum,
                                   nesterov=False,
                                   clipnorm=clipnorm,
                                   clipvalue=clipvalue)

    if optimizer is None:
        raise ValueError(f'Optimizer {optimizer_name} not supported.')

    return optimizer


def create_keras_loss_function(loss_function_name,
                               loss_function_config,
                               backend):
    # TODO: Actually we are currently not supportin loss config
    """ Creates an optimizer based on config attributes.

    Parameters
    ----------
    loss_function_name : str
        Optimizer name
    loss_function_config : dict
        Configuration params of the optimizer

    Returns
    -------
    `Tensor`
        A optimizer.
    """
    _, losses = backend_imports(backend)

    loss = None

    if loss_function_name == MEAN_SQUARED_ERROR:
        loss = losses.mean_squared_error
    elif loss_function_name == MEAN_ABSOLUTE_ERROR:
        loss = losses.mean_absolute_error
    elif loss_function_name == CATEGORICAL_CROSS_ENTROPY:
        loss = losses.categorical_crossentropy
    elif loss_function_name == BINARY_CROSS_ENTROPY:
        loss = losses.binary_crossentropy

    if loss is None:
        raise ValueError(f'Loss_function {loss_function_name} not supported.')

    return loss

# Top functions to create optimizers and loss functions


def build_optimizer(optimizer_config,
                    global_step,
                    backend=TF_BACKEND):
    """Create optimizer based on config.

    Parameters
    ----------
    optimizer_config : dict
        Dictionary that contains optimizer configuration.
        For a complete reference of the optimizer parameters, please follow:
        https://ecoinformatica.atlassian.net/wiki/spaces/CONML/pages/370376715/
    global_step : `Variable`
        Variable to increment by one after the gradients have been applied.

    Returns
    -------
    (`Tensor`, `Tensor`)
        A tuple of tensors that represents the created optimizer and
        learning rate.
    """
    optimizer_name = [x for x in optimizer_config.keys()
                      if x in VALID_OPTIMIZERS][0]
    optimizer_config = get_and_validate_args(optimizer_config,
                                             optimizers_args_def)

    if 'learning_rate' not in optimizer_config[optimizer_name]:
        raise ValueError("You must especify a learning rate configuration")

    optimizer_config = get_and_validate_args(optimizer_config[optimizer_name],
                                             optimizer_args_def[optimizer_name])
    lr_name = [x for x in optimizer_config['learning_rate'].keys()
               if x in VALID_LEARNING_RATES][0]
    learning_rate_config = get_and_validate_args(
        optimizer_config['learning_rate'][lr_name],
        learning_rates_args_def[lr_name])

    if backend == TF_BACKEND:
        learning_rate = create_learning_rate(
            lr_name,
            learning_rate_config,
            global_step)
        optimizer = create_optimizer(
            optimizer_name,
            optimizer_config,
            learning_rate)
    elif backend == KERAS_BACKEND or backend == TF_KERAS_BACKEND:
        learning_rate = create_keras_learning_rate(
            lr_name,
            learning_rate_config,
            backend)
        optimizer = create_keras_optimizer(
            optimizer_name,
            optimizer_config,
            learning_rate,
            backend)
    else:
        raise ValueError("You must specify an available backend end")

    return optimizer, learning_rate


def build_loss_function(loss_function_config,
                        backend=TF_BACKEND):
    """Creates a loss function based on config attributes

    Parameters
    ----------
    loss_function_config : dict
        Dictionary that contains loss_function configuration.
    backend: str
        Backend used to build the loss function

    Returns
    -------
    `Tensor`
        A tensor that represents the loss function
    """
    try:
        loss = None

        loss_function_name = [x for x in loss_function_config.keys()
                              if x in VALID_LOSS_FUNCTIONS][0]
        loss_function_config = get_and_validate_args(loss_function_config, loss_function_args_def)

        if backend == TF_BACKEND:
            pass
        elif backend == KERAS_BACKEND or backend == TF_KERAS_BACKEND:
            loss = create_keras_loss_function(loss_function_name, "", backend)
        else:
            msg = "A backend value must be specified"
            logger.exception(f"{msg}")
            raise ValueError(msg)

        return loss
    except Exception as ex:
        logger.exception(f"{ex}")
        raise(ex)


def backend_imports(backend: str):
    """
    Manages a keras dependencies for optimizers and losses.
    When a tf.keras backend is selected, the modules are obtained by tf.keras, otherwise,
    are obtained from keras


    Arguments:
        backend {str} -- Backend to obtain the modules
    """
    if backend == KERAS_BACKEND:
        import keras
        from keras import optimizers, losses
    elif backend == TF_KERAS_BACKEND:
        from tensorflow.keras import optimizers, losses

    return optimizers, losses
