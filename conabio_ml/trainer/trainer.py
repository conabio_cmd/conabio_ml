#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import TypeVar, Any

from conabio_ml.process import ProcessState
from conabio_ml.datasets.dataset import Dataset
from conabio_ml.trainer.model import Model
from conabio_ml.trainer.trainer_config import TrainerConfig

from abc import abstractmethod


class Trainer(ProcessState):
    """
    Base class that outlines the load options of a dataset to perform
    a training process.

    Attributes
    ----------
    data : Any
        Data to perform the training process.
        It's defined from the process previously chained.
    model : ModelType
        Model to be trained. 
        It need to be wrapped in a Model class
        See: 
    execution_config : TrainerConfigType
        Configuration to perform the training.
        All params to train the model needs to be wrapped in TrainerConfig class
        See: 
    train_config : dict 
        Options to define the training params of the model
        See: 
    """

    TrainerType = TypeVar('TrainerType', bound='Trainer')

    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        TrainerType
        """
        return cls.TrainerType

    @classmethod
    @abstractmethod
    def train(cls,
              data: Dataset.DatasetType,
              model: Model.ModelType,
              execution_config: TrainerConfig.TrainerConfigType,
              train_config: dict = {}) -> Model.ModelType:
        """
        Abstract method to set resources needed to perform a training of a model

        Parameters
        ----------
        data : Dataset:DatasetType
            Dataset to perform the training process.
        model : ModelType
            Model to be trained. 
        execution_config : TrainerConfigType
            Configuration of the training process.
        train_config : dict
            Training options

        Returns
        -------
        ModelType
          Instance of the created Model class

        """
        raise Exception("Not implemented method")
