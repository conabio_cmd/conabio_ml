# Especificación para entrenamientos de modelos de Imágenes

## Especificación para configuración de entrenamiento de modelos de clasificación

```
train_config {
    train_dir : str, optional
        Directory to save the checkpoints and training summaries. (default is '.')
    batch_size : int, optional
        Effective batch size to use for training.
        (default is 32)
    checkpoint_exclude_scopes : str, optional
        Comma-separated list of scopes of variables to exclude when restoring
        from a checkpoint. 
        (default is None)
    trainable_scopes : str, optional
        Comma-separated list of scopes to filter the set of variables to train.
        By default, None would train all the variables.
        (default is None)
    num_steps : int, optional
        Number of steps to train the DetectionModel or ClassificationModel for. 
        If 0, will train the model indefinitely. 
        In image classification is also known as `max_number_of_steps`.
        (default is 0)
    num_epochs : int, optional
        Number of epochs to train the DetectionModel or ClassificationModel for.
        If 0, will train the model indefinitely. 
        This option overwrites `num_steps`.
        (default is 0)
    dataset_partition: TRAIN|TEST|VALIDATION, optional
        The name of the dataset partition for training. (default is TRAIN)
    optimizer : Optimizer
        Optimizer configuration used to train the Model.
}
```
## Especificación para configuración de entrenamiento de modelos de detección

```
train_config {
    train_dir : str, optional
        Directory to save the checkpoints and training summaries. (default is '.')
    batch_size : int, optional
        Effective batch size to use for training.
        (default is 1)
    load_all_detection_checkpoint_vars : bool, optional
        Whether to load all checkpoint vars that match model variable names and
        (default is False)
    num_steps : int, optional
        Number of steps to train the DetectionModel or ClassificationModel for. 
        If 0, will train the model indefinitely. 
        In image classification is also known as `max_number_of_steps`.
        (default is 0)
    num_epochs : int, optional
        Number of epochs to train the DetectionModel or ClassificationModel for.
        If 0, will train the model indefinitely. 
        This option overwrites `num_steps`.
        (default is 0)
    dataset_partition: TRAIN|TEST|VALIDATION, optional
        The name of the dataset partition for training. (default is TRAIN)
    data_augmentation_options : list of PreprocessingStep
        Data augmentation options.
    gradient_clipping_by_norm : float, optional
        If greater than 0, clips gradients by this value. 
        (default is 0.0)
    dropout_keep_probability : float, optional
        Keep probability for dropout. If the value is 1.0 dropout is disabled.
        This option overwrites `use_dropout` and `dropout_keep_probability` in the 
        box predictor model configuration.
        (default is 1.0)
    optimizer : Optimizer
        Optimizer configuration used to train the Model.
    startup_delay_steps : int, optional
        Number of training steps between replica startup.
        This flag must be set to 0 if sync_replicas is set to true. 
        (default is 15)
    bias_grad_multiplier : float, optional
        If greater than 0, multiplies the gradient of bias variables by this
        amount. 
        (default is 0.0)
    update_trainable_variables : list of str
        Variables that should be updated during training. Note that variables which
        also match the patterns in freeze_variables will be excluded.
    freeze_variables : list of str
        Variables that should not be updated during training. 
    merge_multiple_label_boxes : bool, optional
        If true, boxes with the same coordinates will be merged together.
        (default is False)
    use_multiclass_scores : bool, optional
        If true, will use multiclass scores from object annotations as ground
        truth. 
        (default is False)
    add_regularization_loss : bool, optional
        Whether to add regularization loss to `total_loss`. 
        (default is True)
    unpad_groundtruth_tensors : bool, optional
        Whether to remove padding along `num_boxes` dimension of the groundtruth
        tensors. 
        (default is True)
    retain_original_images : bool, optional
        Whether to retain original images (i.e. not pre-processed) in the tensor
        dictionary, so that they can be displayed in Tensorboard. 
        (default is False)
    use_bfloat16 : bool, optional
        Whether to use bfloat16 for training. This is currently only supported for
        TPUs. 
        (default is False)
    summarize_gradients : bool, optional
        Whether to summarize gradients. 
        (default is False)
}
```

## Especificación para configuración de las opciones para ambos modelos

```
PreprocessingStep {
    oneof {
        normalize_image : NormalizeImage
            Normalizes pixel values in the image.
            Moves the pixel values from the current [original_minval, original_maxval]
            range to a the [target_minval, target_maxval] range.
        random_horizontal_flip : RandomHorizontalFlip
            Randomly flips the image and detections horizontally.
        random_pixel_value_scale : RandomPixelValueScale
            Scales each value in the pixels of the image.
        random_image_scale : RandomImageScale
            Scales the image size.
        random_rgb_to_gray : RandomRGBtoGray
            Changes the image from RGB to Grayscale with the given probability.
        random_adjust_brightness : RandomAdjustBrightness
            Randomly adjusts brightness.
        random_adjust_contrast : RandomAdjustContrast
            Randomly adjusts contrast.
        random_adjust_hue : RandomAdjustHue
            Randomly adjusts hue.
        random_adjust_saturation : RandomAdjustSaturation
            Randomly adjusts saturation.
        random_distort_color : RandomDistortColor
            Performs a random color distortion. color_orderings should either be 0 or 1.
        random_jitter_boxes : RandomJitterBoxes
            Randomly jitter boxes in image.
        random_crop_image : RandomCropImage
            Randomly crops the image.
        random_pad_image : RandomPadImage
            Randomly pads the image.
        random_crop_pad_image : RandomCropPadImage
            Randomly crops and pads the image.
        random_crop_to_aspect_ratio : RandomCropToAspectRatio
            Randomly crops an image to the specified aspect ratio.
        random_black_patches : RandomBlackPatches
            Randomly adds some black patches to the image.
        random_resize_method : RandomResizeMethod
            Uses a random resize method to resize the image to target size.
        scale_boxes_to_pixel_coordinates : ScaleBoxesToPixelCoordinates
            Scales boxes from normalized to pixel coordinates.
        resize_image : ResizeImage
            Resizes images to the given height and width.
        subtract_channel_mean : SubtractChannelMean
            Normalizes an image by subtracting a mean from each channel.
        ssd_random_crop : SSDRandomCrop
            Random crop preprocessing with default parameters as in SSD paper.
        ssd_random_crop_pad : SSDRandomCropPad
            Random crop preprocessing with default parameters as in SSD paper.
        ssd_random_crop_fixed_aspect_ratio : SSDRandomCropFixedAspectRatio
            Random crop preprocessing with default parameters as in SSD paper.
        ssd_random_crop_pad_fixed_aspect_ratio : SSDRandomCropPadFixedAspectRatio
            Random crop and pad preprocessing with default parameters as in SSD paper.
        random_vertical_flip : RandomVerticalFlip
            Randomly flips the image and detections vertically.
        random_rotation90 : RandomRotation90
            Randomly rotates the image and detections 90 degrees counter-clockwise.
        rgb_to_gray : RGBtoGray
            Converts a 3 channel RGB image to a 1 channel grayscale image.
        convert_class_logits_to_softmax : ConvertClassLogitsToSoftmax
            Converts multiclass logits to softmax scores after applying temperature.
        random_absolute_pad_image : RandomAbsolutePadImage
            Randomly pads the image by small absolute amounts.
        random_self_concat_image : RandomSelfConcatImage
            Randomly concatenates the image with itself.
    }
}

NormalizeImage {
    original_minval : float
        Current image minimum value.
    original_maxval : float
        Current image maximum value.
    target_minval : float
        Target image minimum value. (default is 0)
    target_maxval : float
        Target image maximum value. (default is 1)
}

RandomHorizontalFlip {
    keypoint_flip_permutation : list of int
        Specifies a mapping from the original keypoint indices to horizontally
        flipped indices. 
}

RandomPixelValueScale { 
    minval : float
        Lower ratio of scaling pixel values. (default is 0.9)
    maxval : float
        Upper ratio of scaling pixel values. (default is 1.1)
}

RandomImageScale {
    min_scale_ratio : float
        Minimum scaling ratio. (default is 0.5)
    max_scale_ratio : float
        Maximum scaling ratio. (default is 2.0)
}

RandomRGBtoGray {
    probability : float
        The probability of returning a grayscale image.
        The probability should be a number between [0, 1]. (default is 0.1)
}

RandomAdjustBrightness {
    max_delta : float
        How much to change the brightness. A value between [0, 1). 
        (default is 0.2)
}

RandomAdjustContrast {
    min_delta : float
        See max_delta. (default is 0.8)
    max_delta : float
        How much to change the contrast. Contrast will change with a
        value between min_delta and max_delta. This value will be
        multiplied to the current contrast of the image. (default is 1.25)
}

RandomAdjustHue {
    max_delta : float
        Change hue randomly with a value between 0 and max_delta. 
        (default is 0.02)
}

RandomAdjustSaturation {
    min_delta : float
        See max_delta. (default is 0.8)
    max_delta : float 
        How much to change the saturation. Saturation will change with a
        value between min_delta and max_delta. This value will be
        multiplied to the current saturation of the image. (default is 1.25)
}

RandomDistortColor {
    color_ordering : int
        A type of distortion (valid values: 0, 1).
}

RandomJitterBoxes {
    ratio : float
        The ratio of the box width and height that the corners can jitter.
        (default is 0.05)
}

RandomCropImage {
    min_object_covered : float
        Cropped image must cover at least one box by this fraction.
        (default is 1.0)
    min_aspect_ratio : float
        Max aspect ratio bounds of cropped image. (default is 0.75)
    max_aspect_ratio : float
        Min aspect ratio bounds of cropped image. (default is 1.33)
    min_area : float
        Allowed area ratio of cropped image to original image. (default is 0.1)
    max_area : float
        Allowed area ratio of cropped image to original image. (default is 0.1)
    overlap_thresh : float
        Minimum overlap threshold of cropped boxes to keep in new image. If the
        ratio between a cropped bounding box and the original is less than this
        value, it is removed from the new image. (default is 0.3)
    clip_boxes : bool
        Whether to clip the boxes to the cropped image. (default is True)
    random_coef : float
        Probability of keeping the original image. (default is 0.0)
}

RandomPadImage {
    min_image_height : int
        Minimum dimensions for padded image.
    min_image_width : int
        Minimum dimensions for padded image.
    max_image_height : int
        Maximum dimensions for padded image. 
    max_image_width : int
        Maximum dimensions for padded image. 
    pad_color : list of float
        Color of the padding. If unset, will pad using average color of the input
        image.
}

RandomCropPadImage {
    min_object_covered : float
        Cropping operation must cover at least one box by this fraction.
        (default is 1.0)
    min_aspect_ratio : float
        Aspect ratio bounds of image after cropping operation.
        (default is 0.75)
    max_aspect_ratio : float
        Aspect ratio bounds of image after cropping operation.
        (default is 1.33)
    min_area : float
        Allowed area ratio of image after cropping operation. (default is 0.1)
    max_area: float
        Allowed area ratio of image after cropping operation. (default is 0.1)
    overlap_thresh : float
        Minimum overlap threshold of cropped boxes to keep in new image. 
        (default is 0.3)
    clip_boxes : bool
        Whether to clip the boxes to the cropped image. (default is True)
    random_coef : float
        Probability of keeping the original image during the crop operation.
        (default is 0.0)
    min_padded_size_ratio : list of float
        Maximum dimensions for padded image. This field should be
        length 2.
    max_padded_size_ratio : list of float
        Maximum dimensions for padded image. This field should be
        length 2.
    pad_color : list of float
        Color of the padding. This field should be of length 3.
}

RandomCropToAspectRatio {
    aspect_ratio : float
        Aspect ratio. (default is 1.0)
    overlap_thresh : float
        Minimum overlap threshold of cropped boxes to keep in new image. If the
        ratio between a cropped bounding box and the original is less than this
        value, it is removed from the new image. (default is 0.3)
    clip_boxes : bool
        Whether to clip the boxes to the cropped image. (default is True)
}

RandomBlackPatches {
    max_black_patches : int
        The maximum number of black patches to add. (default is 10)
    probability : float
        The probability of a black patch being added to an image.
        (default is 0.5)
    size_to_image_ratio : float
        Ratio between the dimension of the black patch to the minimum dimension of
        the image (patch_width = patch_height = min(image_height, image_width)).
        (default is 0.1)
}

RandomResizeMethod {
    target_height : int
        Target height
    target_width : int
        Target width
}

ScaleBoxesToPixelCoordinates { 

}

ResizeImage {
    new_height : int
        Desired height of the image.
    new_width : int
        Desired width of the image.
    method : AREA|BICUBIC|BILINEAR|NEAREST_NEIGHBOR, optional
        interpolation method used in resizing. (default is BILINEAR)
}

SubtractChannelMean {
    means : list of float
        The mean to subtract from each channel. Should be of same dimension of
        channels in the input image.
}

SSDRandomCrop {
    operations : list of SSDRandomCropOperation
        List of operations.
}

SSDRandomCropOperation {
    min_object_covered : float
        Cropped image must cover at least this fraction of one original bounding
        box.
    min_aspect_ratio : float
        The aspect ratio of the cropped image must be within the range of
        [min_aspect_ratio, max_aspect_ratio].
    max_aspect_ratio : float
        The aspect ratio of the cropped image must be within the range of
        [min_aspect_ratio, max_aspect_ratio].
    min_area : float
        The area of the cropped image must be within the range of
        [min_area, max_area].
    max_area : float
        The area of the cropped image must be within the range of
        [min_area, max_area].
    overlap_thresh : float
        Cropped box area ratio must be above this threhold to be kept.
    clip_boxes : bool
        Whether to clip the boxes to the cropped image. (default is True)
    random_coef : float
        Probability a crop operation is skipped.
}

SSDRandomCropPad {
    operations : list of SSDRandomCropPadOperation
        Randomly crops and pads an image according to:
        Liu et al., SSD: Single shot multibox detector.
}

SSDRandomCropPadOperation {
    min_object_covered : float
        Cropped image must cover at least this fraction of one original bounding
        box.
    min_aspect_ratio : float
        The aspect ratio of the cropped image must be within the range of
        [min_aspect_ratio, max_aspect_ratio].
    max_aspect_ratio : float
        The aspect ratio of the cropped image must be within the range of
        [min_aspect_ratio, max_aspect_ratio].
    min_area : float
        The area of the cropped image must be within the range of
        [min_area, max_area].
    max_area : float
        The area of the cropped image must be within the range of
        [min_area, max_area].
    overlap_thresh : float
        Cropped box area ratio must be above this threhold to be kept.
    clip_boxes : bool
        Whether to clip the boxes to the cropped image. (default is True)
    random_coef : float
        Probability a crop operation is skipped.
    min_padded_size_ratio : list of float
        Min ratio of padded image height and width to the input image's height and
        width. Two entries per operation.
    max_padded_size_ratio : list of float
        Max ratio of padded image height and width to the input image's height and
        width. Two entries per operation.
    pad_color_r : float
        Padding color for red channel
    pad_color_g : float
        Padding color for green channel
    pad_color_b : float
        Padding color for blue channel
}

SSDRandomCropFixedAspectRatio {
    operations : list of SSDRandomCropFixedAspectRatioOperation
        Operations to apply.
    aspect_ratio : float
        Aspect ratio to crop to. This value is used for all crop operations.
        (default is 1.0)
}

SSDRandomCropFixedAspectRatioOperation {
    min_object_covered : float
        Cropped image must cover at least this fraction of one original bounding
        box.
    min_area : float
        The area of the cropped image must be within the range of
        [min_area, max_area].
    max_area : float
        The area of the cropped image must be within the range of
        [min_area, max_area].
    overlap_thresh : float
        Cropped box area ratio must be above this threhold to be kept.
    clip_boxes : bool
        Whether to clip the boxes to the cropped image. (default is True)
    random_coef : float
        Probability a crop operation is skipped.
}

SSDRandomCropPadFixedAspectRatio {
    operations : list of SSDRandomCropPadFixedAspectRatioOperation
        List of operations
    aspect_ratio : float
        Aspect ratio to pad to. This value is used for all crop and pad operations.
        (default is 1.0)
    min_padded_size_ratio : list of float
        Min ratio of padded image height and width to the input image's height and
        width. Two entries per operation.
    max_padded_size_ratio : list of float
        Max ratio of padded image height and width to the input image's height and
        width. Two entries per operation.
}

message SSDRandomCropPadFixedAspectRatioOperation {
    min_object_covered : float
        Cropped image must cover at least this fraction of one original bounding
        box.
    min_aspect_ratio : float
        The aspect ratio of the cropped image must be within the range of
        [min_aspect_ratio, max_aspect_ratio].
    max_aspect_ratio : float
        The aspect ratio of the cropped image must be within the range of
        [min_aspect_ratio, max_aspect_ratio].
    min_area : float
        The area of the cropped image must be within the range of
        [min_area, max_area].
    max_area : float
        The area of the cropped image must be within the range of
        [min_area, max_area].
    overlap_thresh : float
        Cropped box area ratio must be above this threhold to be kept.
    clip_boxes : bool
        Whether to clip the boxes to the cropped image. (default is True)
    random_coef : float
        Probability a crop operation is skipped.
}

RandomVerticalFlip {
    keypoint_flip_permutation : list of int
        Specifies a mapping from the original keypoint indices to vertically
        flipped indices.
}

RandomRotation90 {

}

RGBtoGray {

}

ConvertClassLogitsToSoftmax {
    temperature : float
        Scale to use on logits before applying softmax. (default is 1.0)
}

RandomAbsolutePadImage {
    max_height_padding : int
        Height will be padded uniformly at random from [0, max_height_padding).
    max_width_padding : int
        Width will be padded uniformly at random from [0, max_width_padding).
    pad_color : list of float
        Color of the padding. If unset, will pad using average color of the input
        image.
}

RandomSelfConcatImage {
    concat_vertical_probability : float
        Probability of concatenating the image vertically. (default is 0.1)
    concat_horizontal_probability : float
        Probability of concatenating the image horizontally. (default is 0.1)
}

Optimizer {
    oneof {
        rms_prop : RMSPropOptimizer
            Optimizer that implements the RMSProp algorithm.
        momentum : MomentumOptimizer
            Optimizer that implements the Momentum algorithm.
        adam : AdamOptimizer
            Optimizer that implements the Adam algorithm.
        adadelta : AdadeltaOptimizer
            Optimizer that implements the Adadelta algorithm.
            This option is only available for models of type 'classification'.
        adagrad : AdagradOptimizer
            Optimizer that implements the Adagrad algorithm.
            This option is only available for models of type 'classification'.
        ftrl : FtrlOptimizer
            Optimizer that implements the FTRL algorithm.
            This option is only available for models of type 'classification'.
        sgd : SgdOptimizer
            Optimizer that implements the gradient descent algorithm.
            This option is only available for models of type 'classification'.
    }
    use_moving_average : bool, optional
        Whether to use moving average.
        (default is True)
    moving_average_decay : float, optional
        Moving average decay (default is 0.9999)
}

RMSPropOptimizer {
    learning_rate : LearningRate
        Learning rate
    momentum_optimizer_value : float, optional
        Momentum value. (default is 0.9)
    decay : float, optional
        Discounting factor for the history/coming gradient. (default is 0.9)
    epsilon : float, optional
        Small value to avoid zero denominator. (default is 1.0)
}

MomentumOptimizer {
    learning_rate : LearningRate
        Learning rate
    momentum_optimizer_value : float, optional
        Momentum value. (default is 0.9)
}

AdamOptimizer {
    learning_rate : LearningRate
        Learning rate
    beta1 : float, optional
        The exponential decay rate for the 1st moment estimates.
        This option is only available for models of type 'classification'.
        (default is 0.9)
    beta2 : float, optional
        The exponential decay rate for the 2nd moment estimates.
        This option is only available for models of type 'classification'.
        (default is 0.999)
    epsilon : float, optional
        Epsilon term for the optimizer.
        This option is only available for models of type 'classification'.
        (default is 1.0)
}

AdadeltaOptimizer {
    learning_rate : LearningRate
        Learning rate
    rho : float, optional
        The decay rate. (default is 0.95)
    epsilon : float, optional
        A constant epsilon used to better conditioning the grad update.
        (default is 1e-08)
}

AdagradOptimizer {
    learning_rate : LearningRate
        Learning rate
    initial_accumulator_value : float, optional
        Starting value for the accumulators, must be positive. (default is 0.1)
}

FtrlOptimizer {
    learning_rate : LearningRate
        Learning rate
    learning_rate_power : float, optional
        Controls how the learning rate decreases during training. 
        Must be less or equal to zero. Use zero for a fixed learning rate.
        (default is -0.5)
    initial_accumulator_value : float, optional
        The starting value for accumulators. Only zero or positive values 
        are allowed. (default is 0.1)
    l1_regularization_strength : float, optional
        Must be greater than or equal to zero.
        (default is 0.0)
    l2_regularization_strength : float, optional
        Must be greater than or equal to zero.
        (default is 0.0)
}

SgdOptimizer {
    learning_rate : LearningRate
        Learning rate
}

LearningRate {
    oneof {
        constant|fixed : ConstantLearningRate
            Configuration for a constant learning rate.
            In image classification is also known as `fixed`.
        exponential : ExponentialDecayLearningRate
            Exponential decay schedule.
        polynomial : PolynomialDecayLearningRate
            Applies a polynomial decay to the learning rate.
        manual_step : ManualStepLearningRate
            Configuration for a manually defined learning rate schedule.
            This option is only available for models of type 'detection'.
        cosine : CosineDecayLearningRate
            Cosine decay schedule with warm up period.
            This option is only available for models of type 'detection'.
    }
}

ConstantLearningRate {
    learning_rate : float, optional
        Constant value for learning rate. (default is 0.002)
}

ExponentialDecayLearningRate {
    initial_learning_rate : float, optional
        Base learning rate. (default is 0.002)
    decay_steps : int, optional
        Steps to take between decaying the learning rate.
    decay_epochs : int, optional
        Epochs to take between decaying the learning rate.
        This option overwrites `decay_steps`.
        If neither this option nor `decay_steps` is set, then a calculation will be 
        made from dataset size, `batch_size` and the total number of clones in the cluster.
    decay_factor : float, optional
        Multiplicative factor by which to decay learning rate (default is 0.95)
    staircase : bool, optional
        Whether use staircase decay. 
        This option is only available for models of type 'detection'. 
        (default is True)
    burnin_learning_rate : float, optional
        Initial learning rate during burn-in period. If 0.0 (which is the default), 
        then the burn-in learning rate is simply set to learning_rate_base. 
        This option is only available for models of type 'detection'. 
        (default is 0.0)
    burnin_steps : int, optional
        Number of steps to use burnin learning rate. 
        This option is only available for models of type 'detection'. 
        (default is 0)
    burnin_epochs : int, optional
        Number of epochs to use burnin learning rate. 
        This option overwrites `burnin_steps`.
        (default is None)
    min_learning_rate : float, optional
        The minimum learning rate. 
        This option is only available for models of type 'detection'. 
        (default is 0.0)
}

PolynomialDecayLearningRate {
    initial_learning_rate : float, optional
        Base learning rate. (default is 0.002)
    decay_steps : int, optional
        Steps to take between decaying the learning rate.
    decay_epochs : int, optional
        Epochs to take between decaying the learning rate.
        This option overwrites `decay_steps`.
        If neither this option nor `decay_steps` is set, then a calculation will be 
        made from dataset size, `batch_size` and the total number of clones in the cluster.
    end_learning_rate : float, optional
        The minimal end learning rate. (default is 0.0001)
}

ManualStepLearningRate {
    initial_learning_rate : float, optional
        Base learning rate. (default is 0.002)
    schedule : list of LearningRateSchedule
        Schedule for learning rate
    warmup : bool, optional
        Whether to linearly interpolate learning rates for steps in
        [0, schedule[0].step]. (default is False)
}

CosineDecayLearningRate {
    learning_rate_base : float, optional
        Base learning rate (default is 0.002)
    total_steps : int, optional
        Total number of training steps (default is 4000000)
    total_epochs : int, optional
        Total number of training epochs 
        This option overwrites `total_steps`.
        (default is None)
    warmup_learning_rate : float, optional
        Initial learning rate for warm up (default is 0.0002)
    warmup_steps : int, optional
        Number of warmup steps (default is 10000)
    warmup_epochs : int, optional
        Number of warmup epochs 
        This option overwrites `warmup_steps`.
        (default is None)
    hold_base_rate_steps : int, optional
        Optional number of steps to hold base learning rate before decaying 
        (default is 0)
    hold_base_rate_epochs : int, optional
        Optional number of epochs to hold base learning rate before decaying 
        This option overwrites `hold_base_rate_steps`.
        (default is None)
}

LearningRateSchedule {
    step : int
        Step number
    epoch: int, optional
        Epoch number
    learning_rate : float
        Learning rate value (default is 0.002)
}
```


## Especificación para configuración de entorno de entrenamiento de modelos de clasificación

```
trainer_config {
    master : str, optional
        The address of the TensorFlow master to use. (default is '')
    num_clones : int, optional
        Number of model clones to deploy. (default is 1)
    total_clones_in_cluster : int or None, optional
        Total number of model clones to deploy in the cluster.
        This value must be the total number of clones in all replicas of 
        the cluster.
        (default is None)
    clone_on_cpu : bool, optional
        Use CPUs to deploy clones. (default is False)
    worker_hosts : str, optional
        Comma-separated list of hostname:port for the worker jobs. 
        E.g. 'machine1:2222,machine2:1111,machine2:2222'
        (default is '')
    ps_hosts : str, optional
        Comma-separated list of hostname:port for the parameter server jobs. 
        E.g. 'machine1:2222,machine2:1111,machine2:2222'
        (default is '')
    job_name : str, optional
        In a distributed trainig session, whether is a worker or ps job. 
        Allowed values are "ps" and "worker" (default is '')
    worker_replicas : int, optional
        Number of worker replicas. (default is 1)
    num_ps_tasks : int, optional
        The number of parameter servers. If the value is 0, then the parameters
        are handled locally by the worker. 
        (default is 0)
    num_readers : int, optional
        The number of parallel readers that read data from the dataset.
        (default is 4)
    num_preprocessing_threads : int, optional
        The number of threads used to create the batches. 
        (default is 4)
    sync_replicas : bool, optional
        Whether or not to synchronize the replicas during training. (default is False)
    replicas_to_aggregate : int, optional
        Number of replicas to aggregate before making parameter updates. (default is 1)
    task : int, optional
        Task id of the replica running the training. (default is 0)
    log_every_n_steps : int, optional
        The frequency with which logs are print. 
        (default is 10)
    save_summaries_secs : int, optional
            The frequency with which summaries are saved, in seconds. (default is 600)
    save_interval_secs : int, optional
            The frequency with which the model is saved, in seconds. (default is 600)
}
```

## Especificación para configuración de entorno de entrenamiento de modelos de detección

```
trainer_config {
    master : str, optional
        The address of the TensorFlow master to use. (default is '')
    num_clones : int, optional
        Number of model clones to deploy. (default is 1)
    clone_on_cpu : bool, optional
        Use CPUs to deploy clones. (default is False)
    worker_hosts : str, optional
        Comma-separated list of hostname:port for the worker jobs. 
        E.g. 'machine1:2222,machine2:1111,machine2:2222'
        (default is '')
    ps_hosts : str, optional
        Comma-separated list of hostname:port for the parameter server jobs. 
        E.g. 'machine1:2222,machine2:1111,machine2:2222'
        (default is '')
    master_host : str, optional
        String that respresents a hostname:port for the master server job. 
        E.g. 'machine1:2222'
        (default is '')
    job_name : str, optional
        In a distributed trainig session, whether is a worker or ps job. 
        Allowed values are "ps" and "worker" (default is '')
    worker_replicas : int, optional
        Number of worker replicas. (default is 0)
    ps_tasks : int, optional
        The number of parameter servers. If the value is 0, then the parameters
        are handled locally by the worker. 
        In image classification is also known as `num_ps_tasks`.
        (default is 0)
    num_readers : int, optional
        The number of parallel readers that read data from the dataset.
        (default is 4)
    shuffle : bool, optional
        Whether data should be processed in the order they are read in, 
        or shuffled randomly. (default is True)
    shuffle_buffer_size : int, optional
        Buffer size to be used when shuffling. (default is 2048)
    filenames_shuffle_buffer_size : int, optional
        Buffer size to be used when shuffling file names. (default is 100)
    num_parallel_batches : int, optional
        Number of batches to produce in parallel. If this is run on a 2x2 TPU set 
        this to 8. (default is 8)
    num_prefetch_batches : int, optional
        Number of batches to prefetch. Prefetch decouples input pipeline and 
        model so they can be pipelined resulting in higher throughput. Set this 
        to a small constant and increment linearly until the improvements become 
        marginal or you exceed your cpu memory budget. Setting this to -1, 
        automatically tunes this value for you. (default is 2)
    read_block_length : int, optional
        Number of records to read from each reader at once.
        (default is 32)
    max_number_of_boxes : int, optional
        Maximum number of boxes to pad to during training / evaluation. 
        Set this to at least the maximum amount of boxes in the input data, 
        otherwise some groundtruth boxes may be clipped. (default is 100)
    sync_replicas : bool, optional
        Whether or not to synchronize the replicas during training. (default is False)
    replicas_to_aggregate : int, optional
        Number of replicas to aggregate before making parameter updates. (default is 1)
    task : int, optional
        Task id of the replica running the training. (default is 0)
    keep_checkpoint_every_n_hours : float, optional
        How frequently to keep checkpoints. 
        (default is 10000.0)
}
```