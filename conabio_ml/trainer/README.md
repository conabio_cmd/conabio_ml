# Modelos

El módulo model del paquete trainer define la manera en la que un modelo puede ser creado utilizando el API conabio_ml.

## Carga y creación de un modelo

Existen dos métodos generales para crear instancias de modelos, las cuales podrán ser utilizadas para ajustar el modelo en una etapa de entrenamiento o para que realicen la inferencia sobre ciertos datos. Ambos métodos son descritos a continuación.

### Cargar un modelo previamente entrenado

Si se tiene un modelo previamente entrenado con alguna de las biblioteca o APIs utilizadas dentro del API conabio_ml, (p.e., [TensorFlow-Slim](https://github.com/tensorflow/models/tree/master/research/slim "TensorFlow-Slim"), [API de Detección de Objetos de Tensorflow](https://github.com/tensorflow/models/tree/master/research/object_detection "API de Detección de Objetos de Tensorflow") o [Keras](https://www.tensorflow.org/guide/keras/train_and_evaluate "Keras")) y se desea re-entrenar, evaluar o aplicar la inferencia sobre un conjunto de datos, es posible usar el método `load_saved_model` de la subclase de Model adecuada para ese modelo, y que está definido como sigue:

```python
@classmethod
def load_saved_model(cls, 
					 source_path: str, 
					 model_config: dict=None, 
					 pipeline_filepath: str=None,
					 model_name: str=None) -> ModelType:
	[ . . .]
	
```
Al cual se le debe pasar en el parámetro `source_path` la ruta donde se encuentran los archivos del modelo previamente entrenado (p.e. *.ckpt, *.pb, etc.) y una configuración compatible con este modelo de una de las siguientes maneras:

1. Pasar en el parámetro `model_config` un diccionario con la configuración de acuerdo al modelo a utilizar conforme a la especificación que se encuentra en este enlace.
2. Pasar en el parámetro `pipeline_filepath` la ruta de un archivo de pipeline en el que se haya entrenado previamente un modelo con el API conabio_ml usando la opción 1. De esta manera se cargará internamente desde el archivo de pipeline el parámetro `model_config` con el cual se configuró el modelo que se entrenó previamente, del que opcionalmente se puede especificar el nombre con el parámetro `model_name`, ya que de lo contrario se regresará la configuración del primer modelo que se encuentre en los **procesos** del pipeline.

### Crear un modelo
*Nota: Por el momento esta opción sólo está disponible para modelos de texto.*

Si se desea crear un modelo se deben indicar todas las capas que este contendrá. El modelo puede ser creado utilizando la función `create` para construir una instancia de este.

La función `create` está definida como sigue:

```python
@classmethod
def create(cls, model_config: dict, **kwargs) -> ModelType:
	[ . . .]
```

Para la implementación de propósito general para prototipado rápido se deben considerar las siguientes restricciones:

La función `create` espera la definición de las capas para cada uno de los modelos en el parámetro `model_config`, como sigue:

```python
model_config = {
	[Name of the model] = {
		layers: dict
			Definition of all the layers the model will use
		}
}
```

Donde el parámetro layers contiene cada capa y su configuración que se usará y de la cual se desee llevar seguimiento.

```python
layers: {
	"layer1":{ [ Config params ] },
	"layer2":{ [ Config params ] }
}
```

Al término del proceso de creación debe llamarse al método `__init__:` 

```python
def __init__(self, 
				model_config:dict,
				input_layer,
				output_layer):
	[ . . . ]

```

donde:
- `model_config`: es la configuración íntegra enviada a la función.
- `input_layer`: la capa de entrada del modelo
- `output_layer`: la capa de salida del modelo

Por lo tanto, un template sugerido de la función `create` para construir un modelo, se muestra a continuación:

```python
def create(	cls, 
			name: {
				layers: dict = { [ . . . ] }
			}) -> ModelType:

	input_layer, output_layer = [ Model creation ]

	return cls(	model_config = model_config,
				input_layer = input_layer,
				output_layer = output_layer)

```

## Aplicar un modelo

Una vez que se tenga la instancia de un modelo ajustado –ya sea que se haya creado con el método create y entrenado, o cargado de un modelo previo con el método `load_saved_model` y posiblemente re-entrenado– y se deseé utilizar para aplicar la inferencia sobre un conjunto de datos, es posible utilizar el método `predict`, que está definido de la siguiente manera:

```python
def predict(self: Model,
            dataset: Dataset,
            execution_config: PredictorConfig=None,
            prediction_config: dict={}) -> PredictionDataset:
	[ . . . ]
```

Que en su forma general recibe los siguientes parámetros:

- `dataset`: instancia de Dataset que contenga los datos sobre el cuál se desea aplicar la inferencia.
- `execution_config`: es una instancia de un entorno de ejecución en que se ejecutará la predicción y depende de la biblioteca sobre la que se vaya a realizar la inferencia.
- `prediction_config`: es una configuración que indica cómo se va a realizar la predicción, y depende del tipo de modelo que se esté usando.

El valor devuelto es un dataset de predicción, el cual cuenta con los métodos necesarios para realizar la mayoría de tareas de un dataset convencional –p.e., enviarlo a representación como archivos CSV, folders del sistema de archivos, etc.– pero además podrá ser utilizado en métodos de evaluación y para generar etiquetas automáticas en un ambiente de producción, por ejemplo.



# Entrenadores

Un entrenador (**Trainer**) es un elemento que ajusta un **modelo** a partir de un **dataset** y una **función de optimización**. El proceso de entrenamiento se puede llevar a cabo en distintos **entornos** (p.e., distribuído, paralelo, síncrono, asíncrono, etc.), que dependen de la plataforma que se utilice para realizar el entrenamiento. Sin embargo, la configuración del entorno de ejecución es independiente del entrenamiento y no tiene un efecto directo en el rendimiento del modelo, como sí lo tienen la función de optimización, el número de épocas o la configuración de dropout, por ejemplo. Por este motivo, en el API conabio_ml se decidió desacoplar la configuración de los parámetros de entrenamiento de la configuración del entorno de ejecución del entrenamiento.

Por lo anterior, se resume que para entrenar un modelo utilizando el API conabio_ml son necesarios cuatro componentes principales:

- **Dataset**: son los datos que usará el entrenador para ajustar el modelo. Estos datos deberán estar encapsulados en una instancia de Dataset, para lo que puede usarse cualquiera de los métodos que permiten crear un dataset a partir de una colección de datos.
- **Modelo**: es el modelo que se desea ajustar y puede tratarse de un modelo pre-entrenado o de un modelo que se desea entrenar desde cero. Para cualquiera de estos casos se puede usar la metodología para cargar un modelo descrita en la sección Model.
- **Configuración del entorno**: se trata de una subclase de TrainerConfig, que encapsula los parámetros específicos de la plataforma con la que se realizará el entrenamiento. Se han creado implementaciones para las plataformas de entrenamiento más utilizadas, como son Tensorflow-Slim, el API de Detección de Objetos de Tensorflow, Keras, etc. Si se desea usar la configuración por defecto de cada plataforma no es necesario utilizar este parámetro.
- **Configuración del entrenamiento**: se trata de un diccionario en el formato detallado en la especificación del API que contiene la configuración del entrenamiento, como la función de optimización, número de épocas, dropout, etc.

El proceso de entrenamiento se lleva a cabo a través del método estático `train` de la clase específica del entrenador que se utilizará, y que depende del problema que se esté tratando de resolver.

## Configuración del entorno de ejecución

Con la finalidad de separar la configuración del entorno de ejecución de un entrenamiento con la configuración del entrenamiento propiamente dicho, se creó la interfaz TrainerConfig, que encapsula los parámetros específicos de la plataforma con la que se realizará el entrenamiento. Dichos parámetros podrán estar relacionadas, por ejemplo, con la configuración de entornos de ejecución distribuidos o paralelos, y serán propios de la plataforma en que se lleve a cabo el proceso de entrenamiento.

Cuando se usa la implementación de propósito general la función **__init__** se describe como:

```python
@classmethod
def create( cls,
            checkpoint_dir: str,
            log_dir: str) -> TrainerConfigType:
    [ . . . ]
```

donde:

- checkpoint_dir: es el folder destino donde se desean guardar los checkpoints del modelo.
- log_dir: es el folder destino donde se guardarán los logs de ejecución

Por lo tanto, un proceso implementación del Pipeline tendrá el template siguiente:

```python
[ . . . ]
.add_process(   name="create_keras_trainer_cfg",
                action=KerasTrainerConfig.create,
                args={
                    'checkpoint_dir': str,
                    'log_dir': str
                })
[ . . . ]
```

Para modelos de clasificación y detección de objetos en imágenes que ya se encuentran disponibles para ser utilizados en entornos como Slim o el API de detección de objetos de Tensorflow, puede revisar el siguiente enlace.

## Trainer

El entrenador (Trainer) es el módulo que enlaza los componentes necesarios para realizar un entrenamiento: los datos, el modelo, la configuración del ambiente y la configuración del proceso de entrenamiento.

Puede consultar a detalle la documentación del módulo entrenado en el siguiente recurso: TODO.

En esta sección cubriremos el parámetro train_config dentro del proceso Trainer.

El método `train` se define como sigue:

```python
@classmethod
def train( 	cls,
			dataset: Dataset,
			model: ModelType,
			execution_config: TrainerConfigType,
			train_config: dict = {}) -> ModelType:
	[ . . . ]

```

Donde:

- model: es el modelo creado con los métodos cubierto o una subclase de Model
- execution_config: es la configuración de ambiente cubierta en el punto anterior
- train_config: es un diccionario con la configuración de entrenamiento. La configuración general de train_config consiste en los siguientes parámetros:

```python
'train_config': {
	MODEL_NAME: {
		'optimizer': {
			OPTIMIZER_NAME: {
				[ optimizer params ]
				}
			}
		},
		'loss': {
			LOSS_NAME: {
				[ loss params ]
			}
		},
		'epochs': int,
		'batch_size': int,
		'shuffle': bool,
		'callbacks': {
			CALLBACK_NAME: {
				[ callback params ]
			}
		}
	}
}
```