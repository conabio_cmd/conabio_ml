from abc import ABCMeta, abstractmethod

from conabio_ml.process import ProcessState
from typing import TypeVar

class TrainerConfig(ProcessState):
    """Represents a configuration to be used in a Trainer class to perform
    a Model training.

    Attributes
    ----------
    TrainerConfigType: type
        The type of process when is used in conabio_ml API
    """

    TrainerConfigType = TypeVar('TrainerConfigType', bound='TrainerConfig')

    @classmethod
    def get_type(cls):
        """
        Returns the type of this current class

        Returns
        -------
        TrainerConfigType
        """
        return cls.TrainerConfigType

    def __init__(self, **kwargs):
        pass
