API CONABIO ML
==============

El API conabio\_ml es un proyecto de software libre para facilitar el
trabajo en tareas de Aprendizaje Automático, usando la información
generada en la CONABIO y en otros proyectos de conservación de la
biodiversidad.

Esta interfaz de programación se basa principalmente en los siguientes
elementos:

-   **Dataset**: se crea a partir de formatos estándar de datos, como
    archivos JSON, CSV y elementos dentro de una carpeta del sistema de
    archivos.
-   **Modelo**: se puede definir la arquitectura completa de un modelo o
    elegir alguna de las más comúnmente utilizadas y únicamente
    configurar sus parámetros.
-   **Entrenador**: ajusta los pesos de un **Modelo** durante un proceso
    de entrenamiento a partir de los elementos de un dataset.
-   **Evaluador**: calcula una serie de **Métricas** a partir de las
    predicciones que hace un modelo sobre un conjunto de elementos de un
    dataset.

Estos elementos se pueden conectar en un Pipeline de Aprendizaje
Automático que valida que las entradas y salidas de cada proceso sean
las adecuadas, además de registrar el orden en que se ejecutan y los
parámetros con que se configuran, permitiendo replicar experimentos y
comparar el rendimiento de los modelos cuando se varían los parámetros.

A continuación se muestra cómo se pueden conectar los elementos de un
experimento de Aprendizaje Automático utilizando el API conabio\_ml.

![Flujo de un Pipeline de ML](https://conabio-ml.readthedocs.io/es/latest/_images/Diagrama_flujo.png)


# Instalación

La manera más fácil (y la que recomendamos) para instalar la API `conabio_ml` es a través de un ambiente virtual, el cual debe funcionar con Python 3.6 o superior.
Lo primero que debemos hacer es crear un directorio donde instalaremos la biblioteca. Para esto, puede crearse mediante la ejecución del siguiente comando:

    mkdir nombre_del_directorio
    cd nombre_del_directorio

## Preparación del ambiente

Después de crear el directorio, debemos crear nuestro ambiente virtual como se muestra a continuación:

### Windows

En el caso de Windows, el ambiente virtual puede crearse desde el cmd, utilizando la ejecución del comando:

    python -m venv path\to\virtual\environment

Después, activamos el ambiente virtual utilizando el siguiente comando:

    .\path\to\virtual\environment\Scripts\activate

### Linux

Por otro lado, el ambiente virtual para linux puede crearse utilizando **venv** por medio de la ejecución del comando:

    python3 -m venv path/to/virtual/environment

Después, activamos el ambiente virtual utilizando el siguiente comando:

    source path/to/virtual/environment/bin/activate

# Descarga del repositorio

Una opción es descargar el proyecto desde el repositorio para tener acceso al código fuente y realizar la instalación de los paquetes necesarios, como se muestra a continuación:

```bash
git clone https://bitbucket.org/conabio_cmd/conabio_ml.git
cd conabio_ml
pip install -r requirements.txt
```

Luego, instalamos la biblioteca con el siguiente comando:

    pip install -e .

**Nota:** El proceso de descarga e instalacion tiene el mismo tanto para Windows como para Linux

# Recomendaciones

Para empezar a utilizar el `API CONABIO ML` es posible utilizar una imagen `Docker` predeterminada que crea el ambiente  necesario para poder utilizar esta herramienta. Para ello es necesario ejecutar lo siguiente en una terminal de línea de comandos, en un entorno que cuente con la herramienta [Docker](https://www.docker.com/get-started), situado en la carpeta principal del repositorio:

```
cd docker_image
docker image build -t api_conabio_ml:1.0 .
docker run -it --gpus 2 --name api_conabio_ml api_conabio_ml:1.0 bash
```

Lo anterior crea contenedor con un ambiente que realizará la ejecución usando las GPUs con las que cuente el sistema (en este caso 2). Si el sistema no cuenta con estas unidades de procesamiento, solamente es necesario omitir el parámetro `--gpus 2` del comando `docker run` anterior, y cambiar la primera línea del archivo `docker_image/Docker` por la siguiente:

```
FROM tensorflow/tensorflow:1.14.0-py3
```

Una vez creado el contenedor, se pueden ejecutar scripts dentro de este ambiente que utilicen el API.