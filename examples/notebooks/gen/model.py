#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os

from keras.models import Model
from keras.layers import Embedding, Flatten, Dense, Input

from conabio_ml.trainer.keras_bcknd import KerasTrainerConfig
from cfg import conabio_ml

# TODO: We are using a brach references for all scripts, when we
# merge to master those references has to be changed.


# All models that you need to chain using the Conabio_ML pipeline
# must be a subclass of the Conabio_ML model, defined here:
# (https://bitbucket.org/conabio_cmd/conabio_ml/src/CONML-45-definicin-del-esquema-de-entren/conabio_ml/trainer/model.py)

# We are using a model defined in plain Keras,
# so we use the helper subclass Keras Model
# (https://bitbucket.org/conabio_cmd/conabio_ml/src/afe32d9fcbb10957485a8e01765a61436438a353/conabio_ml/trainer/keras_bcknd.py#lines-34).
from conabio_ml.trainer.keras_bcknd import KerasModel as KM
from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)

class MNIST_Dummy_Dense(KM):
    train_config = None

    # For this example, we will build a model by establishing
    # its layers and loss, so, we use the create method defined in:
    # (https://bitbucket.org/conabio_cmd/conabio_ml/src/afe32d9fcbb10957485a8e01765a61436438a353/conabio_ml/trainer/model.py#lines-62)
    @classmethod
    def create(cls: KM.ModelType,
               model_config:{
                   "MNIST_Classifier": {
                       'layers': {
                           'embedding_layer': {
                               'input_dim': 1000,
                               'output_dim': 50,
                               'window': 10
                           },
                           'dense_layer': {
                               'units': 2,
                               'activation': 'softmax'
                           }
                                }
                                        }
                            }) -> KM.ModelType:
        """ Creates the instance of this model

        Parameters
        ----------
        embedding_layer : dict
            Defines the parameters of the embedding layer of the model
                "input_dim": int
                "output_dim":int
                "window":int
        dense_layer: dict
            Defines the parameters of the dense layer
                "units": int
                "activation": str

        Returns
        -------
        MNIST_Classifier instance
            Instance of the MNIST_Classifier model
        """
        try:
            model_name = [k for k in model_config.keys()][0]

            layers = model_config[model_name]["layers"]

            input_layer = layers["input_layer"]
            dense_layer = layers["dense_layer"]

            input_layer = Input(shape=(input_layer["input_dim"], ))
            dense_layer = Dense(units=dense_layer["units"],
                                activation=dense_layer["activation"],
                                name=model_name+"_denser")

            output_layer = dense_layer(input_layer)
            
            # In the init method KerasTFModel needs the following paramaters
            # - input_layer: `Tensor` in the input layer
            # - output_layer: `Tensor` in the output layer
            # - model_config: The configuration of the model
            return cls(model_config,
                       input_layer,
                       output_layer)
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)

    @classmethod
    def from_checkpoint(cls: KM.ModelType,
                        source_path: str,
                        model_config: dict,
                        train_config: dict) -> KM.ModelType:
        i = cls.create(name=model_config["name"],
                       layers=model_config["layers"],
                       loss=model_config["loss"])
        i.model.load_weights(source_path)
        i.train_config = train_config

        return i

class MNIST_Classifier(KM):
    train_config = None

    # For this example, we will build a model by establishing
    # its layers and loss, so, we use the create method defined in:
    # (https://bitbucket.org/conabio_cmd/conabio_ml/src/afe32d9fcbb10957485a8e01765a61436438a353/conabio_ml/trainer/model.py#lines-62)
    @classmethod
    def create(cls: KM.ModelType,
               model_config: {
                   "MNIST_Classifier": {
                       'layers': {
                           'embedding_layer': {
                               'input_dim': 1000,
                               'output_dim': 50,
                               'window': 10
                           },
                           'dense_layer': {
                               'units': 2,
                               'activation': 'softmax'
                           }
                       }
                   }
               }) -> KM.ModelType:
        """ Creates the instance of this model

        Parameters
        ----------
        embedding_layer : dict
            Defines the parameters of the embedding layer of the model
                "input_dim": int
                "output_dim":int
                "window":int
        dense_layer: dict
            Defines the parameters of the dense layer
                "units": int
                "activation": str

        Returns
        -------
        MNIST_Classifier instance
            Instance of the MNIST_Classifier model
        """
        try:
            model_name = [k for k in model_config.keys()][0]

            layers = model_config[model_name]["layers"]

            embedding_layer = layers["embedding_layer"]
            dense_layer = layers["dense_layer"]

            input_layer = Input(shape=(embedding_layer["window"], ))

            # embedding_layer = Embedding(input_dim=embedding_layer["input_dim"],
            #                             output_dim=embedding_layer["output_dim"],
            #                             input_length=embedding_layer["window"],
            #                             name=model_name+"_embedding")
            # flatten_layer = Flatten(name=model_name+"_flatten")
            dense_layer = Dense(units=dense_layer["units"],
                                activation=dense_layer["activation"],
                                name=model_name+"_denser")

            # embedding = embedding_layer(input_layer)
            # flatten = flatten_layer(embedding)
            output_layer = dense_layer(input_layer)
            
            # In the init method KerasTFModel needs the following paramaters
            # - input_layer: `Tensor` in the input layer
            # - output_layer: `Tensor` in the output layer
            # - model_config: The configuration of the model
            return cls(model_config,
                       input_layer,
                       output_layer)
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)

    @classmethod
    def from_checkpoint(cls: KM.ModelType,
                        source_path: str,
                        model_config: dict,
                        train_config: dict) -> KM.ModelType:
        i = cls.create(name=model_config["name"],
                       layers=model_config["layers"],
                       loss=model_config["loss"])
        i.model.load_weights(source_path)
        i.train_config = train_config

        return i