#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import numpy as np

from cfg import conabio_ml
from conabio_ml.pipeline import Pipeline
from conabio_ml.trainer.keras_bcknd import KerasTrainer, KerasTrainerConfig
from conabio_ml.datasets.dataset import PredictionDataset

from model import MNIST_Classifier


def create_test_data(num_samples: int = 10000) -> TypedDict:
    x_test = np.random.randint(1000, size=(int(num_samples/10), 10))
    y_test = np.random.randint(1, size=(int(num_samples/10), 2))

    return {
        "test": (x_test, y_test)
    }


results_path = os.path.join('./results', 'MNIST_AE')

pipeline = Pipeline(results_path)\
    .add_process(name="create_test_data",
                 action=create_test_data,
                 args={
                     "num_samples": 100
                 })\
    .add_process(name="load_MNIST_Classifier_from_pipeline",
                 action=MNIST_Classifier.from_pipeline,
                 args={
                     'model_name': "MNIST_Classifier",
                     'pipeline_filepath': '/Users/rrivera/Documents/mina.de.carbon/projects/conabio_ml/examples/notebooks/keras_minst_ae/results/MNIST_AE/20191001104405/pipeline.json',
                     'checkpoint_filepath': '/Users/rrivera/Documents/mina.de.carbon/projects/conabio_ml/examples/notebooks/keras_minst_ae/results/MNIST_AE/checkpoints/MNIST_Classifier_model.hdf5'
                 })\
    .add_process(name="apply_MNIST_test_data",
                 action=MNIST_Classifier.apply,
                 inputs_from_processes=['load_MNIST_Classifier_from_pipeline',
                                        'create_test_data'])\
    .add_asset(name="as_csv_MSNIST",
             action=PredictionDataset.to_csv,
             inputs_from_processes=['apply_MNIST_test_data'],
             args={
             })

pipeline.run()
