#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import random

import numpy as np
import pandas as pd

# MNIST Dataset
import tensorflow as tf


from cfg import conabio_ml
from conabio_ml.datasets.gen.dataset import Dataset
from conabio_ml.datasets.dataset import Partitions, PredictionDataset

from conabio_ml.pipeline import Pipeline
from conabio_ml.trainer.keras_bcknd import KerasTrainer, KerasTrainerConfig, KerasPredictorConfig
from conabio_ml.trainer.keras_bcknd import TENSORBOARD_CALLBACK
from conabio_ml.evaluator.gen.evaluator import KerasEvaluator

from model import MNIST_Dummy_Dense

# Dataset acquisition through keras MNIST
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

x_train = x_train.reshape(60000, 784)
x_train = x_train.astype('float32')

x_test = x_test.reshape(10000, 784)
x_test = x_test.astype('float32')

# Normalization
x_train /= 255
x_test /= 255


def train_dataset(pick_number, sample_ratio=1) -> Dataset.DatasetType:
    global x_train, y_train, x_test, y_test
    
    # We pick one number to convert this dataset to use in a binary exampl
    ix_not_pick = np.where(y_train != pick_number)
    ix_pick = np.where(y_train == pick_number)
    y_train[ix_not_pick] = False
    y_train[ix_pick] = True
    
    ix_not_pick = np.where(y_test != pick_number)
    ix_pick = np.where(y_test == pick_number)
    y_test[ix_not_pick] = False
    y_test[ix_pick] = True    

    ix_sample = random.sample(range(0, x_train.shape[0]), int(x_train.shape[0]*sample_ratio))
    x_train = x_train[ix_sample]    
    y_train = tf.keras.utils.to_categorical(y_train)
    y_train = y_train[ix_sample]

    ix_sample = random.sample(range(0, x_test.shape[0]), int(x_test.shape[0]*sample_ratio))
    x_test = x_test[ix_sample]
    y_test = y_test[ix_sample]
    
    ds = Dataset.from_matrices({
        Partitions.TRAIN: {
            "item": x_train,
            "label": y_train
        },
        Partitions.TEST: {
            "item": x_test,
            "label": y_test
        }
    })

    return ds


results_path = os.path.join('./results', 'MNIST_AE')

pipeline = Pipeline(results_path)\
    .add_process(name="MNIST_dataset",
                 action=train_dataset,
                 args={
                     "pick_number": 6,
                     "sample_ratio": 0.4
                 })\
    .add_process(name="create_MNIST_Classifier",
                 action=MNIST_Dummy_Dense.create,
                 args={
                     'model_config': {
                         'MNIST_Classifier': {
                             'layers': {
                                 'input_layer': {
                                     'input_dim': 784
                                 },
                                 'dense_layer': {
                                     'units': 2,
                                     'activation': 'softmax'
                                 }
                             }
                         }
                     }})\
    .add_process(name="create_keras_trainer_cfg",
                 action=KerasTrainerConfig.create,
                 args={
                     'checkpoint_dir': os.path.join(results_path, "checkpoints"),
                     'log_dir': os.path.join(results_path, "log_dir")
                 })\
    .add_process(name="train_MNIST_model",
                 action=KerasTrainer.train,
                 inputs_from_processes=['MNIST_dataset',
                                        'create_MNIST_Classifier',
                                        'create_keras_trainer_cfg'],
                 args={
                     'train_config': {
                         'MNIST_Classifier': {
                             'optimizer': {
                                 'rms_prop': {
                                     'learning_rate': {
                                         'constant': {
                                             'learning_rate': 1e-3
                                         }
                                     }
                                 }
                             },
                             'loss': {
                                 'binary_crossentropy': {}
                             },
                             'epochs': 20,
                             'batch_size': 256,
                             'shuffle': True,
                             'metrics': ['accuracy'],
                             'callbacks': {
                                 TENSORBOARD_CALLBACK: {
                                     'log_dir': os.path.join(results_path, "logie"),
                                     'batch_size': 32
                                 }
                             }
                         }
                     }
                 })\
    .add_process(name="predict_MNIST_model",
                 action=MNIST_Dummy_Dense.predict,
                 inputs_from_processes=['train_MNIST_model',
                                        'MNIST_dataset'],
                 args={'execution_config': None,
                       'prediction_config': {
                           "partition": Partitions.TEST  # For the entire dataset
                       }})\
    .add_process(name="evaluate_MNIST_model",
                 action=KerasEvaluator.eval,
                 inputs_from_processes=['MNIST_dataset',
                                        'predict_MNIST_model'],
                 args={'eval_config': {
                     'type_evaluation':KerasEvaluator.CLASSIFICATION.MULTICLASS,
                     
                     "partition": Partitions.TEST
                 }})

pipeline.run()
