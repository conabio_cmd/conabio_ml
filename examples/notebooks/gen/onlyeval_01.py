#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
In this script we perform the process of evaluation
assuming a true dataset and a prediction_dataset
'''
from pdb import set_trace as bp
import os
import pydash
import math
import json

import pandas as pd
import numpy as np
import seaborn

import multiprocessing
from functools import partial
from multiprocessing import Pool

from json import JSONEncoder

from typing import List

from cfg import conabio_ml

from conabio_ml.pipeline import Pipeline

from conabio_ml.trainer.model import Model
from conabio_ml.datasets.dataset import Dataset, PredictionDataset, Partitions
from conabio_ml.evaluator.gen.evaluator import Evaluator, ClassificationDefaults
from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)


procs = multiprocessing.cpu_count() - 1
dataset_filepath = "/Users/rrivera/Documents/mina.de.carbon/projects/datasets/megadetector_eval/pred_dataset_dets.csv"
preddataset_filepath = "/Users/rrivera/Documents/mina.de.carbon/projects/datasets/megadetector_eval/dummy_prediction.csv"


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)


class DummyClassifier(Model):
    THRESHOLD = 0.2

    def _pool(self, apply_args, id):
        res = []
        data = apply_args["data"]
        categories = apply_args["categories"]
        threshold = apply_args["threshold"]

        temp = data[data["item"] == id]
        for cat in categories:
            val = temp[temp["label"] == cat]

            if len(val) > 0:
                val = temp.loc[np.argmax(val["score"])]
                if val["score"] > threshold:
                    res.append({
                        "item": id,
                        "label": val["label"],
                        "score": val["score"]
                    })

        if len(res) == 0:
            res.append({
                "item": id,
                "label": "empty",
                "score": 0.
            })

        return res

    @classmethod
    def create(cls: Model.ModelType,
               model_config={}) -> Model.ModelType:
        """ Only instantiates the Dummy classifier

        Returns
        -------
        DummyClassifier instance
            Instance of the DummyClassifier model
        """
        try:
            return cls("Dummy Classifier")
        except Exception as ex:
            raise(ex)

    def predict(self: Model.ModelType,
                dataset: Dataset.DatasetType) -> Dataset.DatasetType:
        """Since there is no fit process, we transform
        the dataset

        Arguments:
            dataset {Dataset.DatasetType} -- Dataset to predict
            execution_config  -- Dummy
            prediction_config  -- Dummy

        Returns:
            Model.ModelType -- [description]
        """
        data = dataset.data
        uniques = set(data["item"].unique())
        categories = ["Animalia", "Homo"]

        res = []

        # with Pool(processes=procs) as pool:
        #     try:
        #         helper_args = {"categories": categories,
        #                        "data": data,
        #                        "threshold": self.THRESHOLD}
        #         func = partial(self._pool, helper_args)
        #         res = pool.map(func, uniques)
        #     except Exception as err:
        #         logger.exception(err)
        #         raise

        # res = pydash.chain(res).flatten().value()

        res = pd.read_csv(preddataset_filepath)
        return Dataset.from_dataframes([res])


# Simplified the dataset
dataset = None
pred_dataset = None

empty_tag = "empty"
labels = ["Animalia", "Homo"]
labelset = {
    'empty': 'empty',
    'Animalia': 'Animalia',
    'Homo': 'Homo'}


def just_store(results: dict,
               filename: str,
               dest_path: str):
    with open(os.path.join(dest_path, f'{filename}.json'), mode="w") as _f:
        json.dump(results, _f,  cls=NumpyArrayEncoder)


def get_dataset(name) -> List[pd.DataFrame]:
    global dataset, pred_dataset

    if name == "dataset":
        if dataset is None:

            dataset = pd.read_csv(os.path.join(".",
                                               "datasets",
                                               "simplified_dataset.csv"))
        return [dataset]
    elif name == "pred_dataset":
        if pred_dataset is None:
            pred_dataset = pd.read_csv(os.path.join(".",
                                                    "datasets",
                                                    "simplified_pred_dataset.csv"))

        return [pred_dataset]
    else:
        return None


def get_batches(data, batch_length):
    for i in range(0, len(data), batch_length):
        yield data[i:i+batch_length]


def simplify_preddataset_row(name, unique_rows):
    simplified_data = []
    zeros = []

    for ix, value in enumerate(unique_rows):
        try:
            pred_dataset = get_dataset(name)[0]
            row = pred_dataset[pred_dataset["item"] == value]

            values = pydash.chain(labels)\
                .map(lambda x: (row["score"].loc[row["label"] == x]))\
                .map(lambda x: 0. if np.isnan(x.max()) else x.max())\
                .value()

            simplified_data.append([value, *values, np.max(values)])

            if np.max(values) == 0.:
                zeros.append(value)

            if ix % 1000 == 0:
                print(str(ix) + " of " + str(len(unique_rows)))

        except Exception as err:
            print(err)

    return simplified_data


def simplify_dataset_row(name, unique_rows):
    simplified_data = []
    ds = get_dataset(name)[0]

    ds = ds[ds["item"].isin(unique_rows)]

    for ix, item_name in enumerate(unique_rows):
        try:
            rows = ds[ds["item"] == item_name]

            result = np.array(
                pydash.chain(labels).map(
                    lambda x: int(x in set(rows["label"]))).value())

            temp = [item_name, *result, result.any().astype(int)]
            simplified_data.append(temp)

            if ix % 1000 == 0:
                print(str(ix) + " of " + str(len(unique_rows)))
        except Exception as err:
            print(err)

    return simplified_data


def simplify_dataset(process_args):
    process_count = multiprocessing.cpu_count() - 2
    ds = get_dataset("dataset")[0]
    unique_samples = ds["item"].unique()

    batch_length = int(len(unique_samples) / process_count)
    batches = list(get_batches(unique_samples, batch_length))

    with Pool(processes=process_count) as pool:
        func = partial(process_args["process_func"],
                       process_args["dataset_name"])
        results = pool.map(func, batches)

    results = pydash.chain(results).flatten().value()

    df_simplified = pd.DataFrame(data=results,
                                 columns=["item", *labels, empty_tag])
    df_simplified.to_csv(f'./simplified_{process_args["dataset_name"]}.csv')

    return df_simplified


# # This is just when you want to crete the simplified datasets
# simplify_dataset({"process_func": simplify_dataset_row,
#                   "dataset_name": "dataset"})
# simplify_dataset({"process_func": simplify_preddataset_row,
#                   "dataset_name": "pred_dataset"})

results_path = os.path.join('./results', 'multilabel')

pipeline = Pipeline(results_path)\
    .add_process(name="create_dummy_model",
                 action=DummyClassifier.create,
                 args={
                 })\
    .add_process(name="from_csv_dataset",
                 action=Dataset.from_csv,
                 args={
                     "source_path": dataset_filepath
                 })\
    .add_process(name="predict_dataset",
                 action=DummyClassifier.predict,
                 inputs_from_processes=["create_dummy_model",
                                        "from_csv_dataset"],
                 args={
                 })\
    .add_process(name="from_csv_predictiondataset",
                 action=PredictionDataset.from_csv,
                 args={
                     "source_path": preddataset_filepath
                 })\
    .add_process(name="evaluate",
                 action=Evaluator.eval,
                 inputs_from_processes=["predict_dataset",
                                        "from_csv_predictiondataset"],
                 args={
                     "eval_config": {
                         'metrics_set': {
                             ClassificationDefaults.MULTILABEL: {
                                 "average": 'macro',
                                 'per_class': True,
                                 'columns': ["Animalia", "Homo"]
                             },
                             ClassificationDefaults.BINARY: {
                                 'columns': ["empty"]
                             }
                         },
                         'partition': None,
                         'labelset': labelset
                     }
                 })

if __name__ == '__main__':
    pipeline.run()
