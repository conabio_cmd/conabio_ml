import os

from cfg import conabio_ml
from conabio_ml.pipeline import Pipeline

from conabio_ml.trainer.unsupervised import Vectorizers
from conabio_ml.trainer import Trainer

from conabio_ml.preprocessing.text.transform import Transform
from mws_01_dataset import pipeline as dataset_pipeline

results_path = os.path.join('./results', 'mws')
dataset_path = "./datasets"

pipeline = dataset_pipeline\
    .add_process(name="create_word2vec",
                 action=Vectorizers.Word2Vec.create,
                 args={
                     "min_count": 1,
                     "workers": 4,
                     "window": 8,
                     "size": 300
                 })\
    .add_process(name="as_embedding_values_mws",
                 action=Transform.as_embedding_values,
                 inputs_from_processes=["to_csv_mws",
                                        "create_word2vec"],
                 args={
                     "opts": {
                         "word_dictionary": None,
                         "vocab_size": None,
                         "padding_sequences": 100
                     },
                     "train_options": {
                         "epochs": 2
                     }
                 })
    
if __name__ == "__main__":
    pipeline.run()
