import os

from cfg import conabio_ml
from conabio_ml.pipeline import Pipeline
from conabio_ml.datasets.text import MWS

from conabio_ml.preprocessing.text.preprocessing import PreProcessing

results_path = os.path.join('./results', 'mws')
dataset_path = "./datasets"

pipeline = Pipeline(results_path)\
    .add_process(name="create_mws",
                 action=MWS.create,
                 args={
                     "dest_path": dataset_path,
                     "version": "1.0",
                     "categories": [],
                     "ratio": 0.01,
                     "columns": ["item", "label"],
                     "lang": "en"
                 })\
    .add_process(name="split_mws",
                 action=MWS.split,
                 inputs_from_processes=["create_mws"],
                 args={
                     "train_perc": 0.7,
                     "test_perc": 0.2,
                     "val_perc": 0.1
                 })\
    .add_process(name="normalize_mws",
                 action=PreProcessing.normalize,
                 inputs_from_processes=["split_mws"],
                 args={
                     "partitions": [],  # Applied to all partitions
                     "normalize_args": {
                         "delimit": True
                     }
                 })\
    .add_process(name="remove_stop_words_mws",
                 action=PreProcessing.remove_stop_words,
                 inputs_from_processes=["normalize_mws"],
                 args={
                     "partitions": [],  # Applied to all partitions
                 })\
    .add_asset(name="to_csv_mws",
               action=MWS.to_csv,
               inputs_from_processes=["remove_stop_words_mws"])\

if __name__ == "__main__":
    pipeline.run()
