#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os

import numpy as np

from cfg import conabio_ml

from typing import Any

from keras.models import Model
from keras.layers import Activation, Embedding, Flatten, Dense, Input, LSTM

# TODO: We are using a brach references for all scripts, when we
# merge to master those references has to be changed.


# All models that you need to chain using the Conabio_ML pipeline
# must be a subclass of the Conabio_ML model, defined here:
# (https://bitbucket.org/conabio_cmd/conabio_ml/src/CONML-45-definicin-del-esquema-de-entren/conabio_ml/trainer/model.py)

# In this example, we are using a model defined in plain Keras,
# so we use the helper subclass Keras Model
# (https://bitbucket.org/conabio_cmd/conabio_ml/src/afe32d9fcbb10957485a8e01765a61436438a353/conabio_ml/trainer/keras_bcknd.py#lines-34).
from conabio_ml.trainer.keras_bcknd import KerasModel as KM

from conabio_ml.utils.logger import get_logger

logger = get_logger(__name__)


class SAE(KM):
    train_config = None

    # The model will be built from scratch
    # we use the create method defined in:
    # (https://bitbucket.org/conabio_cmd/conabio_ml/src/afe32d9fcbb10957485a8e01765a61436438a353/conabio_ml/trainer/model.py#lines-62)
    @classmethod
    def create(cls: KM.ModelType,
               model_config: {
                   "SAE": {
                       'layers': {
                           'embedding_layer': {
                               'input_dim': 1000,
                               'output_dim': 50,
                               'window': 10
                           },
                           'dense_layer': {
                               'units': 2,
                               'activation': 'softmax'
                           }
                       }
                   }
               }) -> KM.ModelType:
        try:
            model_name = [k for k in model_config.keys()][0]
            layer_options = model_config[model_name]["layers"]

            assert len(layer_options) >= 2, \
                "SAE needs at least 2 layer to operate"

            layer_acc = len(layer_options) - 1

            input_layer = Input(shape=(layer_options[0]["dim"], ))
            temp_input = input_layer

            for ix, hidden in enumerate(layer_options[1:layer_acc]):
                temp = Dense(units=hidden["dim"],
                             activation=hidden["activation"]
                             )(temp_input)
                temp_input = temp

            encoder = Dense(
                layer_options[-1]["dim"],
                activation="relu"
            )(temp_input)
            temp_input = encoder

            for ix, hidden in enumerate(layer_options[-2:-layer_acc-1:-1]):
                temp = Dense(units=hidden["dim"],
                             activation=hidden["activation"]
                             )(temp_input)
                temp_input = temp

            output_layer = Dense(layer_options[0]["dim"],
                                 activation="sigmoid"
                                 )(temp_input)

            temp = cls(model_config=model_config,
                       input_layer=input_layer,
                       output_layer=output_layer)
            return temp
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)


class LSTM_Model(KM):
    # The model will be built from scratch
    # we use the create method defined in:
    # (https://bitbucket.org/conabio_cmd/conabio_ml/src/afe32d9fcbb10957485a8e01765a61436438a353/conabio_ml/trainer/model.py#lines-62)
    @classmethod
    def create(cls: KM.ModelType,
               model_config: {
                   "LSTM": {
                       'layers': {
                           'embedding_layer': {
                               'input_dim': 1000,
                               'output_dim': 50,
                               'window': 10,
                               'initializer': 'uniform'
                           },
                           'lstm_layer': {
                               'output_dim': 128
                           },
                           'dense_layer': {
                               'output_dim': 20
                           },
                           'activation': {
                               'activation': 'softmax'
                           }
                       }
                   }
               }) -> KM.ModelType:
        try:
            model_name = [k for k in model_config.keys()][0]
            layers_options = model_config[model_name]["layers"]

            embedding_layer = layers_options["embedding_layer"]
            lstm_layer = layers_options["lstm_layer"]
            dense_layer = layers_options["dense_layer"]
            activation = layers_options["activation"]

            input_layer = Input(
                shape=(embedding_layer["output_dim"], embedding_layer["output_dim"],))
            # embedding = Embedding(input_dim=embedding_layer["input_dim"],
            #                       output_dim=embedding_layer["output_dim"],
            #                       init=embedding_layer["initializer"],
            #                       input_length=embedding_layer["window"])(input_layer)
            lstm = LSTM(units=lstm_layer["output_dim"])(input_layer)
            dense = Dense(output_dim=dense_layer["output_dim"])(lstm)
            output_layer = Activation(
                activation=activation["activation"])(dense)

            temp = cls(model_config=model_config,
                       input_layer=input_layer,
                       output_layer=output_layer)
            return temp
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)


class LSTM_MATRIX(KM):
    # The model will be built from scratch
    # we use the create method defined in:
    # (https://bitbucket.org/conabio_cmd/conabio_ml/src/afe32d9fcbb10957485a8e01765a61436438a353/conabio_ml/trainer/model.py#lines-62)
    @classmethod
    def create(cls: KM.ModelType,
               embedding_matrix: Any,
               model_config: {
                   "LSTM": {
                       'layers': {
                           'embedding_layer': {
                               'input_dim': 100,
                               'output_dim': 50,
                               'initializer': 'uniform'
                           },
                           'lstm_layer': {
                               'output_dim': 128
                           },
                           'dense_layer': {
                               'output_dim': 20
                           },
                           'activation': {
                               'activation': 'softmax'
                           }
                       }
                   }
               }) -> KM.ModelType:
        try:
            model_name = [k for k in model_config.keys()][0]
            layers_options = model_config[model_name]["layers"]

            embedding_layer = layers_options["embedding_layer"]
            lstm_layer = layers_options["lstm_layer"]
            dense_layer = layers_options["dense_layer"]
            activation = layers_options["activation"]

            input_layer = Input(shape=(embedding_layer["output_dim"], ))

            # Here we are using the size of the vocabulary
            # Since we expect the matrix of the vocabulary transformed to embeddings just
            # to map in the samples.
            embedding = Embedding(input_dim=embedding_matrix.shape[0],
                                  output_dim=embedding_layer["output_dim"],
                                  input_length=embedding_layer["input_dim"],
                                  weights=[embedding_matrix],
                                  trainable=False)(input_layer)
            lstm = LSTM(units=lstm_layer["output_dim"])(embedding)
            dense = Dense(units=dense_layer["output_dim"])(lstm)
            output_layer = Activation(
                activation=activation["activation"])(dense)

            temp = cls(model_config=model_config,
                       input_layer=input_layer,
                       output_layer=output_layer)
            return temp
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)


class Multilabel_Classifier(KM):
    # The model will be built from scratch
    # we use the create method defined in:
    # (https://bitbucket.org/conabio_cmd/conabio_ml/src/afe32d9fcbb10957485a8e01765a61436438a353/conabio_ml/trainer/model.py#lines-62)
    @classmethod
    def create(cls: KM.ModelType,
               embedding_matrix: Any,
               model_config: {
                   "LSTM": {
                       'layers': {
                           'embedding_layer': {
                               'input_dim': 100,
                               'output_dim': 50,
                               'initializer': 'uniform'
                           },
                           'lstm_layer': {
                               'output_dim': 128
                           },
                           'dense_layer': {
                               'output_dim': 6
                           },
                           'activation': {
                               'activation': 'softmax'
                           }
                       }
                   }
               }) -> KM.ModelType:
        try:
            model_name = [k for k in model_config.keys()][0]
            layers_options = model_config[model_name]["layers"]

            embedding_layer = layers_options["embedding_layer"]
            lstm_layer = layers_options["lstm_layer"]
            dense_layer = layers_options["dense_layer"]
            activation = layers_options["activation"]

            input_layer = Input(shape=(embedding_layer["output_dim"], ))

            # Here we are using the size of the vocabulary
            # Since we expect the matrix of the vocabulary transformed to embeddings just
            # to map in the samples.
            embedding = Embedding(input_dim=embedding_matrix.shape[0],
                                  output_dim=embedding_layer["output_dim"],
                                  input_length=embedding_layer["input_dim"],
                                  weights=[embedding_matrix],
                                  trainable=False)(input_layer)
            lstm = LSTM(units=lstm_layer["output_dim"])(embedding)
            dense = Dense(units=dense_layer["output_dim"])(lstm)
            output_layer = Activation(
                activation=activation["activation"])(dense)

            temp = cls(model_config=model_config,
                       input_layer=input_layer,
                       output_layer=output_layer)
            return temp
        except Exception as ex:
            logger.exception(f"{ex}")
            raise(ex)
